## 1.1.24
*(2017-06-09)*

#### Fixed
* RMA's reasons validation
* Dispaying status's messages

#### Improvements
* Added reports by product and by attribute

---

## 1.1.23
*(2017-05-31)*

#### Fixed
* RMA validation

#### Improvements
* Added return address on frontend

---

## 1.1.22
*(2017-05-10)*

#### Improvements
* Added option "Reset Counter For Order"
* Added "Ticket was converted to RMA" event

---

## 1.1.21
*(2017-04-26)*

#### Fixed
* Notifications for guest

---

## 1.1.20
*(2017-04-10)*

#### Fixed
* Configuration for multistore

#### Features
* Added new conditions to workflow rules

---

## 1.1.19
*(2017-04-05)*

#### Features
* Added reports by reasons

---

## 1.1.16
*(2017-04-04)*

#### Features
* Added option that allows request to return only shipped items

---

## 1.1.14
*(2017-03-31)*

#### Fixed
* Fixed RMA for partial shipment
* Fixed issue when admin does not get notification about new RMA

---

## 1.1.13
*(2017-03-30)*

#### Features
* Added Helpdesk integration

#### Fixed
* Fixed RMA for orders migrated from Magento 1

---

## 1.1.12
*(2017-03-13)*

#### Fixed
* Fixed auto selecting items

---

## 1.1.11
*(2017-03-09)*

#### Fixed
* Fixed styles for confirm shipping dialog

---

## 1.1.10
*(2017-03-07)*

#### Fixed
* Fixed a possible issue with store id

---

## 1.1.9
*(2017-03-03)*

#### Fixes
* Fixed translations for Resolutions, Conditions, Reasons
* Fixed option "Allow to request RMA after order completion"
* Fixed  issues with ability to delete reserved Resolutions

## 1.1.8
*(2017-02-28)*

#### Fixes
* Fixed compilation crush due to incomplete aggregation in reports

## 1.1.7
*(2017-02-27)*

#### Fixes
* Fixed compilation crush due to Reports requirement missing

## 1.1.6
*(2017-02-24)*

#### Features
* Added Report by RMA Status

---

## 1.1.5
*(2017-02-24)*

#### Improvements
* Added option to hide Condition, Resolution, Reason for customer

---

## 1.1.4
*(2017-02-15)*

#### Fixed
* Saving of translated dictionary (reasons, resolutions, conditions) titles for storeviews.

---

## 1.1.3
*(2017-01-13)*

#### Improvements
* Added ability to set different Return Addresses for each RMA

---

## 1.1.2
*(2017-01-11)* 

## 1.1.1
*(2016-12-21)*

#### Fixed
* Quick Response in RMA edit form
* Issue with merged js in production mode

---

## 1.1.0
*(2016-12-08)*

#### Fixed
* "Items" column in Rma grid

---

## 1.1.0-beta3
*(2016-11-03)*

#### Improvements
* EE compatibility

#### Fixed
* RMA2-15 - Fixed wrong style issue with cart popup

---

## 1.1.0-beta2
*(2016-08-10)*

#### Improvements
* Improved code

---

## 1.1.0-beta1
*(2016-08-10)*

#### Fixed
* Tests fix

---

## 1.1.0-alpha
*(2016-07-18)*

#### Fixed
* Tests fix
* Fixed option "Allow to request RMA after order completion" ([#3](../../issues/3))

---

## 1.0.8
*(2016-06-24)*

#### Improvements
* Support of Magento 2.1.0

---

## 1.0.7
*(2016-05-06)*

#### Fixed
* Issue with sending emails via office.com smtp

---

## 1.0.6
*(2016-04-11)*

#### Fixed
* Issue with menu
* Fixed an issue with generation RMA increment ID

---

## 1.0.5
*(2016-03-24)*

#### Fixed
* hide button "Print RMA Shipping Label"

---

## 1.0.4
*(2016-03-21)*

#### Improvements
* Email styles

#### Fixed
* Compatibility with PHP7
* Compatibility of print templates with Proto theme

---

## 1.0.3
*(2016-03-07)*

#### Fixed
* Problem with shipping block on the RMA page in frontend

---

## 1.0.2
*(2016-03-03)*

#### Fixed
* RMA2-23 - Visible HTML tags in emails

---

## 1.0.1
*(2016-03-01)*

#### Improvements
* Backend and Frontend styles

#### Fixed
* RMA2-22 - Issues with PHP 7

---



## 1.0.0
*(2016-02-01)*

* initial stable release

