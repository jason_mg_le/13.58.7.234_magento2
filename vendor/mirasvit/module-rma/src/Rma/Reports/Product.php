<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rma
 * @version   1.1.24
 * @copyright Copyright (C) 2017 Mirasvit (https://mirasvit.com/)
 */


namespace Mirasvit\Rma\Reports;

use Mirasvit\Report\Model\AbstractReport;
use Mirasvit\Report\Api\Data\Query\ColumnInterface;

class Product extends AbstractReport
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return __('RMA: Report by Product');
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
        return 'rma_product';
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->setBaseTable('mst_rma_item');
        $this->addFastFilters([]);
        $this->setDefaultColumns([
            'sales_order_item|sku',
            'sales_order_item|name',
            'mst_rma_item|total_rma_cnt',
            'mst_rma_item|total_items_cnt',
            'mst_rma_item|item_qty_requested',
            'mst_rma_item|item_qty_returned',
        ]);

        $this->setDefaultFilters([
            ['mst_rma_item|qty_requested', ['gt' => 0]],
        ]);

        $this->addFastFilters([
            'mst_rma_item|created_at',
        ]);

        $this->addAvailableColumns(
            $this->context->getMapRepository()
                ->getTable('mst_rma_item')->getColumns(ColumnInterface::TYPE_SIMPLE)
        );

        $this->setDefaultDimension('sales_order_item|product_id');

        $this->addAvailableDimensions([
            'mst_rma_item|day',
            'mst_rma_item|week',
            'mst_rma_item|month',
            'mst_rma_item|year',
        ]);

        $this->setGridConfig([
            'paging' => true,
        ]);

        $this->setChartConfig([
            'chartType' => 'column',
            'vAxis'     => 'mst_rma_item|total_items_cnt',
        ]);
    }
}