<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rma
 * @version   1.1.24
 * @copyright Copyright (C) 2017 Mirasvit (https://mirasvit.com/)
 */


namespace Mirasvit\Rma\Reports;

use Mirasvit\Report\Api\Data\Query\ColumnInterface;
use Mirasvit\Report\Model\AbstractReport;
use Mirasvit\Report\Model\Context;

class Attribute extends AbstractReport
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return __('RMA: Report by Attribute');
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
        return 'rma_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->setBaseTable('catalog_product_entity');

        $this->setDefaultFilters([
            ['mst_rma_item|qty_requested', ['gt' => 0]],
        ]);

        $this->addFastFilters([
            'mst_rma_item|created_at',
        ]);

        $this->setRequiredColumns(['catalog_product_entity|entity_id']);

        $this->setDefaultColumns([
            'catalog_product_entity|attribute',
            'mst_rma_item|total_rma_cnt',
            'mst_rma_item|total_items_cnt',
            'mst_rma_item|item_qty_requested',
            'mst_rma_item|item_qty_returned',
        ]);

        $this->setDefaultDimension('sales_order_item|product_id');

        foreach ($this->context->getMapRepository()
                     ->getTable('catalog_product_entity')->getColumns(ColumnInterface::TYPE_SIMPLE) as $column) {
            $this->addAvailableDimensions([$column]);
        }

        $this->addAvailableColumns($this->context->getMapRepository()
            ->getTable('sales_order_item')->getColumns(ColumnInterface::TYPE_AGGREGATION));

        $this->setGridConfig([
            'paging' => true,
        ]);
    }
}
