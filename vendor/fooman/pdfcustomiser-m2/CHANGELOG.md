# Change Log

## [5.0.4] 2017-02-28
### Fixed
- PdfRenderer attachment file naming

## [5.0.3] 2017-02-27
### Fixed
- Bundle Products not displaying custom options

## [5.0.2] 2017-02-27
### Changed
- Wide dependency for Pdf Core

## [5.0.1] 2017-02-18
### Fixed
- Do not set shipment tracking information if no tracks exist

## [5.0.0] 2017-02-13
### Added
- Enable Tcpdf tags in all pdfs
### Changed
- Allow PdfRenderer adapters to be run multiple times to increase compatibility

## [4.0.0] 2017-02-06
### Fixed
- Update to latest PdfCore, includes Cron translation fix, AbstractSalesDocument constructor changed

## [3.6.4] 2017-01-10
### Fixed
- deal with Magento's inconsistent shipment ACL

## [3.6.3] 2016-12-23
### Changed
- Bundle items are now displayed as a group

## [3.6.2] 2016-12-21
### Fixed
- Only show shipment specific tracking information on packing slip
- Product attribute column values are retrieved from underlying simple products for configurable products 

## [3.6.1] 2016-11-15
### Fixed
- Order View Authentication on frontend pdfs

## [3.6.0] 2016-11-11
### Added
- Display Gift Messages
- Replace invoice, shipment and creditmemo pdfs accessible from single order view 

## [3.5.0] 2016-10-21
### Added
- Store Owner Address field is now parsed for custom variables

## [3.4.0] 2016-10-19
### Added
- Custom Text fields are now parsed for custom variables, for example {{var order.getCustomerId}}
### Changed
- Permission check changed -if you can view the sales object you can print it

## [3.3.3] 2016-10-02
### Fixed
- Change template reference to $block

## [3.3.2] 2016-09-16
### Fixed
- Multi store support for logos

## [3.3.1] 2016-09-15
### Fixed
- Some settings were not multistore capable

## [3.3.0] 2016-08-26
### Added
- Columns for Product Attributes
- Replace print actions in the customer frontend with pdfs 

## [3.2.0] 2016-08-20
### Added
- Display Order Comments in the pdf (<?php echo $block->getCommentsBlock() ?> added in pdf template files)

## [3.1.0] 2016-08-19
### Added
- Support displaying Custom Product Options

## [3.0.0] 2016-07-25
### Added
- Ability to restore default values
- Support for Integrated Labels

## [2.1.3] 2016-07-13
### Changed
- Compatibility with Magento 2.1, for Magento 2.0 use earlier versions

## [2.1.2] 2016-06-10
### Fixed
-  Multi currency display in item table

## [2.1.1] 2016-04-05
### Changed
- Improved display of bundle items

## [2.1.0] 2016-04-04
### Added
- Bundle Email Attachments to allow pdfs to be attached to outgoing emails

## [2.0.1] 2016-02-22
### Fixed
- Extra second composer.json to support Magento Setup UI

## [2.0.0] - 2016-02-16
### Changed
- Change folder structure to src/ and tests/ (not applicable to Marketplace release)
### Fixed
- Use correct function for translation
- Virtual orders

## [1.0.4] - 2016-01-01
### Changed
- Implemented Audit Program feedback
