<?php
namespace Dfe\AllPay\Controller\Offline;
/**
 * 2017-02-14
 * @final Unable to use the PHP «final» keyword here because of the M2 code generation.
 * @see \Dfe\AllPay\Charge::OFFLINE
 */
class Index extends \Dfe\AllPay\Controller\Confirm\Index {}