<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Subscribe segment source.
 */
class SubscribeSegment implements ArrayInterface
{
    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            ['value' => 4, 'label' => __('New Product Releases')],
            ['value' => 2, 'label' => __('Installed Product Updates')],
            ['value' => 3, 'label' => __('Marketing')]
        ];
    }
}
