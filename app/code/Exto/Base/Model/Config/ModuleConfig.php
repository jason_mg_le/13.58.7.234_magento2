<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Exto\Base\Api\Data\ModuleMetadataInterface;

/**
 * Class ModuleConfig.
 */
class ModuleConfig
{
    /**
     * @var string
     */
    private static $xmlPathModuleConfigPattern = 'exto_base/modules/%s/%s';

    /**
     * @var string
     */
    private static $xmlKeyInstallationDate = 'installation_date';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ConfigResource
     */
    private $configResource;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ConfigResource $configResource
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ConfigResource $configResource
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configResource = $configResource;
    }

    /**
     * Get installation date.
     *
     * @param ModuleMetadataInterface $module
     * @return int
     */
    public function getInstallationDate(ModuleMetadataInterface $module)
    {
        return (int)$this->scopeConfig->getValue(
            $this->getModuleConfigPath($module, self::$xmlKeyInstallationDate)
        );
    }

    /**
     * Set installation date.
     *
     * @param ModuleMetadataInterface $module
     * @param int $timestamp
     * @return $this
     */
    public function setInstallationTime(ModuleMetadataInterface $module, $timestamp)
    {
        $this->configResource->save(
            $this->getModuleConfigPath($module, self::$xmlKeyInstallationDate),
            $timestamp
        );

        return $this;
    }

    /**
     * Get module config path.
     *
     * @param ModuleMetadataInterface $module
     * @param string $config
     * @return string
     */
    private function getModuleConfigPath(ModuleMetadataInterface $module, $config)
    {
        return sprintf(
            self::$xmlPathModuleConfigPattern,
            strtolower($module->getId()),
            $config
        );
    }
}
