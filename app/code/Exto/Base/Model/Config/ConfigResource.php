<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\TemporaryState\CouldNotSaveException;
use Magento\Config\Model\ResourceModel\Config;

/**
 * Class ConfigResource.
 */
class ConfigResource
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * Save config value.
     *
     * @param string $path
     * @param string $value
     * @param string $scope
     * @param int|null $scopeId
     * @return $this
     * @throws CouldNotSaveException
     */
    public function save($path, $value, $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeId = null)
    {
        try {
            $this->config->saveConfig(
                (string)$path,
                (string)$value,
                (string)$scope,
                (int)$scopeId
            );
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Could not save config value.'));
        }

        return $this;
    }
}
