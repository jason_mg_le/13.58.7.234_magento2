<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Notification\Rss;

use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Config\ConfigOptionsListConstants;
use Exto\Base\Model\Rss\Item;

/**
 * Class LastItemsFilter.
 */
class LastItemsFilter
{
    /**
     * @var DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * @param DeploymentConfig $deploymentConfig
     */
    public function __construct(
        DeploymentConfig $deploymentConfig
    ) {
        $this->deploymentConfig = $deploymentConfig;
    }

    /**
     * Filter items.
     *
     * @param Items[] $items
     * @return Items[]
     */
    public function filter(array $items)
    {
        $magentoInstallDate = strtotime($this->deploymentConfig->get(
            ConfigOptionsListConstants::CONFIG_PATH_INSTALL_DATE
        ));

        $result = array_filter($items, function (Item $item) use ($magentoInstallDate) {
            return $item->getPublicationDate() >= $magentoInstallDate;
        });

        return $result;
    }
}
