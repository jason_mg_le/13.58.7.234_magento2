<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Notification\Rss;

use Magento\Framework\HTTP\Adapter\Curl;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Exto\Base\Model\System\HttpAdapter\ConfigProvider;
use Exto\Base\Model\Notification\Rss\Adapter\RequestDataProvider;

/**
 * Class AdapterBuilder.
 */
class AdapterBuilder
{
    /**
     * @var CurlFactory
     */
    private $curlFactory;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var RequestDataProvider
     */
    private $requestDataProvider;

    /**
     * @var string
     */
    private $url;

    /**
     * @param CurlFactory $curlFactory
     * @param ConfigProvider $configProvider
     * @param RequestDataProvider $requestDataProvider
     */
    public function __construct(
        CurlFactory $curlFactory,
        ConfigProvider $configProvider,
        RequestDataProvider $requestDataProvider
    ) {
        $this->curlFactory = $curlFactory;
        $this->configProvider = $configProvider;
        $this->requestDataProvider = $requestDataProvider;
    }

    /**
     * Set url.
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Build http adapter.
     *
     * @return Curl
     */
    public function build()
    {
        $curl = $this->curlFactory->create();

        $curl->setConfig($this->configProvider->getConfig());

        $requestBody = http_build_query($this->requestDataProvider->getRequestData());

        $curl->write(
            \Zend_Http_Client::POST,
            $this->url,
            '1.0',
            ['Connection: close'],
            $requestBody
        );

        return $curl;
    }
}
