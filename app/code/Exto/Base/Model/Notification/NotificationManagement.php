<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Notification;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\AdminNotification\Model\InboxFactory;
use Exto\Base\Model\Notification\Rss\AdapterBuilder;
use Exto\Base\Model\Notification\Rss\LastItemsFilter;
use Exto\Base\Model\Rss\Item;
use Exto\Base\Model\Rss\Reader;
use Exto\Base\Model\Rss\ReaderFactory;
use Exto\Base\Model\Config\NotificationConfig;

/**
 * Class NotificationManagement.
 */
class NotificationManagement
{
    /**
     * @var \Exto\Base\Model\Rss\ReaderFactory
     */
    private $readerFactory;

    /**
     * @var \Exto\Base\Model\Notification\Rss\AdapterBuilder
     */
    private $adapterBuilder;

    /**
     * @var \Exto\Base\Model\Notification\Rss\LastItemsFilter
     */
    private $lastItemsFilter;

    /**
     * @var \Magento\AdminNotification\Model\InboxFactory
     */
    private $inboxFactory;

    /**
     * @var \Exto\Base\Model\Config\NotificationConfig
     */
    private $notificationConfig;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTime;

    /**
     * @param ReaderFactory $readerFactory
     * @param AdapterBuilder $adapterBuilder
     * @param LastItemsFilter $lastItemsFilter
     * @param InboxFactory $inboxFactory
     * @param NotificationConfig $notificationConfig
     * @param DateTime $dateTime
     */
    public function __construct(
        ReaderFactory $readerFactory,
        AdapterBuilder $adapterBuilder,
        LastItemsFilter $lastItemsFilter,
        InboxFactory $inboxFactory,
        NotificationConfig $notificationConfig,
        DateTime $dateTime
    ) {
        $this->readerFactory = $readerFactory;
        $this->adapterBuilder = $adapterBuilder;
        $this->inboxFactory = $inboxFactory;
        $this->lastItemsFilter = $lastItemsFilter;
        $this->notificationConfig = $notificationConfig;
        $this->dateTime = $dateTime;
    }

    /**
     * Update notifications.
     *
     * @return $this
     */
    public function update()
    {
        $reader = $this->getRssReader();
        $items = $reader->read();
        $items = $this->lastItemsFilter->filter($items);
        $items = array_reverse($items);

        foreach ($items as $item) {
            $this->addNotificationItem($item);
        }

        return $this;
    }

    /**
     * Get rss reader.
     *
     * @return Reader
     */
    private function getRssReader()
    {
        $adapter = $this->adapterBuilder
            ->setUrl($this->notificationConfig->getFeedUrl())
            ->build();

        return $this->readerFactory->create([
            'adapter' => $adapter
        ]);
    }

    /**
     * Add notification item.
     *
     * @param Item $item
     * @return $this
     */
    private function addNotificationItem(Item $item)
    {
        $inbox = $this->inboxFactory->create();
        $inbox->parse([
            [
                'severity' => $item->getSeverity(),
                'date_added' => $this->dateTime->date(null, $item->getPublicationDate()),
                'title' => $item->getTitle(),
                'description' => $item->getDescription(),
                'url' => $item->getLink()
            ]
        ]);

        return $this;
    }
}
