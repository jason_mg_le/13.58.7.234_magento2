<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Notification;

use Magento\Framework\App\CacheInterface;
use Exto\Base\Model\Config\NotificationConfig;

/**
 * Class State.
 */
class State
{
    /**
     * @var string
     */
    private static $cacheKeyLastUpdated = 'exto_notifications_last_updated';

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var NotificationConfig
     */
    private $notificationConfig;

    /**
     * @param CacheInterface $cache
     * @param NotificationConfig $notificationConfig
     */
    public function __construct(
        CacheInterface $cache,
        NotificationConfig $notificationConfig
    ) {
        $this->cache = $cache;
        $this->notificationConfig = $notificationConfig;
    }

    /**
     * Mark notifications as updated.
     *
     * @return $this
     */
    public function markAsUpdated()
    {
        return $this->setLastUpdate(time());
    }

    /**
     * Is update required.
     *
     * @return bool
     */
    public function isUpdateRequired()
    {
        return (time() - $this->getLastUpdate()) > $this->notificationConfig->getFrequency();
    }

    /**
     * Get last update time.
     *
     * @return int
     */
    private function getLastUpdate()
    {
        return (int)$this->cache->load(self::$cacheKeyLastUpdated);
    }

    /**
     * Set last update time.
     *
     * @param int $lastUpdate
     * @return $this
     */
    private function setLastUpdate($lastUpdate)
    {
        $this->cache->save($lastUpdate, self::$cacheKeyLastUpdated);

        return $this;
    }
}
