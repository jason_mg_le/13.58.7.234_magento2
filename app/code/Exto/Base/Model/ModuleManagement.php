<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Exto\Base\Api\Data\ModuleMetadataInterface;
use Exto\Base\Api\ModuleManagementInterface;
use Exto\Base\Model\Config\ModuleConfig;

/**
 * Module management.
 */
class ModuleManagement implements ModuleManagementInterface
{
    /**
     * @var ModuleConfig
     */
    private $moduleConfig;

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @param ModuleConfig $moduleConfig
     * @param DateTime $dateTime
     */
    public function __construct(
        ModuleConfig $moduleConfig,
        DateTime $dateTime
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->dateTime = $dateTime;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleMetadataInterface $module)
    {
        $this->moduleConfig->setInstallationTime(
            $module,
            $this->dateTime->timestamp()
        );

        return $this;
    }
}
