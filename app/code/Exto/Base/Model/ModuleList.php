<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model;

use Exto\Base\Api\Data\ModuleMetadataInterface;
use Exto\Base\Api\ModuleListInterface;

/**
 * Module list.
 */
class ModuleList implements ModuleListInterface
{
    /**
     * @var \Exto\Base\Api\Data\ModuleMetadataInterface[]
     */
    private $modules = [];

    /**
     * @param \Exto\Base\Api\Data\ModuleMetadataInterface[] $modules
     */
    public function __construct(
        array $modules = []
    ) {
        $this->addModules($modules);
    }

    /**
     * {@inheritdoc}
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * {@inheritdoc}
     */
    public function addModule(ModuleMetadataInterface $module)
    {
        $this->modules[] = $module;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addModules(array $modules)
    {
        foreach ($modules as $module) {
            $this->addModule($module);
        }

        return $this;
    }
}
