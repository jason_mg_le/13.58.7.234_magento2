<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\System\HttpAdapter;

use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\UrlInterface;

/**
 * Class ConfigProvider.
 */
class ConfigProvider
{
    /**
     * @var ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @param ProductMetadataInterface $productMetadata
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        ProductMetadataInterface $productMetadata,
        UrlInterface $urlBuilder
    ) {
        $this->productMetadata = $productMetadata;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Get adapter config.
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'timeout'   => 2,
            'useragent' => $this->getUserAgent(),
            'referer'   => $this->urlBuilder->getUrl()
        ];
    }

    /**
     * Get user agent.
     *
     * @return string
     */
    private function getUserAgent()
    {
        return sprintf(
            '%s/%s (%s)',
            $this->productMetadata->getName(),
            $this->productMetadata->getVersion(),
            $this->productMetadata->getEdition()
        );
    }
}
