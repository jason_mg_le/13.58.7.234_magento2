<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Tracking\Export;

use Exto\Base\Exception\Tracking\ExportException;

/**
 * Tracking exporter interface.
 *
 * @api
 */
interface ExporterInterface
{
    /**
     * Export tracks data.
     *
     * @param array $tracks
     * @throws ExportException
     * @return $this
     */
    public function export(array $tracks);
}
