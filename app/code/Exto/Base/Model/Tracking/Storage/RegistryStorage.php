<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Tracking\Storage;

use Exto\Base\Api\Data\ModuleMetadataInterface;
use Exto\Base\Model\Tracking\StorageInterface;

/**
 * Class Registry Storage.
 */
class RegistryStorage implements StorageInterface
{
    /**
     * @var ArrayUtil
     */
    private $arrayUtil;

    /**
     * @var array
     */
    private $registry = [];

    /**
     * @param ArrayUtil $arrayUtil
     */
    public function __construct(
        ArrayUtil $arrayUtil
    ) {
        $this->arrayUtil = $arrayUtil;
    }

    /**
     * {@inheritdoc}
     */
    public function addTrack(ModuleMetadataInterface $module, $action, array $data = [])
    {
        $this->registry = $this->arrayUtil->addTrack(
            $this->registry,
            $module,
            $action,
            $data
        );

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addTracks(array $tracks)
    {
        $this->registry = $this->arrayUtil->addTracks($this->registry, $tracks);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleTracks(ModuleMetadataInterface $module)
    {
        return $this->arrayUtil->getModuleTracks($this->registry, $module);
    }

    /**
     * {@inheritdoc}
     */
    public function getTracks()
    {
        return (array)$this->registry;
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        $this->registry = [];

        return $this;
    }
}
