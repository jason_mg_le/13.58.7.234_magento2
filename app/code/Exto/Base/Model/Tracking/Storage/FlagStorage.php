<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Tracking\Storage;

use Magento\Framework\Flag\FlagResource;
use Exto\Base\Api\Data\ModuleMetadataInterface;
use Exto\Base\Model\Tracking\Storage\FlagStorage\TracksFlag;
use Exto\Base\Model\Tracking\Storage\FlagStorage\TracksFlagFactory;
use Exto\Base\Model\Tracking\StorageInterface;

/**
 * Class Flag Storage.
 */
class FlagStorage implements StorageInterface
{
    /**
     * @var ArrayUtil
     */
    private $arrayUtil;

    /**
     * @var FlagResource
     */
    private $flagResource;

    /**
     * @var TracksFlag
     */
    private $flag;

    /**
     * @param ArrayUtil $arrayUtil
     * @param FlagResource $flagResource
     * @param TracksFlagFactory $flagFactory
     */
    public function __construct(
        ArrayUtil $arrayUtil,
        FlagResource $flagResource,
        TracksFlagFactory $flagFactory
    ) {
        $this->arrayUtil = $arrayUtil;
        $this->flagResource = $flagResource;
        $this->flag = $flagFactory->create();
    }

    /**
     * {@inheritdoc}
     */
    public function addTrack(ModuleMetadataInterface $module, $action, array $data = [])
    {
        $tracks = $this->arrayUtil->addTrack(
            $this->getTracks(),
            $module,
            $action,
            $data
        );

        $this->saveTracks($tracks);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addTracks(array $tracks)
    {
        if (empty($tracks)) {
            return $this;
        }

        $tracks = $this->arrayUtil->addTracks(
            $this->getTracks(),
            $tracks
        );

        $this->saveTracks($tracks);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getModuleTracks(ModuleMetadataInterface $module)
    {
        return $this->arrayUtil->getModuleTracks($this->getTracks(), $module);
    }

    /**
     * {@inheritdoc}
     */
    public function getTracks()
    {
        if (!$this->flag->getFlagData()) {
            $this->flag->loadSelf();
        }

        $tracks = $this->flag->getFlagData();
        $tracks = is_array($tracks) ? $tracks : [];

        return $tracks;
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        $this->saveTracks([]);

        return $this;
    }

    /**
     * Save tracks.
     *
     * @param array $tracks
     * @return void
     */
    private function saveTracks(array $tracks)
    {
        $this->flag->setFlagData($tracks);

        try {
            $this->flagResource->save($this->flag);
        } catch (\Exception $e) {
            // do nothing
        }
    }
}
