<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Tracking\Storage\FlagStorage;

use Magento\Framework\Flag;

/**
 * Class TracksFlag.
 */
class TracksFlag extends Flag
{
    /**
     * {@inheritdoc}
     */
    protected $_flagCode = 'exto_base_tracking_tracks';
}
