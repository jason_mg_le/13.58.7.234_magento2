<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Tracking;

use Exto\Base\Api\Data\ModuleMetadataInterface;

/**
 * Tracking storage interface.
 *
 * @api
 */
interface StorageInterface
{
    /**#@+
     * Key data fields
     */
    const KEY_CALL_COUNT = 'c';
    const KEY_CALL_DATA = 'd';
    /**#@-*/

    /**
     * Add track data.
     *
     * @param ModuleMetadataInterface $module
     * @param string $action
     * @param array $data
     * @return $this
     */
    public function addTrack(ModuleMetadataInterface $module, $action, array $data = []);

    /**
     * Add tracks data.
     *
     * @param array $tracks
     * @return $this
     */
    public function addTracks(array $tracks);

    /**
     * Get module track data.
     *
     * @param ModuleMetadataInterface $module
     * @return array
     */
    public function getModuleTracks(ModuleMetadataInterface $module);

    /**
     * Get track data.
     *
     * @return array
     */
    public function getTracks();

    /**
     * Clear track data.
     *
     * @return $this
     */
    public function clear();
}
