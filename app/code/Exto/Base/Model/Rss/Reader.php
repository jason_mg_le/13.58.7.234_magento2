<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model\Rss;

use Magento\Framework\HTTP\Adapter\Curl;
use Exto\Base\Exception\Rss\ReaderException;

/**
 * Class Reader.
 */
class Reader
{
    /**
     * @var ItemFactory
     */
    private $itemFactory;

    /**
     * @var Curl
     */
    private $adapter;

    /**
     * @param ItemFactory $itemFactory
     * @param Curl $adapter
     */
    public function __construct(
        ItemFactory $itemFactory,
        Curl $adapter
    ) {
        $this->itemFactory = $itemFactory;
        $this->adapter = $adapter;
    }

    /**
     * Read rss feed items.
     *
     * @return Item[]
     * @throws ReaderException
     */
    public function read()
    {
        $xmlData = $this->readXmlData();

        if (!$this->isValidXmlData($xmlData)) {
            throw new ReaderException(__('Invalid xml feed format.'));
        }

        $items = [];

        if ($xmlData->channel->item) {
            foreach ($xmlData->channel->item as $channelItem) {
                /** @var Item $item */
                $item = $this->itemFactory->create();
                $item->setSeverity((int)$channelItem->comments)
                    ->setPublicationDate(strtotime((string)$channelItem->pubDate))
                    ->setTitle((string)$channelItem->title)
                    ->setDescription((string)$channelItem->description)
                    ->setLink((string)$channelItem->link);

                $items[] = $item;
            }
        }

        return $items;
    }

    /**
     * Read xml data.
     *
     * @return \SimpleXMLElement
     * @throws ReaderException
     */
    private function readXmlData()
    {
        $response = $this->adapter->read();
        $this->adapter->close();

        if ($response === false) {
            throw new ReaderException(__('Invalid response.'));
        }

        $response = preg_split('/^\r?$/m', $response, 2);
        $response = trim($response[1]);

        try {
            $xmlData = new \SimpleXMLElement($response);
        } catch (\Exception $e) {
            throw new ReaderException(__('Invalid xml data.'));
        }

        return $xmlData;
    }

    /**
     * Is valid xml data.
     *
     * @param \SimpleXMLElement $xmlData
     * @return bool
     */
    private function isValidXmlData(\SimpleXMLElement $xmlData)
    {
        return $xmlData && $xmlData->channel;
    }
}
