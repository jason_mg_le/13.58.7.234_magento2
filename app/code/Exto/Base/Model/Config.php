<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Exto\Base\Model\Config\ConfigResource;

/**
 * Class Config.
 */
class Config
{
    /**
     * @var string
     */
    private static $xmlPathIsConfigured = 'exto_base/system/is_configured';

    /**
     * @var string
     */
    private static $xmlPathSupportUrl = 'exto_base/system/support_url';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ConfigResource
     */
    private $configResource;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ConfigResource $configResource
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ConfigResource $configResource
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configResource = $configResource;
    }

    /**
     * Is configured.
     *
     * @return bool
     */
    public function isConfigured()
    {
        return $this->scopeConfig->isSetFlag(self::$xmlPathIsConfigured);
    }

    /**
     * Set is configured.
     *
     * @param bool $isConfigured
     * @return $this
     */
    public function setIsConfigured($isConfigured)
    {
        $this->configResource->save(self::$xmlPathIsConfigured, (int)$isConfigured);

        return $this;
    }

    /**
     * Get support url.
     *
     * @return string
     */
    public function getSupportUrl()
    {
        return $this->scopeConfig->getValue(self::$xmlPathSupportUrl);
    }
}
