<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Setup;

use Exto\Base\Api\Data\ModuleMetadataInterface;

/**
 * Class ModuleMetadata.
 */
class ModuleMetadata implements ModuleMetadataInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 'Exto_Base';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Exto Base';
    }
}
