<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Exto\Base\Api\ModuleManagementInterface;

/**
 * Class InstallData.
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var ModuleManagementInterface
     */
    private $moduleManagement;

    /**
     * @var ModuleMetadata
     */
    private $module;

    /**
     * @param ModuleManagementInterface $moduleManagement
     * @param ModuleMetadata $module
     */
    public function __construct(
        ModuleManagementInterface $moduleManagement,
        ModuleMetadata $module
    ) {
        $this->moduleManagement = $moduleManagement;
        $this->module = $module;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->moduleManagement->install($this->module);
    }
}
