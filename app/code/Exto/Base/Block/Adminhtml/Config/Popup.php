<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Block\Adminhtml\Config;

use Magento\Framework\View\Element\BlockInterface;
use Magento\Framework\View\Element\Html\Select;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Model\Config\Source\Yesno as YesnoSource;
use Exto\Base\Model\Config\Source\SubscribeSegment as SubscribeSegmentSource;
use Exto\Base\Model\Config;

/**
 * Class Popup.
 */
class Popup extends Template
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var SubscribeSegmentSource
     */
    private $subscribeSegmentSource;

    /**
     * @var YesnoSource
     */
    private $yesnoSource;

    /**
     * @param Context $context
     * @param Config $config
     * @param SubscribeSegmentSource $subscribeSegmentSource
     * @param YesnoSource $yesnoSource
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        SubscribeSegmentSource $subscribeSegmentSource,
        YesnoSource $yesnoSource,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
        $this->subscribeSegmentSource = $subscribeSegmentSource;
        $this->yesnoSource = $yesnoSource;
    }

    /**
     * Is block visible.
     *
     * @return bool
     */
    public function isVisible()
    {
        return !$this->config->isConfigured();
    }

    /**
     * Get config save url.
     *
     * @return string
     */
    public function getSubscribeSaveUrl()
    {
        return $this->getUrl('exto_base/config/save');
    }

    /**
     * Get subscribe segment element.
     *
     * @return BlockInterface
     */
    public function getSegmentElement()
    {
        $options = $this->subscribeSegmentSource->toOptionArray();
        $value = array_column($options, 'value');

        return $this->createSelectBlock()->setName(
            'subscription_segments'
        )->setId(
            'exto-base-subscribe-segments-select'
        )->setClass(
            'multiselect'
        )->setTitle(
            __('Subscribe to Segments')
        )->setExtraParams(
            'multiple="multiple" size="3"'
        )->setValue(
            $value
        )->setOptions(
            $options
        );
    }

    /**
     * Get send usage data element.
     *
     * @return BlockInterface
     */
    public function getSendUsageDataElement()
    {
        return $this->createSelectBlock()->setName(
            'send_anonymous_data'
        )->setId(
            'exto-base-subscribe-send-anonymous-data-select'
        )->setTitle(
            __('Send Anonymous Data')
        )->setValue(
            '0'
        )->setOptions(
            $this->yesnoSource->toOptionArray()
        );
    }

    /**
     * Create select block.
     *
     * @return BlockInterface
     */
    private function createSelectBlock()
    {
        return $this->getLayout()->createBlock(Select::class);
    }

    /**
     * {@inheritdoc}
     */
    protected function _toHtml()
    {
        return $this->isVisible()
            ? parent::_toHtml()
            : '';
    }
}
