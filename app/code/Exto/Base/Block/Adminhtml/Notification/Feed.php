<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Block\Adminhtml\Notification;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Exto\Base\Model\Config;
use Exto\Base\Model\Notification\State as NotificationState;

/**
 * Class Feed.
 */
class Feed extends Template
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var NotificationState
     */
    private $notificationState;

    /**
     * @param Context $context
     * @param Config $config
     * @param NotificationState $notificationState
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        NotificationState $notificationState,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
        $this->notificationState = $notificationState;
    }

    /**
     * Is block visible.
     *
     * @return bool
     */
    public function isVisible()
    {
        return $this->config->isConfigured() && $this->notificationState->isUpdateRequired();
    }

    /**
     * Get notification feed url.
     *
     * @return string
     */
    public function getSubscribeFeedUrl()
    {
        return $this->getUrl('exto_base/notification/feed');
    }

    /**
     * {@inheritdoc}
     */
    protected function _toHtml()
    {
        return $this->isVisible()
            ? parent::_toHtml()
            : '';
    }
}
