/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'underscore',
    'jquery',
    'mage/translate',
    'Magento_Ui/js/modal/modal'
], function (_, $, $t) {
    'use strict';

    /**
     * Default modal options.
     *
     * @type {Object}
     */
    var defaultModalOptions = {
        modalClass: 'confirm',
        buttons: [
            {
                text: $t('Cancel'),
                class: 'action-secondary',
                click: function () {
                    this.closeModal();
                }
            }, {
                text: $t('Save'),
                class: 'action-primary',
                click: function () {
                    var self = this;
                    var formData = $(this.element).find('form').serializeArray();

                    getPopupMessagesElement(self.element).hide();

                    saveSubscriptionSettings(this.options.saveUrl, prepareFormData(formData))
                        .done(function () {
                            self.closeModal();
                        }).fail(function () {
                            getPopupMessagesElement(self.element).show();
                        });
                }
            }
        ]
    };

    /**
     * Get popup messages element.
     *
     * @param {Element} popup
     * @return {Element}
     */
    function getPopupMessagesElement(popup) {
        return $(popup).find('div.messages');
    }

    /**
     * Build modal.
     *
     * @param {Element} element
     * @param {Object} options
     * @return {jQuery}
     */
    function buildModal(element, options) {
        options = _.extend({}, defaultModalOptions, options);

        return $(element).modal(options);
    }

    /**
     * Save subscription settings.
     *
     * @param {String} url
     * @param {Object} settings
     * @return {Promise}
     */
    function saveSubscriptionSettings(url, settings) {
        return $.ajax({
            url: url,
            method: 'POST',
            data: settings,
            showLoader: true
        });
    }

    /**
     * Prepare form data.
     *
     * @param {Object} data
     * @return {Object}
     */
    function prepareFormData(data) {
        var preparedData = {};

        _.each(data, function (field) {
            var value = field.value;
            if (_.has(preparedData, field.name)) {
                if (!_.isArray(preparedData[field.name])) {
                    preparedData[field.name] = [preparedData[field.name]];
                }

                preparedData[field.name].push(value);
                value = preparedData[field.name];
            }

            preparedData[field.name] = value;
        });

        return preparedData;
    }

    return function (config, element) {
        var modalElement = buildModal(element, config.options);

        $(document).ready(function () {
            modalElement.modal('openModal');
        })
    };
});
