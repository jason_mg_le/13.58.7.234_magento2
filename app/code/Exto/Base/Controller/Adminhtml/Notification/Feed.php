<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Controller\Adminhtml\Notification;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Exto\Base\Model\Notification\NotificationManagement;
use Exto\Base\Model\Notification\State;

/**
 * Class Feed.
 */
class Feed extends Action
{
    /**
     * @var NotificationManagement
     */
    private $notificationManagement;

    /**
     * @var State
     */
    private $state;

    /**
     * @param Context $context
     * @param NotificationManagement $notificationManagement
     * @param State $state
     */
    public function __construct(
        Context $context,
        NotificationManagement $notificationManagement,
        State $state
    ) {
        parent::__construct($context);
        $this->notificationManagement = $notificationManagement;
        $this->state = $state;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        try {
            if ($this->state->isUpdateRequired()) {
                $this->notificationManagement->update();
                $this->state->markAsUpdated();
            }

            $resultData = [
                'success' => true
            ];
        } catch (LocalizedException $e) {
            $resultData = [
                'error' => true,
                'message' => $e->getMessage()
            ];
        } catch (\Exception $e) {
            $resultData = [
                'error' => true,
                'message' => __('Something went wrong.')
            ];
        }

        return $result->setData($resultData);
    }
}
