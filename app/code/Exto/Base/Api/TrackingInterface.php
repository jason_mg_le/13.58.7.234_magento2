<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Api;

use Exto\Base\Api\Data\ModuleMetadataInterface;

/**
 * Tracking interface.
 *
 * @api
 */
interface TrackingInterface
{
    /**
     * Track module action.
     *
     * @param ModuleMetadataInterface $module
     * @param string $action
     * @param array $data
     * @return $this
     */
    public function track(ModuleMetadataInterface $module, $action, array $data = []);
}
