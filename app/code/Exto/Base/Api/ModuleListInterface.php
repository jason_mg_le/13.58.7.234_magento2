<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Api;

use Exto\Base\Api\Data\ModuleMetadataInterface;

/**
 * Module list interface.
 *
 * @api
 */
interface ModuleListInterface
{
    /**
     * Get modules.
     *
     * @return ModuleMetadataInterface[]
     */
    public function getModules();

    /**
     * Add module to list.
     *
     * @param ModuleMetadataInterface $module
     * @return $this
     */
    public function addModule(ModuleMetadataInterface $module);

    /**
     * Add modules to list.
     *
     * @param ModuleMetadataInterface[] $modules
     * @return $this
     */
    public function addModules(array $modules);
}
