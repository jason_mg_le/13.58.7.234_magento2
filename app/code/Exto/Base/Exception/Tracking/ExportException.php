<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Base\Exception\Tracking;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class ExportException.
 */
class ExportException extends LocalizedException
{
}
