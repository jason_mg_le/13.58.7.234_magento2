<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\RmaApi\Api;

/**
 * Module status interface.
 *
 * @api
 */
interface ModuleStatusInterface
{
    /**
     * Is module available.
     *
     * @return bool
     */
    public function isAvailable();
}
