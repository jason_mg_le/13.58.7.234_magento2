<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\RmaApi\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Request interface.
 *
 * @api
 */
interface RequestInterface extends ExtensibleDataInterface
{
    /**
     * Constants for keys of data array
     */
    const ID = 'entity_id';
    const INCREMENT_ID = 'increment_id';
    const OWNER_ID = 'owner_id';
    const ORDER_ID = 'order_id';
    const CUSTOMER_ID = 'customer_id';
    const CUSTOMER_EMAIL = 'customer_email';
    const STATUS_ID = 'status_id';
    const REASON_ID = 'reason_id';
    const RESOLUTION_ID = 'resolution_id';
    const PACKAGE_CONDITION_ID = 'package_condition_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get increment id
     *
     * @return int|null
     */
    public function getIncrementId();

    /**
     * Set increment id
     *
     * @param int $id
     * @return $this
     */
    public function setIncrementId($id);

    /**
     * Get owner id
     *
     * @return int|null
     */
    public function getOwnerId();

    /**
     * Set owner id
     *
     * @param int $id
     * @return $this
     */
    public function setOwnerId($id);

    /**
     * Get order id
     *
     * @return int|null
     */
    public function getOrderId();

    /**
     * Set order id
     *
     * @param int $id
     * @return $this
     */
    public function setOrderId($id);

    /**
     * Get customer id
     *
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Set customer id
     *
     * @param int $id
     * @return $this
     */
    public function setCustomerId($id);

    /**
     * Get customer email
     *
     * @return string|null
     */
    public function getCustomerEmail();

    /**
     * Set customer email
     *
     * @param string $email
     * @return $this
     */
    public function setCustomerEmail($email);

    /**
     * Get status id
     *
     * @return int|null
     */
    public function getStatusId();

    /**
     * Set status id
     *
     * @param int $id
     * @return $this
     */
    public function setStatusId($id);

    /**
     * Get reason id
     *
     * @return int|null
     */
    public function getReasonId();

    /**
     * Set reason id
     *
     * @param int $id
     * @return $this
     */
    public function setReasonId($id);

    /**
     * Get resolution id
     *
     * @return int|null
     */
    public function getResolutionId();

    /**
     * Set resolution id
     *
     * @param int $id
     * @return $this
     */
    public function setResolutionId($id);

    /**
     * Get package condition id
     *
     * @return int|null
     */
    public function getPackageConditionId();

    /**
     * Set package condition id
     *
     * @param int $id
     * @return $this
     */
    public function setPackageConditionId($id);

    /**
     * Get created at time
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created at time
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated at time
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated at time
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
