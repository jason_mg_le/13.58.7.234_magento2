<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\RmaApi\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for request search results.
 *
 * @api
 */
interface RequestSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get requests list.
     *
     * @return \Exto\RmaApi\Api\Data\RequestInterface[]
     */
    public function getItems();

    /**
     * Set requests list.
     *
     * @param \Exto\RmaApi\Api\Data\RequestInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
