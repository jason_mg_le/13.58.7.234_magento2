<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\RmaApi\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Exto\RmaApi\Api\Data\RequestInterface;

/**
 * Request repository.
 *
 * @api
 */
interface RequestRepositoryInterface
{
    /**
     * Save request.
     *
     * @param \Exto\RmaApi\Api\Data\RequestInterface $request
     * @return \Exto\RmaApi\Api\Data\RequestInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(RequestInterface $request);

    /**
     * Get request.
     *
     * @param int $id
     * @return \Exto\RmaApi\Api\Data\RequestInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($id);

    /**
     * Get list of agents matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Exto\RmaApi\Api\Data\RequestSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete request.
     *
     * @param RequestInterface $request
     * @return bool
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(RequestInterface $request);
}
