<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\RmaApi\Api;

/**
 * Request status label provider interface.
 *
 * @api
 */
interface RequestStatusLabelProviderInterface
{
    /**
     * Get status label.
     *
     * @param int $statusId
     * @param null $storeId
     * @return string
     */
    public function getStatusLabel($statusId, $storeId = null);
}
