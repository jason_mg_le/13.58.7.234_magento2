<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\RmaApi\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class NotImplementedException.
 */
class NotImplementedException extends LocalizedException
{
}
