<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\RmaApi\Model\Stub;

use Exto\RmaApi\Api\ModuleStatusInterface;

/**
 * Class ModuleStatus.
 */
class ModuleStatus implements ModuleStatusInterface
{
    /**
     * {@inheritdoc}
     */
    public function isAvailable()
    {
        return false;
    }
}
