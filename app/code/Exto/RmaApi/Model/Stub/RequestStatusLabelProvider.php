<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\RmaApi\Model\Stub;

use Exto\RmaApi\Api\RequestStatusLabelProviderInterface;
use Exto\RmaApi\Model\Error\NotImplementedEmitter;

/**
 * Class RequestStatusLabelProvider.
 */
class RequestStatusLabelProvider implements RequestStatusLabelProviderInterface
{
    /**
     * @var NotImplementedEmitter
     */
    private $emitter;

    /**
     * @param NotImplementedEmitter $emitter
     */
    public function __construct(NotImplementedEmitter $emitter)
    {
        $this->emitter = $emitter;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatusLabel($statusId, $storeId = null)
    {
        $this->emitter->emit(RequestStatusLabelProviderInterface::class);
    }
}
