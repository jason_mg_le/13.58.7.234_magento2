<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\RmaApi\Model\Stub;

use Magento\Framework\Api\SearchCriteriaInterface;
use Exto\RmaApi\Api\Data\RequestInterface;
use Exto\RmaApi\Api\RequestRepositoryInterface;
use Exto\RmaApi\Exception\ModuleInactiveException;
use Exto\RmaApi\Model\Error\NotImplementedEmitter;

/**
 * Class RequestRepository.
 */
class RequestRepository implements RequestRepositoryInterface
{
    /**
     * @var NotImplementedEmitter
     */
    private $emitter;

    /**
     * @param NotImplementedEmitter $emitter
     */
    public function __construct(NotImplementedEmitter $emitter)
    {
        $this->emitter = $emitter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(RequestInterface $request)
    {
        $this->emitter->emit(RequestRepositoryInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function get($id)
    {
        $this->emitter->emit(RequestRepositoryInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $this->emitter->emit(RequestRepositoryInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(RequestInterface $request)
    {
        $this->emitter->emit(RequestRepositoryInterface::class);
    }
}
