<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\RmaApi\Model\Error;

use Exto\RmaApi\Exception\NotImplementedException;

/**
 * Class NotImplementedEmitter.
 */
class NotImplementedEmitter
{
    /**
     * Emit not implemented exception.
     *
     * @param string $serviceName
     * @return void
     * @throws NotImplementedException
     */
    public function emit($serviceName)
    {
        throw new NotImplementedException(
            __('Service %1 not implemented.', $serviceName)
        );
    }
}
