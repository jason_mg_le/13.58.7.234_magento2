<?php
namespace Exto\Rma\Block\Guest;

/**
 * Class Link
 */
class Link extends \Magento\Framework\View\Element\Html\Link\Current
{
    /**
     * @return string
     */
    protected function _toHtml()
    {
        $isGuestAllowed = $this->_scopeConfig->getValue(
            'exto_rma/general/is_allow_quest_returns',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (!$isGuestAllowed) {
            return '';
        }
        return parent::_toHtml();
    }
}
