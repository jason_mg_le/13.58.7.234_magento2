<?php
namespace Exto\Rma\Block\Guest;

/**
 * Class Add
 */
class Add extends \Exto\Rma\Block\Customer\Add
{
    /**
     * @return string|null
     */
    protected function getCustomerEmail()
    {
        return $this->customerSession->getExtoRmaGuestEmail();
    }

    /**
     * @return null|int
     */
    protected function getCustomerId()
    {
        return null;
    }
}
