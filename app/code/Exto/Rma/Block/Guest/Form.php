<?php
namespace Exto\Rma\Block\Guest;

/**
 * Class Form
 */
class Form extends \Magento\Framework\View\Element\Template
{
    /**
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->getUrl('exto_rma/guest/loginPost');
    }
}
