<?php
namespace Exto\Rma\Block\Customer\Add;

/**
 * Class Form
 */
class Form extends \Magento\Framework\View\Element\Template
{
    const XML_PATH_POLICY_CMS_BLOCK = 'exto_rma/return_policy/return_policy_cms_block';

    /** @var \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory */
    protected $resolutionCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory */
    protected $reasonCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Package\CollectionFactory */
    protected $packageConditionCollectionFactory;

    /** @var \Magento\Cms\Model\Block|null */
    protected $cmsBlock = null;

    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory */
    protected $customFieldCollectionFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context             $context
     * @param \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory       $reasonCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory   $resolutionCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Package\CollectionFactory      $packageConditionCollectionFactory
     * @param \Magento\Cms\Model\Block                                     $cmsBlock
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory $customFieldCollectionFactory
     * @param array                                                        $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory $reasonCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory $resolutionCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Package\CollectionFactory $packageConditionCollectionFactory,
        \Magento\Cms\Model\Block $cmsBlock,
        \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory $customFieldCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->reasonCollectionFactory = $reasonCollectionFactory;
        $this->resolutionCollectionFactory = $resolutionCollectionFactory;
        $this->packageConditionCollectionFactory = $packageConditionCollectionFactory;
        $this->cmsBlock = $cmsBlock;
        $this->customFieldCollectionFactory = $customFieldCollectionFactory;
    }

    /**
     * @return array
     */
    public function getReasonOptions()
    {
        $options = [
            ['value' => '', 'label' => '- Select reason -']
        ];
        /** @var \Exto\Rma\Model\ResourceModel\Reason\Collection $collection */
        $collection = $this->reasonCollectionFactory->create();
        $collection->joinLabelForStore($this->_storeManager->getStore()->getId());
        $collection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        return array_merge(
            $options,
            $collection->toOptionArray('label')
        );
    }

    /**
     * @return array
     */
    public function getPackageOptions()
    {
        $options = [
            ['value' => '', 'label' => '- Select package condition -']
        ];
        /** @var \Exto\Rma\Model\ResourceModel\Package\Collection $collection */
        $collection = $this->packageConditionCollectionFactory->create();
        $collection->joinLabelForStore($this->_storeManager->getStore()->getId());
        $collection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        return array_merge(
            $options,
            $collection->toOptionArray('label')
        );
    }

    /**
     * @return array
     */
    public function getResolutionOptions()
    {
        $options = [
            ['value' => '', 'label' => '- Select resolution -']
        ];
        /** @var \Exto\Rma\Model\ResourceModel\Resolution\Collection $collection */
        $collection = $this->resolutionCollectionFactory->create();
        $collection->joinLabelForStore($this->_storeManager->getStore()->getId());
        $collection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        return array_merge(
            $options,
            $collection->toOptionArray('label')
        );
    }

    /**
     * @return string
     */
    public function getPolicyCMSBlockName()
    {
        return $this->getCmsBlock()->getTitle();
    }

    /**
     * @return string
     */
    public function getPolicyCMSBlockContent()
    {
        return $this->getCmsBlock()->getContent();
    }

    /**
     * @return \Magento\Cms\Model\Block
     */
    protected function getCmsBlock()
    {
        if (null === $this->cmsBlock->getId()) {
            $blockId = $this->_scopeConfig->getValue(
                self::XML_PATH_POLICY_CMS_BLOCK,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
            $this->cmsBlock->load($blockId);
        }
        return $this->cmsBlock;
    }

    /**
     * @return array
     */
    public function getCustomFieldList()
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Collection $collection */
        $collection = $this->customFieldCollectionFactory->create();
        $collection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        $collection->addFieldToFilter('is_visible_for_customer', ['eq' => '1']);
        $collection->addFieldToFilter(
            'editable_for_customer_in_statuses',
            ['finset' => [\Exto\Rma\Model\Request::NEW_STATUS_ID]]
        );
        $options = [];
        foreach ($collection as $item) {
            /** @var \Exto\Rma\Model\Custom\Field $item */
            $options[] = [
                'id' => $item->getId(),
                'type_id' => $item->getTypeId(),
                'name' => $item->getLabel($this->_storeManager->getStore()->getId()),
                'is_required' => !!$item->getIsRequired(),
                'options' => $item->getOptions($this->_storeManager->getStore()->getId())
            ];
        }
        return $options;
    }

    /**
     * @param array $customField
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getDateInput($customField)
    {
        $calendar = $this->getLayout()->createBlock(
            \Magento\Framework\View\Element\Html\Date::class
        )->setId(
            'custom_field_' . $customField['id']
        )->setName(
            'custom_field[' . $customField['id'] . ']'
        )->setClass(
            'datetime-picker input-text'
        )->setImage(
            $this->getViewFileUrl('Magento_Theme::calendar.png')
        )->setDateFormat(
            $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT)
        )->setValue(
            ''
        );
        if ($customField['is_required']) {
            $calendar->setClass(
                $calendar->getClass() . ' required-entry'
            );
        }
        return $calendar->getHtml() . "<button style='display:none;'></button>";
    }
}
