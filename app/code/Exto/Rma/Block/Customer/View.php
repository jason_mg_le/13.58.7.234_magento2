<?php
namespace Exto\Rma\Block\Customer;

/**
 * Class View
 */
class View extends \Magento\Framework\View\Element\Template
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /** @var \Magento\Customer\Model\CustomerFactory */
    protected $customerFactory;

    /** @var \Magento\Directory\Model\CountryFactory */
    protected $countryModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory */
    protected $itemCollectionFactory;

    /** @var \Magento\Catalog\Model\ProductFactory */
    protected $productFactory;

    /** @var \Exto\Rma\Model\ReasonFactory */
    protected $reasonFactory;

    /** @var \Exto\Rma\Model\PackageFactory */
    protected $packageFactory;

    /** @var \Exto\Rma\Model\ResolutionFactory */
    protected $resolutionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory */
    protected $customFieldCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory */
    protected $valueCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Chat\Message\CollectionFactory */
    protected $messageCollectionFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context                   $context
     * @param \Magento\Framework\Registry                                        $coreRegistry
     * @param \Magento\Customer\Model\CustomerFactory                            $customerFactory
     * @param \Magento\Directory\Model\CountryFactory                            $countryModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory       $itemCollectionFactory
     * @param \Magento\Catalog\Model\ProductFactory                              $productFactory
     * @param \Exto\Rma\Model\ReasonFactory                                      $reasonFactory
     * @param \Exto\Rma\Model\PackageFactory                                     $packageFactory
     * @param \Exto\Rma\Model\ResolutionFactory                                  $resolutionFactory
     * @param \Exto\Rma\Model\ResourceModel\Chat\Message\CollectionFactory       $messageCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory       $customFieldCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Status\Content\CollectionFactory     $statusContentCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory $valueCollectionFactory
     * @param \Exto\Rma\Model\StatusFactory                                      $statusFactory
     * @param array                                                              $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Directory\Model\CountryFactory $countryModelFactory,
        \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $itemCollectionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Exto\Rma\Model\ReasonFactory $reasonFactory,
        \Exto\Rma\Model\PackageFactory $packageFactory,
        \Exto\Rma\Model\ResolutionFactory $resolutionFactory,
        \Exto\Rma\Model\ResourceModel\Chat\Message\CollectionFactory $messageCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory $customFieldCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Status\Content\CollectionFactory $statusContentCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory $valueCollectionFactory,
        \Exto\Rma\Model\StatusFactory $statusFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $coreRegistry;
        $this->customerFactory = $customerFactory;
        $this->countryModelFactory = $countryModelFactory;
        $this->itemCollectionFactory = $itemCollectionFactory;
        $this->productFactory = $productFactory;
        $this->reasonFactory = $reasonFactory;
        $this->packageFactory = $packageFactory;
        $this->resolutionFactory = $resolutionFactory;
        $this->messageCollectionFactory = $messageCollectionFactory;
        $this->customFieldCollectionFactory = $customFieldCollectionFactory;
        $this->statusContentCollectionFactory = $statusContentCollectionFactory;
        $this->valueCollectionFactory = $valueCollectionFactory;
        $this->statusFactory = $statusFactory;
    }

    /**
     * @param int $type
     * @return null|\Exto\Rma\Model\Status\Content
     */
    protected function getCustomerAction($type)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Status\Content\Collection $collection */
        $collection = $this->statusContentCollectionFactory->create();
        $collection->addTypeFilter($type);
        $collection->addParentFilter($this->getRequest()->getStatusId());
        $result = null;
        foreach ($collection as $item) {
            if (null === $result && $item->getStoreId() == 0) {
                $result = $item;
            }
            if ($item->getStoreId() == $this->_storeManager->getStore()->getId()) {
                $result = $item;
            }
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function isCustomerActionNeeded()
    {
        $result = $this->getCustomerAction(\Exto\Rma\Model\Status\Content::TYPE_CORRESPONDING_CUSTOMER_ACTION);
        if (null === $result || null === $result->getId() || strlen($result->getValue()) < 1) {
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getCustomerActionUrl()
    {
        return $this->getUrl('*/*/changeStatus', ['id' => $this->getRequest()->getId()]);
    }

    /**
     * @return mixed
     */
    public function getCustomerActionLabel()
    {
        $result = $this->getCustomerAction(\Exto\Rma\Model\Status\Content::TYPE_CORRESPONDING_CUSTOMER_ACTION);
        return $result->getValue();
    }

    /**
     * @return bool
     */
    public function getCustomActionConfirmationText()
    {
        $text = $this->getCustomerAction(\Exto\Rma\Model\Status\Content::TYPE_CONFIRMATION_TEXT);
        $status = $this->statusFactory->create();
        $status->load($this->getRequest()->getStatusId());
        $isRequired = !!$status->getData('is_require_confirmation');
        if (!$isRequired) {
            return false;
        }
        return $text->getValue();
    }

    /**
     * @return string
     */
    public function getAddressText()
    {
        /** @var \Magento\Customer\Model\Address\AbstractAddress $billingAddress */
        $billingAddress = $this->getCustomer()->getDefaultBillingAddress();
        if (!$billingAddress || null === $billingAddress->getId()) {
            $billingAddress = $this->getOrder()->getBillingAddress();
        }
        $result = [];
        $result[] = $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
        $result[] = $billingAddress->getStreetFull();
        $result[] = $billingAddress->getCity() . ', ' . $billingAddress->getRegion()
            . ', ' . $billingAddress->getPostcode();
        $countryModel = $this->countryModelFactory->create();
        $countryModel->load($billingAddress->getCountryId());
        $result[] = $countryModel->getName();
        $result[] = $billingAddress->getTelephone();
        return join("\n", $result);
    }

    /**
     * @return string
     */
    public function getOrderUrl()
    {
        return $this->getUrl('sales/order/view', ['order_id' => $this->getOrder()->getId()]);
    }

    /**
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->getRequest()->getOrder();
    }

    /**
     * @return \Exto\Rma\Model\Request
     */
    public function getRequest()
    {
        return $this->coreRegistry->registry('current_request');
    }

    /**
     * @param null|string|\DateTime $date
     *
     * @return string
     */
    public function dateFormat($date)
    {
        return $this->formatDate($date, \IntlDateFormatter::MEDIUM);
    }

    /**
     * @return \Exto\Rma\Model\ResourceModel\Request\Item\Collection
     */
    public function getItemCollection()
    {
        /** @var \Exto\Rma\Model\ResourceModel\Request\Item\Collection $collection */
        $collection = $this->itemCollectionFactory->create();
        $collection->joinProductData();
        $collection->addRequestFilter($this->getRequest()->getId());
        return $collection;
    }

    /**
     * @param \Exto\Rma\Model\ResourceModel\Request\Item $item
     *
     * @return mixed
     */
    public function getProductUrlForItem($item)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->productFactory->create();
        $product->load($item->getProductId());
        return $product->getProductUrl();
    }

    /**
     * @return string
     */
    public function getReasonLabel()
    {
        /** @var \Exto\Rma\Model\Reason $reason */
        $reason = $this->reasonFactory->create();
        $reason->load($this->getRequest()->getReasonId());
        return $reason->getLabel($this->_storeManager->getStore()->getId());
    }

    /**
     * @return string
     */
    public function getPackageLabel()
    {
        /** @var \Exto\Rma\Model\Package $package */
        $package = $this->packageFactory->create();
        $package->load($this->getRequest()->getPackageConditionId());
        return $package->getLabel($this->_storeManager->getStore()->getId());
    }

    /**
     * @return string
     */
    public function getResolutionLabel()
    {
        /** @var \Exto\Rma\Model\Resolution $resolution */
        $resolution = $this->resolutionFactory->create();
        $resolution->load($this->getRequest()->getResolutionId());
        return $resolution->getLabel($this->_storeManager->getStore()->getId());
    }

    /**
     * @return string
     */
    public function getMessagePostUrl()
    {
        return $this->getUrl('*/*/messagePost');
    }

    /**
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        /** @var \Magento\Customer\Model\Customer $customer */
        $customer = $this->customerFactory->create();
        $customer->load($this->getRequest()->getCustomerId());
        return $customer;
    }

    /**
     * @return \Exto\Rma\Model\ResourceModel\Chat\Message\Collection
     */
    public function getMessagesList()
    {
        /** @var \Exto\Rma\Model\ResourceModel\Chat\Message\Collection $collection */
        $collection = $this->messageCollectionFactory->create();
        $collection
            ->addRequestIdFilter($this->getRequest()->getId())
            ->addNotInternalFilter()
            ->setOrder('entity_id')
        ;
        return $collection;
    }

    /**
     * @param \Exto\Rma\Model\Chat\Attachment $attachment
     *
     * @return string
     */
    public function getAttachmentUrl($attachment)
    {
        return $this->getUrl('*/*/getAttachment', ['attachment_id' => $attachment->getId()]);
    }

    /**
     * @return string
     */
    public function getPackagingSlipUrl()
    {
        return $this->getUrl('*/*/printAction', ['id' => $this->getRequest()->getId()]);
    }

    /**
     * @return array
     */
    public function getCustomFieldList()
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Collection $collection */
        $collection = $this->customFieldCollectionFactory->create();
        $collection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        $collection->addFieldToFilter('is_visible_for_customer', ['eq' => '1']);
        $options = [];
        foreach ($collection as $item) {
            /** @var \Exto\Rma\Model\Custom\Field $item */
            $isEditable = in_array(
                $this->getRequest()->getStatusId(),
                explode(',', $item->getData('editable_for_customer_in_statuses'))
            );
            $options[] = [
                'id' => $item->getId(),
                'type_id' => $item->getTypeId(),
                'name' => $item->getLabel($this->_storeManager->getStore()->getId()),
                'is_required' => !!$item->getIsRequired(),
                'is_editable' => $this->isCustomerActionNeeded() && $isEditable,
                'options' => $item->getOptions($this->_storeManager->getStore()->getId()),
                'value_as_text' => $this->getFieldValue($item),
                'value' => $this->getValue($item)
            ];
        }
        return $options;
    }

    /**
     * @param array $customField
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getDateInput($customField)
    {
        $calendar = $this->getLayout()->createBlock(
            \Magento\Framework\View\Element\Html\Date::class
        )->setId(
            'custom_field_' . $customField['id']
        )->setName(
            'custom_field[' . $customField['id'] . ']'
        )->setClass(
            'datetime-picker input-text'
        )->setImage(
            $this->getViewFileUrl('Magento_Theme::calendar.png')
        )->setDateFormat(
            $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT)
        )->setValue(
            $this->getFieldValue($customField['id'])
        );
        if ($customField['is_required']) {
            $calendar->setExtraParams(
                'data-validate="{required:true}"'
            );
        }
        return $calendar->getHtml();
    }

    /**
     * @param \Exto\Rma\Model\Custom\Field $field
     *
     * @return mixed
     */
    protected function getFieldValue($field)
    {
        $value = $this->getValue($field);
        if ($field->getTypeId() == \Exto\Rma\Model\Source\Field\Type::DROPDOWN_VALUE) {
            $options = $field->getOptions($this->_storeManager->getStore()->getId());
            return array_key_exists($value, $options)?$options[$value]:$value;
        }
        return $value;
    }

    /**
     * @param \Exto\Rma\Model\Custom\Field $field
     *
     * @return mixed
     */
    protected function getValue($field)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Value\Collection $valueCollection */
        $valueCollection = $this->valueCollectionFactory->create();
        $valueCollection->addParentFilter($field->getId());
        $valueCollection->addRequestFilter($this->getRequest()->getId());
        return $valueCollection->getFirstItem()->getValue();
    }
}
