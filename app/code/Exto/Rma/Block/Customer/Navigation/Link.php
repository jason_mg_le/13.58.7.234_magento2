<?php
namespace Exto\Rma\Block\Customer\Navigation;

use \Magento\Framework\App\Config;

/**
 * Class Link
 */
class Link extends \Magento\Framework\View\Element\Html\Link\Current
{
    /**
     * @return string
     */
    public function getLabel()
    {
        $configValue = trim($this->_scopeConfig->getValue(
            'exto_rma/general/page_title_in_customer_area',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ));
        if (strlen($configValue) > 0) {
            return $configValue;
        }
        return parent::getLabel();
    }

    /**
     * @return string
     */
    protected function _toHtml()
    {
        $configValue = $this->_scopeConfig->isSetFlag(
            'exto_rma/general/is_show_interface_for_customer',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($configValue) {
            return parent::_toHtml();
        }
        return '';
    }
}
