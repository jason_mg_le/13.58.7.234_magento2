<?php
namespace Exto\Rma\Block\Customer\PrintBlock;

/**
 * Class Content
 */
class Content extends \Magento\Framework\View\Element\Template
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /** @var \Magento\Customer\Model\Customer */
    protected $customer;

    /** @var \Exto\Rma\Helper\Address */
    protected $addressHelper;

    /** @var \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory */
    protected $itemCollectionFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context             $context
     * @param \Magento\Framework\Registry                                  $coreRegistry
     * @param \Magento\Customer\Model\CustomerFactory                      $customerFactory
     * @param \Exto\Rma\Helper\Address                                     $addressHelper
     * @param \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $itemCollectionFactory
     * @param array                                                        $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Exto\Rma\Helper\Address $addressHelper,
        \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $itemCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $coreRegistry;
        $this->customer = $customerFactory->create();
        $this->addressHelper = $addressHelper;
        $this->itemCollectionFactory = $itemCollectionFactory;
    }

    /**
     * @return string
     */
    public function getReturnAddressHtml()
    {
        return $this->_scopeConfig->getValue(
            'exto_rma/general/return_address',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getCustomerInformationHtml()
    {
        return $this->addressHelper->getAddressHtml($this->getCustomer(), $this->getOrder());
    }

    /**
     * @return array
     */
    public function getItemList()
    {
        $result = [];
        /** @var \Exto\Rma\Model\ResourceModel\Request\Item\Collection $collection */
        $collection = $this->itemCollectionFactory->create();
        $collection->addRequestFilter($this->getRequestModel()->getId());
        $collection->joinProductData();
        foreach ($collection as $item) {
            /** @var \Exto\Rma\Model\Request\Item $item */
            $result[] = [
                'product' => $item->getData('product_name'),
                'sku' => $item->getData('product_sku'),
                'qty' => intval($item->getData('qty')),
            ];
        }

        return $result;
    }

    /**
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        if (null === $this->customer->getId() && $this->getRequestModel()->getCustomerId() > 0) {
            $this->customer->load($this->getRequestModel()->getCustomerId());
        }
        return $this->customer;
    }

    /**
     * @return \Magento\Sales\Model\Order
     */
    protected function getOrder()
    {
        return $this->getRequestModel()->getOrder();
    }

    /**
     * @return \Exto\Rma\Model\Request
     */
    protected function getRequestModel()
    {
        return $this->coreRegistry->registry('current_request');
    }
}
