<?php
namespace Exto\Rma\Block\Customer\PrintBlock;

/**
 * Class Title
 */
class Title extends \Magento\Framework\View\Element\Template
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /**
     * @param \Magento\Framework\View\Element\Template\Context                   $context
     * @param \Magento\Framework\Registry                                        $coreRegistry
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory       $customFieldCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory $fieldValueCollectionFactory
     * @param array                                                              $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory $customFieldCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory $fieldValueCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $coreRegistry;
        $this->customFieldCollectionFactory = $customFieldCollectionFactory;
        $this->fieldValueCollectionFactory = $fieldValueCollectionFactory;
    }

    /**
     * @return string
     */
    public function getCreatedDate()
    {
        return $this->formatDate($this->getRequestModel()->getCreatedAt(), \IntlDateFormatter::MEDIUM);
    }

    /**
     * @return string
     */
    public function getOrderIncrementId()
    {
        return $this->getRequestModel()->getOrder()->getIncrementId();
    }

    /**
     * @return string
     */
    public function getOrderCreatedAt()
    {
        return $this->formatDate(
            $this->getRequestModel()->getOrder()->getCreatedAt(),
            \IntlDateFormatter::MEDIUM
        );
    }

    /**
     * @return string
     */
    public function getCustomFieldHtml()
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Collection $collection */
        $collection = $this->customFieldCollectionFactory->create();
        $collection->addFieldToFilter('is_add_to_shipping_label', ['eq' => 1]);
        $result = [];
        foreach ($collection as $customField) {
            /** @var \Exto\Rma\Model\Custom\Field $customField */
            $value = $this->getFieldValue($customField);
            $label = $customField->getLabel($this->_storeManager->getStore()->getId());
            $result[] = $label . ': ' . $value;
        }
        return join("\n", $result);
    }

    /**
     * @return \Exto\Rma\Model\Request
     */
    protected function getRequestModel()
    {
        return $this->coreRegistry->registry('current_request');
    }

    /**
     * @param \Exto\Rma\Model\Custom\Field $field
     *
     * @return string
     */
    protected function getFieldValue($field)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Value\Collection $valueCollection */
        $valueCollection = $this->fieldValueCollectionFactory->create();
        $valueCollection->addParentFilter($field->getId());
        $valueCollection->addRequestFilter($this->getRequestModel()->getId());
        $value = $valueCollection->getFirstItem()->getValue();
        if ($field->getTypeId() == \Exto\Rma\Model\Source\Field\Type::DROPDOWN_VALUE) {
            $options = $field->getOptions($this->_storeManager->getStore()->getId());
            return array_key_exists($value, $options)?$options[$value]:$value;
        }
        return $value;
    }
}
