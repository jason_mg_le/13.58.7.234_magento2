<?php
namespace Exto\Rma\Block\Customer;

/**
 * Class RequestList
 */
class RequestList extends \Magento\Framework\View\Element\Template
{
    /** @var  \Exto\Rma\Model\ResourceModel\Grid\Request\Collection */
    protected $collection;

    /** @var  \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory */
    protected $itemCollectionFactory;

    /** @var  \Exto\Rma\Model\ResourceModel\Status\CollectionFactory */
    protected $statusCollectionFactory;

    /** @var \Magento\Customer\Helper\Session\CurrentCustomer */
    protected $currentCustomer;

    /** @var \Magento\Catalog\Model\ProductFactory */
    protected $productFactory;

    /** @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory */
    protected $orderCollectionFactory;

    /** @var array */
    protected $statusOptions = [];

    /**
     * @param \Magento\Framework\View\Element\Template\Context             $context
     * @param \Exto\Rma\Model\ResourceModel\Grid\Request\CollectionFactory $collectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $itemCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Status\CollectionFactory       $statusCollectionFactory
     * @param \Magento\Customer\Helper\Session\CurrentCustomer             $currentCustomer
     * @param \Magento\Catalog\Model\ProductFactory                        $productFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory   $orderCollectionFactory
     * @param array                                                        $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Exto\Rma\Model\ResourceModel\Grid\Request\CollectionFactory $collectionFactory,
        \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $itemCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Status\CollectionFactory $statusCollectionFactory,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->collection = $collectionFactory->create();
        $this->itemCollectionFactory = $itemCollectionFactory;
        $this->statusCollectionFactory = $statusCollectionFactory;
        $this->currentCustomer = $currentCustomer;
        $this->productFactory = $productFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->initCollection();
    }

    /**
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        if ($this->getItems()) {
            $toolbar = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'customer_rma_list.toolbar'
            )->setCollection(
                $this->getItems()
            );
            $this->setChild('toolbar', $toolbar);
        }
        return parent::_prepareLayout();
    }

    /**
     * @return bool
     */
    public function hasItems()
    {
        return $this->getItems()->getSize() > 0;
    }

    /**
     * @return \Exto\Rma\Model\ResourceModel\Grid\Request\Collection
     */
    public function getItems()
    {
        return $this->collection;
    }

    /**
     * @return $this
     */
    protected function initCollection()
    {
        $this->collection->addFieldToFilter(
            'main_table.customer_id',
            ['eq' => $this->currentCustomer->getCustomerId()]
        );
        $this->statusOptions = [];
        /** @var \Exto\Rma\Model\ResourceModel\Status\Collection $collection */
        $collection = $this->statusCollectionFactory->create();
        $collection->joinLabelForStore($this->_storeManager->getStore()->getId());
        foreach ($collection as $status) {
            $this->statusOptions[$status->getId()] = $status->getData('label');
        }
        return $this;
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return string
     */
    public function getReturnUrl($request)
    {
        return $this->getUrl('exto_rma/customer/view', ['id' => $request->getId()]);
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return string
     */
    public function getOrderUrl($request)
    {
        return $this->getUrl('sales/order/view', ['order_id' => $request->getOrderId()]);
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return \Exto\Rma\Model\ResourceModel\Request\Item\Collection
     */
    public function getItemCollection($request)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Request\Item\Collection $collection */
        $collection = $this->itemCollectionFactory->create();
        $collection->joinProductData();
        $collection->addRequestFilter($request->getId());
        return $collection;
    }

    /**
     * @param \Exto\Rma\Model\ResourceModel\Request\Item $item
     *
     * @return mixed
     */
    public function getProductUrlToItem($item)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->productFactory->create();
        $product->load($item->getProductId());
        return $product->getProductUrl();
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return string
     */
    public function getStatusLabel($request)
    {
        return $this->statusOptions[$request->getStatusId()];
    }

    /**
     * @param null|string|\DateTime $date
     *
     * @return string
     */
    public function dateFormat($date)
    {
        return $this->formatDate($date, \IntlDateFormatter::MEDIUM);
    }

    /**
     * @return string
     */
    public function getUrlToAddNewRMA()
    {
        return $this->getUrl('exto_rma/customer/add');
    }

    /**
     * @return bool
     */
    public function isCanDisplayActionButton()
    {
        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $collection */
        $collection = $this->orderCollectionFactory->create();
        $collection->addFieldToFilter(
            'customer_id',
            ['eq' => $this->currentCustomer->getCustomerId()]
        );
        $collection->addFieldToFilter(
            'state',
            ['in' => [\Magento\Sales\Model\Order::STATE_COMPLETE]]
        );
        return $collection->getSize() > 0;
    }
}
