<?php
namespace Exto\Rma\Block\Customer;

/**
 * Class Add
 */
class Add extends \Magento\Framework\View\Element\Template
{
    /** @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory */
    protected $orderCollectionFactory;

    /** @var \Magento\Customer\Model\Session */
    protected $customerSession;

    /** @var \Magento\Catalog\Model\ProductFactory */
    protected $productFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory */
    protected $requestItemCollectionFactory;

    /** @var \Magento\Sales\Model\ResourceModel\Order\Collection */
    protected $orderCollection = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context             $context
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory   $orderCollectionFactory
     * @param \Magento\Customer\Model\Session                              $customerSession
     * @param \Magento\Catalog\Model\ProductFactory                        $productFactory
     * @param \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $requestItemCollectionFactory
     * @param array                                                        $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $requestItemCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->productFactory = $productFactory;
        $this->requestItemCollectionFactory = $requestItemCollectionFactory;
        $this->initCollection();
    }

    /**
     * @return string
     */
    protected function getCustomerEmail()
    {
        return $this->customerSession->getCustomer()->getEmail();
    }

    /**
     * @return int
     */
    protected function getCustomerId()
    {
        return $this->customerSession->getCustomer()->getId();
    }

    /**
     * @return $this
     */
    protected function initCollection()
    {
        $this->orderCollection = $this->orderCollectionFactory->create();
        if (null !== $this->getCustomerId()) {
            $this->orderCollection->addFieldToFilter(
                [
                    'main_table.customer_id',
                    'main_table.customer_email'
                ],
                [
                    ['eq' => $this->getCustomerId()],
                    ['eq' => $this->getCustomerEmail()]
                ]
            );
        } else {
            $this->orderCollection->addFieldToFilter(
                'main_table.customer_email',
                ['eq' => $this->getCustomerEmail()]
            );
        }

        $this->orderCollection->addFieldToFilter(
            'main_table.state',
            ['in' => [\Magento\Sales\Model\Order::STATE_COMPLETE]]
        );
        $returnDeadline = $this->_scopeConfig->getValue(
            'exto_rma/general/return_deadline',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (intval($returnDeadline) > 0) {
            $minDate = new \Zend_Date();
            $minDate->subDay(intval($returnDeadline));
            $this->orderCollection->addFieldToFilter(
                'main_table.created_at',
                ['gt' => $minDate->toString('YYYY-MM-dd HH:mm:ss')]
            );
        }

        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $processedOrderCollection */
        $processedOrderCollection = $this->orderCollectionFactory->create();
        $processedOrderCollection->getSelect()->joinLeft(
            ['request' => $processedOrderCollection->getTable('exto_rma_request')],
            'request.order_id = main_table.entity_id',
            []
        )->joinLeft(
            ['request_item' => $processedOrderCollection->getTable('exto_rma_request_item')],
            'request_item.request_id = request.entity_id',
            ['SUM(request_item.qty) AS total_rma_qty']
        );
        $processedOrderCollection->getSelect()->group('main_table.entity_id');

        $this->orderCollection->getSelect()->joinLeft(
            ['qty' => $processedOrderCollection->getSelect()],
            'qty.entity_id = main_table.entity_id',
            []
        );

        $this->orderCollection->getSelect()->where(
            'main_table.total_qty_ordered > qty.total_rma_qty OR ISNULL(qty.total_rma_qty)'
        );
        return $this;
    }

    /**
     * @return bool
     */
    public function hasOrders()
    {
        $orderCollection = clone $this->orderCollection;
        return $orderCollection->getSize() > 0;
    }

    /**
     * @return \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    public function getOrderList()
    {
        return $this->orderCollection;
    }

    /**
     * @param null|string|\DateTime $date
     *
     * @return string
     */
    public function dateFormat($date)
    {
        return $this->formatDate($date, \IntlDateFormatter::MEDIUM);
    }

    /**
     * @param string $price
     *
     * @return string
     */
    public function formatCurrency($price)
    {
        return $this->_storeManager->getWebsite()->getBaseCurrency()->format($price);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     *
     * @return string
     */
    public function getQtyLabel($order)
    {
        /** @var  $itemCollection */
        $itemCollection = $this->getItemCollection($order);
        $qty = $itemCollection->getSize();
        if ($qty == 1) {
            return __('%1 item', $qty);
        }
        return __('%1 items', $qty);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     *
     * @return string
     */
    public function getUrlToOrder($order)
    {
        return $this->getUrl('sales/order/view', ['order_id' => $order->getId()]);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     *
     * @return \Magento\Sales\Model\ResourceModel\Order\Item\Collection
     */
    public function getItemCollection($order)
    {
        $itemCollection = clone $order->getItemsCollection([], true);
        return $itemCollection;
    }

    /**
     * @param \Magento\Sales\Model\Order\Item $item
     *
     * @return string|null
     */
    public function getProductUrl($item)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->productFactory->create();
        $product->load($item->getProductId());
        if (null === $product->getId()) {
            return null;
        }
        return $product->getProductUrl();
    }

    /**
     * @param \Magento\Sales\Model\Order\Item $item
     *
     * @return string
     */
    public function getProductName($item)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->productFactory->create();
        $product->load($item->getProductId());
        if (null === $product->getId()) {
            return $item->getName();
        }
        return $product->getName();
    }

    /**
     * @param \Magento\Sales\Model\Order\Item $item
     *
     * @return int
     */
    public function getAvailableQty($item)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Request\Item\Collection $requestItemCollection */
        $requestItemCollection = $this->requestItemCollectionFactory->create();
        $requestItemCollection->addFieldToFilter('order_item_id', ['eq' => $item->getId()]);
        $processedQty = 0;
        foreach ($requestItemCollection as $requestItem) {
            /** @var \Exto\Rma\Model\Request\Item $requestItem */
            $processedQty += $requestItem->getQty();
        }
        return intval($item->getData('qty_ordered')) - $processedQty;
    }

    /**
     * @return string
     */
    public function getSubmitUrl()
    {
        return $this->getUrl('*/*/postNew');
    }
}
