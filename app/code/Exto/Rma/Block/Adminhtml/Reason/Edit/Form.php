<?php
namespace Exto\Rma\Block\Adminhtml\Reason\Edit;

/**
 * Class Form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /** @var \Exto\Rma\Block\Adminhtml\Renderer\ContentLine */
    protected $renderer;

    /** @var int */
    protected $optionCount = 0;

    /** @var \Exto\Rma\Model\ReasonFactory */
    protected $reasonFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Reason */
    protected $reasonResource;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Exto\Rma\Block\Adminhtml\Renderer\ContentLine $renderer
     * @param \Exto\Rma\Model\ReasonFactory $reasonFactory
     * @param \Exto\Rma\Model\ResourceModel\Reason $reasonResource
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Exto\Rma\Block\Adminhtml\Renderer\ContentLine $renderer,
        \Exto\Rma\Model\ReasonFactory $reasonFactory,
        \Exto\Rma\Model\ResourceModel\Reason $reasonResource,
        array $data = []
    ) {
        $this->renderer = $renderer;
        $this->reasonFactory = $reasonFactory;
        $this->reasonResource = $reasonResource;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $reasons = $this->_coreRegistry->registry('current_reasons');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setUseContainer(true);
        $form->setHtmlIdPrefix('reason_');

        if (!count($reasons)) {
            $this->optionCount++;
            /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
            $fieldset = $form->addFieldset(
                'option_fieldset_'.$this->optionCount,
                [
                    'legend' => __(''),
                ]
            );

            $fieldset->addField(
                'name_'. $this->optionCount,
                'text',
                [
                    'name' => 'reasons['.$this->optionCount.'][name]',
                    'label' => __('Name'),
                    'title' => __('Name'),
                    'required' => true
                ]
            );

            $fieldset
                ->addField(
                    'labels_'. $this->optionCount,
                    'text',
                    [
                        'name' => 'reasons['.$this->optionCount.'][labels]',
                        'label' => __(''),
                        'title' => __('')
                    ]
                )
                ->setFrontendLabel(__('Title'))
                ->setFieldCode('reasons['.$this->optionCount.'][labels]')
                ->setOptionValues(null)
                ->setRenderer($this->renderer)
            ;

            $fieldset->addField(
                'sort_order_'. $this->optionCount,
                'text',
                [
                    'name' => 'reasons['.$this->optionCount.'][sort_order]',
                    'label' => __('Sort Order'),
                    'title' => __('Sort Order'),
                    'class' => "validate-zero-or-greater validate-number validate-digits",
                ]
            );
        } else {
            foreach ($reasons as $reason) {
                $this->optionCount = $this->optionCount < $reason->getId() ? $reason->getId(): $this->optionCount;
                $reasonModel = $this->reasonFactory->create();
                $this->reasonResource->load($reasonModel, $reason->getId());
                if (!$reasonModel || !$reasonModel->getId()) {
                    continue;
                }
                /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
                $fieldset = $form->addFieldset(
                    'option_fieldset_'.$reasonModel->getId(),
                    [
                        'legend' => __(''),
                    ]
                );

                $fieldset->addField(
                    'id_'.$reasonModel->getId(),
                    'hidden',
                    ['name' => 'reasons['.$reasonModel->getId().'][id]', 'value' => $reasonModel->getId()]
                );

                $fieldset->addField(
                    'name_'. $reasonModel->getId(),
                    'text',
                    [
                        'name' => 'reasons['.$reasonModel->getId().'][name]',
                        'label' => __('Name'),
                        'title' => __('Name'),
                        'value' => $reasonModel->getName(),
                        'required' => true
                    ]
                );

                $fieldset
                    ->addField(
                        'labels_'. $reasonModel->getId(),
                        'text',
                        [
                            'name' => 'reasons['.$reasonModel->getId().'][labels]',
                            'label' => __(''),
                            'title' => __('')
                        ]
                    )
                    ->setFrontendLabel(__('Title'))
                    ->setFieldCode('reasons['.$reasonModel->getId().'][labels]')
                    ->setOptionValues($reasonModel->getLabels())
                    ->setRenderer($this->renderer)
                ;
                $fieldset->addField(
                    'sort_order_'. $reasonModel->getId(),
                    'text',
                    [
                        'name' => 'reasons['.$reasonModel->getId().'][sort_order]',
                        'label' => __('Sort Order'),
                        'title' => __('Sort Order'),
                        'class' => "validate-zero-or-greater validate-number validate-digits",
                        'value' => $reasonModel->getSortOrder(),
                    ]
                );

                $removeButtonHtml = $this->getLayout()->createBlock(
                    \Magento\Backend\Block\Template::class
                )
                    ->setTemplate('Exto_Rma::renderer/option/remove.phtml')
                    ->toHtml();

                $fieldset->addField(
                    'remove_option_'.$reasonModel->getId(),
                    'label',
                    [
                        'name' => 'remove_option_'.$reasonModel->getId(),
                        'label' => __(''),
                        'title' => __(''),
                        'after_element_html' => $removeButtonHtml
                    ]
                );
            }

        }

        /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
        $fieldset = $form->addFieldset(
            'add_option_fieldset',
            [
                'legend' => __(''),
            ]
        );

        $stores = $this->_storeManager->getStores();
        $addOptionHtml = $this->getLayout()->createBlock(
            \Magento\Backend\Block\Template::class
        )
            ->setTemplate('Exto_Rma::renderer/option/add.phtml')
            ->setOptionCount($this->optionCount)
            ->setFormCode('reason')
            ->setFieldName('reasons')
            ->setStores($stores)
            ->setFieldContentName('reasons[replacement_id][labels]')
            ->toHtml();

        $fieldset->addField(
            'add_option',
            'label',
            [
                'name' => 'add_option',
                'label' => __(''),
                'title' => __(''),
                'after_element_html' => $addOptionHtml
            ]
        );
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
