<?php
namespace Exto\Rma\Block\Adminhtml\Status;

/**
 * Class Edit
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Exto_Rma';
        $this->_controller = 'adminhtml_status';
        $this->_mode = 'edit';
        parent::_construct();
        $this->buttonList->remove('reset');
    }
}
