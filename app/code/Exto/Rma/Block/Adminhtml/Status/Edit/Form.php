<?php
namespace Exto\Rma\Block\Adminhtml\Status\Edit;

/**
 * Class Form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /** @var \Exto\Rma\Block\Adminhtml\Renderer\ContentLine */
    protected $renderer;

    /** @var \Exto\Rma\Block\Adminhtml\Renderer\StatusContentTable */
    protected $rendererStatusTable;

    /** @var \Exto\Rma\Model\ResourceModel\Status\Collection */
    protected $statusCollection;

    /** @var \Magento\Email\Model\ResourceModel\Template\CollectionFactory */
    protected $emailTemplatesFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Exto\Rma\Block\Adminhtml\Renderer\ContentLine $renderer
     * @param \Exto\Rma\Block\Adminhtml\Renderer\StatusContentTable $rendererStatusTable
     * @param \Exto\Rma\Model\ResourceModel\Status\Collection $statusCollection
     * @param \Magento\Email\Model\ResourceModel\Template\CollectionFactory $emailTemplatesFactory
     * @param \Magento\Email\Model\Template\Config $emailConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Exto\Rma\Block\Adminhtml\Renderer\ContentLine $renderer,
        \Exto\Rma\Block\Adminhtml\Renderer\StatusContentTable $rendererStatusTable,
        \Exto\Rma\Model\ResourceModel\Status\Collection $statusCollection,
        \Magento\Email\Model\ResourceModel\Template\CollectionFactory $emailTemplatesFactory,
        \Magento\Email\Model\Template\Config $emailConfig,
        array $data = []
    ) {
        $this->renderer = $renderer;
        $this->rendererStatusTable = $rendererStatusTable;
        $this->statusCollection = $statusCollection;
        $this->emailTemplatesFactory = $emailTemplatesFactory;
        $this->emailConfig = $emailConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_status');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setUseContainer(true);
        $form->setHtmlIdPrefix('status_');

        /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
        $fieldset = $form->addFieldset(
            'general_fieldset',
            ['legend' => __('')]
        );

        if ($model->getId()) {
            $fieldset->addField(
                'entity_id',
                'hidden',
                ['name' => 'entity_id']
            );
        }

        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Name'),
                'title' => __('Name'),
                'required' => true
            ]
        );

        $fieldset
            ->addField(
                'labels',
                'text',
                [
                    'name' => 'labels',
                    'label' => __(''),
                    'title' => __(''),
                ]
            )
            ->setFieldCode('labels')
            ->setFrontendLabel(__('Label'))
            ->setOptionValues($model->getLabels())
            ->setAfterElementHtml('<div class="exto-rma-note">' . __('Label for customers') . '</div>')
            ->setRenderer($this->renderer)
        ;

        $emptyStatus = ['0' => ['value' => '0', 'label' => ' ']];
        $fieldset->addField(
            'next_status_id',
            'select',
            [
                'name' => 'next_status_id',
                'label' => __('Next workflow status'),
                'title' => __('Next workflow status'),
                'values' => array_merge($emptyStatus, $this->statusCollection->toOptionArray()),
                'after_element_html' => '<div class="exto-rma-note">' .
                    __('Which status must follow after this one in your return routine')
                    . '</div>'
            ]
        );

        $fieldset->addField(
            'next_status_caption',
            'text',
            [
                'name' => 'next_status_caption',
                'label' => __('Caption'),
                'title' => __('Caption')
            ]
        );

        $afterElementHtml = '<div class="exto-rma-note">'
            . __(
                'The action button which will be displayed to customers when this status is on. '
                . 'E.g. "Confirm package submission"'
            ) . '</div>';
        $fieldset
            ->addField(
                'data_table',
                'text',
                [
                    'name' => 'data_table',
                    'label' => __(''),
                    'title' => __('')
                ]
            )
            ->setFrontendLabel('')
            ->setColumnValues($model->getCorrespondingData())
            ->setAfterElementHtml($afterElementHtml)
            ->setRenderer($this->rendererStatusTable)
        ;

        $fieldset->addField(
            'customer_action_changes_status_id',
            'select',
            [
                'name' => 'customer_action_changes_status_id',
                'label' => __('Customer action changes status to'),
                'title' => __('Customer action changes status to'),
                'values' => $this->statusCollection->toOptionArray(),
                'after_element_html' => '<div class="exto-rma-note">' .
                    __('The request will obtain this status upon the customer action') .
                    '</div>'
            ]
        );

        $fieldset->addField(
            'is_require_confirmation',
            'checkboxes',
            [
                'name' => 'is_require_confirmation',
                'label' => __('Require confirmation'),
                'title' => __('Require confirmation'),
                'values' => [
                    ['value' => '1', 'label' => ''],
                ],
                'after_element_html' => '<div class="exto-rma-note">' .
                    __('Should we should the action confirmation dialog?') .
                    '</div>'
            ]
        );

        $fieldset->addField(
            'is_send_email_to_customer',
            'checkboxes',
            [
                'name' => 'is_send_email_to_customer',
                'label' => __('Send email to customer'),
                'title' => __('Send email to customer'),
                'values' => [
                    ['value' => '1', 'label' => ''],
                ],
            ]
        );

        $fieldset->addField(
            'send_email_to_customer_template_id',
            'select',
            [
                'name' => 'send_email_to_customer_template_id',
                'label' => __('Select email template'),
                'title' => __('Select email template'),
                'options' => $this->getEmailTemplateOptions()
            ]
        );

        $fieldset->addField(
            'is_send_email_to_admin',
            'checkboxes',
            [
                'name' => 'is_send_email_to_admin',
                'label' => __('Send email to admin'),
                'title' => __('Send email to admin'),
                'values' => [
                    ['value' => '1', 'label' => ''],
                ],
            ]
        );

        $fieldset->addField(
            'send_email_to_admin_template_id',
            'select',
            [
                'name' => 'send_email_to_admin_template_id',
                'label' => __('Select email template'),
                'title' => __('Select email template'),
                'options' => $this->getEmailTemplateOptions()
            ]
        );

        $fieldset->addField(
            'message',
            'text',
            [
                'name' => 'message',
                'label' => __('Add message'),
                'title' => __('Add message'),
                'after_element_html' => '<div class="exto-rma-note">' .
                    __('This message will be added to the RMA request thread')
                    . '</div>'
            ]
        );

        $fieldset->addField(
            'sort_order',
            'text',
            [
                'name' => 'sort_order',
                'label' => __('Sort Order'),
                'title' => __('Sort Order'),
                'class' => "validate-zero-or-greater validate-number validate-digits"
            ]
        );

        $htmlIdPrefix = $form->getHtmlIdPrefix();
        $fieldset
            ->addField(
                'dependence',
                'note',
                [
                    'name' => 'dependence',
                    'after_element_html' => "
                        <script type='text/javascript'>
                            require(['jquery', 'fieldDependencyManager'], function($){
                                $.fieldDependencyManager.setExcludeValueDependence('#{$htmlIdPrefix}'+"
                        . "'next_status_caption', '#{$htmlIdPrefix}'+'next_status_id', ['0']);
                                $.fieldDependencyManager.setCheckboxDependence('#{$htmlIdPrefix}'+"
                        . "'send_email_to_admin_template_id', '#{$htmlIdPrefix}'+'is_send_email_to_admin_1', ['1']);
                                $.fieldDependencyManager.setCheckboxDependence('#{$htmlIdPrefix}'+"
                        . "'send_email_to_customer_template_id', "
                        . "'#{$htmlIdPrefix}'+'is_send_email_to_customer_1', ['1']);
                            });
                        </script>"
                ]
            );

        $data = array_merge([
            'send_email_to_customer_template_id' => 'exto_rma_email_templates_status_changed',
            'send_email_to_admin_template_id' => 'exto_rma_email_templates_status_changed',
            'is_send_email_to_customer' => 1,
            'is_send_email_to_admin' => 1,
        ], $model->getData());
        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @return array
     */
    protected function getEmailTemplateOptions()
    {
        /** @var \Magento\Email\Model\ResourceModel\Template\Collection $collection */
        $collection = $this->emailTemplatesFactory->create();
        $collection->load();
        $options = $collection->toOptionArray();
        $defaultTemplateId = 'exto_rma_email_templates_status_changed';
        $templateLabel = $this->emailConfig->getTemplateLabel($defaultTemplateId);
        $templateLabel = __('%1 (Default)', $templateLabel);
        array_unshift($options, ['value' => 0, 'label' => $templateLabel]);
        $result = [];
        foreach ($options as $option) {
            $result[$option['value']] = $option['label'];
        }
        return $result;
    }
}
