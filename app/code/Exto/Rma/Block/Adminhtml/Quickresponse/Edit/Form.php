<?php
namespace Exto\Rma\Block\Adminhtml\Quickresponse\Edit;

/**
 * Class Form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /** @var \Exto\Rma\Block\Adminhtml\Renderer\ContentTextarea */
    protected $renderer;

    /** @var int */
    protected $optionCount = 0;

    /** @var \Exto\Rma\Model\Chat\QuickresponseFactory */
    protected $responseFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Chat\Quickresponse */
    protected $responseResource;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Exto\Rma\Block\Adminhtml\Renderer\ContentTextarea $renderer
     * @param \Exto\Rma\Model\Chat\QuickresponseFactory $quickresponseFactory
     * @param \Exto\Rma\Model\ResourceModel\Chat\Quickresponse $quickresponseResource
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Exto\Rma\Block\Adminhtml\Renderer\ContentTextarea $renderer,
        \Exto\Rma\Model\Chat\QuickresponseFactory $quickresponseFactory,
        \Exto\Rma\Model\ResourceModel\Chat\Quickresponse $quickresponseResource,
        array $data = []
    ) {
        $this->renderer = $renderer;
        $this->responseFactory = $quickresponseFactory;
        $this->responseResource = $quickresponseResource;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $responses = $this->_coreRegistry->registry('current_responses');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setUseContainer(true);
        $form->setHtmlIdPrefix('response_');

        if (!count($responses)) {
            $this->optionCount++;
            /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
            $fieldset = $form->addFieldset(
                'option_fieldset_'.$this->optionCount,
                [
                    'legend' => __(''),
                ]
            );

            $fieldset->addField(
                'name_'. $this->optionCount,
                'text',
                [
                    'name' => 'responses['.$this->optionCount.'][name]',
                    'label' => __('Caption'),
                    'title' => __('Caption'),
                    'required' => true
                ]
            );

            $fieldset
                ->addField(
                    'labels_'. $this->optionCount,
                    'text',
                    [
                        'name' => 'responses['.$this->optionCount.'][labels]',
                        'label' => __(''),
                        'title' => __('')
                    ]
                )
                ->setFrontendLabel(__('Text'))
                ->setFieldCode('responses['.$this->optionCount.'][labels]')
                ->setOptionValues(null)
                ->setRenderer($this->renderer)
            ;
        } else {
            foreach ($responses as $response) {
                $this->optionCount = $this->optionCount < $response->getId() ? $response->getId(): $this->optionCount;
                $responseModel = $this->responseFactory->create();
                $this->responseResource->load($responseModel, $response->getId());
                if (!$responseModel || !$responseModel->getId()) {
                    continue;
                }
                /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
                $fieldset = $form->addFieldset(
                    'option_fieldset_'.$responseModel->getId(),
                    [
                        'legend' => __(''),
                    ]
                );

                $fieldset->addField(
                    'id_'.$responseModel->getId(),
                    'hidden',
                    ['name' => 'responses['.$responseModel->getId().'][id]', 'value' => $responseModel->getId()]
                );

                $fieldset->addField(
                    'name_'. $response->getId(),
                    'text',
                    [
                        'name' => 'responses['.$response->getId().'][name]',
                        'label' => __('Caption'),
                        'title' => __('Caption'),
                        'value' => $response->getName(),
                        'required' => true
                    ]
                );

                $fieldset
                    ->addField(
                        'labels_'. $response->getId(),
                        'text',
                        [
                            'name' => 'responses['.$response->getId().'][labels]',
                            'label' => __(''),
                            'title' => __('')
                        ]
                    )

                    ->setFrontendLabel(__('Text'))
                    ->setFieldCode('responses['.$responseModel->getId().'][labels]')
                    ->setOptionValues($responseModel->getLabels())
                    ->setRenderer($this->renderer)
                ;
                $removeButtonHtml = $this->getLayout()->createBlock(
                    \Magento\Backend\Block\Template::class
                )
                    ->setTemplate('Exto_Rma::renderer/option/remove.phtml')
                    ->toHtml();

                $fieldset->addField(
                    'remove_option_'.$responseModel->getId(),
                    'label',
                    [
                        'name' => 'remove_option_'.$responseModel->getId(),
                        'label' => __(''),
                        'title' => __(''),
                        'after_element_html' => $removeButtonHtml
                    ]
                );
            }

        }

        /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
        $fieldset = $form->addFieldset(
            'add_option_fieldset',
            [
                'legend' => __(''),
            ]
        );

        $stores = $this->_storeManager->getStores();
        $addOptionHtml = $this->getLayout()->createBlock(
            \Magento\Backend\Block\Template::class
        )
            ->setTemplate('Exto_Rma::renderer/option/quickresponse/add.phtml')
            ->setOptionCount($this->optionCount)
            ->setFormCode('response')
            ->setFieldName('responses')
            ->setStores($stores)
            ->setFieldContentName('responses[replacement_id][labels]')
            ->toHtml();

        $fieldset->addField(
            'add_option',
            'label',
            [
                'name' => 'add_option',
                'label' => __(''),
                'title' => __(''),
                'after_element_html' => $addOptionHtml
            ]
        );
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
