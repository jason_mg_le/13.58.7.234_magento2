<?php
namespace Exto\Rma\Block\Adminhtml\Field\Edit;

/**
 * Class Form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /** @var \Exto\Rma\Block\Adminhtml\Renderer\ContentLine  */
    protected $renderer;

    /** @var \Exto\Rma\Block\Adminhtml\Renderer\ContentTextarea  */
    protected $rendererTextarea;

    /** @var \Exto\Rma\Model\Source\Field\Type  */
    protected $typeSource;

    /** @var \Exto\Rma\Model\ResourceModel\Status\CollectionFactory  */
    protected $statusCollectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Exto\Rma\Block\Adminhtml\Renderer\ContentLine $renderer
     * @param \Exto\Rma\Block\Adminhtml\Renderer\ContentTextarea $rendererTextarea
     * @param \Exto\Rma\Model\Source\Field\Type $typeSource
     * @param \Exto\Rma\Model\ResourceModel\Status\CollectionFactory $statusCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Exto\Rma\Block\Adminhtml\Renderer\ContentLine $renderer,
        \Exto\Rma\Block\Adminhtml\Renderer\ContentTextarea $rendererTextarea,
        \Exto\Rma\Model\Source\Field\Type $typeSource,
        \Exto\Rma\Model\ResourceModel\Status\CollectionFactory $statusCollectionFactory,
        array $data = []
    ) {
        $this->renderer = $renderer;
        $this->rendererTextarea = $rendererTextarea;
        $this->typeSource = $typeSource;
        $this->statusCollectionFactory = $statusCollectionFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_field');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setUseContainer(true);
        $form->setHtmlIdPrefix('field_');

        /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
        $fieldset = $form->addFieldset(
            'general_fieldset',
            [
                'legend' => __(''),
            ]
        );

        if ($model->getId()) {
            $fieldset->addField('entity_id', 'hidden', ['name' => 'entity_id']);
        }

        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Name'),
                'title' => __('Name'),
                'required' => true
            ]
        );

        $fieldset
            ->addField(
                'labels',
                'text',
                [
                    'name' => 'labels',
                    'label' => __(''),
                    'title' => __('')
                ]
            )
            ->setFrontendLabel(__('Frontend Label'))
            ->setFieldCode('labels')
            ->setOptionValues($model->getLabels())
            ->setAfterElementHtml('<div class="exto-rma-note">' . __('Label for customers') . '</div>')
            ->setRenderer($this->renderer)
        ;

        $fieldset->addField(
            'type_id',
            'select',
            [
                'name' => 'type_id',
                'label' => __('Type'),
                'title' => __('Type'),
                'options' => $this->typeSource->getOptionArray()
            ]
        );

        $fieldset
            ->addField(
                'option_values',
                'text',
                [
                    'name' => 'option_values',
                    'label' => __(''),
                    'title' => __('')
                ]
            )
            ->setFrontendLabel(__('Options'))
            ->setFieldCode('option_values')
            ->setOptionValues($model->getOptionValues())
            ->setAfterElementHtml('<div class="exto-rma-note">' . __('One per line') . '</div>')
            ->setRenderer($this->rendererTextarea)
        ;

        $fieldset->addField(
            'is_required',
            'select',
            [
                'name' => 'is_required',
                'label' => __('Is required'),
                'title' => __('Is required'),
                'options' => ['1' => __('Yes'), '0' => __('No')]
            ]
        );

        $fieldset->addField(
            'is_visible_for_customer',
            'select',
            [
                'name' => 'is_visible_for_customer',
                'label' => __('Visible for customer'),
                'title' => __('Visible for customer'),
                'options' => ['1' => __('Yes'), '0' => __('No')]
            ]
        );
        $statusCollection = $this->statusCollectionFactory->create();
        $afterElementHtml = '<div class="exto-rma-note">'
            . __(
                'Customer can update this field while the request is in selected statuses (uncheck all for Never)'
            ) . '</div>';
        $fieldset->addField(
            'editable_for_customer_in_statuses',
            'multiselect',
            [
                'name' => 'editable_for_customer_in_statuses',
                'label' => __('Editable for customer in statuses'),
                'title' => __('Editable for customer in statuses'),
                'values' => $statusCollection->toOptionArray(),
                'after_element_html' => $afterElementHtml
            ]
        );

        $fieldset->addField(
            'is_add_to_shipping_label',
            'select',
            [
                'name' => 'is_add_to_shipping_label',
                'label' => __('Add to shipping label'),
                'title' => __('Add to shipping label'),
                'options' => ['1' => __('Yes'), '0' => __('No')]
            ]
        );

        $fieldset->addField(
            'sort_order',
            'text',
            [
                'name' => 'sort_order',
                'label' => __('Sort Order'),
                'title' => __('Sort Order'),
                'class' => "validate-zero-or-greater validate-number validate-digits"
            ]
        );

        $htmlIdPrefix = $form->getHtmlIdPrefix();
        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock(
                \Magento\Backend\Block\Widget\Form\Element\Dependence::class
            )->addFieldMap(
                "{$htmlIdPrefix}type_id",
                'type'
            )
            ->addFieldMap(
                "{$htmlIdPrefix}options",
                'options'
            )
            ->addFieldDependence(
                'options',
                'type',
                '1'
            )
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
