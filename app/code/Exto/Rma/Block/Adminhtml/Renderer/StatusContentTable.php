<?php
namespace Exto\Rma\Block\Adminhtml\Renderer;

use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class StatusContentTable
 */
class StatusContentTable implements \Magento\Framework\Data\Form\Element\Renderer\RendererInterface
{
    /** @var string */
    protected $_template = 'Exto_Rma::renderer/content/status_table.phtml';

    /** @var \Magento\Backend\Block\TemplateFactory */
    protected $blockFactory;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /**
     * @param \Magento\Backend\Block\TemplateFactory $blockFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Backend\Block\TemplateFactory $blockFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $allStores = $this->storeManager->getStores();
        $tableRows = $this->prepareTableRows($element->getColumnValues());
        return $this->blockFactory
            ->create()
            ->setStores($allStores)
            ->setColumnsHeader(
                [
                    '', __('Corresponding customer action'), __('Confirmation text')
                ]
            )
            ->setTableRows($tableRows)
            ->setAfterElementHtml($element->getAfterElementHtml())
            ->setTemplate($this->_template)
            ->toHtml();
    }

    /**
     * @param array $columnValues
     *
     * @return array
     */
    private function prepareTableRows($columnValues)
    {
        $allStores = $this->storeManager->getStores();
        $tableRowsHtml = [];

        foreach ($allStores as $store) {
            $tableRow = '<tr>';
            if (!isset($columnValues[$store->getId()])) {
                //store name
                $tableRow .= "<td class='admin__field-label'>{$store->getName()}</td>";
                //Corresponding customer action ( text )
                $tableRow .= "<td><input type='text' class='corresponding_customer_action' "
                    . "name='corresponding_customer_action[{$store->getId()}]' value=''/></td>";

                //Confirmation text ( text area )
                $tableRow .= "<td><textarea class='confirmation_text' name='confirmation_text[{$store->getId()}]'>";
                $tableRow .= "</textarea></td>";

            } else {
                $rowValues = $columnValues[$store->getId()];

                //store name
                $tableRow .= "<td class='admin__field-label'>{$store->getName()}</td>";

                //Corresponding customer action ( text )
                $tableRow .= "<td><input type='text' class='corresponding_customer_action' "
                    . "name='corresponding_customer_action[{$store->getId()}]' value='";
                if (array_key_exists('corresponding_customer_action', $rowValues)) {
                    $tableRow .= $rowValues['corresponding_customer_action'];
                }
                $tableRow .= "'/></td>";

                //Confirmation text ( text area )
                $tableRow .= "<td><textarea class='confirmation_text' name='confirmation_text[{$store->getId()}]'>";
                $tableRow .= array_key_exists('confirmation_text', $rowValues)?$rowValues['confirmation_text']:'';
                $tableRow .= "</textarea></td>";
            }
            $tableRow .= '</tr>';
            $tableRowsHtml[] = $tableRow;
        }

        return $tableRowsHtml;
    }
}
