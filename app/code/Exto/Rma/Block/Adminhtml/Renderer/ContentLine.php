<?php

namespace Exto\Rma\Block\Adminhtml\Renderer;

use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class ContentLine
 */
class ContentLine implements \Magento\Framework\Data\Form\Element\Renderer\RendererInterface
{
    /** @var string */
    protected $_template = 'Exto_Rma::renderer/content/line.phtml';

    /** @var \Magento\Backend\Block\TemplateFactory */
    protected $blockFactory;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /**
     * @param \Magento\Backend\Block\TemplateFactory $blockFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Backend\Block\TemplateFactory $blockFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $allStores = $this->storeManager->getStores();
        return $this->blockFactory
            ->create()
            ->setStores($allStores)
            ->setFieldCode($element->getFieldCode())
            ->setFrontendLabel($element->getFrontendLabel())
            ->setOptionValues($element->getOptionValues())
            ->setAfterElementHtml($element->getAfterElementHtml())
            ->setTemplate($this->_template)
            ->toHtml();
    }
}
