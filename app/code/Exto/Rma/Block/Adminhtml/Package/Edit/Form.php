<?php
namespace Exto\Rma\Block\Adminhtml\Package\Edit;

/**
 * Class Form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /** @var \Exto\Rma\Block\Adminhtml\Renderer\ContentLine */
    protected $renderer;

    /** @var int */
    protected $optionCount = 0;

    /** @var \Exto\Rma\Model\PackageFactory */
    protected $conditionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Package */
    protected $conditionResource;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Exto\Rma\Block\Adminhtml\Renderer\ContentLine $renderer
     * @param \Exto\Rma\Model\PackageFactory $conditionFactory
     * @param \Exto\Rma\Model\ResourceModel\Package $conditionResource
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Exto\Rma\Block\Adminhtml\Renderer\ContentLine $renderer,
        \Exto\Rma\Model\PackageFactory $conditionFactory,
        \Exto\Rma\Model\ResourceModel\Package $conditionResource,
        array $data = []
    ) {
        $this->renderer = $renderer;
        $this->conditionFactory = $conditionFactory;
        $this->conditionResource = $conditionResource;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $conditions = $this->_coreRegistry->registry('current_conditions');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setUseContainer(true);
        $form->setHtmlIdPrefix('condition_');

        if (!count($conditions)) {
            $this->optionCount++;
            /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
            $fieldset = $form->addFieldset(
                'option_fieldset_'.$this->optionCount,
                [
                    'legend' => __(''),
                ]
            );

            $fieldset->addField(
                'name_'. $this->optionCount,
                'text',
                [
                    'name' => 'conditions['.$this->optionCount.'][name]',
                    'label' => __('Name'),
                    'title' => __('Name'),
                    'required' => true
                ]
            );

            $fieldset
                ->addField(
                    'labels_'. $this->optionCount,
                    'text',
                    [
                        'name' => 'conditions['.$this->optionCount.'][labels]',
                        'label' => __(''),
                        'title' => __('')
                    ]
                )
                ->setFrontendLabel(__('Title'))
                ->setFieldCode('conditions['.$this->optionCount.'][labels]')
                ->setOptionValues(null)
                ->setRenderer($this->renderer)
            ;

            $fieldset->addField(
                'sort_order_'. $this->optionCount,
                'text',
                [
                    'name' => 'conditions['.$this->optionCount.'][sort_order]',
                    'label' => __('Sort Order'),
                    'title' => __('Sort Order'),
                    'class' => "validate-zero-or-greater validate-number validate-digits",
                ]
            );
        } else {
            foreach ($conditions as $condition) {
                $this->optionCount = $this->optionCount < $condition->getId() ? $condition->getId(): $this->optionCount;
                $conditionModel = $this->conditionFactory->create();
                $this->conditionResource->load($conditionModel, $condition->getId());
                if (!$conditionModel || !$conditionModel->getId()) {
                    continue;
                }
                /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
                $fieldset = $form->addFieldset(
                    'option_fieldset_'.$conditionModel->getId(),
                    [
                        'legend' => __(''),
                    ]
                );

                $fieldset->addField(
                    'id_'.$conditionModel->getId(),
                    'hidden',
                    ['name' => 'conditions['.$conditionModel->getId().'][id]', 'value' => $conditionModel->getId()]
                );

                $fieldset->addField(
                    'name_'. $conditionModel->getId(),
                    'text',
                    [
                        'name' => 'conditions['.$conditionModel->getId().'][name]',
                        'label' => __('Name'),
                        'title' => __('Name'),
                        'value' => $conditionModel->getName(),
                        'required' => true
                    ]
                );

                $fieldset
                    ->addField(
                        'labels_'. $conditionModel->getId(),
                        'text',
                        [
                            'name' => 'conditions['.$conditionModel->getId().'][labels]',
                            'label' => __(''),
                            'title' => __('')
                        ]
                    )
                    ->setFrontendLabel(__('Title'))
                    ->setFieldCode('conditions['.$conditionModel->getId().'][labels]')
                    ->setOptionValues($conditionModel->getLabels())
                    ->setRenderer($this->renderer)
                ;
                $fieldset->addField(
                    'sort_order_'. $conditionModel->getId(),
                    'text',
                    [
                        'name' => 'conditions['.$conditionModel->getId().'][sort_order]',
                        'label' => __('Sort Order'),
                        'title' => __('Sort Order'),
                        'class' => "validate-zero-or-greater validate-number validate-digits",
                        'value' => $conditionModel->getSortOrder(),
                    ]
                );

                $removeButtonHtml = $this->getLayout()->createBlock(
                    \Magento\Backend\Block\Template::class
                )
                    ->setTemplate('Exto_Rma::renderer/option/remove.phtml')
                    ->toHtml();

                $fieldset->addField(
                    'remove_option_'.$conditionModel->getId(),
                    'label',
                    [
                        'name' => 'remove_option_'.$conditionModel->getId(),
                        'label' => __(''),
                        'title' => __(''),
                        'after_element_html' => $removeButtonHtml
                    ]
                );
            }
        }

        /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
        $fieldset = $form->addFieldset(
            'add_option_fieldset',
            [
                'legend' => __(''),
            ]
        );

        $stores = $this->_storeManager->getStores();
        $addOptionHtml = $this->getLayout()->createBlock(
            \Magento\Backend\Block\Template::class
        )
            ->setTemplate('Exto_Rma::renderer/option/add.phtml')
            ->setOptionCount($this->optionCount)
            ->setFormCode('condition')
            ->setFieldName('conditions')
            ->setStores($stores)
            ->setFieldContentName('conditions[replacement_id][labels]')
            ->toHtml();

        $fieldset->addField(
            'add_option',
            'label',
            [
                'name' => 'add_option',
                'label' => __(''),
                'title' => __(''),
                'after_element_html' => $addOptionHtml
            ]
        );
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
