<?php
namespace Exto\Rma\Block\Adminhtml\Package;

/**
 * Class Edit
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Exto_Rma';
        $this->_controller = 'adminhtml_package';
        $this->_mode = 'edit';

        parent::_construct();
        $this->buttonList->remove('delete');
        $this->buttonList->remove('reset');
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('exto_rma/option/index');
    }
}
