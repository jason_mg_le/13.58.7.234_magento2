<?php
namespace Exto\Rma\Block\Adminhtml\Request;

/**
 * Class Edit
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /** @var string */
    protected $_template = 'request/edit.phtml';

    /** @var \Magento\Framework\Url */
    protected $frontendUrlBuilder;

    /** @var \Exto\Rma\Model\RequestFactory */
    protected $requestFactory;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Url                $frontendUrlBuilder
     * @param \Exto\Rma\Model\RequestFactory        $requestFactory
     * @param array                                 $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Url $frontendUrlBuilder,
        \Exto\Rma\Model\RequestFactory $requestFactory,
        array $data = []
    ) {
        $this->frontendUrlBuilder = clone $frontendUrlBuilder;
        $this->requestFactory = $requestFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Exto_Rma';
        $this->_controller = 'adminhtml_request';
        $this->_mode = 'edit';
        parent::_construct();
        $this->buttonList->remove('reset');
        $this->buttonList->remove('save');
        $this->buttonList->update('delete', 'label', __('Delete Request'));
        $this->buttonList->add('exchange', [
            'label' => __('Exchange'),
            'onclick' => 'setLocation(\'' . $this->getExchangeUrl() . '\')',
            'class' => 'save'
        ]);
        $this->buttonList->add('print', [
            'label' => __('Print'),
            'onclick' => 'window.open("' . $this->getPrintUrl() . '", "_blank").focus()',
            'class' => 'save'
        ]);
    }

    /**
     * @return string
     */
    protected function getExchangeUrl()
    {
        return $this->getUrl('exto_rma/request/exchange', [
            'id' => $this->getRequest()->getParam('id')
        ]);
    }

    /**
     * @return string
     */
    protected function getPrintUrl()
    {
        return $this->frontendUrlBuilder->getUrl(
            'exto_rma/guest/printAction',
            [
                'id' => $this->getRequest()->getParam('id'),
                'key' => $this->getRequestModel()->generateGuestKey()
            ]
        );
    }

    /**
     * @return \Exto\Rma\Model\Request
     */
    protected function getRequestModel()
    {
        /** @var \Exto\Rma\Model\Request $request */
        $request = $this->requestFactory->create();
        $request->load(
            $this->getRequest()->getParam('id')
        );
        return $request;
    }
}
