<?php
namespace Exto\Rma\Block\Adminhtml\Request\Create\Tab;

use \Magento\Backend\Block\Widget\Tab\TabInterface;

/**
 * Class Main
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements TabInterface
{
    /** @var \Magento\User\Model\ResourceModel\User\CollectionFactory */
    protected $adminUserCollectionFactory;

    /** @var \Magento\Sales\Model\OrderFactory */
    protected $orderFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Status\CollectionFactory */
    protected $statusCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory */
    protected $resolutionCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory */
    protected $reasonCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Package\CollectionFactory */
    protected $packageConditionCollectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context                    $context
     * @param \Magento\Framework\Registry                                $registry
     * @param \Magento\Framework\Data\FormFactory                        $formFactory
     * @param \Magento\User\Model\ResourceModel\User\CollectionFactory   $adminUserCollectionFactory
     * @param \Magento\Sales\Model\OrderFactory                          $orderFactory
     * @param \Exto\Rma\Model\ResourceModel\Status\CollectionFactory     $statusCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory     $reasonCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory $resolutionCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Package\CollectionFactory    $packageConditionCollectionFactory
     * @param array                                                      $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\User\Model\ResourceModel\User\CollectionFactory $adminUserCollectionFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Exto\Rma\Model\ResourceModel\Status\CollectionFactory $statusCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory $reasonCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory $resolutionCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Package\CollectionFactory $packageConditionCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->adminUserCollectionFactory = $adminUserCollectionFactory;
        $this->orderFactory = $orderFactory;
        $this->statusCollectionFactory = $statusCollectionFactory;
        $this->reasonCollectionFactory = $reasonCollectionFactory;
        $this->resolutionCollectionFactory = $resolutionCollectionFactory;
        $this->packageConditionCollectionFactory = $packageConditionCollectionFactory;
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('request_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('General')]);
        $orderModel = $this->orderFactory->create();
        $orderModel->load($this->_request->getParam('order_id', 0));
        $fieldset->addField(
            'order_id',
            'hidden',
            [
                'name' => 'general[order_id]',
                'value' => $orderModel->getId()
            ]
        );

        $fieldset->addField(
            'order',
            'label',
            [
                'label' => __('Order #'),
                'value' => $orderModel->getIncrementId()
            ]
        );

        $dateFormat = $this->_localeDate->getDateFormat(
            \IntlDateFormatter::SHORT
        );
        $timeFormat = $this->_localeDate->getTimeFormat(
            \IntlDateFormatter::SHORT
        );
        $fieldset->addField(
            'created_at',
            'date',
            [
                'name'     => 'general[created_at]',
                'label'    => __('Created At'),
                'title'    => __('Created At'),
                'date_format' => $dateFormat,
                'time_format' => $timeFormat,
            ]
        );
        $options = [];
        $adminUserCollection = $this->adminUserCollectionFactory->create();
        foreach ($adminUserCollection as $adminUser) {
            $options[] = [
                'value' => $adminUser->getId(),
                'label' => $adminUser->getFirstName() . ' ' . $adminUser->getLastName()
            ];
        }
        $fieldset->addField(
            'owner_id',
            'select',
            [
                'name' => 'general[owner_id]',
                'label' => __('RMA owner'),
                'values' => $options,
            ]
        );
        $statusCollection = $this->statusCollectionFactory->create();
        $statusCollection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        $fieldset->addField(
            'status_id',
            'select',
            [
                'name' => 'general[status_id]',
                'label' => __('Status'),
                'values' => $statusCollection->toOptionArray(),
            ]
        );
        $reasonCollection = $this->reasonCollectionFactory->create();
        $reasonCollection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        $fieldset->addField(
            'reason_id',
            'select',
            [
                'name' => 'general[reason_id]',
                'label' => __('Reason'),
                'values' => $reasonCollection->toOptionArray(),
            ]
        );
        $packageConditionCollection = $this->packageConditionCollectionFactory->create();
        $packageConditionCollection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        $fieldset->addField(
            'package_condition_id',
            'select',
            [
                'name' => 'general[package_condition_id]',
                'label' => __('Condition'),
                'values' => $packageConditionCollection->toOptionArray(),
            ]
        );
        $resolutionCollection = $this->resolutionCollectionFactory->create();
        $resolutionCollection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        $fieldset->addField(
            'resolution_id',
            'select',
            [
                'name' => 'general[resolution_id]',
                'label' => __('Resolution'),
                'values' => $resolutionCollection->toOptionArray(),
            ]
        );

        $data = $this->_backendSession->getExtoRmaReuqestDataForm();
        if ($data && array_key_exists('general', $data)) {
            $form->addValues($data['general']);
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('General');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('General');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
