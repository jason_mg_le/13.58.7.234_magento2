<?php
namespace Exto\Rma\Block\Adminhtml\Request\Create\Tab;

use \Magento\Backend\Block\Widget\Tab\TabInterface;

/**
 * Class Additional
 */
class Additional extends \Magento\Backend\Block\Widget\Form\Generic implements TabInterface
{
    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory */
    protected $customFieldCollectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context                      $context
     * @param \Magento\Framework\Registry                                  $registry
     * @param \Magento\Framework\Data\FormFactory                          $formFactory
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory $customFieldCollectionFactory
     * @param array                                                        $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory $customFieldCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->customFieldCollectionFactory = $customFieldCollectionFactory;
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $stores = $this->_storeManager->getStores();
        $firstStore = array_shift($stores);
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('request_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Additional Information')]);
        $customFieldCollection = $this->customFieldCollectionFactory->create();
        $customFieldCollection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        foreach ($customFieldCollection as $customField) {
            $fieldset->addField(
                'custom_field_' . $customField->getId(),
                $this->getFieldType($customField->getTypeId()),
                [
                    'name' => 'custom_field[' . $customField->getId() . ']',
                    'label' => $customField->getName(),
                    'title' => $customField->getName(),
                    'required' => !!$customField->getIsRequired(),
                    'date_format' => $this->_localeDate->getDateFormat(
                        \IntlDateFormatter::SHORT
                    ),
                    'options' => array_merge(
                        ["" => __('--- Please Select ---')],
                        $customField->getOptions($firstStore->getId())
                    )
                ]
            );
            if ($this->getFieldType($customField->getTypeId())==='checkbox') {
                $element = $fieldset->getElements()->searchById('custom_field_' . $customField->getId());
                $element->setValue('1');
            }
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Additional Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Additional Information');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @param int $typeId
     *
     * @return string
     */
    protected function getFieldType($typeId)
    {
        switch ($typeId) {
            case \Exto\Rma\Model\Source\Field\Type::CHECKBOX_VALUE:
                return 'checkbox';
            case \Exto\Rma\Model\Source\Field\Type::DROPDOWN_VALUE:
                return 'select';
            case \Exto\Rma\Model\Source\Field\Type::TEXT_VALUE:
                return 'text';
            case \Exto\Rma\Model\Source\Field\Type::TEXTAREA_VALUE:
                return 'textarea';
            case \Exto\Rma\Model\Source\Field\Type::DATE_VALUE:
                return 'date';
        }
        return 'text';
    }
}
