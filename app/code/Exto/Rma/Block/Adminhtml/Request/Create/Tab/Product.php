<?php
namespace Exto\Rma\Block\Adminhtml\Request\Create\Tab;

use \Magento\Backend\Block\Widget\Tab\TabInterface;

/**
 * Class Product
 */
class Product extends \Magento\Backend\Block\Template implements TabInterface
{
    /** @var \Magento\Sales\Model\Order */
    protected $orderModel;

    /** @var \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory */
    protected $requestItemCollectionFactory;

    /** @var string */
    protected $_template = 'request/create/order_item.phtml';

    /**
     * @param \Magento\Backend\Block\Template\Context                      $context
     * @param \Magento\Sales\Model\OrderFactory                            $orderModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $requestItemCollectionFactory
     * @param array                                                        $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Sales\Model\OrderFactory $orderModelFactory,
        \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $requestItemCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->requestItemCollectionFactory = $requestItemCollectionFactory;
        $this->orderModel = $orderModelFactory->create();
        $this->orderModel->load($this->_request->getParam('order_id', 0));
    }

    /**
     * @return array
     */
    public function getOrderItems()
    {
        $data = $this->_backendSession->getExtoRmaReuqestDataForm();
        $options = [];
        foreach ($this->orderModel->getItemsCollection() as $item) {
            $qtyValue = $this->getAvailableQty($item);
            $isSelected = $qtyValue > 0;
            if (null !== $data && array_key_exists('order_items', $data)) {
                $qtyValue = $data['order_items'][$item->getId()]['qty'];
                $isSelected = !!$data['order_items'][$item->getId()]['selected'];
            }

            /** @var  \Magento\Sales\Model\Order\Item $item */
            $options[] = [
                'id' => $item->getId(),
                'product_name' => $item->getProduct()->getName(),
                'product_url' => $this->getUrl('catalog/product/edit', ['id' => $item->getProductId()]),
                'qty' => intval($item->getQtyInvoiced()),
                'qty_value' => $qtyValue,
                'is_selected' => $isSelected
            ];
        }
        return $options;
    }

    /**
     * @param \Magento\Sales\Model\Order\Item $item
     *
     * @return int
     */
    protected function getAvailableQty($item)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Request\Item\Collection $requestItemCollection */
        $requestItemCollection = $this->requestItemCollectionFactory->create();
        $requestItemCollection->addFieldToFilter('order_item_id', ['eq' => $item->getId()]);
        $processedQty = 0;
        foreach ($requestItemCollection as $requestItem) {
            /** @var \Exto\Rma\Model\Request\Item $requestItem */
            $processedQty += $requestItem->getQty();
        }
        $availableQty = (int)($item->getQtyOrdered()) - $processedQty;
        return max(0, $availableQty);
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Order Items');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Order Items');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
