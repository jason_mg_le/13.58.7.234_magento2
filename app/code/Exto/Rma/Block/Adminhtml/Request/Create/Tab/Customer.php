<?php
namespace Exto\Rma\Block\Adminhtml\Request\Create\Tab;

use \Magento\Backend\Block\Widget\Tab\TabInterface;

/**
 * Class Customer
 */
class Customer extends \Magento\Backend\Block\Widget\Form\Generic implements TabInterface
{
    /** @var \Magento\Sales\Model\Order */
    protected $orderModel;

    /** @var  \Magento\Customer\Model\GroupFactory */
    protected $customerGroupFactory;

    /** @var  \Magento\Customer\Model\CustomerFactory */
    protected $customerFactory;

    /** @var  \Magento\Directory\Model\CountryFactory */
    protected $countryModelFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\GroupFactory $customerGroupFactory,
        \Magento\Directory\Model\CountryFactory $countryModelFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->orderModel = $orderFactory->create();
        $this->orderModel->load($this->_request->getParam('order_id', 0));
        $this->customerModel = $customerFactory->create();
        $this->customerModel->load($this->orderModel->getCustomerId());
        $this->customerGroupFactory = $customerGroupFactory;
        $this->countryModelFactory = $countryModelFactory;
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('request_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Customer Information')]);
        $fieldset->addField(
            'customer_name',
            'label',
            [
                'name' => 'customer_name',
                'label' => __('Customer Name'),
                'value' => $this->orderModel->getCustomerName()
            ]
        );
        $fieldset->addField(
            'customer_email',
            'label',
            [
                'name' => 'customer_email',
                'label' => __('Customer Email'),
                'value' => $this->orderModel->getCustomerEmail()
            ]
        );
        if (null === $this->customerModel->getId()) {
            $customerGroup = $this->customerGroupFactory->create();
            $customerGroup->load($this->customerModel->getGroupId());
            $fieldset->addField(
                'customer_group',
                'label',
                [
                    'name' => 'customer_group',
                    'label' => __('Customer Group'),
                    'value' => $customerGroup->getCode()
                ]
            );

            $fieldset->addField(
                'customer_signed_up',
                'label',
                [
                    'name' => 'customer_signed_up',
                    'label' => __('Signed up at'),
                    'value' => $this->formatDate($this->customerModel->getCreatedAt())
                ]
            );
        }
        $billingAddress = $this->customerModel->getDefaultBillingAddress();
        if (!$billingAddress || null === $billingAddress->getId()) {
            $billingAddress = $this->orderModel->getBillingAddress();
        }
        $fieldset->addField(
            'billing_name',
            'label',
            [
                'name' => 'billing_name',
                'label' => __('Billing Name'),
                'value' => $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname()
            ]
        );
        $fieldset->addField(
            'billing_street',
            'label',
            [
                'name' => 'billing_street',
                'label' => __('Billing Street'),
                'value' => $billingAddress->getStreetFull()
            ]
        );
        $fieldset->addField(
            'billing_city',
            'label',
            [
                'name' => 'billing_city',
                'label' => __('Billing City'),
                'value' => $billingAddress->getCity()
            ]
        );
        $fieldset->addField(
            'billing_region',
            'label',
            [
                'name' => 'billing_region',
                'label' => __('Billing Region'),
                'value' => $billingAddress->getRegion()
            ]
        );
        $fieldset->addField(
            'billing_postcode',
            'label',
            [
                'name' => 'billing_postcode',
                'label' => __('Billing Postcode'),
                'value' => $billingAddress->getPostcode()
            ]
        );
        $country = $this->countryModelFactory->create();
        $country->load($billingAddress->getCountryId());
        $fieldset->addField(
            'billing_country',
            'label',
            [
                'name' => 'billing_country',
                'label' => __('Billing Country'),
                'value' => $country->getName()
            ]
        );
        $fieldset->addField(
            'billing_phone',
            'label',
            [
                'name' => 'billing_phone',
                'label' => __('Billing Phone'),
                'value' => $billingAddress->getTelephone()
            ]
        );
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Customer Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Customer Information');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
