<?php
namespace Exto\Rma\Block\Adminhtml\Request\Create;

/**
 * Class Tabs
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('request_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Request Information'));
    }
}
