<?php
namespace Exto\Rma\Block\Adminhtml\Request\Edit;

/**
 * Class Chat
 */
class Chat extends \Magento\Backend\Block\Template
{
    /** @var string */
    protected $_template = 'request/edit/chat.phtml';

    /** @var \Exto\Rma\Model\ResourceModel\Chat\Message\CollectionFactory */
    protected $messageCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\CollectionFactory */
    protected $quickResponseCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content\CollectionFactory */
    protected $contentCollectionFactory;

    /** @var \Magento\Sales\Model\OrderFactory */
    protected $orderFactory;

    /** @var \Magento\Framework\Registry|null */
    protected $coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Template\Context                                    $context
     * @param \Magento\Framework\Registry                                                $coreRegistry
     * @param \Exto\Rma\Model\ResourceModel\Chat\Message\CollectionFactory               $messageCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\CollectionFactory         $quickResponseCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content\CollectionFactory $contentCollectionFactory
     * @param \Magento\Sales\Model\OrderFactory                                          $orderFactory
     * @param array                                                                      $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Exto\Rma\Model\ResourceModel\Chat\Message\CollectionFactory $messageCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\CollectionFactory $quickResponseCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content\CollectionFactory $contentCollectionFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->messageCollectionFactory = $messageCollectionFactory;
        $this->quickResponseCollectionFactory = $quickResponseCollectionFactory;
        $this->coreRegistry = $coreRegistry;
        $this->contentCollectionFactory = $contentCollectionFactory;
        $this->orderFactory = $orderFactory;
    }

    /**
     * @return \Exto\Rma\Model\Request
     */
    public function getRequest()
    {
        return $this->coreRegistry->registry('current_request');
    }

    /**
     * @return string
     */
    public function getActionUrl()
    {
        return $this->getUrl('exto_rma/request/postMessage', ['id' => $this->getRequest()->getId()]);
    }

    /**
     * @return array
     */
    public function getQuickResponseList()
    {
        $options = [
            ['value' => 0, 'label' => __('Select Quick Response')]
        ];
        $quickResponseCollection = $this->quickResponseCollectionFactory->create();
        return array_merge(
            $options,
            $quickResponseCollection->toOptionArray()
        );
    }

    /**
     * @return \Exto\Rma\Model\ResourceModel\Chat\Message\Collection
     */
    public function getMessagesList()
    {
        /** @var \Exto\Rma\Model\ResourceModel\Chat\Message\Collection $collection */
        $collection = $this->messageCollectionFactory->create();
        $collection
            ->addRequestIdFilter($this->getRequest()->getId())
            ->addNotInternalFilter()
            ->setOrder('entity_id')
        ;
        return $collection;
    }

    /**
     * @param \Exto\Rma\Model\Chat\Attachment $attachment
     *
     * @return string
     */
    public function getAttachmentUrl($attachment)
    {
        return $this->getUrl('exto_rma/request/getAttachment', ['attachment_id' => $attachment->getId()]);
    }

    /**
     * @return string
     */
    public function getQuickResponseAction()
    {
        return \Zend_Json::encode(
            $this->getUrl('exto_rma/request/getQuickResponse', ['id' => $this->getRequest()->getId()])
        );
    }

    /**
     * @return string
     */
    public function getQuickResponseData()
    {
        $order = $this->orderFactory->create();
        $order->load($this->getRequest()->getOrderId());
        /** @var \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addStoreFilter($order->getStoreId());
        $data = [];
        foreach ($collection as $item) {
            $data[$item->getParentId()] = $item->getContent();
        }
        return \Zend_Json::encode($data);
    }
}
