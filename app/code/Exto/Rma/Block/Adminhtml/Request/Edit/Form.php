<?php
namespace Exto\Rma\Block\Adminhtml\Request\Edit;

use Magento\Backend\Block\Template;

/**
 * Class Form
 */
class Form extends \Magento\Backend\Block\Template
{
    /** @var string */
    protected $_template = 'request/edit/form.phtml';

    /** @var \Magento\Framework\Registry|null */
    protected $coreRegistry = null;

    /** @var \Exto\Rma\Model\ResourceModel\Chat\Message\CollectionFactory */
    protected $messageCollectionFactory;

    /**
     * @param Template\Context                                             $context
     * @param \Magento\Framework\Registry                                  $coreRegistry
     * @param \Exto\Rma\Model\ResourceModel\Chat\Message\CollectionFactory $messageCollectionFactory
     * @param array                                                        $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Exto\Rma\Model\ResourceModel\Chat\Message\CollectionFactory $messageCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $coreRegistry;
        $this->messageCollectionFactory = $messageCollectionFactory;
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        $this->setChild(
            'tabs',
            $this->getLayout()->createBlock(\Exto\Rma\Block\Adminhtml\Request\Tabs::class, 'tabs')
        );
        return parent::_prepareLayout();
    }

    /**
     * @return string
     */
    public function getTabsHtml()
    {
        return $this->getChildHtml('tabs');
    }

    /**
     * @return \Exto\Rma\Model\ResourceModel\Chat\Message\Collection
     */
    public function getMessagesList()
    {
        $requestId = $this->coreRegistry->registry('current_request')->getId();
        /** @var \Exto\Rma\Model\ResourceModel\Chat\Message\Collection $collection */
        $collection = $this->messageCollectionFactory->create();
        $collection
            ->addRequestIdFilter($requestId)
            ->addInternalFilter()
            ->setOrder('entity_id')
        ;
        return $collection;
    }

    /**
     * @return string
     */
    public function getActionUrl()
    {
        $requestId = $this->coreRegistry->registry('current_request')->getId();
        return $this->getUrl('exto_rma/request/postInternal', ['id' => $requestId]);
    }
}
