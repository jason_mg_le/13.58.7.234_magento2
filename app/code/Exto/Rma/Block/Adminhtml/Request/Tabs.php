<?php
namespace Exto\Rma\Block\Adminhtml\Request;

/**
 * Class Tabs
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /** @var string */
    protected $_template = 'Magento_Backend::widget/tabshoriz.phtml';

    /** @var \Magento\Framework\Registry|null */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $jsonEncoder, $authSession, $data);
    }

    /**
     * Initialize Tabs
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('exto_rma_request_info_tabs');
        $this->setDestElementId('request_tab_content');
        $this->setTitle(__('Request Data'));
    }

    /**
     * @return $this
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        $this->addTab(
            'rma',
            [
                'label' => __('RMA'),
                'content' => $this->getLayout()->createBlock(
                    \Exto\Rma\Block\Adminhtml\Request\Tabs\General::class,
                    'exto_rma.request.tabs.general'
                )->toHtml()
            ]
        );
        $this->addTab(
            'customer_info',
            [
                'label' => __('Customer Info'),
                'content' => $this->getLayout()->createBlock(
                    \Exto\Rma\Block\Adminhtml\Request\Tabs\Customer::class,
                    'exto_rma.request.tabs.customer'
                )->toHtml()
            ]
        );
        $this->addTab(
            'additional_info',
            [
                'label' => __('Additional Info'),
                'content' => $this->getLayout()->createBlock(
                    \Exto\Rma\Block\Adminhtml\Request\Tabs\Additional::class,
                    'exto_rma.request.tabs.additional'
                )->toHtml()
            ]
        );
        return parent::_prepareLayout();
    }
}
