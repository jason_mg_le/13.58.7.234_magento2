<?php
namespace Exto\Rma\Block\Adminhtml\Request\Tabs;

/**
 * Class Customer
 */
class Customer extends \Magento\Backend\Block\Template
{
    /** @var string */
    protected $_template = 'request/edit/tabs/customer.phtml';

    /** @var \Magento\Framework\Registry */
    protected $_coreRegistry;

    /** @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory */
    protected $orderCollectionFactory;

    /** @var \Magento\Sales\Model\OrderFactory */
    protected $orderFactory;

    /** @var \Magento\Customer\Model\CustomerFactory */
    protected $customerFactory;

    /** @var \Magento\Customer\Model\GroupFactory */
    protected $customerGroupFactory;

    /** @var \Exto\Rma\Helper\Address */
    protected $addressHelper;

    /** @var \Magento\Sales\Model\Order */
    protected $orderModel = null;

    /** @var \Magento\Customer\Model\Customer */
    protected $customerModel = null;

    /**
     * @param \Magento\Backend\Block\Template\Context                    $context
     * @param \Magento\Framework\Registry                                $coreRegistry
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Sales\Model\OrderFactory                          $orderFactory
     * @param \Magento\Customer\Model\CustomerFactory                    $customerFactory
     * @param \Magento\Customer\Model\GroupFactory                       $customerGroupFactory
     * @param \Exto\Rma\Helper\Address                                   $addressHelper
     * @param array                                                      $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\GroupFactory $customerGroupFactory,
        \Exto\Rma\Helper\Address $addressHelper,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderFactory = $orderFactory;
        $this->customerFactory = $customerFactory;
        $this->customerGroupFactory = $customerGroupFactory;
        $this->addressHelper = $addressHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return \Exto\Rma\Model\Request
     */
    public function getRequest()
    {
        return $this->_coreRegistry->registry('current_request');
    }

    /**
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        if (null === $this->orderModel) {
            $this->orderModel = $this->orderFactory->create();
            $this->orderModel->load($this->getRequest()->getOrderId());
        }
        return $this->orderModel;
    }

    /**
     * @return \Magento\Customer\Model\Customer
     */
    public function getCustomer()
    {
        if (null === $this->customerModel) {
            $this->customerModel = $this->customerFactory->create();
            if ($this->getRequest()->getCustomerId() > 0) {
                $this->customerModel->load($this->getRequest()->getCustomerId());
            }
        }
        return $this->customerModel;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        if (null !== $this->getCustomer()->getId()) {
            return $this->getCustomer()->getFirstname() . ' ' . $this->getCustomer()->getLastname();
        }
        $order = $this->getOrder();
        return $order->getFirstname() . ' ' . $order->getLastname();
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        if (null !== $this->getCustomer()->getId()) {
            return $this->getCustomer()->getEmail();
        }
        return $this->getOrder()->getCustomerEmail();
    }

    /**
     * @return string
     */
    public function getContactInformation()
    {
        return $this->addressHelper->getAddressHtml($this->getCustomer(), $this->getOrder());
    }

    /**
     * @return string
     */
    public function getGroupLabel()
    {
        /** @var \Magento\Customer\Model\Group $customerGroup */
        $customerGroup = $this->customerGroupFactory->create();
        $customerGroup->load($this->getCustomer()->getGroupId());
        return $customerGroup->getCode();
    }

    /**
     * @return string
     */
    public function getRegistrationDate()
    {
        return $this->formatDate($this->getCustomer()->getCreatedAt());
    }

    /**
     * @return array
     */
    public function getAnotherOrderList()
    {
        /** @var \Magento\Sales\Model\ResourceModel\Order\Collection $orderCollection */
        $orderCollection = $this->orderCollectionFactory->create();
        $orderCollection->addFieldToFilter(
            'customer_email',
            ['eq' => $this->getRequest()->getCustomerEmail()]
        );
        $options = [];
        foreach ($orderCollection as $order) {
            /** @var \Magento\Sales\Model\Order $order */
            $options[] = [
                'href' => $this->getUrl('sales/order/view', ['order_id' => $order->getId()]),
                'label' => '#' . $order->getIncrementId()
            ];
        }
        return $options;
    }
}
