<?php
namespace Exto\Rma\Block\Adminhtml\Request\Tabs;

/**
 * Class Additional
 */
class Additional extends \Magento\Backend\Block\Template
{
    /** @var string */
    protected $_template = 'request/edit/tabs/additional.phtml';

    /** @var \Magento\Framework\Registry|null */
    protected $_coreRegistry = null;

    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory */
    protected $customFieldCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory */
    protected $valueCollectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context                            $context
     * @param \Magento\Framework\Registry                                        $coreRegistry
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory       $customFieldCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory $valueCollectionFactory
     * @param array                                                              $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Exto\Rma\Model\ResourceModel\Custom\Field\CollectionFactory $customFieldCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory $valueCollectionFactory,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->customFieldCollectionFactory = $customFieldCollectionFactory;
        $this->valueCollectionFactory = $valueCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return \Exto\Rma\Model\Request
     */
    public function getRequestModel()
    {
        return $this->_coreRegistry->registry('current_request');
    }

    /**
     * @return array
     */
    public function getCustomFieldList()
    {
        $collection = $this->customFieldCollectionFactory->create();
        $collection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        $options = [];
        foreach ($collection as $item) {
            $options[] = [
                'id' => $item->getId(),
                'type_id' => $item->getTypeId(),
                'name' => $item->getLabel($this->_storeManager->getStore()->getId()),
                'is_required' => !!$item->getIsRequired(),
                'options' => $item->getOptions($this->_storeManager->getStore()->getId()),
                'value_as_text' => $this->getFieldValue($item->getId())
            ];
        }
        return $options;
    }

    /**
     * @param array $customField
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getDateInput($customField)
    {
        $calendar = $this->getLayout()->createBlock(
            \Magento\Framework\View\Element\Html\Date::class
        )->setId(
            'custom_field_' . $customField['id']
        )->setName(
            'custom_field[' . $customField['id'] . ']'
        )->setClass(
            'datetime-picker input-text ' . ($customField['is_required']?'required':'')
        )->setImage(
            $this->getViewFileUrl('Magento_Theme::calendar.png')
        )->setDateFormat(
            $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT)
        )->setValue(
            $this->getFieldValue($customField['id'])
        );
        if ($customField['is_required']) {
            $calendar->setExtraParams(
                'data-validate="{required:true}"'
            );
        }
        return $calendar->getHtml();
    }

    /**
     * @param int $fieldId
     *
     * @return string
     */
    protected function getFieldValue($fieldId)
    {
        $valueCollection = $this->valueCollectionFactory->create();
        $valueCollection->addParentFilter($fieldId);
        $valueCollection->addRequestFilter($this->getRequestModel()->getId());
        return $valueCollection->getFirstItem()->getValue();
    }

    /**
     * @return string
     */
    public function getFieldChangeAction()
    {
        return \Zend_Json::encode(
            $this->getUrl('exto_rma/request/fieldChange', ['request_id' => $this->getRequestModel()->getId()])
        );
    }
}
