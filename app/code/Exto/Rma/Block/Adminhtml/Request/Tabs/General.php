<?php
namespace Exto\Rma\Block\Adminhtml\Request\Tabs;

/**
 * Class General
 */
class General extends \Magento\Backend\Block\Template
{
    /** @var string */
    protected $_template = 'request/edit/tabs/general.phtml';

    /** @var \Magento\Framework\Registry|null */
    protected $coreRegistry = null;

    /** @var \Magento\User\Model\ResourceModel\User\CollectionFactory */
    protected $adminUserCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Status\CollectionFactory */
    protected $statusCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory */
    protected $resolutionCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory */
    protected $requestItemCollectionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Request\CollectionFactory */
    protected $requestCollectionFactory;

    /** @var \Exto\Rma\Model\Request */
    protected $requestModel;

    /** @var \Exto\Rma\Model\StatusFactory */
    protected $statusFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context                      $context
     * @param \Magento\Framework\Registry                                  $coreRegistry
     * @param \Magento\User\Model\ResourceModel\User\CollectionFactory     $adminUserCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Status\CollectionFactory       $statusCollectionFactory
     * @param \Exto\Rma\Model\StatusFactory                                $statusFactory
     * @param \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory   $resolutionCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $requestItemCollectionFactory
     * @param \Exto\Rma\Model\ResourceModel\Request\CollectionFactory      $requestCollectionFactory
     * @param \Exto\Rma\Model\RequestFactory                               $requestFactory
     * @param array                                                        $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\User\Model\ResourceModel\User\CollectionFactory $adminUserCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Status\CollectionFactory $statusCollectionFactory,
        \Exto\Rma\Model\StatusFactory $statusFactory,
        \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory $resolutionCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $requestItemCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Request\CollectionFactory $requestCollectionFactory,
        \Exto\Rma\Model\RequestFactory $requestFactory,
        array $data = []
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->statusFactory = $statusFactory;
        $this->adminUserCollectionFactory = $adminUserCollectionFactory;
        $this->statusCollectionFactory = $statusCollectionFactory;
        $this->resolutionCollectionFactory = $resolutionCollectionFactory;
        $this->requestItemCollectionFactory = $requestItemCollectionFactory;
        $this->requestCollectionFactory = $requestCollectionFactory;
        $this->requestModel = $requestFactory->create();
        parent::__construct($context, $data);

        $this->requestModel->load(
            $this->coreRegistry->registry('current_request')->getId()
        );
    }

    /**
     * @return \Exto\Rma\Model\Request
     */
    public function getRequest()
    {
        return $this->requestModel;
    }

    /**
     * @return array
     */
    public function getOwnedOptions()
    {
        $options = [];
        $collection = $this->adminUserCollectionFactory->create();
        foreach ($collection as $adminUser) {
            /** @var \Magento\User\Model\User $adminUser */
            $options[] = [
                'value' => $adminUser->getId(),
                'label' => $adminUser->getFirstName() . ' ' . $adminUser->getLastName()
            ];
        }
        return $options;
    }

    /**
     * @return array
     */
    public function getStatusOptions()
    {
        /** @var \Exto\Rma\Model\ResourceModel\Status\Collection $collection */
        $collection = $this->statusCollectionFactory->create();
        return $collection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC)
            ->load()
            ->toOptionArray()
        ;
    }

    /**
     * @return array
     */
    public function getResolutionOptions()
    {
        $options = [
            ['value' => '0', 'label' => '---']
        ];
        /** @var \Exto\Rma\Model\ResourceModel\Resolution\Collection $collection */
        $collection = $this->resolutionCollectionFactory->create();
        $collection->setOrder('sort_order', \Magento\Framework\Data\Collection::SORT_ORDER_ASC);
        return array_merge(
            $options,
            $collection->toOptionArray()
        );
    }

    /**
     * @return array
     */
    public function getAnotherRmaLinks()
    {
        /** @var \Exto\Rma\Model\ResourceModel\Request\Collection $collection */
        $collection = $this->requestCollectionFactory->create();
        $collection->addOrderFilter($this->getRequest()->getOrderId());
        $collection->addFieldToFilter('entity_id', ['neq' => $this->getRequest()->getId()]);
        $options = [];
        foreach ($collection as $request) {
            /** @var \Exto\Rma\Model\Request $request */
            $options[] = [
                'href' => $this->getUrl('exto_rma/request/edit', ['id' => $request->getId()]),
                'label' => '#' . $request->getIncrementId()
            ];
        }
        return $options;
    }

    /**
     * @return array
     */
    public function getItemOptions()
    {
        /** @var \Exto\Rma\Model\ResourceModel\Request\Item\Collection $collection */
        $collection = $this->requestItemCollectionFactory->create();
        $collection->addRequestFilter($this->getRequest()->getId());
        $collection->joinProductData();

        $options = [];
        foreach ($collection as $item) {
            /** @var \Exto\Rma\Model\Request\Item $item */
            $options[] = [
                'url' => $this->getUrl('catalog/product/edit', ['id' => $item->getProductId()]),
                'name' => $item->getProductName(),
                'sku' => $item->getProductSku(),
                'qty' => intval($item->getQty()),
                'subtotal' => $this->formatCurrency($item->getProductPrice() * $item->getQty())
            ];
        }
        return $options;
    }

    /**
     * @return string
     */
    public function getPackageLabel()
    {
        return $this->getRequest()->getPackageCondition()->getLabel(0, true);
    }

    /**
     * @return string
     */
    public function getReasonLabel()
    {
        return $this->getRequest()->getReason()->getLabel(0, true);
    }

    /**
     * @return string
     */
    public function getOrderUrl()
    {
        return $this->getUrl('sales/order/view', ['order_id' => $this->getRequest()->getOrderId()]);
    }

    /**
     * @return string
     */
    public function getOrderIncrementId()
    {
        return '#' . $this->getRequest()->getOrder()->getIncrementId();
    }

    /**
     * @return \Exto\Rma\Model\Status
     */
    public function getStatus()
    {
        /** @var \Exto\Rma\Model\Status $status */
        $status = $this->statusFactory->create();
        $status->load($this->getRequest()->getStatusId());
        return $status;
    }

    /**
     * @return bool
     */
    public function isOwnerActionExist()
    {
        $nextStatus = $this->statusFactory->create();
        $nextStatus->load($this->getStatus()->getNextStatusId());
        if (null === $nextStatus->getId()) {
            return false;
        }
        if (strlen(trim($this->getOwnerActionLabel())) === 0) {
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getOwnerActionStatusId()
    {
        return $this->getStatus()->getNextStatusId();
    }

    /**
     * @return string
     */
    public function getOwnerActionLabel()
    {
        return $this->getStatus()->getNextStatusCaption();
    }

    /**
     * @param float $price
     * @param null $websiteId
     *
     * @return string
     */
    public function formatCurrency($price, $websiteId = null)
    {
        return $this->_storeManager->getWebsite($websiteId)->getBaseCurrency()->format($price);
    }

    /**
     * @return string
     */
    public function getChangeOwnerAction()
    {
        return \Zend_Json::encode(
            $this->getUrl('exto_rma/request/changeOwner', ['id' => $this->getRequest()->getId()])
        );
    }

    /**
     * @return string
     */
    public function getChangeStatusAction()
    {
        return \Zend_Json::encode(
            $this->getUrl('exto_rma/request/changeStatus', ['id' => $this->getRequest()->getId()])
        );
    }

    /**
     * @return string
     */
    public function getChangeResolutionAction()
    {
        return \Zend_Json::encode(
            $this->getUrl('exto_rma/request/changeResolution', ['id' => $this->getRequest()->getId()])
        );
    }
}
