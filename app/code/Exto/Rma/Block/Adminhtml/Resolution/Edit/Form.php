<?php
namespace Exto\Rma\Block\Adminhtml\Resolution\Edit;

/**
 * Class Form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /** @var \Exto\Rma\Block\Adminhtml\Renderer\ContentLine */
    protected $renderer;

    /** @var int */
    protected $optionCount = 0;

    /** @var \Exto\Rma\Model\ResolutionFactory */
    protected $resolutionFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Resolution */
    protected $resolutionResource;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Exto\Rma\Block\Adminhtml\Renderer\ContentLine $renderer
     * @param \Exto\Rma\Model\ResolutionFactory $resolutionFactory
     * @param \Exto\Rma\Model\ResourceModel\Resolution $resolutionResource
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Exto\Rma\Block\Adminhtml\Renderer\ContentLine $renderer,
        \Exto\Rma\Model\ResolutionFactory $resolutionFactory,
        \Exto\Rma\Model\ResourceModel\Resolution $resolutionResource,
        array $data = []
    ) {
        $this->renderer = $renderer;
        $this->resolutionFactory = $resolutionFactory;
        $this->resolutionResource = $resolutionResource;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $resolutions = $this->_coreRegistry->registry('current_resolutions');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
        $form->setUseContainer(true);
        $form->setHtmlIdPrefix('resolution_');

        if (!count($resolutions)) {
            $this->optionCount++;
            /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
            $fieldset = $form->addFieldset(
                'option_fieldset_'.$this->optionCount,
                [
                    'legend' => __(''),
                ]
            );

            $fieldset->addField(
                'name_'. $this->optionCount,
                'text',
                [
                    'name' => 'resolutions['.$this->optionCount.'][name]',
                    'label' => __('Name'),
                    'title' => __('Name'),
                    'required' => true
                ]
            );

            $fieldset
                ->addField(
                    'labels_'. $this->optionCount,
                    'text',
                    [
                        'name' => 'resolutions['.$this->optionCount.'][labels]',
                        'label' => __(''),
                        'title' => __('')
                    ]
                )
                ->setFrontendLabel(__('Title'))
                ->setFieldCode('resolutions['.$this->optionCount.'][labels]')
                ->setOptionValues(null)
                ->setRenderer($this->renderer)
            ;

            $fieldset->addField(
                'sort_order_'. $this->optionCount,
                'text',
                [
                    'name' => 'resolutions['.$this->optionCount.'][sort_order]',
                    'label' => __('Sort Order'),
                    'title' => __('Sort Order'),
                    'class' => "validate-zero-or-greater validate-number validate-digits",
                ]
            );
        } else {
            foreach ($resolutions as $resolution) {
                $this->optionCount = max($resolution->getId(), $this->optionCount);
                $resolutionModel = $this->resolutionFactory->create();
                $this->resolutionResource->load($resolutionModel, $resolution->getId());
                if (!$resolutionModel || !$resolutionModel->getId()) {
                    continue;
                }
                /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
                $fieldset = $form->addFieldset(
                    'option_fieldset_'.$resolutionModel->getId(),
                    [
                        'legend' => __(''),
                    ]
                );

                $fieldset->addField(
                    'id_'.$resolutionModel->getId(),
                    'hidden',
                    ['name' => 'resolutions['.$resolutionModel->getId().'][id]', 'value' => $resolutionModel->getId()]
                );

                $fieldset->addField(
                    'name_'. $resolutionModel->getId(),
                    'text',
                    [
                        'name' => 'resolutions['.$resolutionModel->getId().'][name]',
                        'label' => __('Name'),
                        'title' => __('Name'),
                        'value' => $resolutionModel->getName(),
                        'required' => true
                    ]
                );

                $fieldset
                    ->addField(
                        'labels_'. $resolutionModel->getId(),
                        'text',
                        [
                            'name' => 'resolutions['.$resolutionModel->getId().'][labels]',
                            'label' => __(''),
                            'title' => __('')
                        ]
                    )
                    ->setFrontendLabel(__('Title'))
                    ->setFieldCode('resolutions['.$resolutionModel->getId().'][labels]')
                    ->setOptionValues($resolutionModel->getLabels())
                    ->setRenderer($this->renderer)
                ;
                $fieldset->addField(
                    'sort_order_'. $resolutionModel->getId(),
                    'text',
                    [
                        'name' => 'resolutions['.$resolutionModel->getId().'][sort_order]',
                        'label' => __('Sort Order'),
                        'title' => __('Sort Order'),
                        'class' => "validate-zero-or-greater validate-number validate-digits",
                        'value' => $resolutionModel->getSortOrder(),
                    ]
                );

                $removeButtonHtml = $this->getLayout()->createBlock(
                    \Magento\Backend\Block\Template::class
                )
                    ->setTemplate('Exto_Rma::renderer/option/remove.phtml')
                    ->toHtml();

                $fieldset->addField(
                    'remove_option_'.$resolutionModel->getId(),
                    'label',
                    [
                        'name' => 'remove_option_'.$resolutionModel->getId(),
                        'label' => __(''),
                        'title' => __(''),
                        'after_element_html' => $removeButtonHtml
                    ]
                );
            }
        }

        /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
        $fieldset = $form->addFieldset(
            'add_option_fieldset',
            [
                'legend' => __(''),
            ]
        );

        $stores = $this->_storeManager->getStores();
        $addOptionHtml = $this->getLayout()->createBlock(
            \Magento\Backend\Block\Template::class
        )
            ->setTemplate('Exto_Rma::renderer/option/add.phtml')
            ->setOptionCount($this->optionCount)
            ->setFormCode('resolution')
            ->setFieldName('resolutions')
            ->setStores($stores)
            ->setFieldContentName('resolutions[replacement_id][labels]')
            ->toHtml();

        $fieldset->addField(
            'add_option',
            'label',
            [
                'name' => 'add_option',
                'label' => __(''),
                'title' => __(''),
                'after_element_html' => $addOptionHtml
            ]
        );
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
