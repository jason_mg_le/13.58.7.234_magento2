<?php
namespace Exto\Rma\Block\Adminhtml\Resolution;

/**
 * Class Edit
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Exto_Rma';
        $this->_controller = 'adminhtml_resolution';
        $this->_mode = 'edit';

        parent::_construct();
        $this->buttonList->remove('reset');
        $this->buttonList->remove('delete');
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('exto_rma/option/index');
    }
}
