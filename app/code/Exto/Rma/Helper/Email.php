<?php
namespace Exto\Rma\Helper;

use Magento\Backend\Model\UrlInterface as BackendUrlInterface;

/**
 * Class Email
 */
class Email
{
    const TEMPLATE_MESSAGE_TO_ADMIN = 'exto_rma_email_templates_from_admin';
    const TEMPLATE_MESSAGE_TO_CUSTOMER = 'exto_rma_email_templates_from_customer';
    const TEMPLATE_STATUS_CHANGED = 'exto_rma_email_templates_status_changed';
    const TEMPLATE_NEW_REQUEST= 'exto_rma_email_templates_new_request';

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    protected $scopeConfig;

    /** @var \Magento\Framework\Mail\Template\TransportBuilder */
    protected $transportBuilder;

    /** @var \Exto\Rma\Model\RequestFactory */
    protected $requestFactory;

    /** @var \Magento\Sales\Model\OrderFactory */
    protected $orderFactory;

    /** @var \Exto\Rma\Model\StatusFactory */
    protected $statusFactory;

    /** @var \Exto\Rma\Model\ResolutionFactory */
    protected $resolutionFactory;

    /** @var \Magento\Framework\Url */
    protected $frontendUrlBuilder;

    /** @var \Magento\User\Model\UserFactory */
    protected $adminUserFactory;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Exto\Rma\Model\RequestFactory $requestFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Exto\Rma\Model\StatusFactory $statusFactory
     * @param \Exto\Rma\Model\ResolutionFactory $resolutionFactory
     * @param \Magento\Framework\Url $frontendUrlBuilder
     * @param BackendUrlInterface $backendUrlBuilder
     * @param \Magento\User\Model\UserFactory $adminUserFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Exto\Rma\Model\RequestFactory $requestFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Exto\Rma\Model\StatusFactory $statusFactory,
        \Exto\Rma\Model\ResolutionFactory $resolutionFactory,
        \Magento\Framework\Url $frontendUrlBuilder,
        BackendUrlInterface $backendUrlBuilder,
        \Magento\User\Model\UserFactory $adminUserFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->transportBuilder = $transportBuilder;
        $this->requestFactory = $requestFactory;
        $this->orderFactory = $orderFactory;
        $this->statusFactory = $statusFactory;
        $this->resolutionFactory = $resolutionFactory;
        $this->frontendUrlBuilder = $frontendUrlBuilder;
        $this->backendUrlBuilder = $backendUrlBuilder;
        $this->adminUserFactory = $adminUserFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @param \Exto\Rma\Model\Chat\Message $message
     * @param null|int $storeId
     *
     * @return $this
     */
    public function messageFromAdmin($message, $storeId = null)
    {
        $request = $this->getRequestFromMessage($message);
        $order = $this->getOrderFromRequest($request);
        $status = $this->getStatusLabelFromRequest($request, $storeId);
        $resolution = $this->getResolutionLabelFromRequest($request, $storeId);
        $template = $this->scopeConfig->getValue(
            'exto_rma/email_templates/from_admin',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $this->send(
            $template,
            $this->getSender($storeId),
            $request->getCustomerEmail(),
            $this->getCustomerName($request),
            $storeId,
            [
                'rma' => $request,
                'order' => $order,
                'status' => $status,
                'resolution' => $resolution,
                'message' => $message->getContent(),
            ]
        );
        return $this;
    }

    /**
     * @param \Exto\Rma\Model\Chat\Message $message
     * @param null|int $storeId
     *
     * @return $this
     */
    public function messageFromCustomer($message, $storeId = null)
    {
        $request = $this->getRequestFromMessage($message);
        $order = $this->getOrderFromRequest($request);
        $status = $this->getStatusLabelFromRequest($request, $storeId);
        $resolution = $this->getResolutionLabelFromRequest($request, $storeId);
        $viewUrl = $this->getViewUrl($request);
        $template = $this->scopeConfig->getValue(
            'exto_rma/email_templates/from_customer',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $this->send(
            $template,
            $this->getSender($storeId),
            $this->getAdminEmail($request),
            $this->getAdminName($request),
            $storeId,
            [
                'rma' => $request,
                'order' => $order,
                'status' => $status,
                'resolution' => $resolution,
                'message' => $message->getContent(),
                'url_to_view' => $viewUrl
            ]
        );
        return $this;
    }

    /**
     * @param int $template
     * @param string $email
     * @param string $name
     * @param \Exto\Rma\Model\Request $request
     * @param null|int $storeId
     *
     * @return $this
     */
    public function statusChanged($template, $email, $name, $request, $storeId = null)
    {
        $order = $this->getOrderFromRequest($request);
        $status = $this->getStatusLabelFromRequest($request, $storeId);
        $resolution = $this->getResolutionLabelFromRequest($request, $storeId);
        if (!$template) {
            $template = self::TEMPLATE_STATUS_CHANGED;
        }
        $this->send(
            $template,
            $this->getSender($storeId),
            $email,
            $name,
            $storeId,
            [
                'rma' => $request,
                'order' => $order,
                'status' => $status,
                'resolution' => $resolution,
            ]
        );
        return $this;
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     * @param null|int $storeId
     *
     * @return $this
     */
    public function statusChangedToCustomer($request, $storeId = null)
    {
        $statusId = $request->getStatusId();
        $status = $this->statusFactory->create();
        $status->load($statusId);
        $template = $status->getData('send_email_to_customer_template_id');
        return $this->statusChanged(
            $template,
            $request->getCustomerEmail(),
            $this->getCustomerName($request),
            $request,
            $storeId
        );
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     * @param null|int $storeId
     *
     * @return $this
     */
    public function statusChangedToAdmin($request, $storeId = null)
    {
        $statusId = $request->getStatusId();
        $status = $this->statusFactory->create();
        $status->load($statusId);
        $template = $status->getData('send_email_to_customer_template_id');
        return $this->statusChanged(
            $template,
            $this->getAdminEmail($request),
            $this->getAdminName($request),
            $request,
            $storeId
        );
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     * @param null|int $storeId
     *
     * @return $this
     */
    public function newRequestSaved($request, $storeId = null)
    {
        $template = $this->scopeConfig->getValue(
            'exto_rma/email_templates/new_request',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $this->send(
            $template,
            $this->getSender($storeId),
            $this->getAdminEmail($request),
            $this->getAdminName($request),
            $storeId,
            [
                'rma_increment_id' => $request->getIncrementId(),
                'customer_name' => $this->getCustomerName($request),
                'rma_backend_url' => $this->getBackendUrl($request),
            ]
        );
        return $this;
    }

    /**
     * @param string $template
     * @param string $sender
     * @param string $receiverEmail
     * @param string $receiverName
     * @param int|null $storeId
     * @param array $params
     *
     * @return $this
     */
    protected function send($template, $sender, $receiverEmail, $receiverName, $storeId, $params)
    {
        if (null === $storeId) {
            $storeId = $this->storeManager->getStore()->getId();
        }
        /** @var \Magento\Framework\Mail\TransportInterface $transport */
        $transport = $this->transportBuilder->setTemplateIdentifier(
            $template
        )->setTemplateOptions(
            ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId]
        )->setTemplateVars(
            $params
        )->setFrom(
            $sender
        )->addTo(
            $receiverEmail,
            $receiverName
        )->getTransport();
        $transport->sendMessage();
        return $this;
    }

    /**
     * @param null|int $storeId
     *
     * @return string
     */
    protected function getSender($storeId = null)
    {
        return $this->scopeConfig->getValue(
            'exto_rma/email_templates/email_sender',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param \Exto\Rma\Model\Chat\Message $message
     *
     * @return \Exto\Rma\Model\Request
     */
    protected function getRequestFromMessage($message)
    {
        $requestId = $message->getRequestId();
        /** @var \Exto\Rma\Model\Request $request */
        $request = $this->requestFactory->create();
        $request->load($requestId);
        return $request;
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return \Magento\Sales\Model\Order
     */
    protected function getOrderFromRequest($request)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create();
        $order->load($request->getOrderId());
        return $order;
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     * @param null|int $storeId
     *
     * @return string
     */
    protected function getStatusLabelFromRequest($request, $storeId)
    {
        $statusId = $request->getStatusId();
        /** @var \Exto\Rma\Model\Status $status */
        $status = $this->statusFactory->create();
        $status->load($statusId);
        return $status->getLabel(null === $storeId?0:$storeId);
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     * @param null|int $storeId
     *
     * @return string
     */
    protected function getResolutionLabelFromRequest($request, $storeId)
    {
        $resolutionId = $request->getResolutionId();
        /** @var \Exto\Rma\Model\Resolution $resolution */
        $resolution = $this->resolutionFactory->create();
        $resolution->load($resolutionId);
        return $resolution->getLabel(null === $storeId?0:$storeId);
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return string
     */
    protected function getViewUrl($request)
    {
        return $this->frontendUrlBuilder->getUrl(
            'exto_rma/guest/view',
            [
                'id' => $request->getId(),
                'key' => $request->generateGuestKey()
            ]
        );
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return string
     */
    protected function getBackendUrl($request)
    {
        return $this->backendUrlBuilder->getUrl(
            'exto_rma/request/edit',
            [
                'id' => $request->getId()
            ]
        );
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return string
     */
    protected function getCustomerName($request)
    {
        return $request->getOrder()->getCustomerName();
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return string
     */
    protected function getAdminEmail($request)
    {
        /** @var \Magento\User\Model\User $adminUser */
        $adminUser = $this->adminUserFactory->create();
        $adminUser->load($request->getOwnerId());
        return $adminUser->getEmail();
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return string
     */
    protected function getAdminName($request)
    {
        /** @var \Magento\User\Model\User $adminUser */
        $adminUser = $this->adminUserFactory->create();
        $adminUser->load($request->getOwnerId());
        return $adminUser->getName();
    }
}
