<?php
namespace Exto\Rma\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\UploaderFactory;
use Magento\Framework\Filesystem;

/**
 * Class Uploader
 */
class Uploader
{
    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * @var UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * @param Filesystem $fileSystem
     * @param UploaderFactory $uploaderFactory
     */
    public function __construct(
        Filesystem $fileSystem,
        UploaderFactory $uploaderFactory
    ) {
        $this->fileSystem = $fileSystem;
        $this->uploaderFactory = $uploaderFactory;
    }

    /**
     * @param \Exto\Rma\Model\Chat\Message $message
     *
     * @return \Exto\Rma\Model\Chat\Attachment
     * @throws \Exception
     */
    public function saveAttachment($message)
    {
        $attachment = clone $message->getAttachment();
        $mediaDir = $this->fileSystem->getDirectoryWrite(
            DirectoryList::MEDIA
        );

        try {
            $uploader = $this->uploaderFactory->create(['fileId' => 'file']);
            $uploader->setFilesDispersion(true);
            $uploader->setFilenamesCaseSensitivity(false);
            $uploader->setAllowRenameFiles(true);
            $result = $uploader->save($mediaDir->getAbsolutePath('exto_rma'));
            $path = str_replace(BP, '', $mediaDir->getAbsolutePath('exto_rma')) . $result['file'];
            $attachment->setData([
                'message_id' => $message->getId(),
                'name' => $result['name'],
                'path' => $path,
            ]);
            $attachment->save();
        } catch (\Exception $e) {
            //file not loaded
        }

        return $attachment;
    }
}
