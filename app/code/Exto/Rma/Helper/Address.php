<?php
namespace Exto\Rma\Helper;

/**
 * Class Address
 */
class Address
{
    /** @var \Magento\Directory\Model\CountryFactory */
    protected $countryFactory;

    /**
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     */
    public function __construct(
        \Magento\Directory\Model\CountryFactory $countryFactory
    ) {
        $this->countryFactory = $countryFactory;
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Magento\Sales\Model\Order $order
     *
     * @return string
     */
    public function getAddressHtml($customer, $order)
    {
        /** @var \Magento\Customer\Model\Address\AbstractAddress $billingAddress */
        $billingAddress = $customer->getDefaultBillingAddress();
        if (!$billingAddress || null === $billingAddress->getId()) {
            $billingAddress = $order->getBillingAddress();
        }
        $result = [];
        $result[] = $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
        $result[] = $billingAddress->getStreetFull();
        $result[] = $billingAddress->getCity() . ', ' . $billingAddress->getRegion()
            . ', ' . $billingAddress->getPostcode();
        $countryModel = $this->countryFactory->create();
        $countryModel->load($billingAddress->getCountryId());
        $result[] = $countryModel->getName();
        $result[] = $billingAddress->getTelephone();
        return join("\n", $result);
    }
}
