<?php
namespace Exto\Rma\Controller;

class RegistryConstants
{
    /**
     * Registry key where current request ID is stored
     */
    const CURRENT_REQUEST_ID = 'exto_rma_current_request_id';
}
