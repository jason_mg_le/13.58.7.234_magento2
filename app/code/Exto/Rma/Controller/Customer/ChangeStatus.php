<?php
namespace Exto\Rma\Controller\Customer;

/**
 * Class ChangeStatus
 */
class ChangeStatus extends \Exto\Rma\Controller\Customer
{
    /** @var \Exto\Rma\Model\RequestFactory */
    protected $requestFactory;

    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    protected $resultJsonFactory;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /** @var \Exto\Rma\Model\StatusFactory */
    protected $statusFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory */
    protected $customFieldValueCollectionFactory;

    /** @var \Exto\Rma\Model\Chat\MessageFactory */
    protected $messageFactory;

    /** @var \Exto\Rma\Helper\Email */
    protected $emailHelper;

    /** @var \Magento\User\Model\UserFactory */
    protected $adminUserFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Status\Content\CollectionFactory */
    protected $statusContentCollectionFactory;

    /** @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface */
    protected $localDate;

    /**
     * @param \Magento\Framework\App\Action\Context                              $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface                 $scopeConfig
     * @param \Magento\Framework\Controller\Result\JsonFactory                   $resultJsonFactory
     * @param \Magento\Customer\Model\Session                                    $customerSession
     * @param \Exto\Rma\Model\RequestFactory                                     $requestFactory
     * @param \Magento\Store\Model\StoreManagerInterface                         $storeManager
     * @param \Exto\Rma\Model\StatusFactory                                      $statusFactory
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory $customFieldValueCollectionFactory
     * @param \Exto\Rma\Model\Chat\MessageFactory                                $messageFactory
     * @param \Magento\User\Model\UserFactory                                    $adminUserFactory
     * @param \Exto\Rma\Model\ResourceModel\Status\Content\CollectionFactory     $statusContentCollectionFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface               $localDate
     * @param \Exto\Rma\Helper\EmailFactory                                      $emailHelperFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Exto\Rma\Model\RequestFactory $requestFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Exto\Rma\Model\StatusFactory $statusFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory $customFieldValueCollectionFactory,
        \Exto\Rma\Model\Chat\MessageFactory $messageFactory,
        \Magento\User\Model\UserFactory $adminUserFactory,
        \Exto\Rma\Model\ResourceModel\Status\Content\CollectionFactory $statusContentCollectionFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localDate,
        \Exto\Rma\Helper\EmailFactory $emailHelperFactory
    ) {
        parent::__construct($context, $customerSession, $scopeConfig);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->requestFactory = $requestFactory;
        $this->storeManager = $storeManager;
        $this->statusFactory = $statusFactory;
        $this->customFieldValueCollectionFactory = $customFieldValueCollectionFactory;
        $this->messageFactory = $messageFactory;
        $this->emailHelper = $emailHelperFactory->create();
        $this->adminUserFactory = $adminUserFactory;
        $this->statusContentCollectionFactory = $statusContentCollectionFactory;
        $this->localDate = $localDate;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = [
            'success' => false
        ];
        /** @var \Exto\Rma\Model\Request $request */
        $request = $this->requestFactory->create();
        $request->load($this->getRequest()->getParam('id', null));
        if (null === $request->getId()) {
            $response['redirect'] = $this->_url->getUrl('*/*/index');
            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultJsonFactory->create();
            return $resultJson->setData($response);
        }
        if ($request->getCustomerId() != $this->getCurrentCustomerId()
            && $request->getCustomerEmail() !== $this->getCurrentCustomerEmail()) {
            $response['redirect'] = $this->_url->getUrl('*/*/index');
            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultJsonFactory->create();
            return $resultJson->setData($response);
        }
        /** @var \Exto\Rma\Model\Status $status */
        $status = $this->statusFactory->create();
        $status->load($request->getStatusId());
        $newStatusId = $status->getData('customer_action_changes_status_id');
        if (null === $newStatusId) {
            $this->messageManager->addErrorMessage(__('Unknown status'));
            $response['redirect'] = $this->_url->getUrl('*/*/view', ['id' => $request->getId()]);
            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultJsonFactory->create();
            return $resultJson->setData($response);
        }
        $request->setStatusId($newStatusId);
        try {
            $request->save();
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong'));
            $response['redirect'] = $this->_url->getUrl('*/*/view', ['id' => $request->getId()]);
            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultJsonFactory->create();
            return $resultJson->setData($response);
        }
        $message = null;
        if (strlen($status->getData('message')) > 0) {
            /** @var \Exto\Rma\Model\Chat\Message $message */
            $message = $this->messageFactory->create();
            /** @var \Magento\User\Model\User $adminUser */
            $adminUser = $this->adminUserFactory->create();
            $adminUser->load($request->getOwnerId());
            $message->addData([
                'request_id' => $request->getId(),
                'author_name' => $adminUser->getName(),
                'is_admin' => 1,
                'is_internal' => 0,
                'content' => $status->getData('message')
            ]);
            $message->save();
        }
        $customFieldData = $this->getRequest()->getParam('custom_field', []);
        foreach ($customFieldData as $id => $fieldValue) {
            /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Value\Collection $valueCollection */
            $valueCollection = $this->customFieldValueCollectionFactory->create();
            $valueCollection->addParentFilter($id);
            $valueCollection->addRequestFilter($request->getId());
            $valueModel = $valueCollection->getFirstItem();
            $valueModel->addData([
                'parent_id' => $id,
                'request_id' => $request->getId(),
                'value' => $fieldValue
            ]);
            try {
                $valueModel->save();
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong.'));
                $response['redirect'] = $this->_url->getUrl('*/*/view', ['id' => $request->getId()]);
                /** @var \Magento\Framework\Controller\Result\Json $resultJson */
                $resultJson = $this->resultJsonFactory->create();
                return $resultJson->setData($response);
            }
        }
        /** @var \Exto\Rma\Model\Status $newStatus */
        $newStatus = $this->statusFactory->create();
        $newStatus->load($newStatusId);
        if ($newStatus->getData('is_send_email_to_admin')) {
            try {
                $this->emailHelper->statusChangedToAdmin($request);
            } catch (\Exception $e) {

            }
        }
        $this->messageManager->addSuccessMessage(__('Status has been changed successfully'));
        $messageData = [];
        if (null !== $message) {
            $message->load($message->getId());
            $messageData = $message->getData();
        }
        if (array_key_exists('created_at', $messageData)) {
            $messageData['created_at'] = $this->localDate->formatDateTime(
                $messageData['created_at'],
                \IntlDateFormatter::MEDIUM,
                \IntlDateFormatter::MEDIUM
            );
        }
        $response['success'] = true;
        $response['data'] = [
            'title' => __(
                'RMA #%1 - %2',
                $request->getIncrementId(),
                $newStatus->getLabel($this->storeManager->getStore()->getId())
            ),
            'caption' => trim($this->getCustomerActionLabel($newStatus)),
            'confirm' => trim($this->getCustomActionConfirmationText($newStatus)),
            'message_data' => $messageData
        ];
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }

    /**
     * @return int
     */
    protected function getCurrentCustomerId()
    {
        return $this->customerSession->getCustomer()->getId();
    }

    /**
     * @return string
     */
    protected function getCurrentCustomerEmail()
    {
        return $this->customerSession->getCustomer()->getEmail();
    }

    /**
     * @param \Exto\Rma\Model\Status $status
     *
     * @return string
     */
    protected function getCustomerActionLabel($status)
    {
        $result = $this->getCustomerAction(
            $status,
            \Exto\Rma\Model\Status\Content::TYPE_CORRESPONDING_CUSTOMER_ACTION
        );
        return null!==$result?$result->getValue():"";
    }

    /**
     * @param \Exto\Rma\Model\Status $status
     * @param string $type
     *
     * @return null|\Exto\Rma\Model\ResourceModel\Status\Content
     */
    protected function getCustomerAction($status, $type)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Status\Content\Collection $collection */
        $collection = $this->statusContentCollectionFactory->create();
        $collection->addTypeFilter($type);
        $collection->addParentFilter($status->getId());
        $result = null;
        foreach ($collection as $item) {
            if (null === $result && $item->getStoreId() == 0) {
                $result = $item;
            }
            if ($item->getStoreId() == $this->storeManager->getStore()->getId()) {
                $result = $item;
            }
        }
        return $result;
    }

    /**
     * @param \Exto\Rma\Model\Status $status
     *
     * @return bool|string
     */
    protected function getCustomActionConfirmationText($status)
    {
        $text = $this->getCustomerAction($status, \Exto\Rma\Model\Status\Content::TYPE_CONFIRMATION_TEXT);
        $isRequired = !!$status->getData('is_require_confirmation');
        if (!$isRequired) {
            return false;
        }
        return $text->getValue();
    }
}
