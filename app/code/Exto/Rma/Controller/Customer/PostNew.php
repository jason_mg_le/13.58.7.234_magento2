<?php
namespace Exto\Rma\Controller\Customer;

/**
 * Class PostNew
 */
class PostNew extends \Exto\Rma\Controller\Customer
{
    /** @var \Exto\Rma\Model\RequestFactory  */
    protected $requestFactory;

    /** @var \Exto\Rma\Model\Request\ItemFactory  */
    protected $itemFactory;

    /** @var \Exto\Rma\Model\Chat\MessageFactory  */
    protected $messageFactory;

    /** @var \Exto\Rma\Model\Custom\Field\ValueFactory */
    protected $customFieldValueFactory;

    /** @var \Exto\Rma\Helper\Uploader */
    protected $uploaderHelper;

    /**
     * @param \Magento\Framework\App\Action\Context              $context
     * @param \Magento\Customer\Model\Session                    $customerSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Exto\Rma\Model\RequestFactory                     $requestFactory
     * @param \Exto\Rma\Model\Request\ItemFactory                $itemFactory
     * @param \Exto\Rma\Model\Chat\MessageFactory                $messageFactory
     * @param \Exto\Rma\Model\Custom\Field\ValueFactory          $customFieldValueFactory
     * @param \Exto\Rma\Helper\Uploader                          $uploaderHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Exto\Rma\Model\RequestFactory $requestFactory,
        \Exto\Rma\Model\Request\ItemFactory $itemFactory,
        \Exto\Rma\Model\Chat\MessageFactory $messageFactory,
        \Exto\Rma\Model\Custom\Field\ValueFactory $customFieldValueFactory,
        \Exto\Rma\Helper\Uploader $uploaderHelper
    ) {
        parent::__construct($context, $customerSession, $scopeConfig);
        $this->requestFactory = $requestFactory;
        $this->itemFactory = $itemFactory;
        $this->messageFactory = $messageFactory;
        $this->customFieldValueFactory = $customFieldValueFactory;
        $this->uploaderHelper = $uploaderHelper;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $orderId = $this->getRequest()->getParam('order_id', null);
        $orderItemIdList = $this->getRequest()->getParam('item_id', []);
        $orderItemQtyList = $this->getRequest()->getParam('qty', []);
        $reasonId = $this->getRequest()->getParam('reason_id', null);
        $resolutionId = $this->getRequest()->getParam('resolution_id', null);
        $packageConditionId = $this->getRequest()->getParam('package_condition_id', null);
        if (null === $orderId || count($orderItemIdList) === 0 || count($orderItemQtyList) === 0
            || null === $reasonId || null === $resolutionId || null === $packageConditionId
        ) {
            $this->messageManager->addErrorMessage(
                __('Incorrect data.')
            );
            $resultRedirect->setPath('*/*/add');
            return $resultRedirect;
        }
        /** @var \Exto\Rma\Model\Request $request */
        $request = $this->requestFactory->create();
        $request->addData([
            'order_id' => $orderId,
            'reason_id' => $reasonId,
            'resolution_id' => $resolutionId,
            'package_condition_id' => $packageConditionId,
            'customer_id' => $this->getCustomerId(),
            'customer_email' => $this->getCustomerEmail(),
            'owner_id' => $this->scopeConfig->getValue(
                'exto_rma/general/autoassign_new_requests_to',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        ]);
        try {
            $request->save();
            foreach ($orderItemIdList as $orderItemId) {
                /** @var \Exto\Rma\Model\Request\Item $requestItem */
                $requestItem = $this->itemFactory->create();
                $requestItem->addData([
                    'request_id' => $request->getId(),
                    'order_item_id' => $orderItemId,
                    'qty' => $orderItemQtyList[$orderItemId]
                ]);
                $requestItem->save();
            }
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the request.'));
            $resultRedirect->setPath('*/*/add');
            return $resultRedirect;
        }
        $customFieldData = $this->getRequest()->getParam('custom_field', []);
        foreach ($customFieldData as $id => $fieldValue) {
            /** @var \Exto\Rma\Model\Custom\Field\Value $valueModel */
            $valueModel = $this->customFieldValueFactory->create();
            $valueModel->setData([
                'parent_id' => $id,
                'request_id' => $request->getId(),
                'value' => $fieldValue
            ]);
            try {
                $valueModel->save();
            } catch (\Exception $e) {

            }
        }
        $messageText = $this->getRequest()->getParam('message', null);
        if (strlen($messageText) > 0) {
            try {
                /** @var \Exto\Rma\Model\Chat\Message $message */
                $message = $this->messageFactory->create();
                $message->addData([
                    'request_id' => $request->getId(),
                    'author_name' => $this->getCustomerName(),
                    'is_admin' => 0,
                    'is_internal' => 0,
                    'content' => $messageText
                ]);
                $message->save();
                $this->uploaderHelper->saveAttachment($message);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    __('Something went wrong while saving the message to request.')
                );
                $resultRedirect->setPath('*/*/add');
                return $resultRedirect;
            }
        }
        $this->messageManager->addSuccessMessage(
            __(
                'Your return request #%1 has been submitted.'
                . ' Our staff will review it and get in touch with you soon. Thank you',
                $request->getIncrementId()
            )
        );
        return $this->successRedirect($resultRedirect, $request);
    }

    /**
     * @param \Magento\Framework\Controller\Result\Redirect $resultRedirect
     * @param \Exto\Rma\Model\Request $request
     *
     * @return mixed
     */
    protected function successRedirect($resultRedirect, $request)
    {
        $resultRedirect->setPath('*/*/view', ['id' => $request->getId()]);
        return $resultRedirect;
    }

    /**
     * @return int|null
     */
    protected function getCustomerId()
    {
        return $this->customerSession->getCustomerId();
    }

    /**
     * @return string
     */
    protected function getCustomerEmail()
    {
        return $this->customerSession->getCustomer()->getEmail();
    }

    /**
     * @return string
     */
    protected function getCustomerName()
    {
        return $this->customerSession->getCustomer()->getName();
    }
}
