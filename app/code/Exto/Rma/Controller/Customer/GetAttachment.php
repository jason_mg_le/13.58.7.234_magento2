<?php
namespace Exto\Rma\Controller\Customer;

/**
 * Class GetAttachment
 */
class GetAttachment extends \Exto\Rma\Controller\Customer
{
    /** @var \Magento\Framework\App\Response\Http\FileFactory */
    protected $fileFactory;

    /** @var \Exto\Rma\Model\Chat\AttachmentFactory */
    protected $attachmentFactory;

    /** @var \Exto\Rma\Model\Chat\MessageFactory */
    protected $messageFactory;

    /** @var \Exto\Rma\Model\RequestFactory */
    protected $requestFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Exto\Rma\Model\Chat\AttachmentFactory $attachmentFactory,
        \Exto\Rma\Model\Chat\MessageFactory $messageFactory,
        \Exto\Rma\Model\RequestFactory $requestFactory
    ) {
        parent::__construct($context, $customerSession, $scopeConfig);
        $this->fileFactory = $fileFactory;
        $this->attachmentFactory = $attachmentFactory;
        $this->requestFactory = $requestFactory;
        $this->messageFactory = $messageFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\Result\Forward
     * @throws \Exception
     */
    public function execute()
    {
        $attachmentId = $this->_request->getParam('attachment_id', null);
        if ($attachmentId) {
            /** @var \Exto\Rma\Model\Chat\Attachment $attachment */
            $attachment = $this->attachmentFactory->create();
            $attachment->load($attachmentId);
            /** @var \Exto\Rma\Model\Chat\Message $message */
            $message = $this->messageFactory->create();
            $message->load($attachment->getMessageId());
            if (null === $message->getId()) {
                $redirect = $this->resultRedirectFactory->create();
                $redirect->setPath('*/*/index');
                return $redirect;
            }
            /** @var \Exto\Rma\Model\Request $request */
            $request = $this->requestFactory->create();
            $request->load($message->getRequestId());
            if (null === $request->getId()) {
                $redirect = $this->resultRedirectFactory->create();
                $redirect->setPath('*/*/index');
                return $redirect;
            }
            if ($request->getCustomerId() != $this->getCurrentCustomerId()
                && $request->getCustomerEmail() !== $this->getCurrentCustomerEmail()) {
                $redirect = $this->resultRedirectFactory->create();
                $redirect->setPath('*/*/index');
                return $redirect;
            }
            return $this->fileFactory->create(
                $attachment->getName(),
                file_get_contents(BP . $attachment->getPath())
            );
        }
        $resultForward = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_FORWARD);
        $resultForward->forward('noroute');
        return $resultForward;
    }

    /**
     * @return int
     */
    protected function getCurrentCustomerId()
    {
        return $this->customerSession->getCustomer()->getId();
    }

    /**
     * @return string
     */
    protected function getCurrentCustomerEmail()
    {
        return $this->customerSession->getCustomer()->getEmail();
    }
}
