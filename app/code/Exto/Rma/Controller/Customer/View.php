<?php
namespace Exto\Rma\Controller\Customer;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class View
 */
class View extends \Exto\Rma\Controller\Customer
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /** @var \Exto\Rma\Model\RequestFactory */
    protected $requestFactory;

    /** @var \Exto\Rma\Model\StatusFactory */
    protected $statusFactory;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /**
     * @param \Magento\Framework\App\Action\Context              $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Registry                        $coreRegistry
     * @param \Magento\Customer\Model\Session                    $customerSession
     * @param \Exto\Rma\Model\RequestFactory                     $requestFactory
     * @param \Exto\Rma\Model\StatusFactory                      $statusFactory
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Model\Session $customerSession,
        \Exto\Rma\Model\RequestFactory $requestFactory,
        \Exto\Rma\Model\StatusFactory $statusFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context, $customerSession, $scopeConfig);
        $this->coreRegistry = $coreRegistry;
        $this->requestFactory = $requestFactory;
        $this->statusFactory = $statusFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $request = $this->initRequest();
        /** @var \Exto\Rma\Model\Request $request $request */
        if (null === $request->getId() || $request->getCustomerId() != $this->getCurrentCustomerId()) {
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath('exto_rma/customer/index');
            return $redirect;
        }
        /** @var \Exto\Rma\Model\Status $status */
        $status = $this->statusFactory->create();
        $status->load($request->getStatusId());

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        if ($navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation')) {
            /** @var \Magento\Framework\View\Element\Html\Links $navigationBlock */
            $navigationBlock->setActive('exto_rma\customer\index');
        }
        $resultPage->getConfig()->getTitle()->set(
            __(
                'RMA #%1 - %2',
                $request->getIncrementId(),
                $status->getLabel($this->storeManager->getStore()->getId())
            )
        );
        return $resultPage;
    }

    /**
     * @return \Exto\Rma\Model\Request $request
     */
    protected function initRequest()
    {
        /** @var \Exto\Rma\Model\Request $request */
        $request = $this->requestFactory->create();
        $request->load($this->getRequest()->getParam('id', null));
        $this->coreRegistry->register('current_request', $request);
        return $request;
    }

    /**
     * @return int
     */
    protected function getCurrentCustomerId()
    {
        return $this->customerSession->getCustomer()->getId();
    }
}
