<?php
namespace Exto\Rma\Controller\Customer;

/**
 * Class MessagePost
 */
class MessagePost extends \Exto\Rma\Controller\Customer
{
    /** @var \Exto\Rma\Model\Chat\MessageFactory  */
    protected $messageFactory;

    /** @var \Exto\Rma\Helper\Email */
    protected $emailHelper;

    /** @var \Exto\Rma\Helper\Uploader */
    protected $uploaderHelper;

    /**
     * @param \Magento\Framework\App\Action\Context              $context
     * @param \Magento\Customer\Model\Session                    $customerSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Exto\Rma\Model\Chat\MessageFactory                $messageFactory
     * @param \Exto\Rma\Helper\Uploader                          $uploaderHelper
     * @param \Exto\Rma\Helper\EmailFactory                      $emailHelperFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Exto\Rma\Model\Chat\MessageFactory $messageFactory,
        \Exto\Rma\Helper\Uploader $uploaderHelper,
        \Exto\Rma\Helper\EmailFactory $emailHelperFactory
    ) {
        parent::__construct($context, $customerSession, $scopeConfig);
        $this->messageFactory = $messageFactory;
        $this->uploaderHelper = $uploaderHelper;
        $this->emailHelper = $emailHelperFactory->create();
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $requestId = $this->getRequest()->getParam('request_id', null);
        $messageText = $this->getRequest()->getParam('message', null);

        /** @var \Exto\Rma\Model\Chat\Message $message */
        $message = $this->messageFactory->create();
        $message->addData([
            'request_id' => $requestId,
            'author_name' => $this->getCustomerName(),
            'is_admin' => 0,
            'is_internal' => 0,
            'content' => $messageText
        ]);
        try {
            $message->save();
            $this->uploaderHelper->saveAttachment($message);
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the message.'));
            $resultRedirect->setPath('*/*/view', ['id' => $requestId]);
            return $resultRedirect;
        }
        try {
            $this->emailHelper->messageFromCustomer($message);
        } catch (\Exception $e) {

        }
        $this->messageManager->addSuccessMessage(
            __('Message has been saved successfully.')
        );
        return $this->successRedirect($resultRedirect, $requestId);
    }

    /**
     * @return string
     */
    protected function getCustomerName()
    {
        return $this->customerSession->getCustomer()->getName();
    }

    /**
     * @param \Magento\Framework\Controller\Result\Redirect $resultRedirect
     * @param int $requestId
     *
     * @return mixed
     */
    protected function successRedirect($resultRedirect, $requestId)
    {
        $resultRedirect->setPath('*/*/view', ['id' => $requestId]);
        return $resultRedirect;
    }
}
