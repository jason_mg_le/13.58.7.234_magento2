<?php
namespace Exto\Rma\Controller\Customer;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class PrintAction
 */
class PrintAction extends \Exto\Rma\Controller\Customer
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /** @var \Exto\Rma\Model\RequestFactory */
    protected $requestFactory;

    /**
     * @param \Magento\Framework\App\Action\Context              $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Registry                        $coreRegistry
     * @param \Magento\Customer\Model\Session                    $customerSession
     * @param \Exto\Rma\Model\RequestFactory                     $requestFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Customer\Model\Session $customerSession,
        \Exto\Rma\Model\RequestFactory $requestFactory
    ) {
        parent::__construct($context, $customerSession, $scopeConfig);
        $this->coreRegistry = $coreRegistry;
        $this->requestFactory = $requestFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $request = $this->initRequest();
        /** @var \Exto\Rma\Model\Request $request $request */
        if (null === $request->getId()) {
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath('*/*/index');
            return $redirect;
        }
        if (!$this->checkPermission($request)) {
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath('*/*/index');
            return $redirect;
        }
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->set(__('RMA #%1', $request->getIncrementId()));
        return $resultPage;
    }

    /**
     * @return \Exto\Rma\Model\Request $request
     */
    protected function initRequest()
    {
        /** @var \Exto\Rma\Model\Request $request */
        $request = $this->requestFactory->create();
        $request->load($this->getRequest()->getParam('id', null));
        $this->coreRegistry->register('current_request', $request);
        return $request;
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return bool
     */
    protected function checkPermission($request)
    {
        return $request->getCustomerEmail() === $this->getCurrentCustomerEmail()
            || $request->getCustomerId() == $this->getCurrentCustomerId()
        ;
    }

    /**
     * @return int|null
     */
    protected function getCurrentCustomerId()
    {
        return $this->customerSession->getCustomer()->getId();
    }

    /**
     * @return string
     */
    protected function getCurrentCustomerEmail()
    {
        return $this->customerSession->getCustomer()->getEmail();
    }
}
