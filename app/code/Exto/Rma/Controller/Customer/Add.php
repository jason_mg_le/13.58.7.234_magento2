<?php
namespace Exto\Rma\Controller\Customer;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Add
 *
 * @package Exto\Rma\Controller\Customer
 */
class Add extends \Exto\Rma\Controller\Customer
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        if ($navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation')) {
            /** @var \Magento\Framework\View\Element\Html\Links $navigationBlock */
            $navigationBlock->setActive('exto_rma\customer\index');
        }
        $title = trim($this->scopeConfig->getValue(
            'exto_rma/general/page_title_in_customer_area',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ));
        if (strlen($title) === 0) {
            $title = __('My Returns');
        }
        $resultPage->getConfig()->getTitle()->set($title);
        return $resultPage;
    }
}
