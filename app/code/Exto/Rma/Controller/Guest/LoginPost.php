<?php
namespace Exto\Rma\Controller\Guest;

/**
 * Class LoginPost
 */
class LoginPost extends \Exto\Rma\Controller\Guest
{
    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /** @var \Magento\Customer\Model\CustomerFactory */
    protected $customerFactory;

    /** @var \Magento\Sales\Model\OrderFactory */
    protected $orderFactory;

    /**
     * @param \Magento\Framework\App\Action\Context              $context
     * @param \Magento\Customer\Model\Session                    $customerSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     * @param \Magento\Customer\Model\CustomerFactory            $customerFactory
     * @param \Magento\Sales\Model\OrderFactory                  $orderFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory
    ) {
        parent::__construct($context, $customerSession, $scopeConfig);
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->orderFactory = $orderFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();
        $loginData = $this->getRequest()->getParam('login', []);
        if (!array_key_exists('email', $loginData) || !array_key_exists('order', $loginData)) {
            $this->messageManager->addErrorMessage(__('Incorrect data entered.'));
            $redirect->setPath('*/*/form');
            return $redirect;
        }
        /** @var \Magento\Customer\Model\Customer $customer */
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($this->storeManager->getWebsite()->getId())
            ->loadByEmail($loginData['email'])
        ;
        if (null !== $customer->getId()) {
            $this->messageManager->addErrorMessage(__('There is an account with this email. Please login to continue.'));
            $redirect->setPath('*/*/form');
            return $redirect;
        }
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create();
        $order->loadByIncrementId($loginData['order']);
        if (null === $order->getId()) {
            $this->messageManager->addErrorMessage(__('The order has not been found.'));
            $redirect->setPath('*/*/form');
            return $redirect;
        }
        if ($order->getCustomerEmail() !== $loginData['email']) {
            $this->messageManager->addErrorMessage(__('The order has not been found.'));
            $redirect->setPath('*/*/form');
            return $redirect;
        }
        $this->customerSession->setExtoRmaGuestEmail($loginData['email']);
        $redirect->setPath('exto_rma/guest/add');
        return $redirect;
    }
}
