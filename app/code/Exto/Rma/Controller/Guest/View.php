<?php
namespace Exto\Rma\Controller\Guest;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class View
 */
class View extends \Exto\Rma\Controller\Customer\View
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $request = $this->initRequest();
        $key = $this->getRequest()->getParam('key', null);
        /** @var \Exto\Rma\Model\Request $request $request */
        if (null === $request->getId() || $key !== $request->generateGuestKey()) {
            $this->messageManager->addErrorMessage('Incorrect link.');
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath('exto_rma/guest/form');
            return $redirect;
        }
        /** @var \Exto\Rma\Model\Status $status */
        $status = $this->statusFactory->create();
        $status->load($request->getStatusId());

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->set(
            __(
                'RMA #%1 - %2',
                $request->getIncrementId(),
                $status->getLabel($this->storeManager->getStore()->getId())
            )
        );
        return $resultPage;
    }

    /**
     * @return $this
     */
    protected function checkAuth()
    {
        return $this;
    }
}
