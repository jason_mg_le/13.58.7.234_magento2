<?php
namespace Exto\Rma\Controller\Guest;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Add
 */
class Add extends \Exto\Rma\Controller\Guest
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $customerEmail = $this->customerSession->getExtoRmaGuestEmail();
        if (null === $customerEmail) {
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath('exto_rma/guest/form');
            return $redirect;
        }
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->set(__('Guest Returns'));
        return $resultPage;
    }
}
