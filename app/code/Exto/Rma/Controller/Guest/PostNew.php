<?php
namespace Exto\Rma\Controller\Guest;

/**
 * Class PostNew
 */
class PostNew extends \Exto\Rma\Controller\Customer\PostNew
{
    /**
     * @param \Magento\Framework\Controller\Result\Redirect $resultRedirect
     * @param \Exto\Rma\Model\Request                       $request
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function successRedirect($resultRedirect, $request)
    {
        $this->customerSession->setExtoRmaGuestEmail(null);
        $key = $request->generateGuestKey();
        $resultRedirect->setPath('*/*/view', ['id' => $request->getId(), 'key' => $key]);
        return $resultRedirect;
    }

    /**
     * @return null
     */
    protected function getCustomerId()
    {
        return null;
    }

    /**
     * @return null|string
     */
    protected function getCustomerEmail()
    {
        return $this->customerSession->getExtoRmaGuestEmail();
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    protected function getCustomerName()
    {
        return __('Guest');
    }

    /**
     * @return $this
     */
    protected function checkAuth()
    {
        return $this;
    }
}
