<?php
namespace Exto\Rma\Controller\Guest;

/**
 * Class MessagePost
 */
class MessagePost extends \Exto\Rma\Controller\Customer\MessagePost
{
    /** @var \Exto\Rma\Model\RequestFactory */
    protected $requestFactory;

    /**
     * @param \Magento\Framework\App\Action\Context              $context
     * @param \Magento\Customer\Model\Session                    $customerSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Exto\Rma\Model\Chat\MessageFactory                $messageFactory
     * @param \Exto\Rma\Helper\Uploader                          $uploaderHelper
     * @param \Exto\Rma\Helper\EmailFactory                      $emailHelperFactory
     * @param \Exto\Rma\Model\RequestFactory                     $requestFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Exto\Rma\Model\Chat\MessageFactory $messageFactory,
        \Exto\Rma\Helper\Uploader $uploaderHelper,
        \Exto\Rma\Helper\EmailFactory $emailHelperFactory,
        \Exto\Rma\Model\RequestFactory $requestFactory
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $scopeConfig,
            $messageFactory,
            $uploaderHelper,
            $emailHelperFactory
        );
        $this->requestFactory = $requestFactory;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    protected function getCustomerName()
    {
        return __('Guest');
    }

    /**
     * @return $this
     */
    protected function checkAuth()
    {
        return $this;
    }

    /**
     * @param \Magento\Framework\Controller\Result\Redirect $resultRedirect
     * @param int                                           $requestId
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function successRedirect($resultRedirect, $requestId)
    {
        /** @var \Exto\Rma\Model\Request $request */
        $request = $this->requestFactory->create();
        $request->load($requestId);
        $key = $request->generateGuestKey();
        $resultRedirect->setPath('*/*/view', ['id' => $requestId, 'key' => $key]);
        return $resultRedirect;
    }
}
