<?php
namespace Exto\Rma\Controller\Guest;

/**
 * Class ChangeStatus
 */
class ChangeStatus extends \Exto\Rma\Controller\Customer\ChangeStatus
{
    /**
     * @return null
     */
    protected function getCurrentCustomerId()
    {
        return null;
    }

    /**
     * @return null|string
     */
    protected function getCurrentCustomerEmail()
    {
        return $this->customerSession->getExtoRmaGuestEmail();
    }

    /**
     * @return $this
     */
    protected function checkAuth()
    {
        return $this;
    }
}
