<?php
namespace Exto\Rma\Controller\Guest;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Form
 */
class Form extends \Exto\Rma\Controller\Guest
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->set(__('Guest Returns'));
        return $resultPage;
    }
}
