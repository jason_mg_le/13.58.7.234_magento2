<?php
namespace Exto\Rma\Controller\Guest;

/**
 * Class GetAttachment
 */
class GetAttachment extends \Exto\Rma\Controller\Customer\GetAttachment
{
    /**
     * @return null
     */
    protected function getCurrentCustomerId()
    {
        return null;
    }

    /**
     * @return null|string
     */
    protected function getCurrentCustomerEmail()
    {
        return $this->customerSession->getExtoRmaGuestEmail();
    }

    /**
     * @return $this
     */
    protected function checkAuth()
    {
        return $this;
    }
}
