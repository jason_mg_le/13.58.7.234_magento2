<?php
namespace Exto\Rma\Controller\Guest;

/**
 * Class PrintAction
 */
class PrintAction extends \Exto\Rma\Controller\Customer\PrintAction
{
    /**
     * @return null
     */
    protected function getCurrentCustomerId()
    {
        return null;
    }

    /**
     * @return null|string
     */
    protected function getCurrentCustomerEmail()
    {
        return $this->customerSession->getExtoRmaGuestEmail();
    }

    /**
     * @param \Exto\Rma\Model\Request $request
     *
     * @return bool
     */
    protected function checkPermission($request)
    {
        $key = $this->getRequest()->getParam('key', null);
        $isKeyValid = $key === $request->generateGuestKey();
        return parent::checkPermission($request) || $isKeyValid;
    }

    /**
     * @return $this
     */
    protected function checkAuth()
    {
        return $this;
    }
}
