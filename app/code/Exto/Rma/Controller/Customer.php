<?php
namespace Exto\Rma\Controller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;

/**
 * Class Customer
 */
abstract class Customer extends Action
{
    /** @var Session */
    protected $customerSession;

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * @param \Magento\Framework\App\Action\Context              $context
     * @param \Magento\Customer\Model\Session                    $customerSession
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->customerSession = $customerSession;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * Check customer authentication for some actions
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $this->checkAuth();
        return parent::dispatch($request);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface
     */
    protected function checkAuth()
    {
        if (!$this->customerSession->authenticate()) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        $isFlag = $this->scopeConfig->isSetFlag(
            'exto_rma/general/is_show_interface_for_customer',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (!$isFlag) {
            return $this->_redirect('customer/account/index');
        }
        return $this;
    }
}
