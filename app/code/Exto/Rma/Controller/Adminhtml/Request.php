<?php
namespace Exto\Rma\Controller\Adminhtml;

use Exto\Rma\Api\RequestRepositoryInterface;

/**
 * RMA Request controller
 */
abstract class Request extends \Magento\Backend\App\Action
{
    /** @var \Magento\Framework\Registry */
    protected $_coreRegistry;

    /** @var RequestRepositoryInterface */
    protected $requestRepository;

    /** @var \Magento\Backend\Model\View\Result\ForwardFactory */
    protected $resultForwardFactory;

    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /**
     * Initialize Group Controller
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param RequestRepositoryInterface $requestRepository
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        RequestRepositoryInterface $requestRepository,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->requestRepository = $requestRepository;
        parent::__construct($context);
        $this->resultForwardFactory = $resultForwardFactory;
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return int
     */
    protected function initCurrentRequest()
    {
        $requestId = (int)$this->getRequest()->getParam('id');
        if ($requestId) {
            $this->_coreRegistry->register(
                \Exto\Rma\Controller\RegistryConstants::CURRENT_REQUEST_ID,
                $requestId
            );
        }
        return $requestId;
    }

    /**
     * Determine if authorized to perform request actions.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Exto_Rma::requests');
    }
}
