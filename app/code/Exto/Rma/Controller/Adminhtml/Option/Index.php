<?php
namespace Exto\Rma\Controller\Adminhtml\Option;

/**
 * Class Index
 */
class Index extends \Exto\Rma\Controller\Adminhtml\Option
{
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::options');
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Options'));
        $resultPage->addBreadcrumb(__('RMA'), __('RMA'));
        $resultPage->addBreadcrumb(__('Manage Options'), __('Manage Options'));
        return $resultPage;
    }
}
