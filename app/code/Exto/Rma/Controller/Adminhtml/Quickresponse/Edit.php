<?php
namespace Exto\Rma\Controller\Adminhtml\Quickresponse;

/**
 * Class Edit
 */
class Edit extends \Magento\Backend\App\Action
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /** @var \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Collection */
    protected $responsesCollection;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\CollectionFactory $responsesCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\CollectionFactory $responsesCollectionFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->responsesCollection = $responsesCollectionFactory->create();
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->coreRegistry->register(
            'current_responses',
            $this->responsesCollection
        );
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::options')
            ->addBreadcrumb(__('RMA'), __('RMA'))
            ->addBreadcrumb(__('Manage Options'), __('Manage Options'))
            ->addBreadcrumb(__('Edit Quick Responses'), __('Edit Quick Responses'))
        ;
        $resultPage->getConfig()->getTitle()->prepend(__('Quick Responses'));
        return $resultPage;
    }
}
