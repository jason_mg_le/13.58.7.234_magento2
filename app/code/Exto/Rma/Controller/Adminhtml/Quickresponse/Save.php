<?php
namespace Exto\Rma\Controller\Adminhtml\Quickresponse;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{
    /** @var \Exto\Rma\Model\Chat\QuickresponseFactory */
    protected $qresponseModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Chat\Quickresponse */
    protected $qresponseResource;

    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\CollectionFactory */
    protected $qresponseCollectionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Exto\Rma\Model\Chat\QuickresponseFactory $responseModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Chat\Quickresponse $responseResource
     * @param \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\CollectionFactory $responseCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Exto\Rma\Model\Chat\QuickresponseFactory $responseModelFactory,
        \Exto\Rma\Model\ResourceModel\Chat\Quickresponse $responseResource,
        \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\CollectionFactory $responseCollectionFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->qresponseModelFactory = $responseModelFactory;
        $this->qresponseResource = $responseResource;
        $this->qresponseCollectionFactory = $responseCollectionFactory;
    }

    /**
     * @return $this|\Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->_request->getParams();

        if (isset($data['responses'])) {
            //remove old options
            $newIds = [];
            foreach ($data['responses'] as $response) {
                if (isset($response['id'])) {
                    $newIds[] = $response['id'];
                }
            }
            $this->removeOldOptions($newIds);
            foreach ($data['responses'] as $response) {
                /** @var \Exto\Rma\Model\Chat\QuickResponse $responseModel */
                $responseModel = $this->qresponseModelFactory->create();
                if (isset($response['id'])) {
                    $this->qresponseResource->load($responseModel, $response['id']);
                }
                $responseModel->setData($data);
                if (isset($response['id'])) {
                    $this->qresponseResource->load($responseModel, $response['id']);
                }
                $responseModel->addData($response);

                try {
                    $this->qresponseResource->save($responseModel);
                } catch (\Exception $e) {
                    $this->messageManager->addExceptionMessage(
                        $e,
                        __('Something went wrong while saving the quick response.')
                    );
                    $this->_getSession()->setFormData($data);
                    return $resultRedirect->setPath('exto_rma/*/edit');
                }
            }
            $this->messageManager->addSuccessMessage(__('The quick responses were successfully saved'));
            $this->_getSession()->setFormData(false);
        }

        $resultRedirect->setPath('exto_rma/option/index');
        return $resultRedirect;
    }

    /**
     * @param array $newIds
     * @return $this
     */
    protected function removeOldOptions($newIds)
    {
        $optionCollection = $this->qresponseCollectionFactory->create();
        $allIds = $optionCollection->getAllIds();
        $removeIds = array_diff($allIds, $newIds);

        foreach ($removeIds as $id) {
            $optionModel = $this->qresponseModelFactory->create();
            $this->qresponseResource->load($optionModel, $id);
            $this->qresponseResource->delete($optionModel);
        }

        return $this;
    }
}
