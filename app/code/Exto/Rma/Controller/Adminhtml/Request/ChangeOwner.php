<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

use Exto\Rma\Api\RequestRepositoryInterface;

/**
 * Class ChangeOwner
 */
class ChangeOwner extends \Exto\Rma\Controller\Adminhtml\Request
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @param \Magento\Backend\App\Action\Context               $context
     * @param \Magento\Framework\Registry                       $coreRegistry
     * @param RequestRepositoryInterface                        $requestRepository
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory        $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory  $resultJsonFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        RequestRepositoryInterface $requestRepository,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $requestRepository,
            $resultForwardFactory,
            $resultPageFactory
        );
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = [
            'errors' => false,
            'message' => __('Change successful.')
        ];

        $ownerId = $this->_request->getParam('owner_id', null);
        $requestId = $this->initCurrentRequest();
        try {
            $requestModel = $this->requestRepository->get($requestId);
            $requestModel->setOwnerId($ownerId);
            $this->requestRepository->save($requestModel);
        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'message' => $e->getMessage()
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
