<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

/**
 * Class Create
 */
class Create extends \Exto\Rma\Controller\Adminhtml\Request
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::requests');
        $resultPage->getConfig()->getTitle()->prepend(__('Select Order'));
        $resultPage->addBreadcrumb(__('RMA'), __('RMA'));
        $resultPage->addBreadcrumb(__('Requests'), __('Requests'));
        $resultPage->addBreadcrumb(__('Create Requests'), __('Create Request'));
        return $resultPage;
    }
}
