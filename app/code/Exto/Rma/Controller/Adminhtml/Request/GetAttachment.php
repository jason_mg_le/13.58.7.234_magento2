<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

use Exto\Rma\Api\RequestRepositoryInterface;

/**
 * Class GetAttachment
 */
class GetAttachment extends \Exto\Rma\Controller\Adminhtml\Request
{
    /** @var \Magento\Framework\App\Response\Http\FileFactory */
    protected $fileFactory;

    /** @var \Exto\Rma\Model\Chat\AttachmentFactory */
    protected $attachmentFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        RequestRepositoryInterface $requestRepository,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Exto\Rma\Model\Chat\AttachmentFactory $attachmentFactory
    ) {
        parent::__construct($context, $coreRegistry, $requestRepository, $resultForwardFactory, $resultPageFactory);
        $this->fileFactory = $fileFactory;
        $this->attachmentFactory = $attachmentFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Exception
     */
    public function execute()
    {
        $attachmentId = $this->_request->getParam('attachment_id', null);
        if ($attachmentId) {
            $attachment = $this->attachmentFactory->create();
            $attachment->load($attachmentId);
            $attachment->getPath();
            return $this->fileFactory->create(
                $attachment->getName(),
                file_get_contents(BP . $attachment->getPath())
            );
        }
        $resultForward = $this->resultForwardFactory->create();
        $resultForward->forward('noroute');
        return $resultForward;
    }
}
