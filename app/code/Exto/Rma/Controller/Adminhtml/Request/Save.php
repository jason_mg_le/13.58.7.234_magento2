<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

use Exto\Rma\Api\RequestRepositoryInterface;

/**
 * Class Save
 */
class Save extends \Exto\Rma\Controller\Adminhtml\Request
{
    /** @var  \Magento\Sales\Model\OrderFactory */
    protected $orderModelFactory;

    /** @var  \Magento\Sales\Model\Order\ItemFactory */
    protected $orderItemModelFactory;

    /** @var  \Exto\Rma\Model\RequestFactory */
    protected $requestModelFactory;

    /** @var  \Exto\Rma\Model\Request\ItemFactory */
    protected $requestItemFactory;

    /** @var \Exto\Rma\Model\Custom\Field\ValueFactory */
    protected $customFieldValueFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     * @param \Magento\Backend\App\Action\Context               $context
     * @param \Magento\Framework\Registry                       $coreRegistry
     * @param RequestRepositoryInterface                        $requestRepository
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory        $resultPageFactory
     * @param \Magento\Sales\Model\OrderFactory                 $orderModelFactory
     * @param \Magento\Sales\Model\Order\ItemFactory            $orderItemModelFactory
     * @param \Exto\Rma\Model\RequestFactory                    $requestModelFactory
     * @param \Exto\Rma\Model\Request\ItemFactory               $requestItemFactory
     * @param \Exto\Rma\Model\Custom\Field\ValueFactory         $customFieldValueFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        RequestRepositoryInterface $requestRepository,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Sales\Model\OrderFactory $orderModelFactory,
        \Magento\Sales\Model\Order\ItemFactory $orderItemModelFactory,
        \Exto\Rma\Model\RequestFactory $requestModelFactory,
        \Exto\Rma\Model\Request\ItemFactory $requestItemFactory,
        \Exto\Rma\Model\Custom\Field\ValueFactory $customFieldValueFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
    ) {
        parent::__construct($context, $coreRegistry, $requestRepository, $resultForwardFactory, $resultPageFactory);
        $this->orderModelFactory = $orderModelFactory;
        $this->orderItemModelFactory = $orderItemModelFactory;
        $this->requestModelFactory = $requestModelFactory;
        $this->requestItemFactory = $requestItemFactory;
        $this->customFieldValueFactory = $customFieldValueFactory;
        $this->localeDate = $localeDate;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $data = $this->_request->getParams();
        $this->_getSession()->setExtoRmaReuqestDataForm($data);
        $requestData = $data['general'];
        $orderId = $requestData['order_id'];
        /** @var \Magento\Sales\Model\Order $orderModel */
        $orderModel = $this->orderModelFactory->create();
        $orderModel->load($orderId);
        if (null === $orderModel->getId()) {
            $this->messageManager->addErrorMessage(__('Order has not been found'));
            $resultRedirect->setPath('exto_rma/request/create');
            return $resultRedirect;
        }
        /** @var \Exto\Rma\Model\Request $requestModel */
        $requestModel = $this->requestModelFactory->create();
        $requestModel->setData([
            'customer_id' => $orderModel->getCustomerId(),
            'customer_email' => $orderModel->getCustomerEmail(),
        ]);
        $requestModel->addData($requestData);
        $requestModel->setCreatedAt(
            $this->localeDate->date($requestData['created_at'])
        );
        try {
            $requestModel->save();
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the request.'));
            $resultRedirect->setPath('exto_rma/request/createForm', ['order_id' => $orderId]);
            return $resultRedirect;
        }
        foreach ($data['order_item'] as $itemId => $itemData) {
            if (!array_key_exists('selected', $itemData)) {
                continue;
            }
            /** @var \Magento\Sales\Model\Order\Item $orderItemModel */
            $orderItemModel = $this->orderItemModelFactory->create();
            $orderItemModel->load($itemId);
            $requestItemModel = $this->requestItemFactory->create();
            $requestItemModel->setData([
                'request_id' => $requestModel->getId(),
                'order_item_id' => $itemId,
                'qty' => max(intval($itemData['qty']), intval($orderItemModel->getInvoicedQty()))
            ]);
            try {
                $requestItemModel->save();
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the request.'));
                $resultRedirect->setPath('exto_rma/request/createForm', ['order_id' => $orderId]);
                return $resultRedirect;
            }
        }
        if (array_key_exists('custom_field', $data)) {
            foreach ($data['custom_field'] as $id => $fieldValue) {
                /** @var \Exto\Rma\Model\Custom\Field\Value $valueModel */
                $valueModel = $this->customFieldValueFactory->create();
                $valueModel->setData([
                    'parent_id' => $id,
                    'request_id' => $requestModel->getId(),
                    'value' => $fieldValue
                ]);
                try {
                    $valueModel->save();
                } catch (\Exception $e) {

                }
            }
        }

        $this->_getSession()->setExtoRmaReuqestDataForm(null);
        $this->messageManager->addSuccessMessage(__('Request has been created.'));
        $resultRedirect->setPath('exto_rma/request/edit', ['id' => $requestModel->getId()]);
        return $resultRedirect;
    }
}
