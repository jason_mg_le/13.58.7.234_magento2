<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

/**
 * Class Delete
 */
class Delete extends \Exto\Rma\Controller\Adminhtml\Request
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $requestId = $this->initCurrentRequest();
        try {
            $this->requestRepository->deleteById($requestId);
            $this->messageManager->addSuccessMessage(__('You deleted the RMA request.'));
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('exto_rma/request/index');
        return $resultRedirect;
    }
}
