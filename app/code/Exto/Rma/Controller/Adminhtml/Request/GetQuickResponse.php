<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

use Exto\Rma\Api\RequestRepositoryInterface;

/**
 * Class GetQuickResponse
 */
class GetQuickResponse extends \Exto\Rma\Controller\Adminhtml\Request
{
    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    protected $resultJsonFactory;

    /** @var \Exto\Rma\Model\Chat\QuickresponseFactory */
    protected $quickResponseModelFactory;

    /** @var \Exto\Rma\Model\RequestFactory */
    protected $requestModelFactory;

    /**
     * @param \Magento\Backend\App\Action\Context               $context
     * @param \Magento\Framework\Registry                       $coreRegistry
     * @param RequestRepositoryInterface                        $requestRepository
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory        $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory  $resultJsonFactory
     * @param \Exto\Rma\Model\Chat\QuickresponseFactory         $quickResponseModelFactory
     * @param \Exto\Rma\Model\RequestFactory                    $requestModelFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        RequestRepositoryInterface $requestRepository,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Exto\Rma\Model\Chat\QuickresponseFactory $quickResponseModelFactory,
        \Exto\Rma\Model\RequestFactory $requestModelFactory
    ) {
        parent::__construct($context, $coreRegistry, $requestRepository, $resultForwardFactory, $resultPageFactory);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->quickResponseModelFactory = $quickResponseModelFactory;
        $this->requestModelFactory = $requestModelFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = [
            'errors' => false,
            'message' => __('Get successful.'),
            'content' => ''
        ];

        $quickResponseId = $this->_request->getParam('quick_response_id', null);
        $requestId = $this->initCurrentRequest();
        try {
            /** @var \Exto\Rma\Model\Chat\QuickResponse $quickResponse */
            $quickResponse = $this->quickResponseModelFactory->create();
            $quickResponse->load($quickResponseId);
            /** @var \Exto\Rma\Model\Request $requestModel */
            $requestModel = $this->requestModelFactory->create();
            $requestModel->load($requestId);
            $storeId = $requestModel->getOrder()->getStoreId();
            $response['content'] = $quickResponse->getContentForStoreId($storeId);
        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'message' => $e->getMessage()
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
