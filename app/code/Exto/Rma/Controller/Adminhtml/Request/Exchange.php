<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

/**
 * Class Exchange
 */
class Exchange extends \Magento\Sales\Controller\Adminhtml\Order\Create
{
    /** @var \Exto\Rma\Model\RequestFactory */
    protected $requestFactory;

    /**
     * @param \Magento\Backend\App\Action\Context               $context
     * @param \Magento\Catalog\Helper\Product                   $productHelper
     * @param \Magento\Framework\Escaper                        $escaper
     * @param \Magento\Framework\View\Result\PageFactory        $resultPageFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Exto\Rma\Model\RequestFactory                    $requestFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Exto\Rma\Model\RequestFactory $requestFactory
    ) {
        parent::__construct($context, $productHelper, $escaper, $resultPageFactory, $resultForwardFactory);
        $this->requestFactory = $requestFactory;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Forward|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $this->_getSession()->clearStorage();
        $requestId = $this->getRequest()->getParam('id');
        $requestModel = $this->requestFactory->create()->load($requestId);

        $this->_getSession()->setUseOldShippingMethod(true);
        $this->_getOrderCreateModel()->initFromOrder($requestModel->getOrder());

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/order_create/index');
        return $resultRedirect;
    }
}
