<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

/**
 * Class CreateForm
 */
class CreateForm extends \Exto\Rma\Controller\Adminhtml\Request
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $orderId = $this->_request->getParam('order_id', null);
        if (null === $orderId) {
            $redirect = $this->resultRedirectFactory->create();
            $redirect->setPath('*/*/create');
            return $redirect;
        }
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::requests');
        $resultPage->getConfig()->getTitle()->prepend(__('Create New Request'));
        $resultPage->addBreadcrumb(__('RMA'), __('RMA'));
        $resultPage->addBreadcrumb(__('Requests'), __('Requests'));
        $resultPage->addBreadcrumb(__('Create Requests'), __('Create Request'));
        return $resultPage;
    }
}
