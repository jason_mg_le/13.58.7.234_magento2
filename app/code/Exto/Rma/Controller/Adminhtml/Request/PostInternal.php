<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

/**
 * Class PostInternal
 */
class PostInternal extends \Exto\Rma\Controller\Adminhtml\Request
{
    /** @var \Exto\Rma\Model\Chat\MessageFactory */
    protected $messageFactory;

    /** @var \Magento\Backend\Model\Auth\Session */
    protected $adminSession;

    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    protected $resultJsonFactory;

    /** @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface */
    protected $localDate;

    /**
     * @param \Magento\Backend\App\Action\Context                  $context
     * @param \Magento\Framework\Registry                          $coreRegistry
     * @param \Exto\Rma\Api\RequestRepositoryInterface             $requestRepository
     * @param \Magento\Backend\Model\View\Result\ForwardFactory    $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory           $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory     $resultJsonFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localDate
     * @param \Exto\Rma\Model\Chat\MessageFactory                  $messageFactory
     * @param \Magento\Backend\Model\Auth\Session                  $adminSession
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Exto\Rma\Api\RequestRepositoryInterface $requestRepository,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localDate,
        \Exto\Rma\Model\Chat\MessageFactory $messageFactory,
        \Magento\Backend\Model\Auth\Session $adminSession
    ) {
        parent::__construct($context, $coreRegistry, $requestRepository, $resultForwardFactory, $resultPageFactory);
        $this->messageFactory = $messageFactory;
        $this->adminSession = $adminSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->localDate = $localDate;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = [
            'errors' => false,
            'message' => __('Save successful.'),
            'data' => []
        ];
        $content = $this->_request->getParam('message', null);
        $requestId = $this->initCurrentRequest();
        /** @var \Exto\Rma\Model\Chat\Message $message */
        $message = $this->messageFactory->create();
        $message->setContent($content);
        $message->setIsAdmin(1);
        $message->setIsInternal(1);
        $message->setRequestId($requestId);
        $message->setAuthorName(
            $this->adminSession->getUser()->getName()
        );
        try {
            $message->save();
            $response['data'] = [
                'author' => $message->getAuthorName(),
                'date' => $this->localDate->formatDate($message->getCreatedAt(), \IntlDateFormatter::MEDIUM, true),
                'content' => nl2br($message->getContent())
            ];
        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'message' => $e->getMessage()
            ];
        }
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
