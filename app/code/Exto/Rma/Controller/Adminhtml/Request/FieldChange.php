<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

use Exto\Rma\Api\RequestRepositoryInterface;

/**
 * Class FieldChange
 */
class FieldChange extends \Exto\Rma\Controller\Adminhtml\Request
{
    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory */
    protected $customFieldValueCollectionFactory;

    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    protected $resultJsonFactory;

    /**
     * @param \Magento\Backend\App\Action\Context                                $context
     * @param \Magento\Framework\Registry                                        $coreRegistry
     * @param RequestRepositoryInterface                                         $requestRepository
     * @param \Magento\Backend\Model\View\Result\ForwardFactory                  $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory                         $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory                   $resultJsonFactory
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory $customFieldValueCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        RequestRepositoryInterface $requestRepository,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field\Value\CollectionFactory $customFieldValueCollectionFactory
    ) {
        parent::__construct($context, $coreRegistry, $requestRepository, $resultForwardFactory, $resultPageFactory);
        $this->customFieldValueCollectionFactory = $customFieldValueCollectionFactory;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = [
            'errors' => false,
            'message' => __('Changed success.')
        ];

        $requestId = $this->getRequest()->getParam('request_id', null);
        $fieldId = $this->getRequest()->getParam('id', null);
        $value = $this->getRequest()->getParam('value', null);
        if (null === $requestId || null === $fieldId || null === $value) {
            $response = [
                'errors' => true,
                'message' => __('Wrong data.')
            ];
            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultJsonFactory->create();
            return $resultJson->setData($response);
        }
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Value\Collection $valueCollection */
        $valueCollection = $this->customFieldValueCollectionFactory->create();
        $valueCollection->addParentFilter($fieldId);
        $valueCollection->addRequestFilter($requestId);
        $valueModel = $valueCollection->getFirstItem();
        $valueModel->addData([
            'parent_id' => $fieldId,
            'request_id' => $requestId,
            'value' => $value
        ]);
        try {
            $valueModel->save();
        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'message' => $e->getMessage()
            ];
        }
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
