<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

/**
 * Class PostStatus
 */
class PostStatus extends \Exto\Rma\Controller\Adminhtml\Request
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $statusId = $this->_request->getParam('status_id', null);
        $requestId = $this->initCurrentRequest();

        try {
            $requestModel = $this->requestRepository->get($requestId);
            $requestModel->setStatusId($statusId);
            $this->requestRepository->save($requestModel);
            $this->messageManager->addSuccessMessage(__('Status has been changed.'));
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the status.'));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('exto_rma/request/edit', ['id' => $requestId]);
        return $resultRedirect;
    }
}
