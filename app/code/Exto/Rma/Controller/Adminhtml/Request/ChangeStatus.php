<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

use Exto\Rma\Api\RequestRepositoryInterface;

/**
 * Class ChangeStatus
 */
class ChangeStatus extends \Exto\Rma\Controller\Adminhtml\Request
{
    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    protected $resultJsonFactory;

    /** @var \Exto\Rma\Model\RequestFactory */
    protected $requestFactory;

    /** @var \Exto\Rma\Model\StatusFactory */
    protected $statusFactory;

    /** @var \Exto\Rma\Model\Chat\MessageFactory */
    protected $messageFactory;

    /** @var \Exto\Rma\Helper\Email */
    protected $emailHelper;

    /** @var \Magento\User\Model\UserFactory */
    protected $adminUserFactory;

    /** @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface */
    protected $localDate;

    /**
     * @param \Magento\Backend\App\Action\Context                  $context
     * @param \Magento\Framework\Registry                          $coreRegistry
     * @param RequestRepositoryInterface                           $requestRepository
     * @param \Magento\Backend\Model\View\Result\ForwardFactory    $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory           $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory     $resultJsonFactory
     * @param \Exto\Rma\Model\RequestFactory                       $requestFactory
     * @param \Exto\Rma\Model\Chat\MessageFactory                  $messageFactory
     * @param \Magento\User\Model\UserFactory                      $adminUserFactory
     * @param \Exto\Rma\Helper\EmailFactory                        $emailHelperFactory
     * @param \Exto\Rma\Model\StatusFactory                        $statusFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localDate
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        RequestRepositoryInterface $requestRepository,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Exto\Rma\Model\RequestFactory $requestFactory,
        \Exto\Rma\Model\Chat\MessageFactory $messageFactory,
        \Magento\User\Model\UserFactory $adminUserFactory,
        \Exto\Rma\Helper\EmailFactory $emailHelperFactory,
        \Exto\Rma\Model\StatusFactory $statusFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localDate
    ) {
        parent::__construct(
            $context,
            $coreRegistry,
            $requestRepository,
            $resultForwardFactory,
            $resultPageFactory
        );
        $this->resultJsonFactory = $resultJsonFactory;
        $this->requestFactory = $requestFactory;
        $this->statusFactory = $statusFactory;
        $this->messageFactory = $messageFactory;
        $this->emailHelper = $emailHelperFactory->create();
        $this->adminUserFactory = $adminUserFactory;
        $this->localDate = $localDate;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = [
            'errors' => false,
            'message' => __('Change successful.'),
            'data' => []
        ];

        $statusId = $this->_request->getParam('status_id', null);
        $requestId = $this->initCurrentRequest();
        try {
            $requestDataObject = $this->requestRepository->get($requestId);
            $requestDataObject->setStatusId($statusId);
            $this->requestRepository->save($requestDataObject);
        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'message' => $e->getMessage()
            ];
        }
        /** @var \Exto\Rma\Model\Request $request */
        $request = $this->requestFactory->create();
        $request->load($requestId);
        /** @var \Exto\Rma\Model\Status $statusModel */
        $statusModel = $this->statusFactory->create();
        $statusModel->load($statusId);
        $message = null;
        if (strlen($statusModel->getData('message')) > 0) {
            /** @var \Exto\Rma\Model\Chat\Message $message */
            $message = $this->messageFactory->create();
            /** @var \Magento\User\Model\User $adminUser */
            $adminUser = $this->adminUserFactory->create();
            $adminUser->load($request->getOwnerId());
            $message->addData([
                'request_id' => $request->getId(),
                'author_name' => $adminUser->getName(),
                'is_admin' => 1,
                'is_internal' => 0,
                'content' => $statusModel->getData('message')
            ]);
            $message->save();
        }
        if ($statusModel->getData('is_send_email_to_customer')) {
            try {
                $this->emailHelper->statusChangedToCustomer($request);
            } catch (\Exception $e) {
            }
        }
        $messageData = [];
        if (null !== $message) {
            $message->load($message->getId());
            $messageData = $message->getData();
        }
        if (array_key_exists('created_at', $messageData)) {
            $messageData['created_at'] = $this->localDate->formatDateTime(
                $messageData['created_at'],
                \IntlDateFormatter::MEDIUM,
                \IntlDateFormatter::MEDIUM
            );
        }
        $response['data'] = [
            'caption' => $statusModel->getNextStatusCaption(),
            'next_id' => $statusModel->getNextStatusId()?$statusModel->getNextStatusId():0,
            'message_data' => $messageData
        ];
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
