<?php
namespace Exto\Rma\Controller\Adminhtml\Request;

/**
 * Class Edit
 */
class Edit extends \Exto\Rma\Controller\Adminhtml\Request
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $requestId = $this->initCurrentRequest();
        try {
            $requestModel = $this->requestRepository->get($requestId);
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Could not load the request model'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('exto_rma/*/index');
            return $resultRedirect;
        }
        if (null === $requestModel->getId()) {
            $this->messageManager->addErrorMessage(__('Something went wrong while editing the request.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('exto_rma/*/index');
            return $resultRedirect;
        }

        $this->_coreRegistry->register('current_request', $requestModel);
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::requests')
            ->addBreadcrumb(__('RMA'), __('RMA'))
            ->addBreadcrumb(__('Manage Requests'), __('Manage Requests'))
            ->addBreadcrumb(__('Edit Request'), __('Edit Request'))
        ;
        $resultPage->getConfig()->getTitle()->prepend(__('Requests'));
        $resultPage->getConfig()->getTitle()
            ->prepend(__('Request #%1', $requestModel->getIncrementId()))
        ;
        return $resultPage;
    }
}
