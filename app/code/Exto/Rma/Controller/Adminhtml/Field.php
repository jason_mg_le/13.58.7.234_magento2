<?php
namespace Exto\Rma\Controller\Adminhtml;

/**
 * RMA Custom Fields controller
 */
abstract class Field extends \Magento\Backend\App\Action
{
    /**
     * Determine if authorized to perform request actions.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Exto_Rma::custom_fields');
    }
}
