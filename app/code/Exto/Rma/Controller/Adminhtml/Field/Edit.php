<?php
namespace Exto\Rma\Controller\Adminhtml\Field;

/**
 * Class Edit
 */
class Edit extends \Exto\Rma\Controller\Adminhtml\Field
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /** @var \Exto\Rma\Model\Custom\FieldFactory */
    protected $fieldModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field */
    protected $fieldResource;

    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Exto\Rma\Model\Custom\FieldFactory $fieldModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field $fieldResource
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Exto\Rma\Model\Custom\FieldFactory $fieldModelFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field $fieldResource,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->fieldModelFactory = $fieldModelFactory;
        $this->fieldResource = $fieldResource;
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $fieldModel = $this->fieldModelFactory->create();
        try {
            $this->fieldResource->load($fieldModel, $id);
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Could not load the custom field model'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('exto_rma/*/index');
            return $resultRedirect;
        }
        if ($id && null === $fieldModel->getId()) {
            $this->messageManager->addErrorMessage(__('Something went wrong while editing the custom field.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('exto_rma/*/index');
            return $resultRedirect;
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $fieldModel->setData($data);
        }
        $this->coreRegistry->register('current_field', $fieldModel);

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::custom_fields')
            ->addBreadcrumb(__('RMA'), __('RMA'))
            ->addBreadcrumb(__('Manage Custom Fields'), __('Manage Custom Fields'))
            ->addBreadcrumb(__('Edit Custom Field'), __('Edit Custom Field'))
        ;

        $title = __('New Custom Field');
        if ($fieldModel->getId()) {
            $title = sprintf("%s \"%s\"", __('Edit Custom Field'), $fieldModel->getName());
        }
        $resultPage->getConfig()->getTitle()->prepend($title);
        return $resultPage;
    }
}
