<?php
namespace Exto\Rma\Controller\Adminhtml\Field;

/**
 * Class Save
 */
class Save extends \Exto\Rma\Controller\Adminhtml\Field
{
    /** @var \Exto\Rma\Model\Custom\FieldFactory */
    protected $fieldModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field */
    protected $fieldResource;

    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Exto\Rma\Model\Custom\FieldFactory $fieldModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field $fieldResource
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Exto\Rma\Model\Custom\FieldFactory $fieldModelFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field $fieldResource
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->fieldModelFactory = $fieldModelFactory;
        $this->fieldResource = $fieldResource;
    }

    /**
     * @return $this|\Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $data = $this->_request->getParams();
        if ($data) {
            $fieldModel = $this->fieldModelFactory->create();
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $this->fieldResource->load($fieldModel, $id);
            }
            $fieldModel->setData($data);

            try {
                $this->fieldResource->save($fieldModel);
                $this->messageManager->addSuccessMessage(__('The custom field was successfully saved'));
                $this->_getSession()->setFormData(false);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    __('Something went wrong while saving the custom field.')
                );
                $data['id'] = $id;
                $this->_getSession()->setFormData($data);
                return $resultRedirect->setPath('exto_rma/*/edit', ['id' => $id]);
            }
        }
        $resultRedirect->setPath('exto_rma/*/index');
        return $resultRedirect;
    }
}
