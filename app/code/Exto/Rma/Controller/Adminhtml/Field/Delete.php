<?php
namespace Exto\Rma\Controller\Adminhtml\Field;

/**
 * Class Delete
 */
class Delete extends \Exto\Rma\Controller\Adminhtml\Field
{
    /** @var \Exto\Rma\Model\Custom\FieldFactory */
    protected $fieldModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field */
    protected $fieldResource;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Exto\Rma\Model\Custom\FieldFactory $fieldModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field $fieldResource
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Exto\Rma\Model\Custom\FieldFactory $fieldModelFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field $fieldResource
    ) {
        parent::__construct($context);
        $this->fieldModelFactory = $fieldModelFactory;
        $this->fieldResource = $fieldResource;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('id', '0');

        $resultRedirect = $this->resultRedirectFactory->create();
        /* @var $fieldModel \Exto\Rma\Model\Custom\Field */
        $fieldModel = $this->fieldModelFactory->create();
        $this->fieldResource->load($fieldModel, $id);
        if ($fieldModel->getId()) {
            try {
                $fieldModel->setData('is_deleted', 1);
                $this->fieldResource->save($fieldModel);
                $this->messageManager->addSuccessMessage(__('Custom Field was successfully deleted.'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/field/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        return $resultRedirect->setPath('*/field/');
    }
}
