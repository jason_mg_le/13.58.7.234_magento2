<?php
namespace Exto\Rma\Controller\Adminhtml\Resolution;

/**
 * Class Edit
 */
class Edit extends \Magento\Backend\App\Action
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /** @var \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory */
    protected $resolutionCollectionFactory;

    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory $resolutionCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory $resolutionCollectionFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->resolutionCollectionFactory = $resolutionCollectionFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->coreRegistry->register('current_resolutions', $this->resolutionCollectionFactory->create());
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::options')
            ->addBreadcrumb(__('RMA'), __('RMA'))
            ->addBreadcrumb(__('Manage Options'), __('Manage Options'))
            ->addBreadcrumb(__('Edit Resolutions'), __('Edit Resolutions'))
        ;
        $resultPage->getConfig()->getTitle()->prepend(__('Resolutions'));
        return $resultPage;
    }
}
