<?php
namespace Exto\Rma\Controller\Adminhtml\Resolution;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{
    /** @var \Exto\Rma\Model\ResolutionFactory */
    protected $resolutionModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Resolution */
    protected $resolutionResource;

    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory */
    protected $resolutionCollectionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Exto\Rma\Model\ResolutionFactory $resolutionModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Resolution $resolutionResource
     * @param \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory $resolutionCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Exto\Rma\Model\ResolutionFactory $resolutionModelFactory,
        \Exto\Rma\Model\ResourceModel\Resolution $resolutionResource,
        \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory $resolutionCollectionFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resolutionModelFactory = $resolutionModelFactory;
        $this->resolutionResource = $resolutionResource;
        $this->resolutionCollectionFactory = $resolutionCollectionFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->_request->getParams();
        if (isset($data['resolutions'])) {
            //remove old options
            $newIds = [];
            foreach ($data['resolutions'] as $resolution) {
                if (isset($resolution['id'])) {
                    $newIds[] = $resolution['id'];
                }
            }
            $this->removeOldOptions($newIds);
            foreach ($data['resolutions'] as $resolution) {
                /** @var \Exto\Rma\Model\Resolution $resolutionModel */
                $resolutionModel = $this->resolutionModelFactory->create();
                if (isset($resolution['id'])) {
                    $this->resolutionResource->load($resolutionModel, $resolution['id']);
                }
                $resolutionModel->addData($resolution);

                try {
                    $this->resolutionResource->save($resolutionModel);
                } catch (\Exception $e) {
                    $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the option.'));
                    $this->_getSession()->setFormData($data);
                    return $resultRedirect->setPath('exto_rma/*/edit');
                }
            }
            $this->messageManager->addSuccessMessage(__('The resolutions were successfully saved'));
            $this->_getSession()->setFormData(false);
        }
        $resultRedirect->setPath('exto_rma/option/index');
        return $resultRedirect;
    }

    /**
     * @param array $newIds
     * @return $this
     */
    protected function removeOldOptions($newIds)
    {
        $optionCollection = $this->resolutionCollectionFactory->create();
        $allIds = $optionCollection->getAllIds();
        $removeIds = array_diff($allIds, $newIds);

        foreach ($removeIds as $id) {
            /** @var \Exto\Rma\Model\Resolution $optionModel */
            $optionModel = $this->resolutionModelFactory->create();
            $this->resolutionResource->load($optionModel, $id);
            $optionModel->setData('is_deleted', 1);
            $this->resolutionResource->save($optionModel);
        }
        return $this;
    }
}
