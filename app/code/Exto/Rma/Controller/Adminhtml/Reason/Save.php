<?php
namespace Exto\Rma\Controller\Adminhtml\Reason;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{
    /** @var \Exto\Rma\Model\ReasonFactory */
    protected $reasonModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Reason */
    protected $reasonResource;

    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory */
    protected $reasonCollectionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Exto\Rma\Model\ReasonFactory $reasonModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Reason $reasonResource
     * @param \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory $reasonCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Exto\Rma\Model\ReasonFactory $reasonModelFactory,
        \Exto\Rma\Model\ResourceModel\Reason $reasonResource,
        \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory $reasonCollectionFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->reasonModelFactory = $reasonModelFactory;
        $this->reasonResource = $reasonResource;
        $this->reasonCollectionFactory = $reasonCollectionFactory;
    }

    /**
     * @return $this|\Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->_request->getParams();

        if (isset($data['reasons'])) {
            //remove old options
            $newIds = [];
            foreach ($data['reasons'] as $reason) {
                if (isset($reason['id'])) {
                    $newIds[] = $reason['id'];
                }
            }
            $this->removeOldOptions($newIds);
            foreach ($data['reasons'] as $reason) {
                /** @var \Exto\Rma\Model\Reason $reasonModel */
                $reasonModel = $this->reasonModelFactory->create();
                if (isset($reason['id'])) {
                    $this->reasonResource->load($reasonModel, $reason['id']);
                }
                $reasonModel->addData($reason);

                try {
                    $this->reasonResource->save($reasonModel);
                } catch (\Exception $e) {
                    $this->messageManager->addExceptionMessage(
                        $e,
                        __('Something went wrong while saving the reason.')
                    );
                    $this->_getSession()->setFormData($data);
                    return $resultRedirect->setPath('exto_rma/*/edit');
                }
            }
            $this->messageManager->addSuccessMessage(__('The reasons were successfully saved'));
            $this->_getSession()->setFormData(false);
        }
        $resultRedirect->setPath('exto_rma/option/index');
        return $resultRedirect;
    }

    /**
     * @param array $newIds
     * @return $this
     */
    protected function removeOldOptions($newIds)
    {
        $optionCollection = $this->reasonCollectionFactory->create();
        $allIds = $optionCollection->getAllIds();
        $removeIds = array_diff($allIds, $newIds);

        foreach ($removeIds as $id) {
            /** @var \Exto\Rma\Model\Reason $optionModel */
            $optionModel = $this->reasonModelFactory->create();
            $this->reasonResource->load($optionModel, $id);
            $optionModel->setData('is_deleted', 1);
            $this->reasonResource->save($optionModel);
        }
        return $this;
    }
}
