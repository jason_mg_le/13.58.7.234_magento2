<?php
namespace Exto\Rma\Controller\Adminhtml\Reason;

/**
 * Class Edit
 */
class Edit extends \Magento\Backend\App\Action
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /** @var \Exto\Rma\Model\ResourceModel\Reason\Collection */
    protected $reasonCollection;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory $reasonCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory $reasonCollectionFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->reasonCollection = $reasonCollectionFactory->create();
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->coreRegistry->register(
            'current_reasons',
            $this->reasonCollection
        );
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::options')
            ->addBreadcrumb(__('RMA'), __('RMA'))
            ->addBreadcrumb(__('Manage Options'), __('Manage Options'))
            ->addBreadcrumb(__('Edit Reasons'), __('Edit Reasons'))
        ;
        $resultPage->getConfig()->getTitle()->prepend(__('Reasons'));
        return $resultPage;
    }
}
