<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Exto\Rma\Controller\Adminhtml\Support;

/**
 * Class Index
 */
class Index extends \Magento\Backend\App\Action
{
    /**
     * Redirect to support page
     *
     * @return \Magento\Framework\Controller\Result\RedirectFactory
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl('http://exto.io/support');
        return $resultRedirect;
    }
}
