<?php
namespace Exto\Rma\Controller\Adminhtml\Package;

/**
 * Class Edit
 */
class Edit extends \Magento\Backend\App\Action
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /** @var \Exto\Rma\Model\ResourceModel\Package\Collection */
    protected $conditionCollection;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Exto\Rma\Model\ResourceModel\Package\CollectionFactory $conditionCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Exto\Rma\Model\ResourceModel\Package\CollectionFactory $conditionCollectionFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->conditionCollection = $conditionCollectionFactory->create();
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->coreRegistry->register(
            'current_conditions',
            $this->conditionCollection
        );
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::options')
            ->addBreadcrumb(__('RMA'), __('RMA'))
            ->addBreadcrumb(__('Manage Options'), __('Manage Options'))
            ->addBreadcrumb(__('Edit Package Conditions'), __('Edit Package Conditions'))
        ;
        $resultPage->getConfig()->getTitle()->prepend(__('Package Conditions'));
        return $resultPage;
    }
}
