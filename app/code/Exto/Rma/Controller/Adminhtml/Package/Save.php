<?php
namespace Exto\Rma\Controller\Adminhtml\Package;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{
    /** @var \Exto\Rma\Model\PackageFactory */
    protected $conditionModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Package */
    protected $conditionResource;

    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Package\CollectionFactory */
    protected $conditionCollectionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Exto\Rma\Model\PackageFactory $conditionModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Package $conditionResource
     * @param \Exto\Rma\Model\ResourceModel\Package\CollectionFactory $conditionCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Exto\Rma\Model\PackageFactory $conditionModelFactory,
        \Exto\Rma\Model\ResourceModel\Package $conditionResource,
        \Exto\Rma\Model\ResourceModel\Package\CollectionFactory $conditionCollectionFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->conditionModelFactory = $conditionModelFactory;
        $this->conditionResource = $conditionResource;
        $this->conditionCollectionFactory = $conditionCollectionFactory;
    }

    /**
     * @return $this|\Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->_request->getParams();
        if (isset($data['conditions'])) {
            //remove old options
            $newIds = [];
            foreach ($data['conditions'] as $condition) {
                if (isset($condition['id'])) {
                    $newIds[] = $condition['id'];
                }
            }
            $this->removeOldOptions($newIds);
            foreach ($data['conditions'] as $condition) {
                $conditionModel = $this->conditionModelFactory->create();
                if (isset($condition['id'])) {
                    $this->conditionResource->load($conditionModel, $condition['id']);
                }
                $conditionModel->addData($condition);

                try {
                    $this->conditionResource->save($conditionModel);
                } catch (\Exception $e) {
                    $this->messageManager->addExceptionMessage(
                        $e,
                        __('Something went wrong while saving the package condition.')
                    );
                    $this->_getSession()->setFormData($data);
                    return $resultRedirect->setPath('exto_rma/*/edit');
                }
            }

            $this->messageManager->addSuccessMessage(__('The package conditions were successfully saved'));
            $this->_getSession()->setFormData(false);
        }
        $resultRedirect->setPath('exto_rma/option/index');
        return $resultRedirect;
    }

    /**
     * @param array $newIds
     * @return $this
     */
    protected function removeOldOptions($newIds)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Package\Collection $optionCollection */
        $optionCollection = $this->conditionCollectionFactory->create();
        $allIds = $optionCollection->getAllIds();
        $removeIds = array_diff($allIds, $newIds);
        foreach ($removeIds as $id) {
            /** @var \Exto\Rma\Model\Package $optionModel */
            $optionModel = $this->conditionModelFactory->create();
            $this->conditionResource->load($optionModel, $id);
            $optionModel->setData('is_deleted', 1);
            $this->conditionResource->save($optionModel);
        }
        return $this;
    }
}
