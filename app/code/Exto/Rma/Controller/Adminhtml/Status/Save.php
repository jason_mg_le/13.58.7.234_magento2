<?php
namespace Exto\Rma\Controller\Adminhtml\Status;

/**
 * Class Save
 */
class Save extends \Exto\Rma\Controller\Adminhtml\Status
{
    /** @var \Exto\Rma\Model\StatusFactory */
    protected $statusModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Status */
    protected $statusResource;

    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /** @var \Magento\Backend\Model\Auth\Session */
    protected $authSession;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Exto\Rma\Model\StatusFactory $statusModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Status $statusResource
     * @param \Magento\Backend\Model\Auth\Session $authSession
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Exto\Rma\Model\StatusFactory $statusModelFactory,
        \Exto\Rma\Model\ResourceModel\Status $statusResource,
        \Magento\Backend\Model\Auth\Session $authSession
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->statusModelFactory = $statusModelFactory;
        $this->statusResource = $statusResource;
        $this->authSession = $authSession;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->_request->getParams();
        if ($data) {
            /** @var \Exto\Rma\Model\Status $statusModel */
            $statusModel = $this->statusModelFactory->create();

            $id = $this->getRequest()->getParam('entity_id');
            if ($id) {
                $this->statusResource->load($statusModel, $id);
            }
            $statusModel->setData($data);
            if (null === $statusModel->getId()) {
                $statusModel->setAddedBy($this->authSession->getUser()->getName());
            }
            try {
                $this->statusResource->save($statusModel);
                $this->messageManager->addSuccessMessage(__('The status was successfully saved'));
                $this->_getSession()->setFormData(false);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the status.'));
                $data['id'] = $id;
                $this->_getSession()->setFormData($data);
                return $resultRedirect->setPath('exto_rma/*/edit', ['id' => $id]);
            }
        }
        $resultRedirect->setPath('exto_rma/*/index');
        return $resultRedirect;
    }
}
