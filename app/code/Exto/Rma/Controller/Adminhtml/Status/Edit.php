<?php
namespace Exto\Rma\Controller\Adminhtml\Status;

/**
 * Class Edit
 */
class Edit extends \Exto\Rma\Controller\Adminhtml\Status
{
    /** @var \Magento\Framework\Registry */
    protected $coreRegistry;

    /** @var \Exto\Rma\Model\StatusFactory */
    protected $statusModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Status */
    protected $statusResource;

    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Exto\Rma\Model\StatusFactory $statusModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Status $statusResource
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Exto\Rma\Model\StatusFactory $statusModelFactory,
        \Exto\Rma\Model\ResourceModel\Status $statusResource,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->statusModelFactory = $statusModelFactory;
        $this->statusResource = $statusResource;
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Exto\Rma\Model\Status $statusModel */
        $statusModel = $this->statusModelFactory->create();
        try {
            $this->statusResource->load($statusModel, $id);
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Could not load the status model'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('exto_rma/*/index');
            return $resultRedirect;
        }
        if ($id && null === $statusModel->getId()) {
            $this->messageManager->addErrorMessage(__('Something went wrong while editing the status.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('exto_rma/*/index');
            return $resultRedirect;
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $statusModel->setData($data);
        }
        $this->coreRegistry->register('current_status', $statusModel);

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::statuses')
            ->addBreadcrumb(__('RMA'), __('RMA'))
            ->addBreadcrumb(__('Workflow and Statuses'), __('Workflow and Statuses'))
            ->addBreadcrumb(__('Edit Status'), __('Edit Status'))
        ;
        $resultPage->getConfig()->getTitle()->prepend(
            $statusModel->getId() ? sprintf("%s \"%s\"", __('Edit Status'), $statusModel->getName()) : __('New Status')
        );
        return $resultPage;
    }
}
