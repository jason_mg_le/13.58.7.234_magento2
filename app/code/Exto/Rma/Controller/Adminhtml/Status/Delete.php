<?php
namespace Exto\Rma\Controller\Adminhtml\Status;

/**
 * Class Delete
 */
class Delete extends \Exto\Rma\Controller\Adminhtml\Status
{
    /** @var \Exto\Rma\Model\StatusFactory */
    protected $statusModelFactory;

    /** @var \Exto\Rma\Model\ResourceModel\Status */
    protected $statusResource;

    /** @var \Exto\Rma\Model\ResourceModel\Request\CollectionFactory */
    protected $requestCollectionFactory;

    /**
     * @param \Magento\Backend\App\Action\Context                     $context
     * @param \Exto\Rma\Model\StatusFactory                           $statusModelFactory
     * @param \Exto\Rma\Model\ResourceModel\Status                    $statusResource
     * @param \Exto\Rma\Model\ResourceModel\Request\CollectionFactory $requestCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Exto\Rma\Model\StatusFactory $statusModelFactory,
        \Exto\Rma\Model\ResourceModel\Status $statusResource,
        \Exto\Rma\Model\ResourceModel\Request\CollectionFactory $requestCollectionFactory
    ) {
        parent::__construct($context);
        $this->statusModelFactory = $statusModelFactory;
        $this->statusResource = $statusResource;
        $this->requestCollectionFactory = $requestCollectionFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('id', null);
        $resultRedirect = $this->resultRedirectFactory->create();
        /* @var $statusModel \Exto\Rma\Model\Custom\Field */
        $statusModel = $this->statusModelFactory->create();
        if (null === $id) {
            return $resultRedirect->setPath('*/status/');
        }
        $this->statusResource->load($statusModel, $id);
        if (null === $statusModel->getId()) {
            return $resultRedirect->setPath('*/status/');
        }
        /** @var \Exto\Rma\Model\ResourceModel\Request\Collection $requestCollection */
        $requestCollection = $this->requestCollectionFactory->create();
        $requestCollection->addFieldToFilter('status_id', ['eq' => $id]);
        if ($requestCollection->getSize() > 0) {
            $this->messageManager->addErrorMessage(
                __('Unable to delete: some RMA use this status.')
            );
            return $resultRedirect->setPath('*/status/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        try {
            $statusModel->setData('is_deleted', 1);
            $this->statusResource->save($statusModel);
            $this->messageManager->addSuccessMessage(__('Status was successfully deleted.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $resultRedirect->setPath('*/status/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/status/');
    }
}
