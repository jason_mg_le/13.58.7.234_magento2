<?php
namespace Exto\Rma\Controller\Adminhtml\Status;

/**
 * Class Index
 */
class Index extends \Exto\Rma\Controller\Adminhtml\Status
{
    /** @var \Magento\Framework\View\Result\PageFactory */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Exto_Rma::statuses');
        $resultPage->getConfig()->getTitle()->prepend(__('Workflow and Statuses'));
        $resultPage->addBreadcrumb(__('RMA'), __('RMA'));
        $resultPage->addBreadcrumb(__('Workflow and Statuses'), __('Workflow and Statuses'));
        return $resultPage;
    }
}
