<?php
namespace Exto\Rma\Controller;

/**
 * Class Guest
 */
abstract class Guest extends \Exto\Rma\Controller\Customer
{
    /**
     * @return $this
     */
    protected function checkAuth()
    {
        return $this;
    }
}
