<?php
namespace Exto\Rma\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

/**
 * Class Product
 */
class Product extends Column
{
    /** @var UrlInterface */
    protected $urlBuilder;

    /** @var \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory */
    protected $itemCollectionFactory;

    /**
     * @param ContextInterface                                             $context
     * @param UiComponentFactory                                           $uiComponentFactory
     * @param UrlInterface                                                 $urlBuilder
     * @param \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $itemCollectionFactory
     * @param array                                                        $components
     * @param array                                                        $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        \Exto\Rma\Model\ResourceModel\Request\Item\CollectionFactory $itemCollectionFactory,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->itemCollectionFactory = $itemCollectionFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Request\Item\Collection $itemCollection */
        $itemCollection = $this->itemCollectionFactory->create();
        $itemCollection->joinProductData();
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $collection = clone $itemCollection;
                $collection->addFieldToFilter('request_id', ['eq' => $item['entity_id']]);
                $result = [];
                foreach ($collection as $requestItem) {
                    $result[] = [
                        'href' => $this->urlBuilder->getUrl(
                            'catalog/product/edit',
                            ['id' => $requestItem->getData('product_id')]
                        ),
                        'label' => $requestItem->getData('product_name')
                    ];
                }
                $item[$this->getData('name')] = $result;
            }
        }
        return $dataSource;
    }
}
