<?php
namespace Exto\Rma\Ui\Component\Listing\Column\Options;

use Magento\Framework\Data\OptionSourceInterface;
use Exto\Rma\Model\ResourceModel\Reason\CollectionFactory;

/**
 * Class Options
 */
class Reason implements OptionSourceInterface
{
    /** @var array */
    protected $options;

    /** @var CollectionFactory */
    protected $collectionFactory;

    /**
     * Constructor
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            /** @var \Exto\Rma\Model\ResourceModel\Reason\Collection $collection */
            $collection = $this->collectionFactory->create();
            $collection->setIncludeDeletedFlag();
            $this->options = $collection->toOptionArray();
        }
        return $this->options;
    }
}
