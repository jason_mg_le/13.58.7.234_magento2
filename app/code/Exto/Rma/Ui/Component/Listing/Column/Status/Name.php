<?php
namespace Exto\Rma\Ui\Component\Listing\Column\Status;

/**
 * Class Name
 */
class Name extends \Exto\Rma\Ui\Component\Listing\Column\Name
{
    /**
     * @return string
     */
    protected function getRoute()
    {
        return 'exto_rma/status/edit';
    }
}
