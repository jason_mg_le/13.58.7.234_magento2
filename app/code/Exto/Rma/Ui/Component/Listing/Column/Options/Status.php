<?php
namespace Exto\Rma\Ui\Component\Listing\Column\Options;

use Magento\Framework\Data\OptionSourceInterface;
use Exto\Rma\Model\ResourceModel\Status\Collection;

/**
 * Class Options
 */
class Status implements OptionSourceInterface
{
    /** @var array */
    protected $options;

    /** @var Collection */
    protected $collection;

    /**
     * Constructor
     *
     * @param Collection $collection
     */
    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = $this->collection->toOptionArray();
        }
        return $this->options;
    }
}
