<?php
namespace Exto\Rma\Ui\DataProvider;

/**
 * Class OptionDataProvider
 */
class OptionDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /** @var \Magento\Framework\UrlInterface */
    protected $urlBuilder;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Magento\Framework\UrlInterface $urlInterface,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->collection;
        $this->urlBuilder = $urlInterface;
    }

    /**
     * @inheritdoc
     */
    public function getData()
    {

        $data = [];
        $data['totalRecords'] = 4;
        $data['items'] = [];
        //reason form
        $itemData = ['name' => __('Reasons'), 'url' => $this->urlBuilder->getUrl('exto_rma/reason/edit')];
        $data['items'][] = $itemData;

        //resolution form
        $itemData = ['name' => __('Resolutions'), 'url' => $this->urlBuilder->getUrl('exto_rma/resolution/edit')];
        $data['items'][] = $itemData;

        //package conditions form
        $itemData = ['name' => __('Package conditions'), 'url' => $this->urlBuilder->getUrl('exto_rma/package/edit')];
        $data['items'][] = $itemData;

        //quick responses form
        $itemData = ['name' => __('Quick responses'), 'url' => $this->urlBuilder->getUrl('exto_rma/quickresponse/edit')];
        $data['items'][] = $itemData;
        return $data;
    }

    /**
     * @param \Magento\Framework\Api\Filter $filter
     *
     * @return null
     */
    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        return null;
    }

    /**
     * Returns search criteria
     *
     * @return null
     */
    public function getSearchCriteria()
    {
        return null;
    }

    /**
     * Returns SearchResult
     *
     * @return null
     */
    public function getSearchResult()
    {
        return null;
    }

    /**
     * Add field to select
     *
     * @param string|array $field
     * @param string|null $alias
     * @return void
     */
    public function addField($field, $alias = null)
    {
    }

    /**
     * self::setOrder() alias
     *
     * @param string $field
     * @param string $direction
     * @return void
     */
    public function addOrder($field, $direction)
    {
    }

    /**
     * Set Query limit
     *
     * @param int $offset
     * @param int $size
     * @return void
     */
    public function setLimit($offset, $size)
    {
    }

    /**
     * Removes field from select
     *
     * @param string|null $field
     * @param bool $isAlias Alias identifier
     * @return void
     */
    public function removeField($field, $isAlias = false)
    {
    }

    /**
     * Removes all fields from select
     *
     * @return void
     */
    public function removeAllFields()
    {
    }

    /**
     * Retrieve count of loaded items
     *
     * @return int
     */
    public function count()
    {
        return 4;
    }
}
