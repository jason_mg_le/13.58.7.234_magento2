<?php
namespace Exto\Rma\Model\ResourceModel\Custom;

/**
 * Class Field
 */
class Field extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /** @var Field\Content */
    protected $contentResource;

    /** @var \Exto\Rma\Model\Custom\Field\ContentFactory */
    protected $contentFactory;

    /** @var Field\Content\CollectionFactory */
    protected $contentCollectionFactory;

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param Field\Content $contentResource
     * @param \Exto\Rma\Model\Custom\Field\ContentFactory $contentFactory
     * @param Field\Content\CollectionFactory $contentCollectionFactory
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Exto\Rma\Model\ResourceModel\Custom\Field\Content $contentResource,
        \Exto\Rma\Model\Custom\Field\ContentFactory $contentFactory,
        \Exto\Rma\Model\ResourceModel\Custom\Field\Content\CollectionFactory $contentCollectionFactory
    ) {
        $this->contentFactory = $contentFactory;
        $this->contentResource = $contentResource;
        $this->contentCollectionFactory = $contentCollectionFactory;
        parent::__construct($context);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_custom_field', 'entity_id');
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this|void
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $values = $object->getEditableForCustomerInStatuses();
        if (is_string($values)) {
            return $this;
        }
        if ($values) {
            $object->setEditableForCustomerInStatuses(implode(',', $values));
        } else {
            $object->setEditableForCustomerInStatuses('');
        }
        return $this;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        //save or update labels
        $labelsData = $object->getLabels();
        foreach ($labelsData as $storeId => $label) {
            $this->updateContent($storeId, $object->getId(), \Exto\Rma\Model\Custom\Field\Content::TYPE_LABEL, $label);
        }
        $this->updateContent(0, $object->getId(), \Exto\Rma\Model\Custom\Field\Content::TYPE_LABEL, $object->getName());

        //save or update options
        $optionsData = $object->getOptionValues();
        foreach ($optionsData as $storeId => $option) {
            $this->updateContent(
                $storeId,
                $object->getId(),
                \Exto\Rma\Model\Custom\Field\Content::TYPE_OPTION,
                $option
            );
        }

        return $this;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addParentFilter($object->getId());
        $collection->addTypeFilter(\Exto\Rma\Model\Status\Content::TYPE_LABEL);

        $labels = [];
        foreach ($collection->getItems() as $label) {
            $labels[$label->getStoreId()] = $label->getValue();
        }
        $object->setLabels($labels);

        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addParentFilter($object->getId());
        $collection->addTypeFilter(\Exto\Rma\Model\Custom\Field\Content::TYPE_OPTION);

        $options = [];
        foreach ($collection->getItems() as $option) {
            $options[$option->getStoreId()] = $option->getValue();
        }
        $object->setOptionValues($options);
        return parent::_afterLoad($object);
    }

    /**
     * @param int $storeId
     * @param int $parentId
     * @param string $type
     * @param string $value
     * @return $this
     */
    private function updateContent($storeId, $parentId, $type, $value)
    {
        $item = $this->getCollectionItemByData($storeId, $parentId, $type);
        if ($item->getId()) {
            $item->setValue($value);
            $item->save();
        } else {
            /** @var \Exto\Rma\Model\Custom\Field\Content $contentItem */
            $contentItem = $this->contentFactory->create();
            $contentItem
                ->setParentId($parentId)
                ->setStoreId($storeId)
                ->setType($type)
                ->setValue($value);
            $this->contentResource->save($contentItem);
        }
        return $this;
    }

    /**
     * @param int $storeId
     * @param int $parentId
     * @param string $type
     * @return \Exto\Rma\Model\Custom\Field\Content
     */
    private function getCollectionItemByData($storeId, $parentId, $type)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addParentFilter($parentId);
        $collection->addStoreFilter($storeId);
        $collection->addTypeFilter($type);
        return $collection->getFirstItem();
    }
}
