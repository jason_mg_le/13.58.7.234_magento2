<?php
namespace Exto\Rma\Model\ResourceModel\Custom\Field\Content;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Exto\Rma\Model\Custom\Field\Content::class,
            \Exto\Rma\Model\ResourceModel\Custom\Field\Content::class
        );
    }

    /**
     * @param int $parentId
     * @return $this
     */
    public function addParentFilter($parentId)
    {
        $this->addFilter('main_table.parent_id', ['eq' => $parentId], 'public');
        return $this;
    }

    /**
     * @param int $storeId
     * @return $this
     */
    public function addStoreFilter($storeId)
    {
        $this->addFilter('main_table.store_id', ['eq' => $storeId], 'public');
        return $this;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function addTypeFilter($type)
    {
        $this->addFilter('main_table.type', ['eq' => $type], 'public');
        return $this;
    }

    /**
     * @return $this
     */
    public function joinAdminLabel()
    {
        $this->getSelect()->joinLeft(
            ['admin_content' => $this->getTable('exto_rma_custom_field_content')],
            'admin_content.type = main_table.type AND admin_content.parent_id = main_table.parent_id' .
            ' AND admin_content.store_id=0',
            []
        );
        $this->getSelect()->columns([
            'admin_label' => 'admin_content.value'
        ]);
        return $this;
    }
}
