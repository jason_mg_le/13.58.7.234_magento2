<?php
namespace Exto\Rma\Model\ResourceModel\Custom\Field;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Custom\Field::class, \Exto\Rma\Model\ResourceModel\Custom\Field::class);
    }

    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->addFieldToFilter('is_deleted', ['neq' => 1]);
        return $this;
    }
}
