<?php
namespace Exto\Rma\Model\ResourceModel\Custom\Field;

/**
 * Class Content
 */
class Content extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_custom_field_content', 'id');
    }
}
