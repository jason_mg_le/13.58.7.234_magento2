<?php
namespace Exto\Rma\Model\ResourceModel\Custom\Field;

/**
 * Class Value
 */
class Value extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_custom_field_value', 'value_id');
    }
}
