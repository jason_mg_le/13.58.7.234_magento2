<?php
namespace Exto\Rma\Model\ResourceModel\Custom\Field\Value;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Exto\Rma\Model\Custom\Field\Value::class,
            \Exto\Rma\Model\ResourceModel\Custom\Field\Value::class
        );
    }

    /**
     * @param int $parentId
     * @return $this
     */
    public function addParentFilter($parentId)
    {
        $this->addFieldToFilter('main_table.parent_id', ['eq' => $parentId]);
        return $this;
    }

    /**
     * @param int $requestId
     * @return $this
     */
    public function addRequestFilter($requestId)
    {
        $this->addFieldToFilter('main_table.request_id', ['eq' => $requestId]);
        return $this;
    }
}
