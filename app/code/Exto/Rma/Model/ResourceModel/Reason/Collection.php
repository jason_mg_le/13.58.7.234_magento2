<?php
namespace Exto\Rma\Model\ResourceModel\Reason;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /** @var bool */
    protected $isIncludeDeleted = false;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Reason::class, \Exto\Rma\Model\ResourceModel\Reason::class);
    }

    /**
     * @return $this
     */
    protected function _beforeLoad()
    {
        if (!$this->isIncludeDeleted) {
            $this->addFieldToFilter('is_deleted', ['neq' => 1]);
        }
        return parent::_beforeLoad();
    }

    /**
     * @return $this
     */
    public function setIncludeDeletedFlag()
    {
        $this->isIncludeDeleted = true;
        return $this;
    }

    /**
     * @param int $storeId
     *
     * @return $this
     */
    public function joinLabelForStore($storeId)
    {
        $this->getSelect()->joinLeft(
            ['reason_label' => $this->getTable('exto_rma_reason_content')],
            'reason_label.parent_id = main_table.entity_id' .
            ' AND reason_label.store_id=' . $storeId,
            []
        );
        $this->getSelect()->joinLeft(
            ['admin_reason_label' => $this->getTable('exto_rma_reason_content')],
            'admin_reason_label.parent_id = main_table.entity_id' .
            ' AND admin_reason_label.store_id=0',
            []
        );
        $this->getSelect()->columns([
            'label' => 'IFNULL(reason_label.value, admin_reason_label.value)'
        ]);
        return $this;
    }

    /**
     * @param string $nameCol
     *
     * @return array
     */
    public function toOptionArray($nameCol = 'name')
    {
        return $this->_toOptionArray('entity_id', $nameCol);
    }
}
