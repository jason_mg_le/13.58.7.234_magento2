<?php
namespace Exto\Rma\Model\ResourceModel\Reason;

/**
 * Class Content
 */
class Content extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_reason_content', 'id');
    }
}
