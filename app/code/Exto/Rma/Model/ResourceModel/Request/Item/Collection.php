<?php
namespace Exto\Rma\Model\ResourceModel\Request\Item;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /** @var \Magento\Catalog\Model\ResourceModel\Product */
    protected $_productResource;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $_storeManager;

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface    $entityFactory
     * @param \Psr\Log\LoggerInterface                                     $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface                    $eventManager
     * @param \Magento\Catalog\Model\ResourceModel\Product                 $productResource
     * @param \Magento\Store\Model\StoreManagerInterface                   $storeManager
     * @param \Magento\Framework\DB\Adapter\AdapterInterface               $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb         $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $connection,
            $resource
        );
        $this->_productResource = $productResource;
        $this->_storeManager = $storeManager;
    }

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Request\Item::class, \Exto\Rma\Model\ResourceModel\Request\Item::class);
    }

    /**
     * @return $this
     */
    public function joinProductData()
    {
        $this->getSelect()->joinLeft(
            ['order_item' => $this->getTable('sales_order_item')],
            'order_item.item_id = main_table.order_item_id',
            []
        );
        $this->getSelect()->columns([
            'product_name' => 'order_item.name',
            'product_id' => 'order_item.product_id',
            'product_sku' => 'order_item.sku',
            'product_price' => 'order_item.base_price'
        ]);
        $this->addFilterToMap('product_name', 'order_item.name');
        $this->addFilterToMap('product_id', 'order_item.product_id');
        $this->addFilterToMap('product_sku', 'order_item.sku');
        $this->addFilterToMap('product_price', 'order_item.base_price');
        return $this;
    }

    /**
     * @return $this
     */
    public function joinOrderItem()
    {
        $this->getSelect()->joinLeft(
            ['order_item' => $this->getTable('sales_order_item')],
            'order_item.item_id = main_table.order_item_id',
            []
        );
        return $this;
    }

    /**
     * @param int $requestId
     *
     * @return $this
     */
    public function addRequestFilter($requestId)
    {
        return $this->addFieldToFilter('request_id', ['eq' => $requestId]);
    }
}
