<?php
namespace Exto\Rma\Model\ResourceModel\Request;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Request::class, \Exto\Rma\Model\ResourceModel\Request::class);
    }

    /**
     * @param int $orderId
     *
     * @return $this
     */
    public function addOrderFilter($orderId)
    {
        return $this->addFieldToFilter('order_id', ['eq' => $orderId]);
    }
}
