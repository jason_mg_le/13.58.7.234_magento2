<?php
namespace Exto\Rma\Model\ResourceModel\Request;

/**
 * Class Item
 */
class Item extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_request_item', 'item_id');
    }
}
