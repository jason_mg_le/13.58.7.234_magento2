<?php
namespace Exto\Rma\Model\ResourceModel\Grid\Sales;

/**
 * Class Collection
 */
class Collection extends \Magento\Sales\Model\ResourceModel\Order\Collection
{
    /** @var \Magento\Framework\DB\Helper */
    protected $coreResourceHelperCopy;

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactory                  $entityFactory
     * @param \Psr\Log\LoggerInterface                                          $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface      $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface                         $eventManager
     * @param \Magento\Framework\Model\ResourceModel\Db\VersionControl\Snapshot $entitySnapshot
     * @param \Magento\Framework\DB\Helper                                      $coreResourceHelper
     * @param \Magento\Framework\DB\Adapter\AdapterInterface                    $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb              $resource
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactory $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\Model\ResourceModel\Db\VersionControl\Snapshot $entitySnapshot,
        \Magento\Framework\DB\Helper $coreResourceHelper,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->coreResourceHelperCopy = $coreResourceHelper;
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $entitySnapshot,
            $coreResourceHelper,
            $connection,
            $resource
        );
    }

    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->addFieldToFilter('main_table.status', ['eq' => \Magento\Sales\Model\Order::STATE_COMPLETE]);
        $this->addAvailableItemExistsFilter();
        $this->addAddressFieldsFromGridTable();
        return $this;
    }

    /**
     * @return $this
     */
    protected function addAvailableItemExistsFilter()
    {
        $processedOrderCollection = clone $this;
        $processedOrderCollection->getSelect()->joinLeft(
            ['request' => $processedOrderCollection->getTable('exto_rma_request')],
            'request.order_id = main_table.entity_id',
            []
        )->joinLeft(
            ['request_item' => $processedOrderCollection->getTable('exto_rma_request_item')],
            'request_item.request_id = request.entity_id',
            ['SUM(request_item.qty) AS total_rma_qty']
        );
        $processedOrderCollection->getSelect()->group('main_table.entity_id');

        $this->getSelect()->joinLeft(
            ['qty' => $processedOrderCollection->getSelect()],
            'qty.entity_id = main_table.entity_id',
            []
        );
        return $this;
    }

    /**
     * @throws \Zend_Db_Exception
     * @return void
     */
    protected function addAddressFieldsFromGridTable()
    {
        $this->coreResourceHelperCopy;
        $aliasName = 'sales_order_grid_table';
        $joinTable = $this->getTable('sales_order_grid');

        $this->addFilterToMap(
            'billing_name',
            $aliasName . '.billing_name'
        )->addFilterToMap(
            'shipping_name',
            $aliasName . '.shipping_name'
        )->addFilterToMap(
            'increment_id',
            'main_table.increment_id'
        )->addFilterToMap(
            'created_at',
            'main_table.created_at'
        )->addFilterToMap(
            'store_id',
            'main_table.store_id'
        )->addFilterToMap(
            'base_grand_total',
            'main_table.base_grand_total'
        )->addFilterToMap(
            'grand_total',
            'main_table.grand_total'
        )->addFilterToMap(
            'status',
            'main_table.status'
        );

        $this->getSelect()->joinLeft(
            [$aliasName => $joinTable],
            "main_table.entity_id = {$aliasName}.entity_id",
            [
                $aliasName . '.billing_name',
                $aliasName . '.shipping_name',
            ]
        );
        $this->coreResourceHelperCopy->prepareColumnsList($this->getSelect());
    }
}
