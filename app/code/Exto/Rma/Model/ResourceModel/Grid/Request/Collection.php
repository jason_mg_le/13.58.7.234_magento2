<?php
namespace Exto\Rma\Model\ResourceModel\Grid\Request;

use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;

/**
 * Class Collection
 */
class Collection extends SearchResult
{
    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->addFilterToMap('entity_id', 'main_table.entity_id');
        $this->addFilterToMap('increment_id', 'main_table.increment_id');
        $this->addFilterToMap('customer_email', 'main_table.customer_email');
        $this->addFilterToMap('created_at', 'main_table.created_at');
        $this->addFilterToMap('updated_at', 'main_table.updated_at');

        $this->getSelect()->joinLeft(
            ['customer' => $this->getTable('customer_entity')],
            'customer.entity_id = main_table.customer_id',
            []
        );
        $expression = new \Zend_Db_Expr(
            "CONCAT(customer.firstname, ' ', customer.lastname)"
        );
        $this->getSelect()->columns(['customer_name' => $expression]);
        $this->addFilterToMap('customer_name', "CONCAT(customer.firstname, ' ', customer.lastname)");
        $orderExpression = new \Zend_Db_Expr(
            "CONCAT(order_address.firstname, ' ', order_address.lastname)"
        );
        $this->getSelect()->columns(['order_customer_name' => $orderExpression]);
        $this->addFilterToMap('order_customer_name', "CONCAT(order_address.firstname, ' ', order_address.lastname)");

        $this->getSelect()->joinLeft(
            ['order' => $this->getTable('sales_order')],
            'order.entity_id = main_table.order_id',
            []
        );
        $this->getSelect()->joinLeft(
            ['order_address' => $this->getTable('sales_order_address')],
            'order_address.parent_id = order.entity_id'
            . ' AND order_address.address_type = "' . \Magento\Sales\Model\Order\Address::TYPE_BILLING . '"',
            []
        );
        $this->getSelect()->columns(['order_increment_id' => 'order.increment_id']);
        $this->getSelect()->columns(['store_id' => 'order.store_id']);
        $this->addFilterToMap('order_increment_id', 'order.increment_id');
        $this->addFilterToMap('store_id', 'order.store_id');
        return $this;
    }
}
