<?php
namespace Exto\Rma\Model\ResourceModel\Chat\Message;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Chat\Message::class, \Exto\Rma\Model\ResourceModel\Chat\Message::class);
    }

    /**
     * @return $this
     */
    public function addNotInternalFilter()
    {
        return $this->addFieldToFilter('is_internal', ['eq' => 0]);
    }

    /**
     * @return $this
     */
    public function addInternalFilter()
    {
        return $this->addFieldToFilter('is_internal', ['eq' => 1]);
    }

    /**
     * @param int $requestId
     *
     * @return $this
     */
    public function addRequestIdFilter($requestId)
    {
        return $this->addFieldToFilter('request_id', ['eq' => $requestId]);
    }
}
