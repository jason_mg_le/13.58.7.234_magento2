<?php
namespace Exto\Rma\Model\ResourceModel\Chat;

/**
 * Class Attachment
 */
class Attachment extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_chat_attachment', 'entity_id');
    }
}
