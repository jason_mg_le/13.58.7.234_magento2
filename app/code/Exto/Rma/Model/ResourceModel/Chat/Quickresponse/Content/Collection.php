<?php
namespace Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Exto\Rma\Model\Chat\Quickresponse\Content::class,
            \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content::class
        );
    }

    /**
     * @param int $quickResponseId
     *
     * @return $this
     */
    public function addParentFilter($quickResponseId)
    {
        return $this->addFieldToFilter('parent_id', ['eq' => $quickResponseId]);
    }

    /**
     * @param int $storeId
     *
     * @return $this
     */
    public function addStoreFilter($storeId)
    {
        return $this->addFieldToFilter('store_id', ['eq' => $storeId]);
    }
}
