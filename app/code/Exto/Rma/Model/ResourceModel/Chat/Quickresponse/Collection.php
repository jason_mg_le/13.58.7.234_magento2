<?php
namespace Exto\Rma\Model\ResourceModel\Chat\Quickresponse;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Exto\Rma\Model\Chat\Quickresponse::class,
            \Exto\Rma\Model\ResourceModel\Chat\Quickresponse::class
        );
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('entity_id', 'name');
    }
}
