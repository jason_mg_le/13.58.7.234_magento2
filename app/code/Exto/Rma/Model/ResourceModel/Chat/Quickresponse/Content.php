<?php
namespace Exto\Rma\Model\ResourceModel\Chat\Quickresponse;

/**
 * Class Content
 */
class Content extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_chat_quick_response_content', 'content_id');
    }
}
