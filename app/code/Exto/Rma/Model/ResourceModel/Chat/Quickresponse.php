<?php
namespace Exto\Rma\Model\ResourceModel\Chat;

/**
 * Class Quickresponse
 */
class Quickresponse extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /** @var Quickresponse\Content */
    protected $contentResource;

    /** @var \Exto\Rma\Model\Chat\Quickresponse\ContentFactory */
    protected $contentFactory;

    /** @var Quickresponse\Content\CollectionFactory */
    protected $contentCollectionFactory;

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content $contentResource
     * @param \Exto\Rma\Model\Chat\Quickresponse\ContentFactory $contentFactory
     * @param \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content\CollectionFactory $contentCollectionFactory
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content $contentResource,
        \Exto\Rma\Model\Chat\Quickresponse\ContentFactory $contentFactory,
        \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content\CollectionFactory $contentCollectionFactory
    ) {
        $this->contentFactory = $contentFactory;
        $this->contentResource = $contentResource;
        $this->contentCollectionFactory = $contentCollectionFactory;
        parent::__construct($context);
    }

    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_chat_quick_response', 'entity_id');
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        //save or update labels
        $labelsData = $object->getLabels();
        foreach ($labelsData as $storeId => $label) {
            $this->updateContent($storeId, $object->getId(), $label);
        }
        $this->updateContent(0, $object->getId(), $object->getName());
        return $this;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addParentFilter($object->getId());

        $labels = [];
        foreach ($collection->getItems() as $label) {
            $labels[$label->getStoreId()] = $label->getContent();
        }
        $object->setLabels($labels);
        return parent::_afterLoad($object);
    }

    /**
     * @param int $storeId
     * @param int $parentId
     * @param string $value
     * @return $this
     */
    private function updateContent($storeId, $parentId, $value)
    {
        $item = $this->getCollectionItemByData($storeId, $parentId);
        if ($item->getId()) {
            $item->setContent($value);
            $item->save();
        } else {
            /** @var \Exto\Rma\Model\Custom\Field\Content $contentItem */
            $contentItem = $this->contentFactory->create();
            $contentItem
                ->setParentId($parentId)
                ->setStoreId($storeId)
                ->setContent($value);
            $this->contentResource->save($contentItem);
        }
        return $this;
    }

    /**
     * @param int $storeId
     * @param int $parentId
     * @return \Exto\Rma\Model\Custom\Field\Content
     */
    private function getCollectionItemByData($storeId, $parentId)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addParentFilter($parentId);
        $collection->addStoreFilter($storeId);
        return $collection->getFirstItem();
    }
}
