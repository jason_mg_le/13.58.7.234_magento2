<?php
namespace Exto\Rma\Model\ResourceModel\Chat\Attachment;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Chat\Attachment::class, \Exto\Rma\Model\ResourceModel\Chat\Attachment::class);
    }
}
