<?php
namespace Exto\Rma\Model\ResourceModel\Chat;

/**
 * Class Message
 */
class Message extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_chat_message', 'entity_id');
    }
}
