<?php
namespace Exto\Rma\Model\ResourceModel;

/**
 * Class Request
 */
class Request extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /** @var \Exto\Rma\Helper\Email */
    protected $emailHelper;

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Exto\Rma\Helper\EmailFactory $emailHelperFactory
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Exto\Rma\Helper\EmailFactory $emailHelperFactory
    ) {
        $this->emailHelper = $emailHelperFactory->create();
        parent::__construct($context);
    }

    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_request', 'entity_id');
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     * @return $this
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        if (empty($object->getData('created_at'))) {
            $object->unsetData('created_at');
        }
        parent::_beforeSave($object);
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        parent::_afterSave($object);

        if ($object->isObjectNew()) {
            try {
                $this->emailHelper->newRequestSaved($object);
            } catch (\Exception $e) {

            }
        }
        return $this;
    }
}
