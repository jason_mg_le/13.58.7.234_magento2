<?php
namespace Exto\Rma\Model\ResourceModel;

/**
 * Class Option
 */
class Option extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_option', 'option_id');
    }
}
