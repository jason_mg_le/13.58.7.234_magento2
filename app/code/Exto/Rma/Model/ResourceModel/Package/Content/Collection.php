<?php
namespace Exto\Rma\Model\ResourceModel\Package\Content;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Package\Content::class, \Exto\Rma\Model\ResourceModel\Package\Content::class);
    }

    /**
     * @param int $packageConditionId
     * @return $this
     */
    public function addParentFilter($packageConditionId)
    {
        $this->addFilter('parent_id', ['eq' => $packageConditionId], 'public');
        return $this;
    }

    /**
     * @param int $storeId
     * @return $this
     */
    public function addStoreFilter($storeId)
    {
        $this->addFilter('store_id', ['eq' => $storeId], 'public');
        return $this;
    }
}
