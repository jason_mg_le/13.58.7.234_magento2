<?php
namespace Exto\Rma\Model\ResourceModel\Package;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /** @var bool */
    protected $isIncludeDeleted = false;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Package::class, \Exto\Rma\Model\ResourceModel\Package::class);
    }

    /**
     * @return $this
     */
    protected function _beforeLoad()
    {
        if (!$this->isIncludeDeleted) {
            $this->addFieldToFilter('is_deleted', ['neq' => 1]);
        }
        return parent::_beforeLoad();
    }

    /**
     * @return $this
     */
    public function setIncludeDeletedFlag()
    {
        $this->isIncludeDeleted = true;
        return $this;
    }

    /**
     * @param int $storeId
     *
     * @return $this
     */
    public function joinLabelForStore($storeId)
    {
        $this->getSelect()->joinLeft(
            ['package_label' => $this->getTable('exto_rma_package_condition_content')],
            'package_label.parent_id = main_table.entity_id' .
            ' AND package_label.store_id=' . $storeId,
            []
        );
        $this->getSelect()->joinLeft(
            ['admin_package_label' => $this->getTable('exto_rma_package_condition_content')],
            'admin_package_label.parent_id = main_table.entity_id' .
            ' AND admin_package_label.store_id=0',
            []
        );
        $this->getSelect()->columns([
            'label' => 'IFNULL(package_label.value, admin_package_label.value)'
        ]);
        return $this;
    }

    /**
     * @param string $nameCol
     *
     * @return array
     */
    public function toOptionArray($nameCol = 'name')
    {
        return $this->_toOptionArray('entity_id', $nameCol);
    }
}
