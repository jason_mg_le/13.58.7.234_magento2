<?php
namespace Exto\Rma\Model\ResourceModel\Package;

/**
 * Class Content
 */
class Content extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_package_condition_content', 'id');
    }
}
