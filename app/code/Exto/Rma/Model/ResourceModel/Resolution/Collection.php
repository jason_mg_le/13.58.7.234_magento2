<?php
namespace Exto\Rma\Model\ResourceModel\Resolution;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /** @var bool */
    protected $isIncludeDeleted = false;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Resolution::class, \Exto\Rma\Model\ResourceModel\Resolution::class);
    }

    /**
     * @return $this
     */
    protected function _beforeLoad()
    {
        if (!$this->isIncludeDeleted) {
            $this->addFieldToFilter('is_deleted', ['neq' => 1]);
        }
        return parent::_beforeLoad();
    }

    /**
     * @return $this
     */
    public function setIncludeDeletedFlag()
    {
        $this->isIncludeDeleted = true;
        return $this;
    }

    /**
     * @param int $storeId
     *
     * @return $this
     */
    public function joinLabelForStore($storeId)
    {
        $this->getSelect()->joinLeft(
            ['resolution_label' => $this->getTable('exto_rma_resolution_content')],
            'resolution_label.parent_id = main_table.entity_id' .
            ' AND resolution_label.store_id=' . $storeId,
            []
        );
        $this->getSelect()->joinLeft(
            ['admin_resolution_label' => $this->getTable('exto_rma_resolution_content')],
            'admin_resolution_label.parent_id = main_table.entity_id' .
            ' AND admin_resolution_label.store_id=0',
            []
        );
        $this->getSelect()->columns([
            'label' => 'IFNULL(resolution_label.value, admin_resolution_label.value)'
        ]);
        return $this;
    }

    /**
     * @param string $nameCol
     *
     * @return array
     */
    public function toOptionArray($nameCol = 'name')
    {
        return $this->_toOptionArray('entity_id', $nameCol);
    }
}
