<?php
namespace Exto\Rma\Model\ResourceModel\Resolution\Content;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Exto\Rma\Model\Resolution\Content::class,
            \Exto\Rma\Model\ResourceModel\Resolution\Content::class
        );
    }

    /**
     * @param int $parentId
     * @return $this
     */
    public function addParentFilter($parentId)
    {
        $this->addFilter('parent_id', ['eq' => $parentId], 'public');
        return $this;
    }

    /**
     * @param int $storeId
     * @return $this
     */
    public function addStoreFilter($storeId)
    {
        $this->addFilter('store_id', ['eq' => $storeId], 'public');
        return $this;
    }
}
