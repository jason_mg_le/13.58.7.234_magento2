<?php
namespace Exto\Rma\Model\ResourceModel\Resolution;

/**
 * Class Content
 */
class Content extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_resolution_content', 'id');
    }
}
