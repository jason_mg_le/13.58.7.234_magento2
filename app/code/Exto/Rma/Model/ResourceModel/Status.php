<?php
namespace Exto\Rma\Model\ResourceModel;

/**
 * Class Status
 */
class Status extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /** @var Status\Content */
    protected $contentResource;

    /** @var \Exto\Rma\Model\Status\ContentFactory */
    protected $contentFactory;

    /** @var Status\Content\CollectionFactory */
    protected $contentCollectionFactory;

    /**
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param Status\Content $contentResource
     * @param \Exto\Rma\Model\Status\ContentFactory $contentFactory
     * @param Status\Content\CollectionFactory $contentCollectionFactory
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Exto\Rma\Model\ResourceModel\Status\Content $contentResource,
        \Exto\Rma\Model\Status\ContentFactory $contentFactory,
        \Exto\Rma\Model\ResourceModel\Status\Content\CollectionFactory $contentCollectionFactory
    ) {
        $this->contentFactory = $contentFactory;
        $this->contentResource = $contentResource;
        $this->contentCollectionFactory = $contentCollectionFactory;
        parent::__construct($context);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('exto_rma_status', 'entity_id');
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        //save or update labels
        $labelsData = $object->getLabels();
        if ($labelsData) {
            foreach ($labelsData as $storeId => $label) {
                $this->updateContent($storeId, $object->getId(), \Exto\Rma\Model\Status\Content::TYPE_LABEL, $label);
            }
        }
        $this->updateContent(0, $object->getId(), \Exto\Rma\Model\Status\Content::TYPE_LABEL, $object->getName());

        //save or update corresponding data
        if ($object->getCorrespondingCustomerAction()) {
            foreach ($object->getCorrespondingCustomerAction() as $storeId => $data) {
                $this->updateContent(
                    $storeId,
                    $object->getId(),
                    \Exto\Rma\Model\Status\Content::TYPE_CORRESPONDING_CUSTOMER_ACTION,
                    $data
                );
            }
        }

        if ($object->getConfirmationText()) {
            foreach ($object->getConfirmationText() as $storeId => $data) {
                $this->updateContent(
                    $storeId,
                    $object->getId(),
                    \Exto\Rma\Model\Status\Content::TYPE_CONFIRMATION_TEXT,
                    $data
                );
            }
        }

        return $this;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return $this
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Status\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addParentFilter($object->getId());
        $collection->addTypeFilter(\Exto\Rma\Model\Status\Content::TYPE_LABEL);

        $labels = [];
        foreach ($collection->getItems() as $label) {
            $labels[$label->getStoreId()] = $label->getValue();
        }
        $object->setLabels($labels);

        /** @var \Exto\Rma\Model\ResourceModel\Status\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addParentFilter($object->getId());
        $collection->addExcludeTypeFilter(\Exto\Rma\Model\Status\Content::TYPE_LABEL);

        $correspondingData = [];
        foreach ($collection->getItems() as $item) {
            switch ($item->getType()) {
                case \Exto\Rma\Model\Status\Content::TYPE_CORRESPONDING_CUSTOMER_ACTION:
                    $correspondingData[$item->getStoreId()]['corresponding_customer_action'] = $item->getValue();
                    break;
                case \Exto\Rma\Model\Status\Content::TYPE_CONFIRMATION_TEXT:
                    $correspondingData[$item->getStoreId()]['confirmation_text'] = $item->getValue();
                    break;
            }
        }
        $object->setCorrespondingData($correspondingData);
        return parent::_afterLoad($object);
    }

    /**
     * @param int $storeId
     * @param int $parentId
     * @param int $type
     * @param string $value
     * @return $this
     */
    private function updateContent($storeId, $parentId, $type, $value)
    {
        $item = $this->getCollectionItemByData($storeId, $parentId, $type);
        if ($item->getId()) {
            $item->setValue($value);
            $item->save();
        } else {
            /** @var \Exto\Rma\Model\Status\Content $contentItem */
            $contentItem = $this->contentFactory->create();
            $contentItem
                ->setParentId($parentId)
                ->setStoreId($storeId)
                ->setType($type)
                ->setValue($value);
            $this->contentResource->save($contentItem);
        }
        return $this;
    }

    /**
     * @param int $storeId
     * @param int $parentId
     * @param int $type
     * @return mixed
     */
    private function getCollectionItemByData($storeId, $parentId, $type)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Status\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addParentFilter($parentId);
        $collection->addStoreFilter($storeId);
        $collection->addTypeFilter($type);
        return $collection->getFirstItem();
    }
}
