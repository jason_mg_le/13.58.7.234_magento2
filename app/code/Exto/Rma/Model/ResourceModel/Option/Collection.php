<?php
namespace Exto\Rma\Model\ResourceModel\Option;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Option::class, \Exto\Rma\Model\ResourceModel\Option::class);
    }
}
