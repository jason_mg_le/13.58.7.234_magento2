<?php
namespace Exto\Rma\Model\ResourceModel\Status;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Status::class, \Exto\Rma\Model\ResourceModel\Status::class);
    }

    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->addFieldToFilter('is_deleted', ['neq' => 1]);
        return $this;
    }

    /**
     * @param int $storeId
     *
     * @return $this
     */
    public function joinLabelForStore($storeId)
    {
        $this->getSelect()->joinLeft(
            ['status_label' => $this->getTable('exto_rma_status_content')],
            'status_label.parent_id = main_table.entity_id' .
            ' AND status_label.store_id=' . $storeId,
            []
        );
        $this->getSelect()->joinLeft(
            ['admin_status_label' => $this->getTable('exto_rma_status_content')],
            'admin_status_label.parent_id = main_table.entity_id' .
            ' AND admin_status_label.store_id=0',
            []
        );
        $this->getSelect()->columns([
            'label' => 'IFNULL(status_label.value, admin_status_label.value)'
        ]);
        $this->getSelect()->group('main_table.entity_id');
        return $this;
    }

    /**
     * @param string $nameCol
     *
     * @return array
     */
    public function toOptionArray($nameCol = 'name')
    {
        return $this->_toOptionArray('entity_id', $nameCol);
    }
}
