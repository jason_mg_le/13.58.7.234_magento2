<?php
namespace Exto\Rma\Model\ResourceModel\Status\Content;

/**
 * Class Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Exto\Rma\Model\Status\Content::class, \Exto\Rma\Model\ResourceModel\Status\Content::class);
    }

    /**
     * @param int $parentId
     * @return $this
     */
    public function addParentFilter($parentId)
    {
        $this->addFieldToFilter('parent_id', ['eq' => $parentId]);
        return $this;
    }

    /**
     * @param int $storeId
     * @return $this
     */
    public function addStoreFilter($storeId)
    {
        $this->addFieldToFilter('store_id', ['eq' => $storeId]);
        return $this;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function addTypeFilter($type)
    {
        $this->addFieldToFilter('type', ['eq' => $type]);
        return $this;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function addExcludeTypeFilter($type)
    {
        $this->addFieldToFilter('type', ['neq' => $type]);
        return $this;
    }
}
