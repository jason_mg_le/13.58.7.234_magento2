<?php
namespace Exto\Rma\Model;

/**
 * Class Package
 */
class Package extends \Magento\Framework\Model\AbstractModel
{
    /** @var ResourceModel\Package\CollectionFactory */
    protected $packageCollectionFactory;

    /**
     * @param \Magento\Framework\Model\Context                        $context
     * @param \Magento\Framework\Registry                             $registry
     * @param ResourceModel\Package\CollectionFactory                 $packageCollectionFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb           $resourceCollection
     * @param array                                                   $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Exto\Rma\Model\ResourceModel\Package\CollectionFactory $packageCollectionFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->packageCollectionFactory = $packageCollectionFactory;
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Package::class);
    }

    /**
     * @param int $storeId
     * @param bool $isDeletedIncluded
     *
     * @return string
     */
    public function getLabel($storeId = 0, $isDeletedIncluded = false)
    {
        /** @var ResourceModel\Package\Collection $collection */
        $collection  = $this->packageCollectionFactory->create();
        if ($isDeletedIncluded) {
            $collection->setIncludeDeletedFlag();
        }
        $collection->joinLabelForStore($storeId);
        $collection->addFieldToFilter('main_table.entity_id', ['eq' => $this->getId()]);
        return $collection->getFirstItem()->getData('label');
    }
}
