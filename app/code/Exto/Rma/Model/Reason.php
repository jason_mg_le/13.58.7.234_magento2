<?php
namespace Exto\Rma\Model;

/**
 * Class Reason
 */
class Reason extends \Magento\Framework\Model\AbstractModel
{
    /** @var ResourceModel\Reason\CollectionFactory */
    protected $reasonCollectionFactory;

    /**
     * @param \Magento\Framework\Model\Context                        $context
     * @param \Magento\Framework\Registry                             $registry
     * @param ResourceModel\Reason\CollectionFactory                  $reasonCollectionFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb           $resourceCollection
     * @param array                                                   $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Exto\Rma\Model\ResourceModel\Reason\CollectionFactory $reasonCollectionFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->reasonCollectionFactory = $reasonCollectionFactory;
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Reason::class);
    }

    /**
     * @param int $storeId
     * @param bool $isDeletedIncluded
     *
     * @return string
     */
    public function getLabel($storeId = 0, $isDeletedIncluded = false)
    {
        /** @var ResourceModel\Reason\Collection $collection */
        $collection  = $this->reasonCollectionFactory->create();
        if ($isDeletedIncluded) {
            $collection->setIncludeDeletedFlag();
        }
        $collection->joinLabelForStore($storeId);
        $collection->addFieldToFilter('main_table.entity_id', ['eq' => $this->getId()]);
        return $collection->getFirstItem()->getData('label');
    }
}
