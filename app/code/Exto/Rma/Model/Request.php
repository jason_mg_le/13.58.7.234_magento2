<?php
namespace Exto\Rma\Model;

/**
 * Class Request
 */
class Request extends \Magento\Framework\Model\AbstractModel
{
    const NEW_STATUS_ID = 1;

    /** @var \Magento\Sales\Model\Order */
    protected $order;

    /** @var Reason */
    protected $reason;

    /** @var Package */
    protected $package;

    /** @var \Magento\Framework\Api\DataObjectHelper */
    protected $dataObjectHelper;

    /** @var \Exto\Rma\Api\Data\RequestInterfaceFactory */
    protected $requestDataFactory;

    /** @var \Magento\Framework\Encryption\EncryptorInterface */
    protected $encryptor;

    /** @var \Magento\Framework\Stdlib\DateTime\DateTime */
    protected $date;

    /** @var \Magento\User\Model\ResourceModel\User\CollectionFactory */
    protected $adminCollectionFactory;

    /** @var ResourceModel\Request\CollectionFactory */
    protected $requestCollectionFactory;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Model\Order $order
     * @param Reason $reason
     * @param Package $package
     * @param \Magento\Framework\Api\DataObjectHelper $dataObjectHelper
     * @param \Exto\Rma\Api\Data\RequestInterfaceFactory $requestDataFactory
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\User\Model\ResourceModel\User\CollectionFactory $adminCollectionFactory
     * @param ResourceModel\Request\CollectionFactory $requestCollectionFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Model\Order $order,
        \Exto\Rma\Model\Reason $reason,
        \Exto\Rma\Model\Package $package,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Exto\Rma\Api\Data\RequestInterfaceFactory $requestDataFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\User\Model\ResourceModel\User\CollectionFactory $adminCollectionFactory,
        \Exto\Rma\Model\ResourceModel\Request\CollectionFactory $requestCollectionFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->order = $order;
        $this->reason = $reason;
        $this->package = $package;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->requestDataFactory = $requestDataFactory;
        $this->encryptor = $encryptor;
        $this->date = $date;
        $this->adminCollectionFactory = $adminCollectionFactory;
        $this->requestCollectionFactory = $requestCollectionFactory;
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Request::class);
    }

    /**
     * @return \Exto\Rma\Api\Data\RequestInterface
     */
    public function getDataModel()
    {
        $dataObject = $this->requestDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $dataObject,
            $this->getData(),
            \Exto\Rma\Api\Data\RequestInterface::class
        );
        $dataObject->setId($this->getId());
        return $dataObject;
    }

    /**
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        if (null === $this->order->getId()) {
            $this->order->load($this->getOrderId());
        }
        return $this->order;
    }

    /**
     * @return \Exto\Rma\Model\Reason
     */
    public function getReason()
    {
        if (null === $this->reason->getId()) {
            $this->reason->load($this->getReasonId());
        }
        return $this->reason;
    }

    /**
     * @return \Exto\Rma\Model\Package
     */
    public function getPackageCondition()
    {
        if (null === $this->package->getId()) {
            $this->package->load($this->getPackageConditionId());
        }
        return $this->package;
    }

    /**
     * @return string
     */
    public function generateGuestKey()
    {
        return $this->encryptor->getHash($this->getCustomerEmail());
    }

    /**
     * @return string
     */
    public function generateNewIncrementId()
    {
        /** @var \Exto\Rma\Model\ResourceModel\Request\Collection $collection */
        $collection = $this->requestCollectionFactory->create();
        $collection->addOrderFilter($this->getOrderId());
        $incrementId = $this->getOrder()->getIncrementId();
        $incrementId .= '-' . ($collection->getSize() + 1);
        return $incrementId;
    }

    /**
     * @return $this
     */
    public function beforeSave()
    {
        if (null === $this->getId()) {
            if (null === $this->getStatusId()) {
                $this->setStatusId(\Exto\Rma\Model\Request::NEW_STATUS_ID);
            }
            if (!$this->getOwnerId()) {
                $adminCollection = $this->adminCollectionFactory->create();
                $this->setOwnerId($adminCollection->getFirstItem()->getId());
            }
            if (null === $this->getIncrementId()) {
                $this->setIncrementId($this->generateNewIncrementId());
            }
            if (null === $this->getCreatedAt()) {
                $this->setCreatedAt($this->date->gmtDate());
            }
        }
        $this->setUpdatedAt($this->date->gmtDate());
        return parent::beforeSave();
    }
}
