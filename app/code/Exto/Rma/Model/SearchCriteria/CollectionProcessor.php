<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Rma\Model\SearchCriteria;

use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Data\Collection;
use Magento\Framework\Data\Collection\AbstractDb;

/**
 * Class CollectionProcessor.
 */
class CollectionProcessor implements CollectionProcessorInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(SearchCriteriaInterface $searchCriteria, AbstractDb $collection)
    {
        $this->processFilters($searchCriteria, $collection)
            ->processPagination($searchCriteria, $collection)
            ->processSorting($searchCriteria, $collection);
    }

    /**
     * Process collection filters.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @param AbstractDb $collection
     * @return $this
     */
    private function processFilters(SearchCriteriaInterface $searchCriteria, AbstractDb $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }

        return $this;
    }

    /**
     * Process collection pagination.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @param AbstractDb $collection
     * @return $this
     */
    private function processPagination(SearchCriteriaInterface $searchCriteria, AbstractDb $collection)
    {
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        return $this;
    }

    /**
     * Process collection sorting.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @param AbstractDb $collection
     * @return $this
     */
    private function processSorting(SearchCriteriaInterface $searchCriteria, AbstractDb $collection)
    {
        if (!is_array($searchCriteria->getSortOrders())) {
            return $this;
        }

        /** @var SortOrder $sortOrder */
        foreach ($searchCriteria->getSortOrders() as $sortOrder) {
            $order = $sortOrder->getDirection() == SortOrder::SORT_ASC
                ? Collection::SORT_ORDER_ASC
                : Collection::SORT_ORDER_DESC;
            $collection->addOrder($sortOrder->getField(), $order);
        }

        return $this;
    }

    /**
     * Add Filter Group to the collection
     *
     * @param FilterGroup $filterGroup
     * @param AbstractDb $collection
     * @return void
     */
    private function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        AbstractDb $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }

        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }
}
