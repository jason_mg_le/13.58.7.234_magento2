<?php
namespace Exto\Rma\Model\Package;

/**
 * Class Content
 */
class Content extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Package\Content::class);
    }
}
