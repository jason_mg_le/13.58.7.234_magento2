<?php
namespace Exto\Rma\Model;

/**
 * Class Option
 */
class Option extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Option::class);
    }
}
