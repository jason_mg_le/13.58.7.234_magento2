<?php
namespace Exto\Rma\Model\Custom\Field;

/**
 * Class Value
 */
class Value extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Custom\Field\Value::class);
    }
}
