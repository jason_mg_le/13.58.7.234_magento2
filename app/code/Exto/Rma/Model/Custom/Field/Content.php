<?php
namespace Exto\Rma\Model\Custom\Field;

/**
 * Class Content
 */
class Content extends \Magento\Framework\Model\AbstractModel
{
    const TYPE_LABEL = 1;
    const TYPE_OPTION = 2;

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Custom\Field\Content::class);
    }
}
