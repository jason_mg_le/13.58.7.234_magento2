<?php
namespace Exto\Rma\Model\Custom;

/**
 * Class Field
 */
class Field extends \Magento\Framework\Model\AbstractModel
{
    /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Content\CollectionFactory */
    protected $contentCollectionFactory;

    /**
     * @param \Magento\Framework\Model\Context                              $context
     * @param \Magento\Framework\Registry                                   $registry
     * @param \Exto\Rma\Model\ResourceModel\Custom\Field\Content\CollectionFactory $contentCollectionFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource       $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb                 $resourceCollection
     * @param array                                                         $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Exto\Rma\Model\ResourceModel\Custom\Field\Content\CollectionFactory $contentCollectionFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->contentCollectionFactory = $contentCollectionFactory;
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Custom\Field::class);
    }

    /**
     * @param int $storeId
     *
     * @return array
     */
    public function getOptions($storeId = 0)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addTypeFilter(\Exto\Rma\Model\Custom\Field\Content::TYPE_OPTION);
        $collection->addParentFilter($this->getId());
        $collection->addStoreFilter($storeId);
        $collection->joinAdminLabel();
        $value = $collection->getFirstItem()->getValue();
        if (null === $value && $storeId != 0) {
            $value = $this->getLabel(0);
        }
        return explode("\n", $value);
    }

    /**
     * @param int $storeId
     *
     * @return string
     */
    public function getLabel($storeId = 0)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addTypeFilter(\Exto\Rma\Model\Custom\Field\Content::TYPE_LABEL);
        $collection->addParentFilter($this->getId());
        $collection->addStoreFilter($storeId);
        $collection->joinAdminLabel();
        $value = $collection->getFirstItem()->getValue();
        if (null === $value && $storeId != 0) {
            $value = $this->getLabel(0);
        }
        return $value;
    }
}
