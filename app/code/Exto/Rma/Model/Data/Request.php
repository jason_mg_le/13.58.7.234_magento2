<?php
namespace Exto\Rma\Model\Data;

use \Exto\Rma\Api\Data\RequestInterface;

/**
 * Class Request
 */
class Request extends \Magento\Framework\Api\AbstractExtensibleObject implements RequestInterface
{
    /**
     * Get id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * Get increment id
     *
     * @return int|null
     */
    public function getIncrementId()
    {
        return $this->_get(self::INCREMENT_ID);
    }

    /**
     * Get owner id
     *
     * @return int|null
     */
    public function getOwnerId()
    {
        return $this->_get(self::OWNER_ID);
    }

    /**
     * Get order id
     *
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->_get(self::ORDER_ID);
    }

    /**
     * Get customer id
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * Get customer email
     *
     * @return string|null
     */
    public function getCustomerEmail()
    {
        return $this->_get(self::CUSTOMER_EMAIL);
    }

    /**
     * Get status id
     *
     * @return int|null
     */
    public function getStatusId()
    {
        return $this->_get(self::STATUS_ID);
    }

    /**
     * Get reason id
     *
     * @return int|null
     */
    public function getReasonId()
    {
        return $this->_get(self::REASON_ID);
    }

    /**
     * Get resolution id
     *
     * @return int|null
     */
    public function getResolutionId()
    {
        return $this->_get(self::RESOLUTION_ID);
    }

    /**
     * Get package condition id
     *
     * @return int|null
     */
    public function getPackageConditionId()
    {
        return $this->_get(self::PACKAGE_CONDITION_ID);
    }

    /**
     * Get created at time
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Get updated at time
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return $this->_get(self::UPDATED_AT);
    }

    /**
     * Set id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Set increment id
     *
     * @param int $id
     * @return $this
     */
    public function setIncrementId($id)
    {
        return $this->setData(self::INCREMENT_ID, $id);
    }

    /**
     * Set owner id
     *
     * @param int $id
     * @return $this
     */
    public function setOwnerId($id)
    {
        return $this->setData(self::OWNER_ID, $id);
    }

    /**
     * Set order id
     *
     * @param int $id
     * @return $this
     */
    public function setOrderId($id)
    {
        return $this->setData(self::ORDER_ID, $id);
    }

    /**
     * Set customer id
     *
     * @param int $id
     * @return $this
     */
    public function setCustomerId($id)
    {
        return $this->setData(self::CUSTOMER_ID, $id);
    }

    /**
     * Set customer email
     *
     * @param string $email
     * @return $this
     */
    public function setCustomerEmail($email)
    {
        return $this->setData(self::CUSTOMER_EMAIL, $email);
    }

    /**
     * Set status id
     *
     * @param int $id
     * @return $this
     */
    public function setStatusId($id)
    {
        return $this->setData(self::STATUS_ID, $id);
    }

    /**
     * Set reason id
     *
     * @param int $id
     * @return $this
     */
    public function setReasonId($id)
    {
        return $this->setData(self::REASON_ID, $id);
    }

    /**
     * Set resolution id
     *
     * @param int $id
     * @return $this
     */
    public function setResolutionId($id)
    {
        return $this->setData(self::RESOLUTION_ID, $id);
    }

    /**
     * Set package condition id
     *
     * @param int $id
     * @return $this
     */
    public function setPackageConditionId($id)
    {
        return $this->setData(self::PACKAGE_CONDITION_ID, $id);
    }

    /**
     * Get created at time
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updated at time
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}
