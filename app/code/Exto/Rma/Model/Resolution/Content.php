<?php
namespace Exto\Rma\Model\Resolution;

/**
 * Class Content
 */
class Content extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Resolution\Content::class);
    }
}
