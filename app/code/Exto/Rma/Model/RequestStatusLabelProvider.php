<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Rma\Model;

use Exto\Rma\Model\ResourceModel\Status as StatusResource;
use Exto\RmaApi\Api\RequestStatusLabelProviderInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class RequestStatusLabelProvider.
 */
class RequestStatusLabelProvider implements RequestStatusLabelProviderInterface
{
    /**
     * @var StatusResource
     */
    private $statusResource;

    /**
     * @var StatusFactory
     */
    private $statusFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * RequestStatusLabelProvider constructor.
     * @param StatusResource $statusResource
     * @param StatusFactory $statusFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StatusResource $statusResource,
        StatusFactory $statusFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->statusResource = $statusResource;
        $this->statusFactory = $statusFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatusLabel($statusId, $storeId = null)
    {
        if ($storeId === null) {
            $storeId = $this->storeManager->getStore()->getId();
        }

        $status = $this->getStatusModel($statusId);

        return $status->getId() ? $status->getLabel($storeId) : null;
    }

    /**
     * Get status model.
     *
     * @param int $statusId
     * @return Status
     */
    private function getStatusModel($statusId)
    {
        $status = $this->statusFactory->create();
        $this->statusResource->load($status, $statusId);

        return $status;
    }
}
