<?php
namespace Exto\Rma\Model\Request;

/**
 * Class Item
 */
class Item extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Request\Item::class);
    }
}
