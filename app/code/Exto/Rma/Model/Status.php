<?php
namespace Exto\Rma\Model;

/**
 * Class Status
 */
class Status extends \Magento\Framework\Model\AbstractModel
{
    /** @var ResourceModel\Status\CollectionFactory */
    protected $statusCollectionFactory;

    /**
     * @param \Magento\Framework\Model\Context                        $context
     * @param \Magento\Framework\Registry                             $registry
     * @param ResourceModel\Status\CollectionFactory                  $statusCollectionFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb           $resourceCollection
     * @param array                                                   $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Exto\Rma\Model\ResourceModel\Status\CollectionFactory $statusCollectionFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->statusCollectionFactory = $statusCollectionFactory;
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Status::class);
    }

    /**
     * @param int $storeId
     *
     * @return string
     */
    public function getLabel($storeId = 0)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Status\Collection $collection */
        $collection  = $this->statusCollectionFactory->create();
        $collection->joinLabelForStore($storeId);
        $collection->addFieldToFilter('main_table.entity_id', ['eq' => $this->getId()]);
        return $collection->getFirstItem()->getData('label');
    }
}
