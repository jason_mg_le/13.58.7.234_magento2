<?php
namespace Exto\Rma\Model\Status;

/**
 * Class Content
 */
class Content extends \Magento\Framework\Model\AbstractModel
{
    const TYPE_LABEL = 1;
    const TYPE_CORRESPONDING_CUSTOMER_ACTION = 2;
    const TYPE_CUSTOMER_ACTION_STATUS = 3;
    const TYPE_IS_REQUIRED_CONFIRMATION = 4;
    const TYPE_CONFIRMATION_TEXT = 5;

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Status\Content::class);
    }
}
