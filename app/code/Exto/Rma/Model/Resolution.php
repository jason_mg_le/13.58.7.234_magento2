<?php
namespace Exto\Rma\Model;

/**
 * Class Resolution
 */
class Resolution extends \Magento\Framework\Model\AbstractModel
{
    /** @var ResourceModel\Resolution\CollectionFactory */
    protected $resolutionCollectionFactory;

    /**
     * @param \Magento\Framework\Model\Context                        $context
     * @param \Magento\Framework\Registry                             $registry
     * @param ResourceModel\Resolution\CollectionFactory              $resolutionCollectionFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb           $resourceCollection
     * @param array                                                   $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Exto\Rma\Model\ResourceModel\Resolution\CollectionFactory $resolutionCollectionFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->resolutionCollectionFactory = $resolutionCollectionFactory;
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Resolution::class);
    }

    /**
     * @param int $storeId
     *
     * @return mixed
     */
    public function getLabel($storeId = 0)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Resolution\Collection $collection */
        $collection  = $this->resolutionCollectionFactory->create();
        $collection->joinLabelForStore($storeId);
        $collection->addFieldToFilter('main_table.entity_id', ['eq' => $this->getId()]);
        return $collection->getFirstItem()->getData('label');
    }
}
