<?php
namespace Exto\Rma\Model\Source\Admin;

/**
 * Class User
 */
class User implements \Magento\Framework\Option\ArrayInterface
{
    /** @var array */
    protected $options;

    /** @var \Magento\User\Model\ResourceModel\User\CollectionFactory */
    protected $collectionFactory;

    /**
     * @param \Magento\User\Model\ResourceModel\User\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\User\Model\ResourceModel\User\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * To option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->options) {
            $this->options = [];
            foreach ($this->collectionFactory->create() as $adminUser) {
                /** @var \Magento\User\Model\User $adminUser */
                $this->options[] = [
                    'value' => $adminUser->getId(),
                    'label' => $adminUser->getFirstName() . ' ' . $adminUser->getLastName()
                ];
            }
        }
        return $this->options;
    }
}
