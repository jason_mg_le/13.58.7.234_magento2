<?php
namespace Exto\Rma\Model\Source\Cms;

/**
 * Class Block
 */
class Block implements \Magento\Framework\Option\ArrayInterface
{
    /** @var array */
    protected $options;

    /** @var \Magento\Cms\Model\ResourceModel\Block\CollectionFactory */
    protected $collectionFactory;

    /**
     * @param \Magento\Cms\Model\ResourceModel\Block\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Cms\Model\ResourceModel\Block\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * To option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->options) {
            $this->options = $this->collectionFactory->create()->toOptionArray();
        }
        return $this->options;
    }
}
