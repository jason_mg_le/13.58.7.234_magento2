<?php
namespace Exto\Rma\Model\Source\Field;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Type
 */
class Type implements OptionSourceInterface
{
    const DROPDOWN_VALUE = 1;
    const TEXT_VALUE = 2;
    const TEXTAREA_VALUE = 3;
    const CHECKBOX_VALUE = 4;
    const DATE_VALUE = 5;

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::DROPDOWN_VALUE => __('Drop-down'),
            self::TEXT_VALUE     => __('Text'),
            self::TEXTAREA_VALUE => __('Text area'),
            self::CHECKBOX_VALUE => __('Checkbox'),
            self::DATE_VALUE     => __('Date')
        ];
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $optionArray = self::getOptionArray();
        $result = [];
        foreach ($optionArray as $value => $label) {
            $result[] = ['value' => $value, 'label' => $label];
        }
        return $result;
    }
}
