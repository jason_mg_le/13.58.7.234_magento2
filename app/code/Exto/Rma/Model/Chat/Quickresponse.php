<?php
namespace Exto\Rma\Model\Chat;

/**
 * Class Quickresponse
 */
class Quickresponse extends \Magento\Framework\Model\AbstractModel
{
    /** @var \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content\CollectionFactory */
    protected $contentCollectionFactory;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content\CollectionFactory $contentCollectionFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content\CollectionFactory $contentCollectionFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->contentCollectionFactory = $contentCollectionFactory;
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Chat\Quickresponse::class);
    }

    /**
     * @param int $storeId
     * @return mixed
     */
    public function getContentForStoreId($storeId)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Custom\Field\Content\Collection $collection */
        $collection = $this->contentCollectionFactory->create();
        $collection->addParentFilter($this->getId());
        $collection->addStoreFilter($storeId);
        return $collection->getFirstItem()->getContent();
    }
}
