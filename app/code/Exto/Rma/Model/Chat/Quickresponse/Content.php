<?php
namespace Exto\Rma\Model\Chat\Quickresponse;

/**
 * Class Content
 */
class Content extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Chat\Quickresponse\Content::class);
    }
}
