<?php
namespace Exto\Rma\Model\Chat;

/**
 * Class Message
 */
class Message extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var \Exto\Rma\Model\Chat\Attachment
     */
    protected $attachment;

    /**
     * @param \Magento\Framework\Model\Context                        $context
     * @param \Magento\Framework\Registry                             $registry
     * @param Attachment                                              $attachment
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb           $resourceCollection
     * @param array                                                   $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Exto\Rma\Model\Chat\Attachment $attachment,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->attachment = clone $attachment;
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Chat\Message::class);
    }

    /**
     * @return Attachment
     */
    public function getAttachment()
    {
        if (null === $this->attachment->getId()) {
            $this->attachment->loadByMessageId($this->getId());
        }
        return $this->attachment;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return !!$this->getData('is_admin');
    }
}
