<?php
namespace Exto\Rma\Model\Chat;

/**
 * Class Attachment
 */
class Attachment extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Chat\Attachment::class);
    }

    /**
     * @param int $messageId
     *
     * @return $this
     */
    public function loadByMessageId($messageId)
    {
        return $this->load($messageId, 'message_id');
    }
}
