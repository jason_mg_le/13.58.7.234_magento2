<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Rma\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Exto\RmaApi\Api\Data\RequestInterface;
use Exto\Rma\Api\RequestRepositoryInterface;
use Exto\Rma\Api\Data\RequestSearchResultsInterfaceFactory;
use Exto\Rma\Model\SearchCriteria\CollectionProcessorInterface;
use Exto\Rma\Model\Request as RequestModel;
use Exto\Rma\Model\ResourceModel\Request as ResourceRequest;
use Exto\Rma\Model\ResourceModel\Request\CollectionFactory as RequestCollectionFactory;

/**
 * Class RequestRepository.
 */
class RequestRepository implements RequestRepositoryInterface
{
    /**
     * @var \Exto\Rma\Model\ResourceModel\Request
     */
    protected $resource;

    /**
     * @var \Exto\Rma\Model\RequestFactory
     */
    protected $requestFactory;

    /**
     * @var \Exto\Rma\Model\ResourceModel\Request\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Exto\Rma\Api\Data\RequestSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var \Exto\Rma\Model\SearchCriteria\CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param ResourceRequest $resource
     * @param RequestFactory $requestFactory
     * @param RequestCollectionFactory $collectionFactory
     * @param RequestSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceRequest $resource,
        RequestFactory $requestFactory,
        RequestCollectionFactory $collectionFactory,
        RequestSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->requestFactory = $requestFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public function save(RequestInterface $request)
    {
        try {
            $requestModel = $this->requestFactory->create();
            $requestModel->setId($request->getId());
            $requestModel->addData($request->__toArray());
            $this->resource->save($requestModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $request;
    }

    /**
     * {@inheritdoc}
     */
    public function get($requestId)
    {
        $request = $this->requestFactory->create();
        $request->load($requestId);
        if (!$request->getId()) {
            throw new NoSuchEntityException(__('RMA Request with id "%1" does not exist.', $requestId));
        }
        return $request->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(RequestInterface $request)
    {
        try {
            $requestModel = $this->requestFactory->create();
            $requestModel->addData($request->__toArray());
            $requestModel->setId($request->getId());
            $this->resource->delete($requestModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $requestId
     *
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($requestId)
    {
        return $this->delete($this->get($requestId));
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Exto\Rma\Model\ResourceModel\Request\Collection $collection */
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        $items = array_map(
            function (Request $request) {
                return $request->getDataModel();
            },
            $collection->getItems()
        );

        /** @var \Exto\Rma\Api\Data\RequestSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria)
            ->setItems($items)
            ->setTotalCount($collection->getSize());

        return $searchResults;
    }
}
