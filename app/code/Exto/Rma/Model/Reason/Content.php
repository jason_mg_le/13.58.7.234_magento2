<?php
namespace Exto\Rma\Model\Reason;

/**
 * Class Content
 * @package Exto\Rma\Model\Reason
 */
class Content extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Exto\Rma\Model\ResourceModel\Reason\Content::class);
    }
}
