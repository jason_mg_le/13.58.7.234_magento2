<?php
namespace Exto\Rma\Api\Data;

use Exto\RmaApi\Api\Data\RequestInterface as ApiRequestInterface;

/**
 * RMA request interface.
 */
interface RequestInterface extends ApiRequestInterface
{
}
