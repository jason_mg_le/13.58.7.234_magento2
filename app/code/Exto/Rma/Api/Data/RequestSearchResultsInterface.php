<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Rma\Api\Data;

use Exto\RmaApi\Api\Data\RequestSearchResultsInterface as ApiRequestSearchResultsInterface;

/**
 * Interface for request search results.
 *
 * @api
 */
interface RequestSearchResultsInterface extends ApiRequestSearchResultsInterface
{
    /**
     * Get requests list.
     *
     * @return \Exto\Rma\Api\Data\RequestInterface[]
     */
    public function getItems();

    /**
     * Set requests list.
     *
     * @param \Exto\Rma\Api\Data\RequestInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
