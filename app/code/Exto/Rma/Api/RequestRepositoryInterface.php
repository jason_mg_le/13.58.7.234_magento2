<?php
namespace Exto\Rma\Api;

use Exto\RmaApi\Api\RequestRepositoryInterface as ApiRequestRepositoryInterface;

/**
 * RMA request CRUD interface
 */
interface RequestRepositoryInterface extends ApiRequestRepositoryInterface
{
}
