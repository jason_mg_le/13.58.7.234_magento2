define([
    'jquery'
], function ($) {
    'use strict';
    var checkBtn = function () {
        var isShow = false;
        $('#exto-rma-order-table input[type="checkbox"]').each(function () {
            if ($(this).prop('disabled') || !$(this).prop('checked')) {
                return;
            }
            isShow = true;
        });
        if (isShow) {
            $('button.next').removeProp('disabled');
        } else {
            $('button.next').prop('disabled', 'disabled');
        }
    };

    $('tr.exto-rma-order input').click(function (e) {
        var orderId = $(this).parent().parent().data('order-id');
        $('tr.exto-rma-order-item input').each(function () {
            $(this).prop('disabled', 'disabled');
        });
        $('tr.exto-rma-order-item').each(function () {
            if ($(this).data('parent-order-id') != orderId) {
                $(this).hide();
                return;
            }
            $(this).find('input[type="checkbox"]').each(function () {
                var qty = $(this).parent().parent().find('input[type="text"]:first').val();
                if (qty === 0) {
                    return;
                }
                $(this).removeProp('disabled');
                $(this).removeProp('checked');
            });
            $('button.next').prop('disabled', 'disabled');
            $(this).show();
        });
    });

    $('tr.exto-rma-order-item input[type="checkbox"]').click(function () {
        var isChecked = $(this).prop('checked');
        var textInput = $(this).parent().parent().find('input[type="text"]:first');
        var hiddenInput = $(this).parent().parent().find('input[type="hidden"]:first');
        if (isChecked && textInput.val() > 1) {
            textInput.removeProp('disabled');
        } else {
            textInput.prop('disabled', 'disabled');
        }
        if (isChecked && textInput.val() > 0) {
            hiddenInput.removeProp('disabled');
        } else {
            hiddenInput.prop('disabled', 'disabled');
        }
        checkBtn();
    });

    $('button.next').click(function (e) {
        var validator = $.data($('#exto-rma-add')[0], 'validator');
        $.data($('#exto-rma-add')[0], 'validator', null);
        $('#exto-rma-add').validate({
            onsubmit: false
        });
        var isValid = $('#exto-rma-add').valid();
        $.data($('#exto-rma-add')[0], 'validator', validator);
        if (!isValid) {
            return;
        }
        $('.exto-rma-step-one').hide();
        $('.exto-rma-step-two').show();
        var selectedTr = null;
        $('tr.exto-rma-order input').each(function () {
            if (!$(this).prop('checked')) {
                return;
            }
            selectedTr = $(this).parent().parent();
        });

        $('.exto-rma-step-two .block-title span').html(
            selectedTr.find('.exto-rma-increment-id').html()
        );
        var orderId = selectedTr.data('order-id');
        $('tr.exto-rma-order-item[data-parent-order-id="' + orderId + '"]').each(function () {
            if (!$(this).find('input[type="checkbox"]').prop('checked')) {
                return;
            }
            var tmpl = window.extoRmaOrderItemInfoTmpl;
            tmpl = tmpl.replace('{{product}}', $(this).find('.exto-rma-order-item-product a')[0].outerHTML);
            tmpl = tmpl.replace('{{qty}}', $(this).find('input[type="text"]').val());
            $('.exto-rma-item-list ul').append(tmpl);
        });
        e.preventDefault();
    });

    $('.exto-rma-step-two button.back').click(function (e) {
        $('.exto-rma-step-one').show();
        $('.exto-rma-step-two').hide();
        $('.exto-rma-item-list ul').html('');
        e.preventDefault();
    });

    $('#exto_rma_file_upload-input').change(function (e) {
        if (this.files.length == 0) {
            return;
        }
        var fileName = this.files[0].name;
        $('label[for="exto_rma_file_upload-input"]').hide();
        $('.exto_rma_file_upload-file_info-name').html(fileName);
        $('.exto_rma_file_upload-file_info').show();
    });

    $('.exto_rma_file_upload-file_info-remove').click(function (e) {
        $('#exto_rma_file_upload-input').val('');
        $('.exto_rma_file_upload-file_info').hide();
        $('.exto_rma_file_upload-file_info-name').html('');
        $('label[for="exto_rma_file_upload-input"]').show();
    });

    $('.policy a').click(function (e) {
        var overlay = document.createElement("div");
        var popup = document.createElement("div");
        $(overlay).addClass('exto-rma-overlay');
        $(popup).addClass('exto-rma-popup');
        $(popup).html(window.extoRmaPolicyBlockContent);
        $(overlay).click(function (e) {
            $(popup).remove();
            $(overlay).remove();
            e.preventDefault();
        });
        $('body').append(overlay);
        $('body').append(popup);
        e.preventDefault();
    });

    $('#exto-rma-add').submit(function(){
        var btn = $(this).find('.save');
        if (!$(this).valid()) {
            btn.attr('disabled', null);
            return;
        }
        btn.attr('disabled', 'disabled');
    });
});