require([
    'jquery'
], function ($) {
    'use strict';
    $('#exto_rma_file_upload-input').change(function(e){
        if (this.files.length == 0) {
            return;
        }
        var fileName = this.files[0].name;
        $('label[for="exto_rma_file_upload-input"]').hide();
        $('.exto_rma_file_upload-file_info-name').html(fileName);
        $('.exto_rma_file_upload-file_info').show();
        $('#exto-rma-message-form textarea').addClass('ignore-validate');
    });

    $('.exto_rma_file_upload-file_info-remove').click(function(e){
        $('#exto_rma_file_upload-input').val('');
        $('.exto_rma_file_upload-file_info').hide();
        $('.exto_rma_file_upload-file_info-name').html('');
        $('label[for="exto_rma_file_upload-input"]').show();
        $('#exto-rma-message-form textarea').removeClass('ignore-validate');
    });

    $('.exto-rma-customer-action').click(function(e){
        var confirmText = $(this).attr('data-confirm');
        var url = $(this).attr('data-url');
        var data = {};
        $('.exto-rma-customer-field-box input, ' +
        '.exto-rma-customer-field-box textarea, ' +
        '.exto-rma-customer-field-box select').each(function(){
            var name = $(this).attr('name');
            var value = $(this).val();
            if ($(this).attr('type') === 'checkbox') {
                value = $(this).is(':checked')?'1':'0';
            }
            data[name] = value;
        });
        if (confirmText && !confirm(confirmText)) {
            return;
        }
        var btn = $(this);
        $.ajax({
            type: "POST",
            showLoader: true,
            url: url,
            data: data,
            success: function(json) {
                if (!json.success) {
                    document.location.href = json.redirect;
                    return;
                }

                $('.page-title span').html(json.data.title);
                if (json.data.caption.length > 0) {
                    btn.find('span').html(json.data.caption);
                    btn.show();
                } else {
                    btn.hide();
                }
                if (json.data.confirm.length > 0) {
                    btn.attr('data-confirm', json.data.confirm);
                } else {
                    btn.attr('data-confirm', '');
                }
                if (json.data.message_data.entity_id) {
                    var data = json.data.message_data;
                    var cnt = "<li class='admin'>"
                    + "<h4>" + data.author_name + ", " + data.created_at +"</h4>"
                    + "<div>" + data.content + "</div>"
                    + "</li>";
                    $('.exto_rma_chat_thread').prepend(cnt);
                }
            }
        });
        e.preventDefault();
    });

    $('form textarea[name="message"]').keyup(function(e){
        if (!e.ctrlKey || e.key !== "Enter") {
            return;
        }
        e.preventDefault();
        $(this).parent().submit();
    });
});