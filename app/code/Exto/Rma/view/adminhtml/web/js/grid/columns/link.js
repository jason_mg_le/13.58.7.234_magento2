define([
    'mageUtils',
    'Magento_Ui/js/grid/columns/column'
], function (utils, Column) {
    'use strict';
    return Column.extend({
        defaults: {
            bodyTmpl: 'ui/grid/cells/html'
        },
        getLabel: function (record) {
            record = record[this.index];
            if ('href' in record) {
                return '<a href="' + record['href'] + '" onclick="setLocation(\''
                    + record['href'] + '\')">' + record['label'] + '</a>'
                ;
            }
            return record['label'];
        }
    });
});
