require([
    'jquery',
    'jquery/validate'
], function ($, validate) {
    'use strict';
    $(document).ready(function(){
        var showMessageIntervalId = null;
        var showMsg = function(msg, additionalCssClass) {
            clearTimeout(showMessageIntervalId);
            var container = $('.exto-rma-msg');
            if (container.length === 0) {
                var el = new Element('div');
                $(el).addClass('exto-rma-msg');
                $(el).html(msg);
                $('body')[0].appendChild(el);
                container = $('.exto-rma-msg');
            } else {
                container.attr('class', '');
                container.addClass('exto-rma-msg');
            }
            container.addClass(additionalCssClass);
            container.addClass('exto-rma-msg__show', 500, "swing");
            setTimeout(function(){
                $('.exto-rma-msg').removeClass('exto-rma-msg__show', 500, "swing");
            }, 5000);
        };

        var showSuccessMsg = function() {
            showMsg(window.exto_rma.successMsg, 'exto-rma-msg__success');
        };
        var showErrorMsg = function(msg) {
            showMsg(msg, 'exto-rma-msg__error');
        };

        $('#general_owned_by').change(function(e){
            $.ajax({
                url: window.exto_rma.changeOwnerAction,
                showLoader: true,
                data: {
                    'owner_id': this.getValue()
                },
                success: function(){
                    showSuccessMsg();
                }
            });
        });

        $('#general_status').change(function(e){
            $.ajax({
                url: window.exto_rma.changeStatusAction,
                showLoader: true,
                data: {
                    'status_id': this.getValue()
                },
                success: function(json){
                    if (json.errors) {
                        showErrorMsg(json.message);
                        return;
                    }
                    showSuccessMsg();
                    var btn = $("#exto-rma-admin-action-btn");
                    if (!json.data || json.data.next_id < 1
                        || !Object.isString(json.data.caption)
                        || json.data.caption.trim().length === 0) {
                        btn.hide();
                    } else {
                        btn.html(json.data.caption);
                        btn.attr('data-status-id', json.data.next_id);
                        btn.show();
                    }
                    if (json.data.message_data.entity_id) {
                        var data = json.data.message_data;
                        var cnt = "<li class='admin'>"
                            + "<h4>" + data.author_name
                            + "<span class='exto-rma_message-date'>" + data.created_at +"</span>"
                            + "</h4>"
                            + "<div>"
                            + data.content + "<span class='corner'></span>"
                            + "</div>"
                            + "</li>";
                        $('.exto_rma_chat_thread').prepend(cnt);
                    }
                }
            });
        });

        $('#general_resolution').change(function(e){
            $.ajax({
                url: window.exto_rma.changeResolutionAction,
                showLoader: true,
                data: {
                    'resolution_id': this.getValue()
                },
                success: function(){
                    showSuccessMsg();
                }
            });
        });

        $('.exto_rma_chat_quick_response-wrapper select').change(function(e){
            if (this.getValue() == 0) {
                return;
            }
            $('#exto_rma_chat_message').val(window.exto_rma.quickResponseData[this.getValue()]);
            $(this).val('0');
            /*
            var me = this;
            $.ajax({
                url: window.exto_rma.quickResponseAction,
                showLoader: true,
                data: {
                    'quick_response_id': this.getValue()
                },
                success: function(json){
                    $('#exto_rma_chat_message').val(json.content);
                },
                complete: function(){
                    $(me).val('0');
                }
            });*/
        });

        $('#exto_rma_chat_file_upload-input').change(function(e){
            if (this.files.length == 0) {
                return;
            }
            var fileName = this.files[0].name;
            $('label[for="exto_rma_chat_file_upload-input"]').hide();
            $('.exto_rma_chat_file_upload-file_info-name').html(fileName);
            $('.exto_rma_chat_file_upload-file_info').show();
        });

        $('.exto_rma_chat_file_upload-file_info-remove').click(function(e){
            $('#exto_rma_chat_file_upload-input').val('');
            $('.exto_rma_chat_file_upload-file_info').hide();
            $('.exto_rma_chat_file_upload-file_info-name').html('');
            $('label[for="exto_rma_chat_file_upload-input"]').show();
        });

        var internalChatSubmitFn = function(){
            var validator = $( "#exto_rma_internal_chat-container").validate();
            $('#exto_rma_internal_chat-message').rules('add', {required: true});
            if (!validator.element($('#exto_rma_internal_chat-message'))) {
                return;
            }
            var url = $('#exto_rma_internal_chat-container').data('url');
            $.ajax({
                url: url,
                showLoader: true,
                data: {
                    'message': $('#exto_rma_internal_chat-message').val()
                },
                success: function(json){
                    if (json.errors) {
                        return;
                    }
                    if (json.ajaxRedirect) {
                        setLocation(json.ajaxRedirect);
                        return;
                    }
                    var itemHTML = "<li style='display: none'>"
                        + "<h4>" + json.data.author
                        + "<span class='exto-rma_message-date'>" + json.data.date +"</span>"
                        + "</h4>"
                        + "<div>"
                        + json.data.content + "<span class='corner'></span>"
                        + "</div>" +
                    "</li>";
                    $('#exto_rma_internal_chat-thread').prepend(itemHTML);
                    $('#exto_rma_internal_chat-thread li:first').fadeIn(1000);
                    $('#exto_rma_internal_chat-message').val('');
                }
            });
        };

        $('#exto_rma_internal_chat-container button').click(function(e){
            e.preventDefault();
            internalChatSubmitFn();
        });
        $('#exto_rma_internal_chat-container textarea').keyup(function(e){
            if (!e.ctrlKey || e.key !== "Enter") {
                return;
            }
            e.preventDefault();
            internalChatSubmitFn();
        });

        var chatSubmitFn = function() {
            if (!$('#exto_rma_chat_file_upload-input').val()) {
                var validator = $("#exto_rma_chat_container").validate();
                $('#exto_rma_chat_message').rules('add', {required: true});
                if (!validator.element($('#exto_rma_chat_message'))) {
                    return;
                }
            }
            var url = $('#exto_rma_chat_container').data('url');
            var files = $('#exto_rma_chat_file_upload-input').prop('files');
            var formData = new FormData();
            if (files.length > 0) {
                formData.append('file', files[0]);
            }
            formData.append('message', $('#exto_rma_chat_message').val());
            formData.append('form_key', $('#exto_rma_chat_container input[name="form_key"]').val());
            $.ajax({
                url: url,
                showLoader: true,
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                type: 'post',
                success: function(json){
                    if (json.errors) {
                        return;
                    }
                    if (json.ajaxRedirect) {
                        setLocation(json.ajaxRedirect);
                        return;
                    }
                    var itemHTML = "<li class='admin' style='display: none'>" +
                        "<h4>" + json.data.author +
                        "<span class='exto-rma_message-date'>" + json.data.date +"</span>" +
                        "</h4>" +
                        "<div>" + json.data.content +
                        "<span class='corner'></span>" +
                        "<span class='attachment'>" +
                        "<a href='" + json.data.attachment_url + "'>" + json.data.attachment_name + "</a>" +
                        "</span>" +
                        "</div>" +
                    "</li>";
                    $('.exto_rma_chat_thread').prepend(itemHTML);
                    $('.exto_rma_chat_thread li:first').fadeIn(1000);
                    $('#exto_rma_chat_message').val('');
                    $('.exto_rma_chat_file_upload-file_info-remove').click();
                }
            });
        };
        $('#exto_rma_chat_container button').click(function(e){
            e.preventDefault();
            chatSubmitFn();
        });
        $('#exto_rma_chat_container textarea').keyup(function(e){
            if (!e.ctrlKey || e.key !== "Enter") {
                return;
            }
            e.preventDefault();
            chatSubmitFn();
        });

        $('#exto_rma_request_info_tabs_additional_info_content input, ' +
        '#exto_rma_request_info_tabs_additional_info_content select,' +
        '#exto_rma_request_info_tabs_additional_info_content textarea').change(function(){
            $(this).parents('form').validate().form();
            if ($(this).hasClass('error')) {
                return;
            }
            var id = $(this).attr('name').match(/[0-9]+/)[0];
            var value = $(this).val();
            if ($(this).attr('type') === 'checkbox') {
                value = $(this).is(':checked')?'1':'0';
            }
            $.ajax({
                url: window.exto_rma.fieldChangeAction,
                showLoader: true,
                data: {
                    'id': id,
                    'value': value
                }
            });
        });

        $('#exto-rma-admin-action-btn').click(function(){
            $('#general_status').val($(this).attr('data-status-id'));
            $('#general_status').trigger('change');
        });
    });
});
