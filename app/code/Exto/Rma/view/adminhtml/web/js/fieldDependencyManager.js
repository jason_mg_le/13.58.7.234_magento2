define([
    "jquery",
    "loadingPopup"
], function($){
    'use strict';

    $.fieldDependencyManager = {
        setDependence: function(dependent, parent, values)
        {
            jQuery(parent).change(function(){
                var me = this;
                var result = values.filter(function(el) {
                    return jQuery(me).val().indexOf(el) != -1
                });

                if (result.length == 0) {
                    jQuery(dependent).parent().parent().hide();
                } else {
                    jQuery(dependent).parent().parent().show();
                }
            });

            jQuery(document).ready(function(){
                var result = values.filter(function(el) {
                    return jQuery(parent).val().indexOf(el) != -1
                });
                if (result.length == 0) {
                    jQuery(dependent).parent().parent().hide();
                } else {
                    jQuery(dependent).parent().parent().show();
                }
            });
        },

        setExcludeValueDependence: function(dependent, parent, excludeValues)
        {
            jQuery(parent).change(function(){
                var me = this;
                var result = excludeValues.filter(function(el) {
                    return jQuery(me).val() !== el;
                });

                if (result.length == 0) {
                    jQuery(dependent).parent().parent().hide();
                } else {
                    jQuery(dependent).parent().parent().show();
                }
            });

            jQuery(document).ready(function(){
                var result = excludeValues.filter(function(el) {
                    return jQuery(this).val() !== el;
                });
                if (result.length == 0) {
                    jQuery(dependent).parent().parent().hide();
                } else {
                    jQuery(dependent).parent().parent().show();
                }
            });
        },

        setCheckboxDependence: function(dependent, parentCheckbox, checkedValues)
        {
            jQuery(parentCheckbox).change(function(){
                var me = this;
                var result = checkedValues.filter(function(el) {
                    var checked = '0';
                    if (jQuery(me).is(":checked")) {
                        checked = '1';
                    }
                    return checked.indexOf(el) != -1
                });

                if (result.length == 0) {
                    jQuery(dependent).parent().parent().hide();
                } else {
                    jQuery(dependent).parent().parent().show();
                }
            });

            jQuery(document).ready(function(){
                var result = checkedValues.filter(function(el) {
                    var checked = '0';
                    if (jQuery(parentCheckbox).is(":checked")) {
                        checked = '1';
                    }
                    return checked.indexOf(el) != -1
                });
                if (result.length == 0) {
                    jQuery(dependent).parent().parent().hide();
                } else {
                    jQuery(dependent).parent().parent().show();
                }
            });
        }
    };

    return $.fieldDependencyManager;
});