define([
    'mageUtils',
    'Magento_Ui/js/grid/columns/column'
], function (utils, Column) {
    'use strict';
    return Column.extend({
        defaults: {
            bodyTmpl: 'ui/grid/cells/html'
        },
        getLabel: function (records) {
            records = records[this.index];
            var result = [];
            jQuery.map(records, function(item){
                result.push(
                    '<a href="' + item.href + '" onclick="setLocation(\''
                    + item.href + '\')">' + item.label + '</a>'
                );
            });
            return result.join('<br />');
        }
    });
});
