<?php
namespace Exto\Rma\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @package Exto\Rma\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     *
     * @throws \Zend_Db_Exception
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'status'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_status')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Status ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Name'
        )->addColumn(
            'next_status_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => '0'],
            'Next Status ID'
        )->addColumn(
            'next_status_caption',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Next Status Caption'
        )->addColumn(
            'customer_action_changes_status_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => '0'],
            'Customer action changes status to'
        )->addColumn(
            'is_require_confirmation',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Is require confirmation'
        )->addColumn(
            'is_send_email_to_customer',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Is Send Email To Customer'
        )->addColumn(
            'send_email_to_customer_template_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => false, 'default' => ''],
            'Send Email To Customer Template ID'
        )->addColumn(
            'is_send_email_to_admin',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            [],
            'Is Send Email To Admin'
        )->addColumn(
            'send_email_to_admin_template_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => false, 'default' => ''],
            'Send Email To Admin Template ID'
        )->addColumn(
            'message',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Message'
        )->addColumn(
            'sort_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => '0'],
            'Sort Order'
        )->addColumn(
            'added_by',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            [],
            'Admin name'
        )->addColumn(
            'is_deleted',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'default' => '0'],
            'Is Deleted'
        )->setComment(
            'Status Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'exto_rma_status_content'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_status_content')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Label ID'
        )->addColumn(
            'parent_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Status ID'
        )->addColumn(
            'type',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true],
            'Value Type'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Store ID'
        )->addColumn(
            'value',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Label'
        )->addIndex(
            $installer->getIdxName('exto_rma_status_content', ['parent_id']),
            ['parent_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_status_content', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName('exto_rma_status_content', 'parent_id', 'exto_rma_status', 'entity_id'),
            'parent_id',
            $installer->getTable('exto_rma_status'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('exto_rma_status_content', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Status Content'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'exto_rma_reason'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_reason')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Reason ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Name'
        )->addColumn(
            'sort_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true],
            'Sort Order'
        )->addColumn(
            'is_deleted',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'default' => '0'],
            'Is Deleted'
        )->setComment(
            'Reason Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'exto_rma_reason_content'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_reason_content')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'parent_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Reason ID'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Store ID'
        )->addColumn(
            'value',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Value'
        )->addIndex(
            $installer->getIdxName('exto_rma_reason_content', ['parent_id']),
            ['parent_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_reason_content', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName('exto_rma_reason_content', 'parent_id', 'exto_rma_reason', 'entity_id'),
            'parent_id',
            $installer->getTable('exto_rma_reason'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('exto_rma_reason_content', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Reason Content Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'resolution'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_resolution')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Resolution ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Name'
        )->addColumn(
            'sort_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true],
            'Sort Order'
        )->addColumn(
            'is_deleted',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'default' => '0'],
            'Is Deleted'
        )->setComment(
            'Resolution Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'exto_rma_resolution_content'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_resolution_content')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'parent_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Resolution ID'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Store ID'
        )->addColumn(
            'value',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Value'
        )->addIndex(
            $installer->getIdxName('exto_rma_resolution_content', ['parent_id']),
            ['parent_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_resolution_content', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName('exto_rma_resolution_content', 'parent_id', 'exto_rma_resolution', 'entity_id'),
            'parent_id',
            $installer->getTable('exto_rma_resolution'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('exto_rma_resolution_content', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Resolution Content Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'package_condition'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_package_condition')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Package Condition ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Name'
        )->addColumn(
            'sort_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true],
            'Sort Order'
        )->addColumn(
            'is_deleted',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'default' => '0'],
            'Is Deleted'
        )->setComment(
            'Package Condition Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'exto_rma_package_condition_content'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_package_condition_content')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'parent_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Package Condition ID'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true],
            'Store ID'
        )->addColumn(
            'value',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Value'
        )->addIndex(
            $installer->getIdxName('exto_rma_package_condition_content', ['parent_id']),
            ['parent_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_package_condition_content', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName(
                'exto_rma_package_condition_content',
                'parent_id',
                'exto_rma_package_condition',
                'entity_id'
            ),
            'parent_id',
            $installer->getTable('exto_rma_package_condition'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('exto_rma_package_condition_content', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Package Condition Content Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'request'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_request')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Request ID'
        )->addColumn(
            'increment_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            [],
            'Increment Id'
        )->addColumn(
            'owner_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Owner Id'
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Order ID'
        )->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => true],
            'Customer Id'
        )->addColumn(
            'customer_email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Customer Email'
        )->addColumn(
            'status_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Status ID'
        )->addColumn(
            'reason_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Reason ID'
        )->addColumn(
            'resolution_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Resolution ID'
        )->addColumn(
            'package_condition_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Package Condition ID'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        )->addIndex(
            $installer->getIdxName('exto_rma_request', ['owner_id']),
            ['owner_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_request', ['order_id']),
            ['order_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_request', ['customer_id']),
            ['customer_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_request', ['status_id']),
            ['status_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_request', ['reason_id']),
            ['reason_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_request', ['resolution_id']),
            ['resolution_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_request', ['package_condition_id']),
            ['package_condition_id']
        )->addForeignKey(
            $installer->getFkName('exto_rma_request', 'owner_id', 'admin_user', 'user_id'),
            'owner_id',
            $installer->getTable('admin_user'),
            'user_id'
        )->addForeignKey(
            $installer->getFkName('exto_rma_request', 'order_id', 'sales_order', 'entity_id'),
            'order_id',
            $installer->getTable('sales_order'),
            'entity_id'
        )->addForeignKey(
            $installer->getFkName('exto_rma_request', 'customer_id', 'customer_entity', 'entity_id'),
            'customer_id',
            $installer->getTable('customer_entity'),
            'entity_id'
        )->addForeignKey(
            $installer->getFkName('exto_rma_request', 'status_id', 'exto_rma_status', 'entity_id'),
            'status_id',
            $installer->getTable('exto_rma_status'),
            'entity_id'
        )->addForeignKey(
            $installer->getFkName('exto_rma_request', 'reason_id', 'exto_rma_reason', 'entity_id'),
            'reason_id',
            $installer->getTable('exto_rma_reason'),
            'entity_id'
        )->addForeignKey(
            $installer->getFkName('exto_rma_request', 'resolution_id', 'exto_rma_resolution', 'entity_id'),
            'resolution_id',
            $installer->getTable('exto_rma_resolution'),
            'entity_id'
        )->addForeignKey(
            $installer->getFkName(
                'exto_rma_request',
                'package_condition_id',
                'exto_rma_package_condition',
                'entity_id'
            ),
            'package_condition_id',
            $installer->getTable('exto_rma_package_condition'),
            'entity_id'
        )->setComment(
            'Request Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'request_item'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_request_item')
        )->addColumn(
            'item_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Item ID'
        )->addColumn(
            'request_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => '0'],
            'Request Id'
        )->addColumn(
            'order_item_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => '0'],
            'Order Item Id'
        )->addColumn(
            'qty',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            [],
            'Qty'
        )->addIndex(
            $installer->getIdxName('exto_rma_request_item', ['request_id']),
            ['request_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_request_item', ['order_item_id']),
            ['order_item_id']
        )->addForeignKey(
            $installer->getFkName('exto_rma_request_item', 'request_id', 'exto_rma_request', 'entity_id'),
            'request_id',
            $installer->getTable('exto_rma_request'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('exto_rma_request_item', 'order_item_id', 'sales_order_item', 'item_id'),
            'order_item_id',
            $installer->getTable('sales_order_item'),
            'item_id'
        )->setComment(
            'Request Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'custom_field'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_custom_field')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Label ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Name'
        )->addColumn(
            'type_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Type ID'
        )->addColumn(
            'is_required',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => '0'],
            'Is Required'
        )->addColumn(
            'is_visible_for_customer',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => '0'],
            'Is Visible For Customer'
        )->addColumn(
            'editable_for_customer_in_statuses',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Editable for Customer in Statuses'
        )->addColumn(
            'is_add_to_shipping_label',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => '0'],
            'Is Add To Shipping Label'
        )->addColumn(
            'sort_order',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => '0'],
            'Sort Order'
        )->addColumn(
            'is_deleted',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'default' => '0'],
            'Is Deleted'
        )->setComment(
            'Custom Field Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'exto_rma_custom_field_content'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_custom_field_content')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'parent_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Custom Field ID'
        )->addColumn(
            'type',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true],
            'Value Type'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Store ID'
        )->addColumn(
            'value',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Label'
        )->addIndex(
            $installer->getIdxName('exto_rma_custom_field_content', ['parent_id']),
            ['parent_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_custom_field_content', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName(
                'exto_rma_custom_field_content',
                'parent_id',
                'exto_rma_custom_field',
                'entity_id'
            ),
            'parent_id',
            $installer->getTable('exto_rma_status'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('exto_rma_custom_field_content', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Custom Field Content Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'exto_rma_custom_field_value'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_custom_field_value')
        )->addColumn(
            'value_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'parent_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Custom Field ID'
        )->addColumn(
            'request_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Request ID'
        )->addColumn(
            'value',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Value'
        )->addIndex(
            $installer->getIdxName('exto_rma_custom_field_value', ['parent_id']),
            ['parent_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_custom_field_value', ['request_id']),
            ['request_id']
        )->addForeignKey(
            $installer->getFkName('exto_rma_custom_field_value', 'parent_id', 'exto_rma_custom_field', 'entity_id'),
            'parent_id',
            $installer->getTable('exto_rma_custom_field'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('exto_rma_custom_field_value', 'request_id', 'exto_rma_request', 'entity_id'),
            'request_id',
            $installer->getTable('exto_rma_request'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Custom Field Value Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'chat_message'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_chat_message')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Message ID'
        )->addColumn(
            'request_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Request ID'
        )->addColumn(
            'author_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Author Name'
        )->addColumn(
            'is_admin',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Is Message Posted by Admin'
        )->addColumn(
            'is_internal',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Is Message Posted for admins only'
        )->addColumn(
            'content',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Content'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        )->addIndex(
            $installer->getIdxName('exto_rma_chat_message', ['request_id']),
            ['request_id']
        )->addForeignKey(
            $installer->getFkName('exto_rma_chat_message', 'request_id', 'exto_rma_request', 'entity_id'),
            'request_id',
            $installer->getTable('exto_rma_request'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Chat Messages Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'chat_attachment'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_chat_attachment')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Attachment ID'
        )->addColumn(
            'message_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Message ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Name'
        )->addColumn(
            'path',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Path'
        )->addIndex(
            $installer->getIdxName('exto_rma_chat_attachment', ['message_id']),
            ['message_id']
        )->addForeignKey(
            $installer->getFkName('exto_rma_chat_attachment', 'message_id', 'exto_rma_chat_message', 'entity_id'),
            'message_id',
            $installer->getTable('exto_rma_chat_message'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Chat Attachments Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'chat_quick_response'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_chat_quick_response')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Quick Response ID'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Name'
        )->setComment(
            'Chat Quick Response Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'chat_quick_response_content'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('exto_rma_chat_quick_response_content')
        )->addColumn(
            'content_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Content ID'
        )->addColumn(
            'parent_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Quick Response ID'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Store ID'
        )->addColumn(
            'content',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Content'
        )->addIndex(
            $installer->getIdxName('exto_rma_chat_quick_response_content', ['parent_id']),
            ['parent_id']
        )->addIndex(
            $installer->getIdxName('exto_rma_chat_quick_response_content', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName(
                'exto_rma_chat_quick_response_content',
                'parent_id',
                'exto_rma_chat_quick_response',
                'entity_id'
            ),
            'parent_id',
            $installer->getTable('exto_rma_chat_quick_response'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('exto_rma_chat_quick_response_content', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE,
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment(
            'Quick Response Content Table'
        );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
