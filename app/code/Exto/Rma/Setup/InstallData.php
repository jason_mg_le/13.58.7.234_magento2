<?php
namespace Exto\Rma\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\StoreManagerInterface;
use Exto\Base\Api\ModuleManagementInterface;

/**
 * Class InstallData
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ModuleManagementInterface
     */
    private $moduleManagement;

    /**
     * @var ModuleMetadata
     */
    private $moduleMetadata;

    /**
     * @param StoreManagerInterface $storeManager
     * @param ModuleManagementInterface $moduleManagement
     * @param ModuleMetadata $moduleMetadata
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ModuleManagementInterface $moduleManagement,
        ModuleMetadata $moduleMetadata
    ) {
        $this->storeManager = $storeManager;
        $this->moduleManagement = $moduleManagement;
        $this->moduleMetadata = $moduleMetadata;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $stores = $this->storeManager->getStores();
        $defaultStore = array_shift($stores);

        // insert default statuses
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status'),
            [
                'entity_id'                 => 1, 'name' => 'Pending approval', 'next_status_id' => 2,
                'next_status_caption'       => 'Approve',
                'customer_action_changes_status_id' => 0, 'is_require_confirmation' => 0,
                'is_send_email_to_customer' => 0, 'send_email_to_customer_template_id' => 0,
                'is_send_email_to_admin'    => 1,
                'send_email_to_admin_template_id' => 'exto_rma_email_templates_status_changed',
                'message'                   => '', 'sort_order' => 10,
                'added_by'                  => 'Default'
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 1, 'parent_id' => 1, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => 0, 'value' => 'Pending approval',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 2, 'parent_id' => 1, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => $defaultStore->getId(), 'value' => 'Pending approval',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status'),
            [
                'entity_id'                 => 2, 'name' => 'Approved', 'next_status_id' => 0,
                'next_status_caption'       => '',
                'customer_action_changes_status_id' => 3, 'is_require_confirmation' => 0,
                'is_send_email_to_customer' => 1,
                'send_email_to_customer_template_id' => 'exto_rma_email_templates_status_changed',
                'is_send_email_to_admin'    => 0, 'send_email_to_admin_template_id' => 0,
                'message'                   => '', 'sort_order' => 20,
                'added_by'                  => 'Default'
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 3, 'parent_id' => 2, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => 0, 'value' => 'Approved',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 4, 'parent_id' => 2, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => $defaultStore->getId(), 'value' => 'Approved',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 5,
                'parent_id' => 2,
                'type' => \Exto\Rma\Model\Status\Content::TYPE_CORRESPONDING_CUSTOMER_ACTION,
                'store_id' => 0,
                'value' => 'Package sent',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 6,
                'parent_id' => 2,
                'type' => \Exto\Rma\Model\Status\Content::TYPE_CORRESPONDING_CUSTOMER_ACTION,
                'store_id' => $defaultStore->getId(),
                'value' => 'Package sent',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status'),
            [
                'entity_id'                 => 3, 'name' => 'Package sent', 'next_status_id' => 4,
                'next_status_caption'       => 'Confirm receiving package',
                'customer_action_changes_status_id' => 0, 'is_require_confirmation' => 0,
                'is_send_email_to_customer' => 0, 'send_email_to_customer_template_id' => 0,
                'is_send_email_to_admin'    => 1,
                'send_email_to_admin_template_id' => 'exto_rma_email_templates_status_changed',
                'message'                   => '', 'sort_order' => 30,
                'added_by'                  => 'Default'
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 7, 'parent_id' => 3, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => 0, 'value' => 'Package sent',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 8, 'parent_id' => 3, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => $defaultStore->getId(), 'value' => 'Package sent',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status'),
            [
                'entity_id'                 => 4, 'name' => 'Package received', 'next_status_id' => 5,
                'next_status_caption'       => 'Refund/Exchange',
                'customer_action_changes_status_id' => 0, 'is_require_confirmation' => 0,
                'is_send_email_to_customer' => 1,
                'send_email_to_customer_template_id' => 'exto_rma_email_templates_status_changed',
                'is_send_email_to_admin'    => 0, 'send_email_to_admin_template_id' => 0,
                'message'                   => '', 'sort_order' => 40,
                'added_by'                  => 'Default'
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 9, 'parent_id' => 4, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => 0, 'value' => 'Package received',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 10, 'parent_id' => 4, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => $defaultStore->getId(), 'value' => 'Package received',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status'),
            [
                'entity_id'                 => 5, 'name' => 'Refund/Exchange', 'next_status_id' => 0,
                'next_status_caption'       => '',
                'customer_action_changes_status_id' => 0, 'is_require_confirmation' => 0,
                'is_send_email_to_customer' => 1,
                'send_email_to_customer_template_id' => 'exto_rma_email_templates_status_changed',
                'is_send_email_to_admin'    => 0, 'send_email_to_admin_template_id' => 0,
                'message'                   => '', 'sort_order' => 50,
                'added_by'                  => 'Default'
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 11, 'parent_id' => 5, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => 0, 'value' => 'Refund/Exchange',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 12, 'parent_id' => 5, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => $defaultStore->getId(), 'value' => 'Refund/Exchange',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status'),
            [
                'entity_id'                 => 6, 'name' => 'Closed', 'next_status_id' => 0,
                'next_status_caption'       => '',
                'customer_action_changes_status_id' => 0, 'is_require_confirmation' => 0,
                'is_send_email_to_customer' => 1,
                'send_email_to_customer_template_id' => 'exto_rma_email_templates_status_changed',
                'is_send_email_to_admin'    => 0, 'send_email_to_admin_template_id' => 0,
                'message'                   => '', 'sort_order' => 60,
                'added_by'                  => 'Default'
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 13, 'parent_id' => 6, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => 0, 'value' => 'Closed',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 14, 'parent_id' => 6, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => $defaultStore->getId(), 'value' => 'Closed',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status'),
            [
                'entity_id'                 => 7, 'name' => 'Canceled', 'next_status_id' => 0,
                'next_status_caption'       => '',
                'customer_action_changes_status_id' => 0, 'is_require_confirmation' => 0,
                'is_send_email_to_customer' => 1,
                'send_email_to_customer_template_id' => 'exto_rma_email_templates_status_changed',
                'is_send_email_to_admin'    => 0, 'send_email_to_admin_template_id' => 0,
                'message'                   => '', 'sort_order' => 70,
                'added_by'                  => 'Default'
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 15, 'parent_id' => 7, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => 0, 'value' => 'Canceled',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_status_content'),
            [
                'id' => 16, 'parent_id' => 7, 'type' => \Exto\Rma\Model\Status\Content::TYPE_LABEL,
                'store_id' => $defaultStore->getId(), 'value' => 'Canceled',
            ]
        );
        // insert default reason
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason'),
            [
                'entity_id' => 1, 'name' => 'Wrong size', 'sort_order' => 10
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason_content'),
            [
                'id' => 1, 'parent_id' => 1, 'store_id' => 0, 'value' => 'Wrong size',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason_content'),
            [
                'id' => 2, 'parent_id' => 1, 'store_id' => $defaultStore->getId(), 'value' => 'Wrong size',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason'),
            [
                'entity_id' => 2, 'name' => 'Wrong color', 'sort_order' => 20
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason_content'),
            [
                'id' => 3, 'parent_id' => 2, 'store_id' => 0, 'value' => 'Wrong color',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason_content'),
            [
                'id' => 4, 'parent_id' => 2, 'store_id' => $defaultStore->getId(), 'value' => 'Wrong color',
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason'),
            [
                'entity_id' => 3, 'name' => "Don't like", 'sort_order' => 30
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason_content'),
            [
                'id' => 5, 'parent_id' => 3, 'store_id' => 0, 'value' => "Don't like",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason_content'),
            [
                'id' => 6, 'parent_id' => 3, 'store_id' => $defaultStore->getId(), 'value' => "Don't like",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason'),
            [
                'entity_id' => 4, 'name' => "I've changed my mind", 'sort_order' => 40
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason_content'),
            [
                'id' => 7, 'parent_id' => 4, 'store_id' => 0, 'value' => "I've changed my mind",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason_content'),
            [
                'id' => 8, 'parent_id' => 4, 'store_id' => $defaultStore->getId(), 'value' => "I've changed my mind",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason'),
            [
                'entity_id' => 5, 'name' => 'Item broken', 'sort_order' => 50
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason_content'),
            [
                'id' => 9, 'parent_id' => 5, 'store_id' => 0, 'value' => "Item broken",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_reason_content'),
            [
                'id' => 10, 'parent_id' => 5, 'store_id' => $defaultStore->getId(), 'value' => "Item broken",
            ]
        );
        // insert default resolution
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_resolution'),
            [
                'entity_id' => 1, 'name' => 'Refund', 'sort_order' => 10
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_resolution_content'),
            [
                'id' => 1, 'parent_id' => 1, 'store_id' => 0, 'value' => "Refund",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_resolution_content'),
            [
                'id' => 2, 'parent_id' => 1, 'store_id' => $defaultStore->getId(), 'value' => "Refund",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_resolution'),
            [
                'entity_id' => 2, 'name' => 'Replacement', 'sort_order' => 20
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_resolution_content'),
            [
                'id' => 3, 'parent_id' => 2, 'store_id' => 0, 'value' => "Replacement",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_resolution_content'),
            [
                'id' => 4, 'parent_id' => 2, 'store_id' => $defaultStore->getId(), 'value' => "Replacement",
            ]
        );

        // insert default package condition
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_package_condition'),
            [
                'entity_id' => 1, 'name' => 'Opened', 'sort_order' => 10
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_package_condition_content'),
            [
                'id' => 1, 'parent_id' => 1, 'store_id' => 0, 'value' => "Opened",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_package_condition_content'),
            [
                'id' => 2, 'parent_id' => 1, 'store_id' => $defaultStore->getId(), 'value' => "Opened",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_package_condition'),
            [
                'entity_id' => 2, 'name' => 'Not opened', 'sort_order' => 20
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_package_condition_content'),
            [
                'id' => 3, 'parent_id' => 2, 'store_id' => 0, 'value' => "Not opened",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_package_condition_content'),
            [
                'id' => 4, 'parent_id' => 2, 'store_id' => $defaultStore->getId(), 'value' => "Not opened",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_package_condition'),
            [
                'entity_id' => 3, 'name' => 'Damaged', 'sort_order' => 30
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_package_condition_content'),
            [
                'id' => 5, 'parent_id' => 3, 'store_id' => 0, 'value' => "Damaged",
            ]
        );
        $setup->getConnection()->insertForce(
            $setup->getTable('exto_rma_package_condition_content'),
            [
                'id' => 6, 'parent_id' => 3, 'store_id' => $defaultStore->getId(), 'value' => "Damaged",
            ]
        );

        $setup->endSetup();

        $this->moduleManagement->install($this->moduleMetadata);
    }
}
