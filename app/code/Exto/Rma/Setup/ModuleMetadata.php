<?php
/**
 * Copyright © 2016 Exto. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Exto\Rma\Setup;

use Exto\Base\Api\Data\ModuleMetadataInterface;

/**
 * Class ModuleMetadata.
 */
class ModuleMetadata implements ModuleMetadataInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 'Exto_Rma';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Exto Rma';
    }
}
