<?php
namespace Mgroup\Export\Plugin;
class Csv extends \Aitoc\OrdersExportImport\Model\Converter
{
    protected $_header = ['FirstName', 'LastName', 'Street', 'Phone', 'Estimated time of arrival', 'Product ID', 'Product Name', 'Product SKU', 'QTY', 'Order Grand Total', 'Comment'];
    protected $_allow_col = ['address1:firstname', 'address1:lastname', 'address1:street', 'address1:telephone', 'delivery_date', 'item1:product_id', 'item1:name', 'item1:sku', 'item1:qty_ordered', 'grand_total'];

    public function aroundToFile(
        \Aitoc\OrdersExportImport\Model\Converter\CSV $_csv,
        \Closure $work,
        $filename
    )
    {
        $config = $_csv->getConfig();
        $collection = $_csv->filter();
        if ($config['profile_id'] === "3")
            $csv = $this->appendCollection($_csv, $collection);
        else
            $csv = $_csv->appendCollection($collection);
        if (!$config['path_local']) {
            $config['path_local'] = 'var/tmp/';
        }
        $this->publish->setPath($config['path_local']);
        $this->publish->initCSV($filename);
        foreach ($csv as $elements) {
            $this->publish->writeRow($elements, $config['delimeter'], $config['enclose']);
        }
        $this->publish->destruct();
        return [$this->publish->getPathFile(), $this->publish->getFilename()];
    }

    public function appendCollection($_csv, $collection)
    {
        //First Name、Last Name,Street Address 、Phone Number,Estimated time of arrival,Product SKU、QTY,Order Grand Total、ID、Comment
        $csv = [];
        $counts = round($collection->getSize() / 90, 1);
        $percent = 10;
        $_csv->groupHeaders($collection);
        $csv[0] = $this->_header;
        $keys = $this->_allow_col;
        foreach ($this->partCollection($collection) as $key => $element) {
            $_csv->unsetColumn();
            $_csv->setKeys($keys);
            $_csv->setColumns($_csv->unsetForeignes($element->toArray()));
            foreach ($_csv->getEntities($element) as $label => $collect) {
                if (count($collect)) {
                    if (!isset($collect['inner'])) {
                        foreach ($collect as $code => $lines) {
                            if (count($lines['collection'])) {
                                $inc = 1;
                                foreach ($lines['collection'] as $key => $item) {
                                    $_csv->setColumns($_csv->createNewEntity($item->toArray(), $lines['item'] . $inc));
                                }
                            }
                        }
                    }
                }
            }
            $csv[] = $_csv->getColumns();
            $percent += $counts;
            $stack = $_csv->getStack();
            $stack->setPercent($percent);
            $stack->save();
        }
        return $csv;
    }

    public function beforeSetColumns(\Aitoc\OrdersExportImport\Model\Converter\CSV $subject, $columns)
    {
        $conf = $subject->getConfig();
        if ($conf['profile_id'] !== "3")
            return [$columns];
        $remain_col = [];
        foreach ($this->_allow_col as $allow_col) {
            if (in_array($allow_col, array_keys($columns))) {
                $remain_col[$allow_col] = $columns[$allow_col];
            }
        }
        return [$remain_col];
    }

}