<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */


namespace Mgroup\PartialPayment\Controller\Adminhtml;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;
use Psr\Log\LoggerInterface;
use Magento\Ui\Component\MassAction\Filter;

abstract class Partial extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $_coreRegistry = null;
    protected $logInterface;
    protected $session;
    protected $resourceModel;
    protected $tintervalCollection;
    protected $filter;
    protected $model;
    protected $storeManager;
    protected $date;
    protected $deliveryHelper;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry,
        LoggerInterface $logInterface,
        \Mgroup\PartialPayment\Model\ResourceModel\Partial $resourceModel,
        \Mgroup\PartialPayment\Model\PartialFactory $model,
        \Mgroup\PartialPayment\Model\ResourceModel\Partial\Collection $tintervalCollection,
        Filter $filter,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->logInterface = $logInterface;
        $this->session = $context->getSession();
        $this->resourceModel = $resourceModel;
        $this->tintervalCollection = $tintervalCollection;
        $this->filter = $filter;
        $this->model = $model;
        $this->storeManager = $storeManager;
        $this->date = $date;
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Mgroup_PartialPayment::partialpayment');
        $resultPage->addBreadcrumb(__('Manage Partial Payment'), __('Partial Payment'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Partial Payment'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Mgroup_PartialPayment::partialpayment_list');
    }
}
