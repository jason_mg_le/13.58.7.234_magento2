<?php

namespace Mgroup\PartialPayment\Controller\Adminhtml\Orders;
class Index extends \Mgroup\PartialPayment\Controller\Adminhtml\Partial
{
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        return $resultPage;

    }

}