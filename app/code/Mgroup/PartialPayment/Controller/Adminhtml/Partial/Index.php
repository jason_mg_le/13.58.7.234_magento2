<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */

namespace Mgroup\PartialPayment\Controller\Adminhtml\Partial;


class Index extends \Mgroup\PartialPayment\Controller\Adminhtml\Partial
{

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        return $resultPage;
    }
}
