<?php
namespace Mgroup\PartialPayment\Controller\Adminhtml\Partial;

class NewAction extends \Mgroup\PartialPayment\Controller\Adminhtml\Partial
{

    public function execute()
    {
        $this->_forward('edit');
    }
}
