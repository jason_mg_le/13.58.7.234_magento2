<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */


namespace Mgroup\PartialPayment\Controller\Adminhtml\Partial;

class Edit extends \Mgroup\PartialPayment\Controller\Adminhtml\Partial
{

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create('Mgroup\PartialPayment\Model\Partial');
        if ($id) {
            $this->resourceModel->load($model, $id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This item no longer exists.'));
                return $this->_redirect('mgroup_partialpayment/*');
            }
        }
        $data = $this->session->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        $this->_coreRegistry->register('current_mgroup_partialpayment', $model);
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Mgroup_PartialPayment::partialpayment_list');
        $title = $model->getId() ? __('Edit PartialPayment List') : __('New Partial');
        $resultPage->getConfig()->getTitle()->prepend($title);
        $resultPage->addBreadcrumb($title, $title);
        return $resultPage;

    }
}
