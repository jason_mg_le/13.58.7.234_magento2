<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */

namespace Mgroup\PartialPayment\Controller\Adminhtml\Partial;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Mgroup\PartialPayment\Controller\Adminhtml\Partial
{
    public function execute()
    {
        if ($this->getRequest()->getPostValue()) {
            $data = $this->getRequest()->getPostValue();

            try {
                $model = $this->_objectManager->create('Mgroup\PartialPayment\Model\Partial');

                $id = $this->getRequest()->getParam('entity_id');

                if ($id) {
                    $this->resourceModel->load($model, $id);
                    if ($id != $model->getId()) {
                        throw new LocalizedException(__('The wrong date interval is specified.'));
                    }
                }

                $model->setData($data);
                $this->prepareForSave($model);

                $this->resourceModel->save($model);

                $this->messageManager->addSuccessMessage(__('Record has been successfully saved'));

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('mgroup_partialpayment/*/edit', ['id' => $model->getId()]);
                    return;
                }
                $this->_redirect('mgroup_partialpayment/*/');
                return;

            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $id = (int)$this->getRequest()->getParam('entity_id');
                if (!empty($id)) {
                    $this->_redirect('mgroup_partialpayment/*/edit', ['id' => $id]);
                } else {
                    $this->_redirect('mgroup_partialpayment/*/index');
                }
                return;
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(
                    __('Something went wrong while saving the date interval data. Please review the error log.')
                );
                $this->logInterface->critical($e);
                $this->session->setPageData($data);
                $this->_redirect('mgroup_partialpayment/*/edit', ['id' => $this->getRequest()->getParam('entity_id')]);
                return;
            }
        }
    }

    protected function prepareForSave($model)
    {
        $stores = $model->getData('store_ids');
        if (is_array($stores)) {
            // need commas to simplify sql query
            $model->setData('store_ids', implode(',', $stores));
        } else { // need for null value
            $model->setData('store_ids', '');
        }
        return true;
    }
}