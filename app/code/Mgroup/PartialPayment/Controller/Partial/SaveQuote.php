<?php

namespace Mgroup\PartialPayment\Controller\Partial;
class SaveQuote extends \Magento\Framework\App\Action\Action
{
    private $_orderFactory;
    private $_listPartialPayment;
    protected $_currency;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Directory\Model\Currency $currencySymbol,
        \Mgroup\PartialPayment\Helper\Data $helper)
    {
        parent::__construct($context);
        $this->resultFactory = $resultJsonFactory;
        $this->_orderFactory = $quoteFactory;
        $this->_listPartialPayment = $helper;
        $this->_currency = $currencySymbol;
    }

    public function execute()
    {
        $json = $this->resultFactory->create();
        if (!$this->getRequest()->isAjax()) {
            $json->setData(['error' => 1]);
            return $json;
        }
        try {
            $data = $this->getRequest()->getParams();
            $order = $this->_orderFactory->create()->load($data['order_id']);
            $order->setPartials($data['p_num']);
            $order->save();
            $json->setData(['error' => 0]);
            return $json;
        } catch (\Exception $exception) {
        }
        $json->setData(['error' => 1]);
        return $json;
    }

}