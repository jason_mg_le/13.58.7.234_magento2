<?php

namespace Mgroup\PartialPayment\Controller\Partial;
class ListPartial extends \Magento\Framework\App\Action\Action
{
    private $_orderFactory;
    private $_listPartialPayment;
    protected $_currency;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Directory\Model\Currency $currencySymbol,
        \Mgroup\PartialPayment\Helper\Data $helper)
    {
        parent::__construct($context);
        $this->resultFactory = $resultJsonFactory;
        $this->_orderFactory = $quoteFactory;
        $this->_listPartialPayment = $helper;
        $this->_currency = $currencySymbol;
    }

    public function execute()
    {
        $json = $this->resultFactory->create();
        if (!$this->getRequest()->isAjax()) {
            $json->setData([]);
            return $json;
        }
        try {
            $data = $this->getRequest()->getParams();
            $store = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface')->getStore();
            $order = $this->_orderFactory->create()->load($data['order_id']);
            $listPartial = $this->_listPartialPayment->getPartials($store->getStoreId());
            $arrPartial = [];
            $index = 0;
            foreach ($listPartial as $item => $value) {
                if ($item > $order->getBaseSubtotal())
                    continue;
                $arrPartial[$index]['num'] = $value;
                if ($order->getPartials())
                    $arrPartial[$index]['isChecked'] = $value == $order->getPartials();

                //Process Label
                $subTotal = $order->getBaseSubtotal();
                if ($subTotal > 0) {
                    $_overbalance = round($subTotal % $value);
                    $_balance = round($subTotal / $value);
                    $first = $_balance + $_overbalance;
                    if ($_balance == $first)
                        $arrPartial[$index]['label'] = __('%3, %2%1 per period', $_balance, $this->_currency->getCurrencySymbol(), $value);
                    else
                        $arrPartial[$index]['label'] = __('%4, First: %3%1 per period, the other: %3%2 per period', $first, $_balance, $this->_currency->getCurrencySymbol(), $value);
                }
                ++$index;
            }
            $json->setData($arrPartial);
            return $json;
        } catch (\Exception $exception) {
        }
        $json->setData([]);
        return $json;
    }

}