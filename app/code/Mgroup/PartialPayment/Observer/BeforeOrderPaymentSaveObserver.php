<?php

namespace Mgroup\PartialPayment\Observer;
use Magento\Framework\Event\ObserverInterface;

class BeforeOrderPaymentSaveObserver implements ObserverInterface
{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();
        $order->setPartials($quote->getPartials());
    }
}