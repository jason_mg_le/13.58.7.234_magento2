<?php

namespace Mgroup\PartialPayment\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.0', '<')) {

            $table = $setup->getConnection()->newTable(
                $setup->getTable('mgroup_partialpayment')
            )->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                8,
                ['unsigned' => true, 'identity' => true, 'nullable' => false, 'primary' => true]
            )->addColumn(
                'store_ids',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false]
            )->addColumn(
                'order_total_limit',
                \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
                255,
                ['nullable' => false]
            )->addColumn(
                'partial',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                255,
                ['nullable' => false]
            );
            $setup->getConnection()->createTable($table);
        }
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $tableName = $setup->getTable('sales_order');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'partials',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => true,
                        'comment' => 'Partials.',
                    ],
                    null
                );
            }
            $tableName = $setup->getTable('quote');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'partials',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => true,
                        'comment' => 'Partials.',
                    ],
                    null
                );
            }

        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $tableName = $setup->getTable('sales_order_grid');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'partials',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => true,
                        'comment' => 'Installment',
                    ],
                    null
                );
            }
        }
        $setup->endSetup();
    }
}
