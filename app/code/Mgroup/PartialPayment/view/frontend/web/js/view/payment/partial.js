/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        'ko',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'mage/url'
    ],
    function ($, ko, Component, quote, url) {
        'use strict';

        var totals = quote.getTotals(),
            partial_num = ko.observable(null),
            partials = ko.observable(null),
            isApplied = ko.observable(partial_num() != '');

        if (totals()) {
            partial_num(totals()['partial_num']);
        }
        return Component.extend({
            defaults: {
                template: 'Mgroup_PartialPayment/payment/partial'
            },
            partialNum: partial_num,
            partials: partials,
            isApplied: isApplied,

            initialize: function () {
                this.listPartial();
                return this._super();
            },
            bindClick: function()
            {
                var _self = this;
                $('#partial-form input[type="radio"]').live('click',function(e){
                    _self.updateData($(e.target).val());
                });
            },
            updateData: function (value) {
                console.log('dsdsa');
                this.partialNum(value);
                var _self = this;
                var _url = url.build('mgroup_partialpayment/partial/saveQuote');
                $.ajax({
                    dataType: 'json',
                    url: _url,
                    cache: true,
                    data: {order_id: quote.getQuoteId(), p_num: _self.partialNum()},
                    type: 'post',
                    showLoader: true
                });
            },
            listPartial: function () {
                var _self = this;
                var _url = url.build('mgroup_partialpayment/partial/listPartial');
                $.ajax({
                    dataType: 'json',
                    url: _url,
                    cache: true,
                    data: {order_id: quote.getQuoteId()},
                    type: 'post',
                    success: function (result) {
                        _self.partials(result);
                    }
                });
            },
            validate: function () {
                var form = '#partial-form';
                return $(form).validation() && $(form).validation('isValid');
            }
        });
    }
);
