<?php

namespace Mgroup\PartialPayment\Helper;

use Magento\Framework\Session\Storage;
use Magento\Store\Model\Store;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    private $objectManager;

    private $session;

    private $storeManager;

    private $checkoutSession;

    private $productCollectionFactory;

    private $partialFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        Storage $sessionStorage,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Mgroup\PartialPayment\Model\PartialFactory $partialFactory)
    {
        parent::__construct($context);
        $this->objectManager = $objectManager;
        $this->session = $sessionStorage;
        $this->storeManager = $storeManager;
        $this->checkoutSession = $checkoutSession;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->partialFactory = $partialFactory;
    }


    public function moduleEnabled()
    {
        return (bool)$this->scopeConfig->getValue('partialpayment/partial/is_enabled');
    }


    /**
     * @param int $currentStore
     *
     * @return array
     */
    public function getPartials($currentStore = 0)
    {
        $collection = $this->partialFactory->create()->getCollection();

        foreach ($collection as $partial) {
            $storeIds = trim($partial->getData('store_ids'), ',');
            $storeIds = explode(',', $storeIds);
            if (!in_array($currentStore, $storeIds) && !in_array(0, $storeIds)) {
                continue;
            }
            $value = $partial->getData('partial');
            $_partial[$partial->getData('order_total_limit')] = $value;
        }
        return $_partial;
    }

    public function getDefaultScopeValue($path)
    {
        return $this->scopeConfig->getValue('partialpayment/' . $path);
    }


    public function getWebsiteScopeValue($path, $website = null)
    {
        return $this->scopeConfig->getValue(
            'partialpayment/' . $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE,
            $website
        );
    }

    public function getStoreScopeValue($path, $store = null)
    {
        return $this->scopeConfig->getValue(
            'partialpayment/' . $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }
}
