<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */

namespace Mgroup\PartialPayment\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\VersionControl\AbstractDb;

class Partial extends AbstractDb
{

    protected function _construct()
    {
        $this->_init('mgroup_partialpayment', 'entity_id');
    }
}
