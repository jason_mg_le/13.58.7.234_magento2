<?php
namespace Mgroup\PartialPayment\Model;

class Partial extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Mgroup\PartialPayment\Model\ResourceModel\Partial');
        $this->setIdFieldName('entity_id');
    }
}
