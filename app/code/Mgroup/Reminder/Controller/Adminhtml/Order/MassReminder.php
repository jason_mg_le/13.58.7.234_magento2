<?php

namespace Mgroup\Reminder\Controller\Adminhtml\Order;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magecomp\Deleteorder\Model\OrderFactory;
use Magento\Framework\Exception\LocalizedException;
use Mgroup\Reminder\Helper\Data as Helper;

class MassReminder extends \Magento\Backend\App\Action
{

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    protected $_modelOrderFactory;
    protected $_remineHelper;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory, OrderFactory $modelOrderFactory, \Mgroup\Reminder\Helper\Data $data)
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->_modelOrderFactory = $modelOrderFactory;
        $this->_remineHelper = $data;
        parent::__construct($context);
    }

    /**
     * Reminder action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        $collectionSize = $collection->getSize();
        $emailSent = 0;
        foreach ($collection as $item) {
            try {
                $this->_remineHelper->send($item);
                $emailSent++;
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                break;
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Some emails were not sent.'));
                break;
            }
        }
        $this->messageManager->addSuccess(__('A total of %1/%2 email(s) is sent .', $emailSent, $collectionSize));

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('sales/order/index');
    }
}
