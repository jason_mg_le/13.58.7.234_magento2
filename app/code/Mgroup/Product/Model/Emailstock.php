<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */

namespace Mgroup\Product\Model;

class Emailstock extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Mgroup\Product\Model\ResourceModel\Partial');
        $this->setIdFieldName('entity_id');
    }
}
