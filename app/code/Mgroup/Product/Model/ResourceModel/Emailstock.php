<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */

namespace Mgroup\Product\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\VersionControl\AbstractDb;

class Emailstock extends AbstractDb
{

    protected function _construct()
    {
        $this->_init('mgroup_product_emailstock', 'entity_id');
    }
}
