<?php

namespace Mgroup\Product\Model\Config\Source\Email;

class Identity implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Email Identity options
     *
     * @var array
     */
    protected $_options = null;

    /**
     * Configuration structure
     *
     * @var \Magento\Config\Model\Config\Structure
     */
    protected $_configStructure;

    protected $_emailStock;
    protected $_storeManager;

    /**
     * @param \Magento\Config\Model\Config\Structure $configStructure
     */
    public function __construct(\Magento\Config\Model\Config\Structure $configStructure,
                                \Mgroup\Product\Model\EmailstockFactory $model,
                                \Magento\Store\Model\StoreManagerInterface $storeManager)
    {
        $this->_configStructure = $configStructure;
        $this->_emailStock = $model;
        $this->_storeManager = $storeManager;
    }

    /**
     * Retrieve list of options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->_options === null) {
            $this->_options = [];
            $collection = $this->_emailStock->create()->getCollection();
            foreach ($collection as $item) {
                $storeIds = trim($item->getData('store_ids'), ',');
                $storeIds = explode(',', $storeIds);
                if (!in_array($this->getStoreId(), $storeIds) && !in_array(0, $storeIds)) {
                    continue;
                }

                $this->_options[] = [
                    'value' => $item->getData('entity_id'),
                    'label' => $item->getData('email_name')
                ];

            }
            ksort($this->_options);
        }
        return $this->_options;
    }

    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }
}
