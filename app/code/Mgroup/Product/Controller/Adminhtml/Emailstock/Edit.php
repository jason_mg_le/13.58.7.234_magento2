<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */


namespace Mgroup\Product\Controller\Adminhtml\Emailstock;

class Edit extends \Mgroup\Product\Controller\Adminhtml\Partial
{

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create('Mgroup\Product\Model\Partial');

        if ($id) {
            $this->resourceModel->load($model, $id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This item no longer exists.'));
                return $this->_redirect('mgroup_product/*');
            }
        }
        // set entered data if was error when we do save
        $data = $this->session->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        $this->_coreRegistry->register('current_mgroup_product_emailstock', $model);

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Mgroup_Product::emailstock_list');

        $title = $model->getId() ? __('Edit EmailStock List') : __('New Email Group');
        $resultPage->getConfig()->getTitle()->prepend($title);
        $resultPage->addBreadcrumb($title, $title);

        return $resultPage;

    }
}
