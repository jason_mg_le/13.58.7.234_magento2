<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */


namespace Mgroup\Product\Controller\Adminhtml\Emailstock;

class NewAction extends \Mgroup\Product\Controller\Adminhtml\Partial
{

    public function execute()
    {
        $this->_forward('edit');
    }
}
