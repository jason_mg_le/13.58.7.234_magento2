<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */


namespace Mgroup\Product\Controller\Adminhtml;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;
use Psr\Log\LoggerInterface;
use Magento\Backend\Model\Session;
use Magento\Ui\Component\MassAction\Filter;

abstract class Emailstock extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;
    /**
     * @var LoggerInterface
     */
    protected $logInterface;
    /**
     * @var Session
     */
    protected $session;
    /**
     * @var \Amasty\Deliverydate\Model\ResourceModel\Emailstock
     */
    protected $resourceModel;
    /**
     * @var \Amasty\Deliverydate\Model\ResourceModel\Tinterval\Collection
     */
    protected $tintervalCollection;
    /**
     * @var Filter
     */
    protected $filter;
    /**
     * @var \Amasty\Deliverydate\Model\EmailStock
     */
    protected $model;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;
    /**
     * @var \Amasty\Deliverydate\Helper\Data
     */
    protected $deliveryHelper;

    /**
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry,
        LoggerInterface $logInterface,
        \Mgroup\Product\Model\ResourceModel\Partial $resourceModel,
        \Mgroup\Product\Model\EmailstockFactory $model,
        \Mgroup\Product\Model\ResourceModel\Emailstock\Collection $tintervalCollection,
        Filter $filter,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Mgroup\Product\Helper\Data $deliveryHelper
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->logInterface = $logInterface;
        $this->session = $context->getSession();
        $this->resourceModel = $resourceModel;
        $this->tintervalCollection = $tintervalCollection;
        $this->filter = $filter;
        $this->model = $model;
        $this->storeManager = $storeManager;
        $this->date = $date;
        $this->deliveryHelper = $deliveryHelper;
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Mgroup_Product::emailstock');
        $resultPage->addBreadcrumb(__('Manage Email Group Stock Alert'), __('Email Group'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Email Alert Group'));

        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Mgroup_Product::emailstock_list');
    }
}
