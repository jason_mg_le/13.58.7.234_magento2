<?php

namespace Mgroup\Shipping\Block\Order\Item\Renderer;
class DefaultRenderer extends \Magento\Sales\Block\Order\Item\Renderer\DefaultRenderer
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Model\Product\OptionFactory $productOptionFactory,
        array $data = []
    )
    {
        parent::__construct($context, $string, $productOptionFactory, $data);
    }
    public function getItemSerialsHtml($item = null)
    {
        $block = $this->getLayout()->getBlock('item_unit_serials');
        if (!$item) {
            $item = $this->getItem();
        }
        $block->setItem($item);
        return $block->toHtml();
    }
}