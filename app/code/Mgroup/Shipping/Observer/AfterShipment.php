<?php

namespace Mgroup\Shipping\Observer;

use Magento\Framework\Event\ObserverInterface;

class AfterShipment implements ObserverInterface
{
    private $_request;
    protected $order;

    public function __construct(
        \Magento\Sales\Api\Data\OrderInterface $order,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->order = $order;
        $this->_request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $shipment_data = $this->_request->getParam('shipment');
        $shipment = $observer->getEvent()->getShipment();
        $shipment->setData("ship_partno", $shipment_data['ship_partno']);
        $shipment->setData("delivery_date", $shipment_data['delivery_date']);
        $shipment->setData("deadline_rma", $shipment_data['deadline_rma']);
        $shipment->setData("date_of_arrival", $shipment_data['date_of_arrival']);

        if (key_exists('serial', $shipment_data)) {
            $serials = $shipment_data['serial'];
            $items = $shipment->getItems();
            $order_serials = [];
            foreach ($items as $item) {
                if (in_array($item->getOrderItemId(), array_keys($serials))) {

                    $data_serials = $serials[$item->getOrderItemId()];

                    if (!is_array($data_serials))
                        array_push($order_serials, $data_serials);
                    else
                        $order_serials = array_merge($order_serials, $data_serials);

                    if (!is_array($data_serials))
                        $serial_text = $data_serials;
                    else
                        $serial_text = trim(implode(",", $data_serials), ",");

                    $item->setData('serials', $serial_text);
                    $item->getOrderItem()->setData('serials', $serial_text);
                }
            }
            $shipment->getOrder()->setSerials(trim(implode(",", $order_serials), ","));
        }
    }
}