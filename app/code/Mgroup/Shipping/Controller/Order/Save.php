<?php

/**
 * Created by PhpStorm.
 * User: trongkhoa1505
 * Date: 27/10/2017
 * Time: 00:00
 */

namespace Mgroup\Shipping\Controller\Order;
class Save extends \Magento\Framework\App\Action\Action
{

    protected $shipmentLoader;
    protected $labelGenerator;
    protected $shipmentSender;
    private $_orderFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader,
        \Magento\Shipping\Model\Shipping\LabelGenerator $labelGenerator,
        \Magento\Sales\Model\Order\Email\Sender\ShipmentSender $shipmentSender,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory
    )
    {
        $this->shipmentLoader = $shipmentLoader;
        $this->labelGenerator = $labelGenerator;
        $this->shipmentSender = $shipmentSender;
        parent::__construct($context);
        $this->resultFactory = $resultJsonFactory;
        $this->_orderFactory = $orderFactory;
    }

    public function execute()
    {
        $json = $this->resultFactory->create();
        if (!$this->getRequest()->isAjax()) {
            $json->setData(['error' => 1]);
            return $json;
        }
        try {
            $data = $this->getRequest()->getParams();
            $order = $this->_orderFactory->create()->load($data['id']);
            $shipment = $order->getShipmentsCollection();
            foreach ($shipment as $item) {
                $item->setData('date_of_arrival', $data['date_of_arrival']);
                $item->setData('deadline_rma', $data['deadline_rma']);
                $item->save();
                break;
            }
            $order->setData('delivery_date', $data['time_of_arrival']);
            $order->setData('visible_in_storefront', $data['isvisible']=='true');
            $order->save();
            if ($data['isnotify']) {
                $emailSender = $this->_objectManager->create('\Magento\Sales\Model\Order\Email\Sender\OrderSender');
                $emailSender->send($order);
            }
            $json->setData(['error' => 0]);
            return $json;
        } catch (\Exception $exception) {
        }
        $json->setData(['error' => 1]);
        return $json;
    }

}