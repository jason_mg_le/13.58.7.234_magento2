<?php

/**
 * Created by PhpStorm.
 * User: trongkhoa1505
 * Date: 26/10/2017
 * Time: 22:28
 */

namespace Mgroup\Shipping\Controller\Adminhtml;
use Magento\Framework\App\Action\Context;
use Magento\Store\Model\StoreManagerInterface;
abstract class Admin extends \Magento\Backend\App\Action
{
    protected $resultJsonFactory;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->resultJsonFactory = $resultJsonFactory;
    }


    protected function _initAction()
    {
        $resultPage = $this->resultJsonFactory->create();
        return $resultPage;
    }


}