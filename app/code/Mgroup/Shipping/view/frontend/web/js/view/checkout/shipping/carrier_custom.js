define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'mage/url'
], function ($, ko, Component, quote, url) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Mgroup_Shipping/checkout/shipping/carrier_custom',
            time_of_arrival: null
        },
        initObservable: function () {
            this._super()
                .observe('time_of_arrival');
            var _self = this;
            var _url = url.build('deliverydate/time/index');
            this.selectedMethod = ko.computed(function () {
                var method = quote.shippingMethod();
                var selectedMethod = method != null ? method.carrier_code + '_' + method.method_code : null;

                $.ajax({
                    dataType: 'json',
                    url: _url,
                    cache: true,
                    type: 'post',
                    success: function (result) {
                        _self.time_of_arrival(result);
                    },
                    error: function () {
                    }
                });
                return selectedMethod;
            }, this);

            return this;
        },

    });
});