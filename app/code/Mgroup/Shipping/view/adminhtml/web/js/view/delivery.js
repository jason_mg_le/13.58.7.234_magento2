define([
    'jquery',
    'ko',
    'uiComponent',
    'mage/url',
    "jquery/ui",
    'Magento_Ui/js/modal/modal',
    "mage/calendar"
], function ($, ko, Component, url, modal) {
    'use strict';

    return Component.extend({
        defaults: {
            time_of_arrival: null,
            time_of_arrivals: null,
            delivery_date: cur_delivery,
            date_of_arrival: cur_dateofarrival,
            deadline_rma: cur_deadlinerma,
            shippartno: cur_shippartno,
            isnotify: false,
            isvisible: isvisible,
            order_status: order_status_completed,
            _canSave: false
        },
        initObservable: function () {
            this._super()
                .observe('time_of_arrivals')
                .observe('date_of_arrival')
                .observe('time_of_arrival')
                .observe('delivery_date')
                .observe('deadline_rma')
                .observe('shippartno')
                .observe('isnotify')
                .observe('isvisible')
                .observe('_canSave');
            var _self = this;
            var _url = url.build('deliverydate/time/index');
            $.ajax({
                dataType: 'json',
                url: _url,
                cache: true,
                type: 'post',
                success: function (result) {
                    _self.time_of_arrivals(result);
                    _self.time_of_arrival(cur_esttime);
                },
                error: function () {
                },
                complete: function () {
                    _self._canSave(false);
                }
            });

            return this;
        },
        initialize: function () {
            this._super();
            var _self = this;
            ko.bindingHandlers.calendar = {
                init: function (element, valueAccessor, allBindingsAccessor) {
                    var $el = $(element);
                    //initialize datepicker with some optional options
                    var options = {
                        minDate: 0,
                        dateFormat: "yy-mm-d",
                        showsTime: false,
                        changeYear: true,
                        changeMonth: true,
                        yearRange: "1970:2050",
                        buttonText: "Select Date",
                        closeText: 'Clear'
                    };
                    $el.calendar(options);
                    var writable = valueAccessor();
                    if (!ko.isObservable(writable)) {
                        var propWriters = allBindingsAccessor()._ko_property_writers;
                        if (propWriters && propWriters.datepicker) {
                            writable = propWriters.datepicker;
                        } else {
                            return;
                        }
                    }
                    writable($(element).calendar("getDate"));
                },
                update: function (element, valueAccessor) {
                    var widget = $(element).data("DateTimePicker");
                    if (_self.date_of_arrival()) {
                        var date = new Date(_self.date_of_arrival());
                        date.setDate(date.getDate() + 100);
                        var date_text = $.datepicker.formatDate('yy-mm-dd', date);
                        _self.deadline_rma(date_text);
                    }
                    //when the view model is updated, update the widget
                    if (widget) {
                        var date = ko.utils.unwrapObservable(valueAccessor());
                        widget.date(date);
                    }
                }
            };
            this.date_of_arrival.subscribe(function () {
                _self._canSave(true)
            }, null, 'change');
            this.time_of_arrival.subscribe(function () {
                _self._canSave(true)
            }, null, 'change');
            this.isnotify.subscribe(function () {
                _self._canSave(true)
            }, null, 'change');
            this.isvisible.subscribe(function () {
                _self._canSave(true)
            }, null, 'change');
            return this;
        },
        submit: function () {
            this._canSave(false);
            this._submit();
        },
        _submit: function () {
            var _self = this;
            var _url = url.build('mgroup_shipping/order/save');
            $.ajax({
                dataType: 'json',
                url: _url,
                data: {
                    id: order_id,
                    date_of_arrival: _self.date_of_arrival(),
                    time_of_arrival: _self.time_of_arrival(),
                    deadline_rma: _self.deadline_rma(),
                    isnotify: _self.isnotify(),
                    isvisible: _self.isvisible()
                },
                cache: false,
                type: 'post',
                showLoader: true, // enable loader
                success: function (result) {
                    if (!result.error) {
                        _self._canSave(false);
                    }
                },
                error: function () {
                }
            });
            return this;
        },
        canSave: function () {
            return this._canSave();
        },
        isShowShipPartNo: function () {
            var t = this.shippartno() != '';
            return t;
        }
    });
});