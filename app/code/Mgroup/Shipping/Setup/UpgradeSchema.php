<?php
namespace Mgroup\Shipping\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var string
     */
    private static $connectionName = 'sales';

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $tableName = $setup->getTable('sales_shipment_grid');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'ship_partno',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Ship PartNo',
                        'default' => null,
                        'afters' => 'customer_name'
                    ],
                    null
                );
            }
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $tableName = $setup->getTable('sales_shipment');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'ship_partno',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Ship PartNo',
                        'default' => null,
                    ],
                    null
                );
            }
        }
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $tableName = $setup->getTable('sales_shipment');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'deadline_rma',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                        'nullable' => true,
                        'comment' => 'Dealine RMA',
                    ],
                    null
                );
                $connection->addColumn(
                    $tableName,
                    'delivery_date',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                        'nullable' => true,
                        'comment' => 'Delivery Date',
                    ],
                    null
                );
            }
        }
        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            $tableName = $setup->getTable('sales_order_item');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'serials',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Serials No.',
                    ],
                    null
                );
            }
        }
        if (version_compare($context->getVersion(), '1.0.7', '<')) {
            $tableName = $setup->getTable('sales_shipment_item');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'serials',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Serials No.',
                    ],
                    null
                );
            }
        }
        if (version_compare($context->getVersion(), '1.0.8', '<')) {
            $tableName = $setup->getTable('sales_order');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'serials',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Serials No.',
                    ],
                    null
                );
            }
        }
        if (version_compare($context->getVersion(), '1.0.9', '<')) {
            $tableName = $setup->getTable('sales_order_grid');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'serials',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Serials No.',
                    ],
                    null
                );
            }
        }
        if (version_compare($context->getVersion(), '1.1.0', '<')) {
            $tableName = $setup->getTable('quote');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'serials',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Serials No.',
                    ],
                    null
                );
            }
        }
        if (version_compare($context->getVersion(), '1.1.1', '<')) {
            $tableName = $setup->getTable('sales_shipment');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'date_of_arrival',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Date of Arrival',
                    ],
                    null
                );
            }
            $tableName = $setup->getTable('sales_shipment_grid');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'date_of_arrival',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment' => 'Date of Arrival',
                    ],
                    null
                );
            }
        }
        if (version_compare($context->getVersion(), '1.1.2', '<')) {
            $tableName = $setup->getTable('sales_order');
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->addColumn(
                    $tableName,
                    'visible_in_storefront',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                        'nullable' => true,
                        'comment' => 'Visible In StoreFront',
                    ],
                    null
                );
            }
        }
        $installer->endSetup();

    }

}
