<?php
/**
 * Created by PhpStorm.
 * User: trongkhoa1505
 * Date: 25/10/2017
 * Time: 11:16
 */

namespace Mgroup\Shipping\Plugin;

class CustomShipping
{
    public function arroundCollectRates(
        \MagePsycho\Customshipping\Model\Carrier\Customshipping $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Address\RateRequest $request
    )
    {
        $items = $request->getAllItems();
        $false = false;
        foreach ($items as $item) {
            if ($item->getProduct()->getEnableEstTimeOfArrival()) {
                $false = true;
            }
        }
        if (!$false)
            return false;
        return $proceed();
    }
}