<?php
/**
 * Created by PhpStorm.
 * User: trongkhoa1505
 * Date: 23/10/2017
 * Time: 23:33
 */

namespace Mgroup\Shipping\Plugin;


class DefaultRendererPlugin
{
    public function aroundGetColumnHtml(\Magento\Sales\Block\Adminhtml\Order\View\Items\Renderer\DefaultRenderer $defaultRenderer, \Closure $proceed, \Magento\Framework\DataObject $item, $column, $field = null)
    {
        if ($column == 'serials') {
            $html = $item->getSerials();
            $result = $html;
        } else {
            if ($field) {
                $result = $proceed($item, $column, $field);
            } else {
                $result = $proceed($item, $column);

            }
        }
        return $result;
    }

    public function afterGetColumns(\Magento\Sales\Block\Adminhtml\Order\View\Items\Renderer\DefaultRenderer $defaultRenderer, $result)
    {
        $result['serials'] = 'col-serials';
        return $result;
    }
}