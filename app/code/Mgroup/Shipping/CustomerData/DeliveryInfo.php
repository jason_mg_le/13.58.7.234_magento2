<?php

namespace Mgroup\Shipping\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;

/**
 * Cart source
 */
class DeliveryInfo extends \Magento\Framework\DataObject implements SectionSourceInterface
{
    protected $_checkoutSession;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    )
    {
        parent::__construct($data);
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        $quote = $this->_checkoutSession->getQuote();
        return [
            'delivery_date' => $quote->getDeliveryDate()
        ];
    }

}
