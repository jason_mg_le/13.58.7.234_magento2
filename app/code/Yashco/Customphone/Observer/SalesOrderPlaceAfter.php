<?php

namespace Yashco\Customphone\Observer;


use Magento\Framework\Event\ObserverInterface;

class SalesOrderPlaceAfter implements ObserverInterface
{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getOrder();
            $address = $order->getShippingAddress();
            if ($address) $phone = $address->getTelephone();
            $order->setPhoneNumber($phone);
            $order->save();
        } catch (\Exception $e) {
        }
    }

}