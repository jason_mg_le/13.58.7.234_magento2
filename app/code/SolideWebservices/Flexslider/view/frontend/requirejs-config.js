var config = {
    paths: {
        'solidewebservices/flexslider': 'SolideWebservices_Flexslider/js/jquery.flexslider-min',
        'solidewebservices/picturefill': 'SolideWebservices_Flexslider/js/picturefill.min',
        'solidewebservices/easing': 'SolideWebservices_Flexslider/js/jquery.easing.1.3',
        'solidewebservices/hoverintent': 'SolideWebservices_Flexslider/js/jquery.hoverIntent',
        'solidewebservices/fitvids': 'SolideWebservices_Flexslider/js/jquery.fitvids',
        'solidewebservices/vimeo': 'SolideWebservices_Flexslider/js/jquery.vimeo.api.min'
    },
    shim: {
        'solidewebservices/flexslider': {
            deps: ['jquery']
        },
        'solidewebservices/picturefill': {
            deps: ['jquery']
        },
        'solidewebservices/easing': {
            deps: ['jquery']
        },
        'solidewebservices/hoverintent': {
            deps: ['jquery']
        },
        'solidewebservices/fitvids': {
            deps: ['jquery']
        },
        'solidewebservices/vimeo': {
            deps: ['jquery']
        }
    }
};
