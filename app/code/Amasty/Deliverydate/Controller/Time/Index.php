<?php

namespace Amasty\Deliverydate\Controller\Time;

use Magento\Framework\App\Action\Context;
use Magento\Store\Model\StoreManagerInterface;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_timeHelper;
    protected $resultJsonFactory;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        \Amasty\Deliverydate\Helper\Data $helper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->_timeHelper = $helper;
        $this->resultJsonFactory = $resultJsonFactory;
    }


    public function execute()
    {

        $arrs = $this->_timeHelper->getTIntervals();
        $data = [];
        foreach ($arrs as $key => $value) {
            if (!empty($key)) {
                $data[] = array('name' => $value,'type' => $value);
            }
        }
        $result = $this->resultJsonFactory->create();
        $result = $result->setData($data);
        return $result;
    }

}