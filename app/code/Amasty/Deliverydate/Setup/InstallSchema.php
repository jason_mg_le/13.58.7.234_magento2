<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Deliverydate
 */


namespace Amasty\Deliverydate\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'amasty_amdeliverydate_tinterval'
         */

        $table = $installer->getConnection()->newTable(
            $installer->getTable('amasty_amdeliverydate_tinterval')
        )->addColumn(
            'tinterval_id',
            Table::TYPE_INTEGER,
            8,
            ['unsigned' => true, 'identity' => true, 'nullable' => false, 'primary' => true]
        )->addColumn(
            'store_ids',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false]
        )->addColumn(
            'time_from',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false]
        )->addColumn(
            'time_from_sql',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false]
        )->addColumn(
            'time_to',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false]
        )->addColumn(
            'time_to_sql',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false]
        )->addColumn(
            'sorting_order',
            Table::TYPE_SMALLINT,
            5,
            ['unsigned' => true, 'nullable' => false]
        )->addColumn(
            'quota',
            Table::TYPE_INTEGER,
            10,
            ['unsigned' => true, 'default' => null]
        );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
