<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Block;

use Magento\Framework\View\Element\Template;

class Ourquote extends Template
{

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->pageConfig->getTitle()->set(__('Quick RFQ'));
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('home', [
            'label' => __('Home'),
            'title' => __('Home'),
            'link' => $this->getBaseUrl()
                ]
        );
        $breadcrumbs->addCrumb('rfq', [
            'label' => __('Quick RFQ'),
            'title' => __('Quick RFQ'),
                ]
        );
    }

    /* 
     * Get form action
     */
    public function getFormAction() {
        return $this->getUrl('ourquote/quote/save');
    }
    /**
     * Get base url with store code
     */
    public function getBaseUrl() {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

}