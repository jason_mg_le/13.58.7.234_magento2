<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Block;

class Link extends \Magento\Framework\View\Element\Html\Link
{

    protected $_template = 'Solwin_Ourquote::link.phtml';

    /* 
     * Get RFQ url
     */
    public function getHref() {
        return $this->getUrl('ourquote');
    }

    /* 
     * Get RFQ label
     */
    public function getLabel() {
        return __('Quick RFQ\'s');
    }

}