<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Block\Adminhtml\Quote\Edit\Tab;

class Quote extends \Magento\Backend\Block\Widget\Form\Generic
implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Solwin\Ourquote\Model\BudgetStatus
     */
    protected $_budgetstatus;

    /**
     * @var \Solwin\Ourquote\Model\ChangeStatus
     */
    protected $_changestatus;

    /**
     * @var \Solwin\Ourquote\Helper\Data
     */
    protected $_helper;
    protected $_storeFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Solwin\Ourquote\Helper\Data $helper
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param \Solwin\Ourquote\Model\BudgetStatus $budgetstatus
     * @param \Solwin\Ourquote\Model\ChangeStatus $changestatus
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Solwin\Ourquote\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Solwin\Ourquote\Model\BudgetStatus $budgetstatus,
        \Solwin\Ourquote\Model\ChangeStatus $changestatus,
        \Magento\Store\Model\StoreFactory $storeFactory,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_helper = $helper;
        $this->_budgetstatus = $budgetstatus;
        $this->_changestatus = $changestatus;
        $this->_storeFactory = $storeFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm() {
        /** @var \Solwin\Ourquote\Model\Quote $quote */
        $quote = $this->_coreRegistry->registry('solwin_ourquote_quote');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('quote_');
        $form->setFieldNameSuffix('quote');
        $fieldset = $form->addFieldset(
                'base_fieldset', [
            'legend' => __('Quote Information'),
            'class' => 'fieldset-wide'
                ]
        );
        $fieldset->addType(
                'file',
                'Solwin\Ourquote\Block\Adminhtml\Quote\Helper\File'
                );
        if ($quote->getId()) {
            $fieldset->addField(
                    'quote_id', 'hidden', ['name' => 'quote_id']
            );
        }

        $fieldset->addField(
                'change_status', 'select', [
            'label' => __('Change Status'),
            'title' => __('Change Status'),
            'name' => 'change_status',
            'required' => true,
            'options' => $this->_changestatus->getOptionArray(),
                ]
        );

        $fieldset->addField(
                'created_at', 'label', [
            'name' => 'created_at',
            'label' => __('Quote requested on'),
            'title' => __('Quote requested on'),
            'required' => false,
            'note' => '<span>(YYYY-MM-DD H:M:S)</span>'
                ]
        );

        $fieldset->addField(
                'company', 'text', [
            'name' => 'company',
            'label' => __('Company'),
            'title' => __('Company'),
                ]
        );
        $fieldset->addField(
                'contact_name', 'text', [
            'name' => 'contact_name',
            'label' => __('Contact Name'),
            'title' => __('Contact Name'),
            'required' => true,
                ]
        );
        $fieldset->addField(
                'phone', 'text', [
            'name' => 'phone',
            'label' => __('Phone'),
            'title' => __('Phone'),
                ]
        );
        $fieldset->addField(
                'email', 'text', [
            'name' => 'email',
            'label' => __('Email Address'),
            'title' => __('Email Address'),
            'required' => false,
            'after_element_html' => '<a href="mailto:' . $quote->getEmail() 
                . '?subject=' . $this->_helper
                ->getConfigValue('quotesection/customergroup/subject')
                . '&body=' . $this->_helper
                ->getConfigValue('quotesection/customergroup/message')
                . '" style="text-decoration: none;"><small>(Send Email)</small>'
                . '</a>'
                ]
        );

        if ($quote->getProductId() != 0) {
            $prodUrlkey = $this->_helper->getProductUrl($quote->getProductId());
            $prodExt = $this->_helper->getCatalogSeo();
            $productUrl = self::getBaseUrl() . $prodUrlkey . $prodExt;
            $prodSku = $this->_helper->getProductSku($quote->getProductId());
            $fieldset->addField(
                    '', 'label', [
                'name' => '',
                'label' => __('RFQ of Product'),
                'title' => __('RFQ of Product'),
                'required' => false,
                'after_element_html' => $prodSku . ' <a href="' . $productUrl 
                . '" style="text-decoration: none;" target="_blank">'
                . '<small>(View Product)</small></a>'
                    ]
            );
        }
        
        if ($quote->getCustomerId() != 0) {
            $customerId = $quote->getCustomerId();
            $customerName = $this->_helper
                    ->getCustomerName($customerId);
            $customerUrl = $this->getUrl(
                    'customer/index/edit',['id' => $customerId]);
            
            $fieldset->addField(
                    'customer', 'label', [
                'name' => '',
                'label' => __('Customer Name'),
                'title' => __('Customer Name'),
                'required' => false,
                'after_element_html' => $customerName . ' <a href="' . $customerUrl 
                . '" style="text-decoration: none;" target="_blank">'
                . '<small>(View Customer)</small></a>'
                    ]
                    
            );
        } else {
            $fieldset->addField(
                    'customer', 'label', [
                'name' => '',
                'label' => __('Customer'),
                'title' => __('Customer'),
                'required' => false,
                'after_element_html' => '<span style="float:left;'
                        . 'line-height:32px;">'
                        . 'Guest Customer</span>'
                    ]
            );
        }
        
        
        if ($quote->getStoreId() != 0) {
            $storeId = $quote->getStoreId();
            
            $storeName = $this->_storeFactory->create()->load($storeId)->getName();
            
            $fieldset->addField(
                    'store', 'label', [
                'name' => '',
                'label' => __('Store Name'),
                'title' => __('Store Name'),
                'required' => false,
                'after_element_html' => $storeName
                    ]
                    
            );
        }
        
        
        
        

        $fieldset->addField(
                'date', 'label', [
            'name' => 'date',
            'label' => __('Date Quote Needed by the Client'),
            'title' => __('Date Quote Needed by the Client'),
            'required' => false,
            'note' => '<span>(YYYY-MM-DD H:M:S)</span>'
                ]
        );

        $fieldset->addField(
                'project_title', 'text', [
            'name' => 'project_title',
            'label' => __('Project Title'),
            'title' => __('Project Title'),
            'required' => false
                ]
        );

        if ($quote->getUploaddoc()) {
            $attachedFile = $this->_helper
                    ->getBaseUrl() . 'pub/media/ourquote'
                    . $quote->getUploaddoc();
            $fieldset->addField(
                    'uploaddoc', 'label', [
                'name' => 'uploaddoc',
                'label' => __('Attached File'),
                'title' => __('Attached File'),
                'required' => false,
                'note' => '<a href="' . $attachedFile 
                . '" style="text-decoration: none;" target="_blank">'
                . '<small>(Open File)</small></a>'
                    ]
            );
        }

        $fieldset->addField(
                'budget_status', 'select', [
            'label' => __('Budget Status'),
            'title' => __('Budget Status'),
            'name' => 'budget_status',
            'required' => true,
            'options' => $this->_budgetstatus->getOptionArray(),
                ]
        );

        $fieldset->addField(
                'overview', 'editor', [
            'name' => 'description',
            'label' => __('Brief Overview'),
            'title' => __('Brief Overview'),
            'required' => true,
            'config' => $this->_wysiwygConfig->getConfig()
                ]
        );


        $quoteData = $this->_session
                ->getData('solwin_ourquote_quote_data', true);
        if ($quoteData) {
            $quote->addData($quoteData);
        } else {
            if (!$quote->getId()) {
                $quote->addData($quote->getDefaultValues());
            }
        }
        $form->addValues($quote->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return __('General');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return $this->getTabLabel();
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab() {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden() {
        return false;
    }

}