<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Block\Adminhtml\Quote\Edit;

/**
 * @method Tabs setTitle(\string $title)
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{

    /**
     * constructor
     *
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('quote_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Quote Information'));
    }

}