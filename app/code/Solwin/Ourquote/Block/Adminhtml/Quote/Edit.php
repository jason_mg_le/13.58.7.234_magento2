<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Block\Adminhtml\Quote;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     * 
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * constructor
     * 
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize Quote edit block
     *
     * @return void
     */
    protected function _construct() {
        $this->_objectId = 'quote_id';
        $this->_blockGroup = 'Solwin_Ourquote';
        $this->_controller = 'adminhtml_quote';
        parent::_construct();
        $this->buttonList->update('save', 'label', __('Save Quote'));
        $this->buttonList->add(
                'save-and-continue', [
            'label' => __('Save and Continue Edit'),
            'class' => 'save',
            'data_attribute' => [
                'mage-init' => [
                    'button' => [
                        'event' => 'saveAndContinueEdit',
                        'target' => '#edit_form'
                    ]
                ]
            ]
                ], -100
        );
        $this->buttonList->update('delete', 'label', __('Delete Quote'));
    }

    /**
     * Retrieve text for header element depending on loaded Quote
     *
     * @return string
     */
    public function getHeaderText() {
        /** @var \Solwin\Ourquote\Model\Quote $quote */
        $quote = $this->_coreRegistry->registry('solwin_ourquote_quote');
        if ($quote->getId()) {
            return __(
                    "Edit Quote '%1'", 
                    $this->escapeHtml($quote->getContact_name())
                    );
        }
        return __('New Quote');
    }

}