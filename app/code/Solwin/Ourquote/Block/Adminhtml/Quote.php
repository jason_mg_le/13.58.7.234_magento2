<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Block\Adminhtml;

class Quote extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * constructor
     *
     * @return void
     */
    protected function _construct() {
        $this->_controller = 'adminhtml_quote';
        $this->_blockGroup = 'Solwin_Ourquote';
        $this->_headerText = __('Quotes');
        $this->_addButtonLabel = __('Create New Quote');
        parent::_construct();
    }

}