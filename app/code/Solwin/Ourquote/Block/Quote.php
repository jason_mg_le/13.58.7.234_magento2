<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Block;

use Solwin\Ourquote\Model\QuoteFactory;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

class Quote extends \Magento\Customer\Block\Account\Dashboard
{
    /**
     * @var QuoteFactory
     */
    protected $_quoteFactory;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $customerAccountManagement,
        QuoteFactory $quoteFactory,
        array $data = []
    ) {
        
        parent::__construct($context, $customerSession, $subscriberFactory, $customerRepository, $customerAccountManagement, $data);
        $this->_quoteFactory = $quoteFactory;
        $this->pageConfig->getTitle()->set(__('RFQ Information'));
    }
    
    
    public function getQuoteData($custId) {
        
        $quoteCollection = $this->_quoteFactory->create()
                ->getCollection()
                ->addFieldToFilter('customer_id',$custId);
        return $quoteCollection;
        
    }
    
    public function getSingleQuoteCollection($quoteId) {
        
        $quoteModel = $this->_quoteFactory->create();
        $quoteColl = $quoteModel->load($quoteId);
  
        return $quoteColl;
        
    }
    

}