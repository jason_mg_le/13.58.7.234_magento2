<?php

namespace Solwin\Ourquote\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{

    /**
     * install tables
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {

        $installer = $setup;

        $installer->startSetup();

        if ($installer->tableExists('solwin_ourquote_quote')) {
            if (!$installer->getConnection()
                    ->tableColumnExists(
                            'solwin_ourquote_quote',
                            'customer_id'
                            )
                    ) {
                        $installer->getConnection()->addColumn(
                                $installer->getTable('solwin_ourquote_quote'),
                                'customer_id',
                                    [
                                        'type' => Table::TYPE_INTEGER,
                                        'nullable' => false,
                                        'comment' => 'Customer ID'
                                    ]
                                );
            }
            if (!$installer->getConnection()
                    ->tableColumnExists(
                            'solwin_ourquote_quote',
                            'store_id'
                            )
                    ) {
                        $installer->getConnection()->addColumn(
                                $installer->getTable('solwin_ourquote_quote'),
                                'store_id',
                                    [
                                        'type' => Table::TYPE_INTEGER,
                                        'nullable' => false,
                                        'comment' => 'Store ID'
                                    ]
                                );
            }
        }

        $installer->endSetup();
    }

}