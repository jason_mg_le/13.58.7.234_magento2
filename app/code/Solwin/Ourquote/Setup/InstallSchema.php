<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */

namespace Solwin\Ourquote\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

    /**
     * install tables
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('solwin_ourquote_quote')) {
            $table = $installer->getConnection()->newTable(
                            $installer->getTable('solwin_ourquote_quote')
                    )
                    ->addColumn(
                        'quote_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary' => true,
                            'unsigned' => true,
                        ], 'Quote ID'
                    )
                    ->addColumn(
                            'company',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            [],
                            'Quote Company'
                    )
                    ->addColumn(
                            'contact_name',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable => false'],
                            'Quote Contact Name'
                    )
                    ->addColumn(
                            'phone',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            [],
                            'Quote Phone'
                    )
                    ->addColumn(
                            'email', 
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            [],
                            'Quote Email'
                    )
                    ->addColumn(
                            'product_id',
                            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            null,
                            [],
                            'Product Id'
                    )
                    ->addColumn(
                            'project_title',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            [],
                            'Quote Project Title'
                    )
                    ->addColumn(
                            'date',
                            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                            null,
                            [],
                            'Quote Date'
                    )
                    ->addColumn(
                            'budget_status',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable => false'],
                            'Quote Budget Status'
                    )
                    ->addColumn(
                            'overview',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            [],
                            'Quote Overview'
                    )
                    ->addColumn(
                            'uploaddoc',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            [],
                            'Quote Upload Document'
                    )
                    ->addColumn(
                            'change_status',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            255,
                            ['nullable => false'],
                            'Quote Change Status'
                    )
                    ->addColumn(
                            'created_at',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                            null,
                            [],
                            'Quote Created At'
                    )
                    ->addColumn(
                            'updated_at',
                            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                            null,
                            [],
                            'Quote Updated At'
                    )
                    ->setComment('Quote Table');
            $installer->getConnection()->createTable($table);

            $installer->getConnection()->addIndex(
                    $installer->getTable('solwin_ourquote_quote'), 
                    $setup->getIdxName(
                            $installer->getTable('solwin_ourquote_quote'),
                            ['contact_name', 'company', 'phone', 'email',
                            'project_title', 'budget_status', 'overview',
                            'uploaddoc', 'change_status'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                    ), ['contact_name', 'company', 'phone', 'email',
                        'project_title', 'budget_status', 'overview',
                        'uploaddoc','change_status'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }
        $installer->endSetup();
    }

}