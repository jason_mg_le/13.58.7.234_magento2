<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;
    /**
     * @var \Magento\Customer\Model\ProductFactory
     */
    protected $_customerFactory;
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_status;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Solwin\Ourquote\Model\ChangeStatus $status,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_productFactory = $productFactory;
        $this->_customerFactory = $customerFactory;
        $this->_storeManager = $storeManager;
        $this->_status = $status;
        parent::__construct($context);
    }

    /* get product sku */

    public function getProductSku($prodId) {
        $product = $this->_productFactory->create();
        $product->load($prodId);
        return $product->getSku();
    }

    /* get product url */

    public function getProductUrl($prodId) {
        $product = $this->_productFactory->create();
        $product->load($prodId);
        return $product->getUrlKey();
    }
    public function getProdUrl($prodId) {
        $product = $this->_productFactory->create();
        $product->load($prodId);
        return $product->getProductUrl();
    }
    public function getCustomerName($custId) {
        $customer = $this->_customerFactory->create()->load($custId);
        $firstName = $customer->getFirstname();
        $lastName = $customer->getLastname();
        $customerName = $firstName .' '. $lastName;
        return $customerName;
    }

    /* get product url suffix */

    public function getCatalogSeo() {
        return $this->scopeConfig->getValue('catalog/seo/product_url_suffix',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * get base url with store code
     */
    public function getBaseUrlWithStoreCode() {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    /**
     * get base url without store code
     */
    public function getBaseUrl() {
        return $this->_storeManager->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
    }
    /**
     * get base url without store code
     */
    public function getMediaUrl() {
        return $this->_storeManager->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getConfigValue($value = '') {
        return $this->scopeConfig
                ->getValue(
                        $value,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                        );
    }

    /* get file upload extension */

    public function getUploadFileExtension() {
        return explode(',', $this->scopeConfig
                ->getValue('quotesection/uploaddoc/fileext', 
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
    }
    public function getChangeStatusLabel($statusValue) {
        $statusArray = $this->_status->getOptionArray();
        return $statusArray[$statusValue];
    }

    
    

}
