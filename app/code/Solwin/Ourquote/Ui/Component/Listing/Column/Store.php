<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Ui\Component\Listing\Column;

use Magento\Framework\Escaper;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Store extends \Magento\Ui\Component\Listing\Columns\Column
{
/**
     * Escaper
     *
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;
 
    /**
     * System store
     *
     * @var SystemStore
     */
    protected $systemStore;
 
    protected $_storeFactory;
 
    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param SystemStore $systemStore
     * @param Escaper $escaper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Escaper $escaper,
        \Magento\Store\Model\StoreFactory $storeFactory,
        array $components = [],
        array $data = []
    ) {
        $this->_storeFactory = $storeFactory;
        $this->escaper = $escaper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
 
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $store = $this->_storeFactory->create()->load((int)$item[$this->getData('name')]);
                $item[$this->getData('name')] = $store->getName();
            }
        }
 
        return $dataSource;
    }
}

