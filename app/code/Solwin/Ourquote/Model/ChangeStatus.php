<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Model;

class ChangeStatus implements \Magento\Framework\Option\ArrayInterface
{

    const STATUS_NEW = 'new';
    const STATUS_PROCESS = 'process';
    const STATUS_PENDING = 'pending';
    const STATUS_DONE = 'done';

    /* returns change status options array */

    public static function getOptionArray() {
        return [
            self::STATUS_NEW => __('New'),
            self::STATUS_PROCESS => __('Under Process'),
            self::STATUS_PENDING => __('Pending'),
            self::STATUS_DONE => __('Done')
        ];
    }

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray() {
        $options = [
            [
                'value' => self::STATUS_NEW,
                'label' => __('New')
            ],
            [
                'value' => self::STATUS_PROCESS,
                'label' => __('Under Process')
            ],
            [
                'value' => self::STATUS_PENDING,
                'label' => __('Pending')
            ],
            [
                'value' => self::STATUS_DONE,
                'label' => __('Done')
            ],
        ];
        return $options;
    }

}