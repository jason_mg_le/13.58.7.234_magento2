<?php
/**
 * Solwin Infotech
 * Solwin Advanced Testimonial Extension
 *
 * @category   Solwin
 * @package    Solwin_Testimonial
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php

namespace Solwin\Ourquote\Model;

use Magento\Framework\File\Uploader;

class Upload
{
    /**
     * Upload model factory
     * 
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_uploaderFactory;

    /**
     * @var \Solwin\Ourquote\Helper\Data
     */
    protected $_helper;
    
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;
    
    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $_resultRedirectFactory;
    
    /**
     * constructor
     * 
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory
     */
    public function __construct(
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Solwin\Ourquote\Helper\Data $helper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
    ) {
        $this->_uploaderFactory = $uploaderFactory;
        $this->_helper = $helper;
        $this->_messageManager = $messageManager;
        $this->_resultRedirectFactory = $resultRedirectFactory;
    }

    /**
     * upload file
     *
     * @param $input
     * @param $destinationFolder
     * @param $data
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function uploadFileAndGetName(
        $input,
        $destinationFolder,
        $data,
        $pid,
        $productUrl
    ) {
        try {
            $_allowedExtensions = $this->_helper->getUploadFileExtension();
            $_uploadSize = $this->_helper
                ->getConfigValue('quotesection/uploaddoc/filesize');
            $resultRedirect = $this->_resultRedirectFactory->create();
            if (isset($data[$input]['delete'])) {
                return '';
            } else {
                $uploader = $this->_uploaderFactory
                        ->create(['fileId' => $input]);
                $uploader->setAllowedExtensions(
                                    $_allowedExtensions
                                    );
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $uploader->setAllowCreateFolders(true);
                
                $fileSize = $uploader->validateFile();
                if ($fileSize['size'] < $_uploadSize) {
                    $result = $uploader->save($destinationFolder);
                    return $result['file'];
                } else {
                    $this->_messageManager
                                ->addError(
                                    'The file you\'re uploading exceeds the '
                                    . 'server size limit of '
                                    . $_uploadSize . ' kilobytes.'
                                    );
                        if ($pid) {
                            return $resultRedirect->setUrl($productUrl);
                        } else {
                            return $resultRedirect->setRefererUrl();
                    }
                }
                
            }
        } catch (\Exception $e) {
            if ($e->getCode() != Uploader::TMP_NAME_EMPTY) {
                throw new \Magento\Framework\Exception\LocalizedException(
                        $e->getMessage()
                        );
            } else {
                if (isset($data[$input]['value'])) {
                    return $data[$input]['value'];
                }
            }
        }
        return '';
    }
}