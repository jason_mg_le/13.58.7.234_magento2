<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Model;

class BudgetStatus implements \Magento\Framework\Option\ArrayInterface
{

    const STATUS_APPROVED = 'Approved';
    const STATUS_PENDING = 'Approval Pending';
    const STATUS_OPEN = 'Open';
    const STATUS_NOAPPROVAL = 'No Approval';

    /* returns budget status options array */

    public static function getOptionArray() {
        return [
            self::STATUS_APPROVED => __('Approved'),
            self::STATUS_PENDING => __('Approval Pending'),
            self::STATUS_OPEN => __('Open'),
            self::STATUS_NOAPPROVAL => __('No Approval')
        ];
    }

    /**
     * to option array
     *
     * @return array
     */
    public function toOptionArray() {
        $options = [
            [
                'value' => self::STATUS_APPROVED,
                'label' => __('Approved')
            ],
            [
                'value' => self::STATUS_PENDING,
                'label' => __('Approval Pending')
            ],
            [
                'value' => self::STATUS_OPEN,
                'label' => __('Open')
            ],
            [
                'value' => self::STATUS_NOAPPROVAL,
                'label' => __('No Approval')
            ],
        ];
        return $options;
    }

}