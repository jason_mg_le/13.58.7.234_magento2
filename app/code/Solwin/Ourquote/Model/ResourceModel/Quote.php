<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Model\ResourceModel;

class Quote extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Date time handler
     * 
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    /**
     * Date model
     * 
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * constructor
     * 
     * @param \Magento\Framework\Stdlib\DateTime $dateTime
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime $dateTime,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        $this->_dateTime = $dateTime;
        $this->_date = $date;
        parent::__construct($context);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('solwin_ourquote_quote', 'quote_id');
    }

    /**
     * Retrieves Quote Contact Name from DB by passed id.
     *
     * @param string $id
     * @return string|bool
     */
    public function getQuoteContact_nameById($id) {
        $adapter = $this->getConnection();
        $select = $adapter->select()
                ->from($this->getMainTable(), 'contact_name')
                ->where('quote_id = :quote_id');
        $binds = ['quote_id' => (int) $id];
        return $adapter->fetchOne($select, $binds);
    }

    /**
     * before save callback
     *
     * @param \Magento\Framework\Model\AbstractModel|
     * \Solwin\Ourquote\Model\Quote $object
     * @return $this
     */
    protected function _beforeSave(
        \Magento\Framework\Model\AbstractModel $object
    ) {
        $object->setUpdatedAt($this->_date->date());
        if ($object->isObjectNew()) {
            $object->setCreatedAt($this->_date->date());
        }
        foreach (['date'] as $field) {
            $value = !$object
                    ->getData($field) ? null : $object->getData($field);
            $object->setData($field, $this->_dateTime->formatDate($value));
        }
        return parent::_beforeSave($object);
    }

}
