<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Model\System\Config\Source;

class Theme implements \Magento\Framework\Option\ArrayInterface
{

    const THEME_LIGHT = 'light';
    const THEME_DARK = 'dark';

    /* get recaptcha theme */

    public function toOptionArray() {
        return [
            self::THEME_LIGHT => __('Light'),
            self::THEME_DARK => __('Dark')
        ];
    }

}