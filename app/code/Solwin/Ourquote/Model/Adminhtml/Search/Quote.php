<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Model\Adminhtml\Search;

use Solwin\Ourquote\Model\ResourceModel\Quote\CollectionFactory;

class Quote extends \Magento\Framework\DataObject
{

    /**
     * Quote Collection factory
     * 
     * @var \Solwin\Ourquote\Model\ResourceModel\Quote\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Backend data helper
     * 
     * @var \Magento\Backend\Helper\Data
     */
    protected $_adminhtmlData;

    /**
     * constructor
     * 
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Backend\Helper\Data $adminhtmlData
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        \Magento\Backend\Helper\Data $adminhtmlData
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_adminhtmlData = $adminhtmlData;
        parent::__construct();
    }

    /**
     * Load search results
     *
     * @return $this
     */
    public function load() {
        $result = [];
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($result);
            return $this;
        }

        $query = $this->getQuery();
        $collection = $this->_collectionFactory->create()
                ->addFieldToFilter(
                        'contact_name',
                        ['like' => '%' . $query . '%']
                        )
                ->setCurPage($this->getStart())
                ->setPageSize($this->getLimit())
                ->load();

        foreach ($collection as $quote) {
            $result[] = [
                'id' => 'solwin_ourquote_quote/1/' . $quote->getId(),
                'type' => __('Quote'),
                'name' => $quote->getContact_name(),
                'description' => $quote->getContact_name(),
                'form_panel_title' => __(
                        'Quote %1', $quote->getContact_name()
                ),
                'url' => $this->_adminhtmlData
                    ->getUrl(
                            'solwin_ourquote/quote/edit',
                            ['quote_id' => $quote->getId()]
                            ),
            ];
        }

        $this->setResults($result);

        return $this;
    }

}