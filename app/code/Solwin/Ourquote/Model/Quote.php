<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */

namespace Solwin\Ourquote\Model;

/**
 * @method Quote setCompany($company)
 * @method Quote setContactName($contactName)
 * @method Quote setPhone($phone)
 * @method Quote setEmail($email)
 * @method Quote setProjectTitle($projectTitle)
 * @method Quote setDate($date)
 * @method Quote setBudgetStatus($budgetStatus)
 * @method Quote setOverview($overview)
 * @method Quote setUploaddoc($uploaddoc)
 * @method Quote setChangeStatus($changeStatus)
 * @method mixed getCompany()
 * @method mixed getContactName()
 * @method mixed getPhone()
 * @method mixed getEmail()
 * @method mixed getProjectTitle()
 * @method mixed getDate()
 * @method mixed getBudgetStatus()
 * @method mixed getOverview()
 * @method mixed getUploaddoc()
 * @method mixed getChangeStatus()
 * @method Quote setCreatedAt(\string $createdAt)
 * @method string getCreatedAt()
 * @method Quote setUpdatedAt(\string $updatedAt)
 * @method string getUpdatedAt()
 */
class Quote extends \Magento\Framework\Model\AbstractModel
{

    /**
     * Cache tag
     * 
     * @var string
     */
    const CACHE_TAG = 'solwin_ourquote_quote';

    /**
     * Cache tag
     * 
     * @var string
     */
    protected $_cacheTag = 'solwin_ourquote_quote';

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'solwin_ourquote_quote';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Solwin\Ourquote\Model\ResourceModel\Quote');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * get entity default values
     *
     * @return array
     */
    public function getDefaultValues() {
        $values = [];

        return $values;
    }

}