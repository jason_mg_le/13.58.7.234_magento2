<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Controller\Adminhtml;

abstract class Quote extends \Magento\Backend\App\Action
{

    /**
     * Quote Factory
     * 
     * @var \Solwin\Ourquote\Model\QuoteFactory
     */
    protected $_quoteFactory;

    /**
     * Core registry
     * 
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * constructor
     * 
     * @param \Solwin\Ourquote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Solwin\Ourquote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_quoteFactory = $quoteFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init Quote
     *
     * @return \Solwin\Ourquote\Model\Quote
     */
    protected function initQuote() {
        $quoteId = (int) $this->getRequest()->getParam('quote_id');
        /** @var \Solwin\Ourquote\Model\Quote $quote */
        $quote = $this->_quoteFactory->create();
        if ($quoteId) {
            $quote->load($quoteId);
        }
        $this->_coreRegistry->register('solwin_ourquote_quote', $quote);
        return $quote;
    }

}