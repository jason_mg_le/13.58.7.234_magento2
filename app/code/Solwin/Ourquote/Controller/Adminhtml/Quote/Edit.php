<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Controller\Adminhtml\Quote;

use Magento\Framework\Controller\Result\JsonFactory;

class Edit extends \Solwin\Ourquote\Controller\Adminhtml\Quote
{

    /**
     * Page factory
     * 
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Result JSON factory
     * 
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * constructor
     * 
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     * @param \Solwin\Ourquote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        \Solwin\Ourquote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct(
                $quoteFactory,
                $registry,
                $context
                );
    }

    /**
     * is action allowed
     *
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Solwin_Ourquote::quote');
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page |
     * \Magento\Backend\Model\View\Result\Redirect |
     * \Magento\Framework\View\Result\Page
     */
    public function execute() {
        $id = $this->getRequest()->getParam('quote_id');
        /** @var \Solwin\Ourquote\Model\Quote $quote */
        $quote = $this->initQuote();
        /** @var \Magento\Backend\Model\View\Result\Page |
         * \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Solwin_Ourquote::quote');
        $resultPage->getConfig()->getTitle()->set(__('Quotes'));
        if ($id) {
            $quote->load($id);
            if (!$quote->getId()) {
                $this->messageManager
                        ->addError(__('This Quote no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath(
                        'solwin_ourquote/*/edit', [
                    'quote_id' => $quote->getId(),
                    '_current' => true
                        ]
                );
                return $resultRedirect;
            }
        }
        $title = $quote->getId() ? $quote->getContact_name() : __('New Quote');
        $resultPage->getConfig()->getTitle()->prepend($title);
        $data = $this->_session
                ->getData('solwin_ourquote_quote_data', true);
        if (!empty($data)) {
            $quote->setData($data);
        }
        return $resultPage;
    }

}