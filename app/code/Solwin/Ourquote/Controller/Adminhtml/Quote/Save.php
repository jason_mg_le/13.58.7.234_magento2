<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Controller\Adminhtml\Quote;

class Save extends \Solwin\Ourquote\Controller\Adminhtml\Quote
{

    /**
     * Date filter
     * 
     * @var \Magento\Framework\Stdlib\DateTime\Filter\Date
     */
    protected $_dateFilter;

    /**
     * constructor
     * @param \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter
     * @param \Solwin\Ourquote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter,
        \Solwin\Ourquote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_dateFilter = $dateFilter;
        parent::__construct(
                $quoteFactory,
                $registry,
                $context
                );
    }

    /**
     * is action allowed
     *
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Solwin_Ourquote::quote');
    }

    /**
     * run the action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {
        $data = $this->getRequest()->getPost('quote');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $data = $this->filterData($data);
            $quote = $this->initQuote();
            $quote->setData($data);
            $this->_eventManager->dispatch(
                    'solwin_ourquote_quote_prepare_save', [
                'quote' => $quote,
                'request' => $this->getRequest()
                    ]
            );
            try {
                $quote->save();
                $this->messageManager
                        ->addSuccess(__('The quote request has been saved.'));
                $this->_session->setSolwinOurquoteQuoteData(false);
                if ($this->getRequest()->getParam('back')) {
                    $resultRedirect->setPath(
                            'solwin_ourquote/*/edit', [
                        'quote_id' => $quote->getId(),
                        '_current' => true
                            ]
                    );
                    return $resultRedirect;
                }
                $resultRedirect->setPath('solwin_ourquote/*/');
                return $resultRedirect;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager
                        ->addException(
                            $e,
                            __('Something went wrong while saving the '
                                . 'quote request.')
                            );
            }
            $this->_getSession()->setSolwinOurquoteQuoteData($data);
            $resultRedirect->setPath(
                    'solwin_ourquote/*/edit', [
                'quote_id' => $quote->getId(),
                '_current' => true
                    ]
            );
            return $resultRedirect;
        }
        $resultRedirect->setPath('solwin_ourquote/*/');
        return $resultRedirect;
    }

    /**
     * filter values
     *
     * @param array $data
     * @return array
     */
    protected function filterData($data) {
        $inputFilter = new \Zend_Filter_Input(
                [
            'date' => $this->_dateFilter,
                ], [], $data
        );
        $data = $inputFilter->getUnescaped();
        return $data;
    }

}