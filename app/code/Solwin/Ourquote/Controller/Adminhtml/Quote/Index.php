<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Controller\Adminhtml\Quote;

class Index extends \Magento\Backend\App\Action
{

    /**
     * Page result factory
     * 
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Page factory
     * 
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $_resultPage;

    /**
     * constructor
     * 
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * is action allowed
     *
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Solwin_Ourquote::quote');
    }

    /**
     * execute the action
     *
     * @return \Magento\Backend\Model\View\Result\Page|
     * \Magento\Framework\View\Result\Page
     */
    public function execute() {
        $this->setPageData();
        return $this->getResultPage();
    }

    /**
     * instantiate result page object
     *
     * @return \Magento\Backend\Model\View\Result\Page|
     * \Magento\Framework\View\Result\Page
     */
    public function getResultPage() {
        if ($this->_resultPage === null) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }
        return $this->_resultPage;
    }

    /**
     * set page data
     *
     * @return $this
     */
    protected function setPageData() {
        $resultPage = $this->getResultPage();
        //$resultPage->setActiveMenu('Solwin_Ourquote::quote');
        $resultPage->getConfig()->getTitle()->prepend((__('Quotes')));
        return $this;
    }

}