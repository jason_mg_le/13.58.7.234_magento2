<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Controller\Adminhtml\Quote;

class Delete extends \Solwin\Ourquote\Controller\Adminhtml\Quote
{

    /**
     * is action allowed
     *
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Solwin_Ourquote::quote');
    }

    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('quote_id');
        if ($id) {
            $contactName = "";
            try {
                /** @var \Solwin\Ourquote\Model\Quote $quote */
                $quote = $this->_quoteFactory->create();
                $quote->load($id);
                $contactName = $quote->getContact_name();
                $quote->delete();
                $this->messageManager
                        ->addSuccess(__('The quote request has been deleted.'));
                $this->_eventManager->dispatch(
                        'adminhtml_solwin_ourquote_quote_on_delete', 
                        ['contact_name' => $contactName, 'status' => 'success']
                );
                $resultRedirect->setPath('solwin_ourquote/*/');
                return $resultRedirect;
            } catch (\Exception $e) {
                $this->_eventManager->dispatch(
                        'adminhtml_solwin_ourquote_quote_on_delete', 
                        ['contact_name' => $contactName, 'status' => 'fail']
                );
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                $resultRedirect->setPath(
                        'solwin_ourquote/*/edit', ['quote_id' => $id]
                        );
                return $resultRedirect;
            }
        }
        // display error message
        $this->messageManager
                ->addError(__('Quote request to delete was not found.'));
        // go to grid
        $resultRedirect->setPath('solwin_ourquote/*/');
        return $resultRedirect;
    }

}