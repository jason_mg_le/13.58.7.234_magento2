<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Controller\Adminhtml\Quote;

abstract class InlineEdit extends \Magento\Backend\App\Action
{

    /**
     * JSON Factory
     * 
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_jsonFactory;

    /**
     * Quote Factory
     * 
     * @var \Solwin\Ourquote\Model\QuoteFactory
     */
    protected $_quoteFactory;

    /**
     * constructor
     * 
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     * @param \Solwin\Ourquote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Solwin\Ourquote\Model\QuoteFactory $quoteFactory,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->_quoteFactory = $quoteFactory;
        parent::__construct($context);
    }

    /**
     * is action allowed
     *
     * @return bool
     */
    protected function _isAllowed() {
        return $this->_authorization->isAllowed('Solwin_Ourquote::quote');
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->_jsonFactory->create();
        $error = false;
        $messages = [];
        $postItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($postItems))) {
            return $resultJson->setData([
                        'messages' => [__('Please correct the data sent.')],
                        'error' => true,
            ]);
        }
        foreach (array_keys($postItems) as $quoteId) {
            /** @var \Solwin\Ourquote\Model\Quote $quote */
            $quote = $this->_quoteFactory->create()->load($quoteId);
            try {
                $quoteData = $postItems[$quoteId];
                $quote->addData($quoteData);
                $quote->save();
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithQuoteId(
                        $quote,
                        $e->getMessage()
                        );
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithQuoteId(
                        $quote,
                        $e->getMessage()
                        );
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithQuoteId(
                    $quote, 
                    __('Something went wrong while saving the quote request.')
                );
                $error = true;
            }
        }
        return $resultJson->setData([
                    'messages' => $messages,
                    'error' => $error
        ]);
    }

    /**
     * Add Quote id to error message
     *
     * @param \Solwin\Ourquote\Model\Quote $quote
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithQuoteId(
        \Solwin\Ourquote\Model\Quote $quote,
        $errorText
    ) {
        return '[Quote ID: ' . $quote->getId() . '] ' . $errorText;
    }

}