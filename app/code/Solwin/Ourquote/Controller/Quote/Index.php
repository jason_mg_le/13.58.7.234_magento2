<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Controller\Quote;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;

class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Session
     */
    protected $session;
    
    /**
     * @var AccountRedirect
     */
    protected $accountRedirect;
    
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Session $customerSession
     * @param AccountRedirect $accountRedirect
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Session $customerSession,
        AccountRedirect $accountRedirect
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->session = $customerSession;
        $this->accountRedirect = $accountRedirect;
        parent::__construct($context);
    }
    
    public function dispatch(RequestInterface $request) {
        
        if(!$this->session->isLoggedIn()) {
            $this->messageManager->addNotice('Please sign in or create a new account');
            return $this->accountRedirect->getRedirect();
        }
        return parent::dispatch($request);
    }

    /**
     * Customer order history
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Product Inquiry Information'));
        $block = $resultPage->getLayout()->getBlock('customer_quote');
        if ($block) {
            $block->setRefererUrl($this->_redirect->getRefererUrl());
        }
        return $resultPage;
    }

}
