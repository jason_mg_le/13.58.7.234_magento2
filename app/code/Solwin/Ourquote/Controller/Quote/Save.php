<?php
/**
 * Solwin Infotech
 * Solwin Request For Quote Extension
 * 
 * @category   Solwin
 * @package    Solwin_Ourquote
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/
 */
namespace Solwin\Ourquote\Controller\Quote;

use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\MediaStorage\Model\File\UploaderFactory;

class Save extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var \Solwin\Ourquote\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;
    
    /**
     * @var \Solwin\Ourquote\Model\Upload
     */
    protected $_uploadModel;
    
    /**
     * @var \Solwin\Ourquote\Model\Testimonial\Image
     */
    protected $_imageModel;
    
     /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    const XML_PATH_EMAIL_TEMPLATE = 'quotesection/emailopt/email_template';
    const XML_PATH_EMAIL_SENDER = 'quotesection/emailopt/emailsender';
    const XML_PATH_EMAIL_RECIPIENT = 'quotesection/emailopt/emailto';
    const REQUEST_URL = 'https://www.google.com/recaptcha/api/siteverify';
    const REQUEST_RESPONSE = 'g-recaptcha-response';

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param StateInterface $inlineTranslation
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param TransportBuilder $transportBuilder
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Solwin\Ourquote\Helper\Data $helper
     * @param \Magento\Framework\Filesystem $filesystem
     * @param UploaderFactory $fileUploaderFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        StateInterface $inlineTranslation,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Solwin\Ourquote\Helper\Data $helper,
        \Magento\Framework\Filesystem $filesystem,
        UploaderFactory $fileUploaderFactory,
        \Solwin\Ourquote\Model\Upload $uploadModel,
        \Magento\Customer\Model\Session $customerSession,
        \Solwin\Ourquote\Model\Ourquote\Image $imageModel
    ) {
        $this->_productFactory = $productFactory;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_helper = $helper;
        parent::__construct($context);
        $this->_customerSession = $customerSession;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_storeManager = $storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->_uploadModel    = $uploadModel;
        $this->_imageModel     = $imageModel;
    }
    
    public function getCustomerId() {
        return $this->_customerSession->getCustomerId();
    }
    
    public function getCurrentStoreId() {
        return $this->_storeManager->getStore()->getStoreId(); 
    }

    public function execute() {
        
        $remoteAddr = filter_input(
                INPUT_SERVER,
                'REMOTE_ADDR',
                FILTER_SANITIZE_STRING
                );
        $pid = $this->getRequest()->getParam('pid');
        $productUrl = '';
        if ($pid) {
            $product = $this->_productFactory->create();
            $product->load($pid);
            $productUrl = $product->getProductUrl();
        }

        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();

        $isCaptchaEnable = $this->_helper
                ->getConfigValue('quotesection/recaptcha/enablecaptcha');
        if ($isCaptchaEnable) {
            $secretkey = $this->_helper
                    ->getConfigValue('quotesection/recaptcha/secretkey');
            $captchaErrorMsg = $this->_helper
                    ->getConfigValue('quotesection/recaptcha/errormsg');
            if ($captchaErrorMsg == '') {
                $captchaErrorMsg = 'Invalid captcha. Please try again.';
            }
            $captcha = '';
            if (filter_input(INPUT_POST, 'g-recaptcha-response') !== null) {
                $captcha = filter_input(INPUT_POST, 'g-recaptcha-response');
            }

            if (!$captcha) {
                $this->messageManager->addError($captchaErrorMsg);
                if ($pid) {
                    return $resultRedirect->setUrl($productUrl);
                } else {
                    return $resultRedirect->setRefererUrl();
                }
            } else {
                $response = file_get_contents(
                        "https://www.google.com/recaptch/api/siteverify"
                        . "?secret=" . $secretkey
                        . "&response=" . $captcha
                        . "&remoteip=" . $remoteAddr);
                $response = json_decode($response, true);

                if ($response["success"] === false) {
                    $this->messageManager->addError($captchaErrorMsg);
                    if ($pid) {
                        return $resultRedirect->setUrl($productUrl);
                    } else {
                        return $resultRedirect->setRefererUrl();
                    }
                }
            }
        }

        if (!$data) {
            if ($pid) {
                return $resultRedirect->setUrl($productUrl);
            } else {
                return $resultRedirect->setRefererUrl();
            }
        }

        try {
            $model = $this->_objectManager
                    ->create('Solwin\Ourquote\Model\Quote');

            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($data);

            $error = false;

            if (!\Zend_Validate::is(trim($data['contact_name']), 'NotEmpty')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data['email']), 'NotEmpty')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data['overview']), 'NotEmpty')) {
                $error = true;
            }

            if ($error) {
                throw new \Exception();
            }
            
            $uploaddoc = $this->_uploadModel
                    ->uploadFileAndGetName(
                            'uploaddoc',
                            $this->_imageModel->getBaseDir(),
                            $data,
                            $pid,
                            $productUrl
                            );

            $id = $this->getRequest()->getParam('quote_id');
            if ($id) {
                $model->load($id);
            }
            $custId = $this->getCustomerId();
            $storeId = $this->getCurrentStoreId();
            
            $model->setData($data);
            $model->setUploaddoc($uploaddoc);
            $model->setCustomerId($custId);
            $model->setStoreId($storeId);
            $model->save();


            // send mail to recipients
            $this->_inlineTranslation->suspend();
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $transport = $this->_transportBuilder->setTemplateIdentifier(
                            $this->_scopeConfig
                    ->getValue(self::XML_PATH_EMAIL_TEMPLATE, $storeScope)
                    )->setTemplateOptions(
                        [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->_storeManager->getStore()->getId(),
                        ]
                    )->setTemplateVars(['data' => $postObject])
                    ->setFrom($this->_scopeConfig
                        ->getValue(self::XML_PATH_EMAIL_SENDER, $storeScope))
                    ->addTo($this->_scopeConfig
                        ->getValue(self::XML_PATH_EMAIL_RECIPIENT, $storeScope))
                    ->getTransport();

            $transport->sendMessage();
            $this->_inlineTranslation->resume();

            $this->messageManager
                    ->addSuccess(__('The Quote request has been saved.'
                            . 'We\'ll respond to you very soon.'));
            $this->_objectManager
                    ->get('Magento\Backend\Model\Session')
                    ->setFormData(false);
            if ($pid) {
                return $resultRedirect->setUrl($productUrl);
            } else {
                return $resultRedirect->setRefererUrl();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->_inlineTranslation->resume();
            $this->messageManager
                    ->addException(
                    $e,
                    __('Something went wrong while saving the Quote request.')
                    );
        }

        if ($pid) {
            return $resultRedirect->setUrl($productUrl);
        } else {
            return $resultRedirect->setRefererUrl();
        }
    }

}