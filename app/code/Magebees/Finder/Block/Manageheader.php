<?php
namespace Magebees\Finder\Block;
class Manageheader extends \Magento\Framework\View\Element\Template 
{	
	public function manageHeaderContent(){
		$om = \Magento\Framework\App\ObjectManager::getInstance();
		$page = $om->get('Magento\Framework\View\Page\Config');
		$page->addPageAsset('Magebees_Finder::css/mbfinder.css');
	}
}