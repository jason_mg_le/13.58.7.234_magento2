<?php
namespace Magebees\Finder\Block;
use Magento\Store\Model\ScopeInterface;
class Finder extends \Magento\Framework\View\Element\Template {
	
	protected $_dropdownsFactory;
	protected $_finderFactory;
	protected $_productsFactory;
	protected $categoryFactory;
		
	public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
		\Magebees\Finder\Model\DropdownsFactory $dropdownsFactory,
		\Magebees\Finder\Model\FinderFactory $finderFactory,
		\Magebees\Finder\Model\ProductsFactory $productsFactory,
		\Magento\Catalog\Model\CategoryFactory $categoryFactory
		
    ) {
        parent::__construct($context);
		$this->_dropdownsFactory = $dropdownsFactory;
		$this->_finderFactory = $finderFactory;
		$this->_productsFactory = $productsFactory;
		$this->categoryFactory = $categoryFactory;
    						
		//Set Configuration values
		$this->setEnabled($this->_scopeConfig->getValue('finder/general/enabled',ScopeInterface::SCOPE_STORE));
		$this->setAutosearch($this->_scopeConfig->getValue('finder/general/autosearch',ScopeInterface::SCOPE_STORE));
		$this->setCategoryPageEnabled($this->_scopeConfig->getValue('finder/general/categorypage_enabled',ScopeInterface::SCOPE_STORE));
		$this->setCategoryPageReset($this->_scopeConfig->getValue('finder/general/category_page_reset',ScopeInterface::SCOPE_STORE));
	}
	
	public function getDropdownsCollectionByFinderId($finderId){
		return $this->_dropdownsFactory->create()->getCollection()
						->addFieldToFilter('finder_id',$finderId)
		;
	}
	
	public function getFinderById($finderId){
		return $this->_finderFactory->create()->load($finderId);
	}
	
	//get dropdowns value by finder id and also do sorting of data specified in admin
	public function getProductsByFinderId($finderId,$dropdownId,$field){
		$dropdown_data = $this->_dropdownsFactory->create()->load($dropdownId);
		$productsCollection = $this->_productsFactory->create()->getCollection()
									->addFieldToFilter('finder_id',$finderId);
		if($field>1){
			$finderStr = array();
			$finderStr = $this->getRequest()->getParam('finder');
			$str = array();
			if($finderStr){
				$str = explode(":",$finderStr);
				for($i=0;$i<($field-1);$i++){
					if(array_key_exists($i,$str)){
						$productsCollection->addFieldToFilter("field".($i+1),$str[$i]);
					}
				}
			}
		}
		return $productsCollection->setOrder("field".$field,$dropdown_data->getSort());
	}
	
	//get finder ids by category ids
	public function getFinderIdsByCategoryId(){
		$category = $this->getCurrentCategory();
		//get current category
		$collection = $this->_finderFactory->create()->getCollection();
		$collection->addFieldToFilter('category_ids',array(array('finset' => $category->getId())));
		$finderIds = array_map(array($this,"getFinderIdssArr"), $collection->getData());
		return $finderIds;
	}
	
	public function getFinderIdssArr($element){return $element['finder_id'];}	
	
	public function getCategoryUrl(){
		return $this->getCurrentCategory()->getUrl();
	}
	
	public function getProductsByFinderIdForCategory($finderId,$dropdownId,$field,$skus){
		$dropdown_data = $this->_dropdownsFactory->create()->load($dropdownId);
		$productsCollection = $this->_productsFactory->create()->getCollection()
									->addFieldToFilter('finder_id',$finderId)
									->addFieldToFilter('sku',array('in'=>$skus))
									;
		if($field>1){
			$finderStr = array();
			$finderStr = $this->getRequest()->getParam('finder');
			$str = array();
			if($finderStr){
				$str = explode(":",$finderStr);
				for($i=0;$i<($field-1);$i++)	{
					if(array_key_exists($i,$str)){
						$productsCollection->addFieldToFilter("field".($i+1),$str[$i]);
					}
				}
			}
		}
		return $productsCollection->setOrder("field".$field,$dropdown_data->getSort());
	}
	
	public function getCurrentCategory(){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$category = $objectManager->get('Magento\Framework\Registry')->registry('current_category');
		return $category;
	}
	
	public function getProductsByCategoryId()	{
		$categoryId = $this->getCurrentCategory()->getId();
		$category = $this->categoryFactory->create()->load($categoryId);
		$skus = $category->getProductCollection()->addAttributeToSelect('sku')
										 ->getColumnValues('sku')
										;
		return $skus;
	}
	
	public function displayFinder($finderId,$skus){
		return $this->_productsFactory->create()->getCollection()
									->addFieldToFilter('finder_id',$finderId)
									->addFieldToFilter('sku',array('in'=>$skus))
									->getSize();
									;
	}
}