<?php
namespace Magebees\Finder\Block;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Store\Model\ScopeInterface;

class FinderProduct extends \Magento\Catalog\Block\Product\ListProduct {

	/**
     * Product Finder collection model
     *
     * @var Magebees\Catalog\Model\Resource\Product\Collection
     */
	protected $_productsFactory;
    /**
     * Product collection model
     *
     * @var Magento\Catalog\Model\Resource\Product\Collection
     */
    protected $_collection;

    /**
     * Product collection model
     *
     * @var Magento\Catalog\Model\Resource\Product\Collection
     */
    protected $_productCollection;

    /**
     * Image helper
     *
     * @var Magento\Catalog\Helper\Image
     */
    protected $_imageHelper;

    /**
     * Catalog Layer
     *
     * @var \Magento\Catalog\Model\Layer\Resolver
     */
    protected $_catalogLayer;

    /**
     * @var \Magento\Framework\Data\Helper\PostHelper
     */
    protected $_postDataHelper;

    /**
     * @var \Magento\Framework\Url\Helper\Data
     */
    protected $urlHelper;

    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepository;
	
	protected $storeManager;

    /**
     * Initialize
     *
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver
     * @param \CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Framework\Url\Helper\Data $urlHelper
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param array $data
     */
    public function __construct(
    \Magento\Catalog\Block\Product\Context $context, \Magento\Framework\Data\Helper\PostHelper $postDataHelper, \Magento\Catalog\Model\Layer\Resolver $layerResolver, CategoryRepositoryInterface $categoryRepository, \Magento\Framework\Url\Helper\Data $urlHelper, \Magento\Catalog\Model\ResourceModel\Product\Collection $collection, \Magebees\Finder\Model\ProductsFactory $productsFactory,
	\Magento\Catalog\Model\CategoryFactory $categoryFactory,
	//\Magento\Store\Model\StoreManagerInterface $storeManager,	
	array $data = []
    ) {
        $this->imageBuilder = $context->getImageBuilder();
        $this->_catalogLayer = $layerResolver->get();
        $this->_postDataHelper = $postDataHelper;
        $this->categoryRepository = $categoryRepository;
        $this->urlHelper = $urlHelper;
        $this->_collection = $collection;
        //$this->_imageHelper = $imageHelper;
        //$this->_scopeConfig = $scopeConfig;
		$this->_productsFactory = $productsFactory;
		$this->_categoryFactory = $categoryFactory;
		//$this->_storeManager = $storeManager;
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);

        $this->pageConfig->getTitle()->set(__($this->getPageTitle()));//set Page title
		//set config values
		$this->setProductlistHeading($this->_scopeConfig->getValue('finder/general/productlist_heading',ScopeInterface::SCOPE_STORE));
		$this->setCategorylistHeading($this->_scopeConfig->getValue('finder/general/categorylist_heading',ScopeInterface::SCOPE_STORE));
		$this->setCategoryListEnabled($this->_scopeConfig->getValue('finder/general/categorylist_enabled',ScopeInterface::SCOPE_STORE));
		$this->setProductsPerPage($this->_scopeConfig->getValue('finder/general/products_per_page',ScopeInterface::SCOPE_STORE));
    }

    
	/**
     * Get product collection
     */
    protected function getProducts() {
		$skus = array();
		$skus = $this->getFinderProductsSkus();
		$collection = $this->_collection
					->addAttributeToFilter('sku',array('in',$skus))
					->addAttributeToSelect('*')
                ;
		$collection->getSelect()
                ->order('rand()');

		$this->_productCollection = $collection;
		return $this->_productCollection;
    }
	
	protected function _beforeToHtml(){
		$pager = $this->getLayout()->createBlock(
					'Magento\Theme\Block\Html\Pager')
						->setUseContainer(true)
						->setShowAmounts(false)                    
						->setLimit($this->getProductsPerPage())
						//->setTotalLimit(20)
						->setCollection($this->getLoadedProductCollection());	
						$this->setChild('pager', $pager);
	}
	
	public function getCategoriesCollection(){
		$this->getLoadedProductCollection();
		$product_ids = array_map(array($this,"getProdcutIdsArr"), $this->_productCollection->getData());
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$cats = array();
		foreach($product_ids as $product_id){
			$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
			$cats = array_merge($cats,$product->getCategoryIds());
		} 
		return array_unique($cats);
	}
	
	public function loadCategoriesById($cat_id){
		$_category = $this->_categoryFactory->create()->load($cat_id);
		return $_category;
	}
	
    /*
     * Load and return product collection 
     */

    public function getLoadedProductCollection() {
        return $this->getProducts();
    }

    /*
     * Get product toolbar
     */

    public function getToolbarHtml() {
        return $this->getChildHtml('pager');
    }

    /*
     * Get grid mode
     */

    public function getMode() {
        return 'grid';
    }

    /**
     * Get image helper
     */
    public function getImageHelper() {
        return $this->_imageHelper;
    }

    /* Get the configured title of section */

    public function getPageTitle() {
		return $this->_scopeConfig->getValue('finder/general/finderpage_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	} 

    public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product) {
        $url = $this->getAddToCartUrl($product);
        return [
            'action' => $url,
            'data' => [
                'product' => $product->getEntityId(),
                \Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
                $this->urlHelper->getEncodedUrl($url),
            ]
        ];
    }
	
	public function getFinderProductsSkus(){
		$finderId = $this->getRequest()->getParam('finder_id');
		$productsCollection = $this->_productsFactory->create()->getCollection()
									->addFieldToFilter('finder_id',$finderId);
			$finderStr = array();
			$finderStr = $this->getRequest()->getParam('finder');
			$str = array();
			if($finderStr){
				$str = explode(":",$finderStr);
				for($i=0;$i<(count($str));$i++)	{
					$productsCollection->addFieldToFilter("field".($i+1),$str[$i]);
				}
			}
		$product_skus = array_map(array($this,"getProdcutSkusArr"), $productsCollection->getData());
		return $product_skus;
		
	}
	
	public function getProdcutSkusArr($element){return $element['sku'];}	
	
	public function getProdcutIdsArr($element){return $element['entity_id'];}
	
	public function getCurrentStore(){
		return $this->_storeManager->getStore(); // give the information about current store
	}
	
}
