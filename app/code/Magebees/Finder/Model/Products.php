<?php
namespace Magebees\Finder\Model;

class Products extends \Magento\Framework\Model\AbstractModel {
	/**
     * Initialization
     *
     * @return void
     */
    protected function _construct() {
        $this->_init('Magebees\Finder\Model\ResourceModel\Products');
    }
	
}
